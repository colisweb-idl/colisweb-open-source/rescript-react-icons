
  module Fa500Px = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Fa500Px";
  };
 
  module FaAccessibleIcon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAccessibleIcon";
  };
 
  module FaAccusoft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAccusoft";
  };
 
  module FaAcquisitionsIncorporated = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAcquisitionsIncorporated";
  };
 
  module FaAdn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAdn";
  };
 
  module FaAdobe = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAdobe";
  };
 
  module FaAdversal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAdversal";
  };
 
  module FaAffiliatetheme = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAffiliatetheme";
  };
 
  module FaAirbnb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAirbnb";
  };
 
  module FaAlgolia = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlgolia";
  };
 
  module FaAlipay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlipay";
  };
 
  module FaAmazonPay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAmazonPay";
  };
 
  module FaAmazon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAmazon";
  };
 
  module FaAmilia = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAmilia";
  };
 
  module FaAndroid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAndroid";
  };
 
  module FaAngellist = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngellist";
  };
 
  module FaAngrycreative = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngrycreative";
  };
 
  module FaAngular = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngular";
  };
 
  module FaAppStoreIos = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAppStoreIos";
  };
 
  module FaAppStore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAppStore";
  };
 
  module FaApper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaApper";
  };
 
  module FaApplePay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaApplePay";
  };
 
  module FaApple = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaApple";
  };
 
  module FaArtstation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArtstation";
  };
 
  module FaAsymmetrik = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAsymmetrik";
  };
 
  module FaAtlassian = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAtlassian";
  };
 
  module FaAudible = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAudible";
  };
 
  module FaAutoprefixer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAutoprefixer";
  };
 
  module FaAvianex = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAvianex";
  };
 
  module FaAviato = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAviato";
  };
 
  module FaAws = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAws";
  };
 
  module FaBandcamp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBandcamp";
  };
 
  module FaBattleNet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBattleNet";
  };
 
  module FaBehanceSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBehanceSquare";
  };
 
  module FaBehance = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBehance";
  };
 
  module FaBimobject = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBimobject";
  };
 
  module FaBitbucket = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBitbucket";
  };
 
  module FaBitcoin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBitcoin";
  };
 
  module FaBity = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBity";
  };
 
  module FaBlackTie = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlackTie";
  };
 
  module FaBlackberry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlackberry";
  };
 
  module FaBloggerB = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBloggerB";
  };
 
  module FaBlogger = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlogger";
  };
 
  module FaBluetoothB = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBluetoothB";
  };
 
  module FaBluetooth = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBluetooth";
  };
 
  module FaBootstrap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBootstrap";
  };
 
  module FaBtc = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBtc";
  };
 
  module FaBuffer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBuffer";
  };
 
  module FaBuromobelexperte = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBuromobelexperte";
  };
 
  module FaBuyNLarge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBuyNLarge";
  };
 
  module FaBuysellads = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBuysellads";
  };
 
  module FaCanadianMapleLeaf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCanadianMapleLeaf";
  };
 
  module FaCcAmazonPay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcAmazonPay";
  };
 
  module FaCcAmex = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcAmex";
  };
 
  module FaCcApplePay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcApplePay";
  };
 
  module FaCcDinersClub = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcDinersClub";
  };
 
  module FaCcDiscover = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcDiscover";
  };
 
  module FaCcJcb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcJcb";
  };
 
  module FaCcMastercard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcMastercard";
  };
 
  module FaCcPaypal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcPaypal";
  };
 
  module FaCcStripe = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcStripe";
  };
 
  module FaCcVisa = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCcVisa";
  };
 
  module FaCentercode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCentercode";
  };
 
  module FaCentos = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCentos";
  };
 
  module FaChrome = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChrome";
  };
 
  module FaChromecast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChromecast";
  };
 
  module FaCloudscale = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudscale";
  };
 
  module FaCloudsmith = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudsmith";
  };
 
  module FaCloudversify = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudversify";
  };
 
  module FaCodepen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCodepen";
  };
 
  module FaCodiepie = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCodiepie";
  };
 
  module FaConfluence = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaConfluence";
  };
 
  module FaConnectdevelop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaConnectdevelop";
  };
 
  module FaContao = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaContao";
  };
 
  module FaCottonBureau = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCottonBureau";
  };
 
  module FaCpanel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCpanel";
  };
 
  module FaCreativeCommonsBy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsBy";
  };
 
  module FaCreativeCommonsNcEu = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsNcEu";
  };
 
  module FaCreativeCommonsNcJp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsNcJp";
  };
 
  module FaCreativeCommonsNc = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsNc";
  };
 
  module FaCreativeCommonsNd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsNd";
  };
 
  module FaCreativeCommonsPdAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsPdAlt";
  };
 
  module FaCreativeCommonsPd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsPd";
  };
 
  module FaCreativeCommonsRemix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsRemix";
  };
 
  module FaCreativeCommonsSa = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsSa";
  };
 
  module FaCreativeCommonsSamplingPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsSamplingPlus";
  };
 
  module FaCreativeCommonsSampling = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsSampling";
  };
 
  module FaCreativeCommonsShare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsShare";
  };
 
  module FaCreativeCommonsZero = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommonsZero";
  };
 
  module FaCreativeCommons = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreativeCommons";
  };
 
  module FaCriticalRole = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCriticalRole";
  };
 
  module FaCss3Alt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCss3Alt";
  };
 
  module FaCss3 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCss3";
  };
 
  module FaCuttlefish = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCuttlefish";
  };
 
  module FaDAndDBeyond = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDAndDBeyond";
  };
 
  module FaDAndD = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDAndD";
  };
 
  module FaDailymotion = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDailymotion";
  };
 
  module FaDashcube = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDashcube";
  };
 
  module FaDelicious = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDelicious";
  };
 
  module FaDeploydog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDeploydog";
  };
 
  module FaDeskpro = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDeskpro";
  };
 
  module FaDev = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDev";
  };
 
  module FaDeviantart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDeviantart";
  };
 
  module FaDhl = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDhl";
  };
 
  module FaDiaspora = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiaspora";
  };
 
  module FaDigg = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDigg";
  };
 
  module FaDigitalOcean = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDigitalOcean";
  };
 
  module FaDiscord = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiscord";
  };
 
  module FaDiscourse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiscourse";
  };
 
  module FaDochub = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDochub";
  };
 
  module FaDocker = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDocker";
  };
 
  module FaDraft2Digital = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDraft2Digital";
  };
 
  module FaDribbbleSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDribbbleSquare";
  };
 
  module FaDribbble = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDribbble";
  };
 
  module FaDropbox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDropbox";
  };
 
  module FaDrupal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDrupal";
  };
 
  module FaDyalog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDyalog";
  };
 
  module FaEarlybirds = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEarlybirds";
  };
 
  module FaEbay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEbay";
  };
 
  module FaEdge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEdge";
  };
 
  module FaElementor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaElementor";
  };
 
  module FaEllo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEllo";
  };
 
  module FaEmber = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEmber";
  };
 
  module FaEmpire = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEmpire";
  };
 
  module FaEnvira = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEnvira";
  };
 
  module FaErlang = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaErlang";
  };
 
  module FaEthereum = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEthereum";
  };
 
  module FaEtsy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEtsy";
  };
 
  module FaEvernote = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEvernote";
  };
 
  module FaExpeditedssl = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExpeditedssl";
  };
 
  module FaFacebookF = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFacebookF";
  };
 
  module FaFacebookMessenger = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFacebookMessenger";
  };
 
  module FaFacebookSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFacebookSquare";
  };
 
  module FaFacebook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFacebook";
  };
 
  module FaFantasyFlightGames = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFantasyFlightGames";
  };
 
  module FaFedex = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFedex";
  };
 
  module FaFedora = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFedora";
  };
 
  module FaFigma = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFigma";
  };
 
  module FaFirefoxBrowser = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirefoxBrowser";
  };
 
  module FaFirefox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirefox";
  };
 
  module FaFirstOrderAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirstOrderAlt";
  };
 
  module FaFirstOrder = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirstOrder";
  };
 
  module FaFirstdraft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirstdraft";
  };
 
  module FaFlickr = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlickr";
  };
 
  module FaFlipboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlipboard";
  };
 
  module FaFly = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFly";
  };
 
  module FaFontAwesomeAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFontAwesomeAlt";
  };
 
  module FaFontAwesomeFlag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFontAwesomeFlag";
  };
 
  module FaFontAwesomeLogoFull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFontAwesomeLogoFull";
  };
 
  module FaFontAwesome = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFontAwesome";
  };
 
  module FaFonticonsFi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFonticonsFi";
  };
 
  module FaFonticons = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFonticons";
  };
 
  module FaFortAwesomeAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFortAwesomeAlt";
  };
 
  module FaFortAwesome = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFortAwesome";
  };
 
  module FaForumbee = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaForumbee";
  };
 
  module FaFoursquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFoursquare";
  };
 
  module FaFreeCodeCamp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFreeCodeCamp";
  };
 
  module FaFreebsd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFreebsd";
  };
 
  module FaFulcrum = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFulcrum";
  };
 
  module FaGalacticRepublic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGalacticRepublic";
  };
 
  module FaGalacticSenate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGalacticSenate";
  };
 
  module FaGetPocket = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGetPocket";
  };
 
  module FaGgCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGgCircle";
  };
 
  module FaGg = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGg";
  };
 
  module FaGitAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGitAlt";
  };
 
  module FaGitSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGitSquare";
  };
 
  module FaGit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGit";
  };
 
  module FaGithubAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGithubAlt";
  };
 
  module FaGithubSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGithubSquare";
  };
 
  module FaGithub = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGithub";
  };
 
  module FaGitkraken = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGitkraken";
  };
 
  module FaGitlab = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGitlab";
  };
 
  module FaGitter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGitter";
  };
 
  module FaGlideG = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlideG";
  };
 
  module FaGlide = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlide";
  };
 
  module FaGofore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGofore";
  };
 
  module FaGoodreadsG = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGoodreadsG";
  };
 
  module FaGoodreads = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGoodreads";
  };
 
  module FaGoogleDrive = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGoogleDrive";
  };
 
  module FaGooglePlay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGooglePlay";
  };
 
  module FaGooglePlusG = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGooglePlusG";
  };
 
  module FaGooglePlusSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGooglePlusSquare";
  };
 
  module FaGooglePlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGooglePlus";
  };
 
  module FaGoogleWallet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGoogleWallet";
  };
 
  module FaGoogle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGoogle";
  };
 
  module FaGratipay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGratipay";
  };
 
  module FaGrav = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrav";
  };
 
  module FaGripfire = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGripfire";
  };
 
  module FaGrunt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrunt";
  };
 
  module FaGulp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGulp";
  };
 
  module FaHackerNewsSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHackerNewsSquare";
  };
 
  module FaHackerNews = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHackerNews";
  };
 
  module FaHackerrank = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHackerrank";
  };
 
  module FaHips = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHips";
  };
 
  module FaHireAHelper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHireAHelper";
  };
 
  module FaHooli = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHooli";
  };
 
  module FaHornbill = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHornbill";
  };
 
  module FaHotjar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHotjar";
  };
 
  module FaHouzz = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHouzz";
  };
 
  module FaHtml5 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHtml5";
  };
 
  module FaHubspot = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHubspot";
  };
 
  module FaIdeal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIdeal";
  };
 
  module FaImdb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaImdb";
  };
 
  module FaInstagramSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInstagramSquare";
  };
 
  module FaInstagram = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInstagram";
  };
 
  module FaIntercom = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIntercom";
  };
 
  module FaInternetExplorer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInternetExplorer";
  };
 
  module FaInvision = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInvision";
  };
 
  module FaIoxhost = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIoxhost";
  };
 
  module FaItchIo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaItchIo";
  };
 
  module FaItunesNote = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaItunesNote";
  };
 
  module FaItunes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaItunes";
  };
 
  module FaJava = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJava";
  };
 
  module FaJediOrder = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJediOrder";
  };
 
  module FaJenkins = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJenkins";
  };
 
  module FaJira = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJira";
  };
 
  module FaJoget = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJoget";
  };
 
  module FaJoomla = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJoomla";
  };
 
  module FaJsSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJsSquare";
  };
 
  module FaJs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJs";
  };
 
  module FaJsfiddle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJsfiddle";
  };
 
  module FaKaggle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKaggle";
  };
 
  module FaKeybase = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKeybase";
  };
 
  module FaKeycdn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKeycdn";
  };
 
  module FaKickstarterK = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKickstarterK";
  };
 
  module FaKickstarter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKickstarter";
  };
 
  module FaKorvue = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKorvue";
  };
 
  module FaLaravel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaravel";
  };
 
  module FaLastfmSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLastfmSquare";
  };
 
  module FaLastfm = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLastfm";
  };
 
  module FaLeanpub = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLeanpub";
  };
 
  module FaLess = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLess";
  };
 
  module FaLine = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLine";
  };
 
  module FaLinkedinIn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLinkedinIn";
  };
 
  module FaLinkedin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLinkedin";
  };
 
  module FaLinode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLinode";
  };
 
  module FaLinux = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLinux";
  };
 
  module FaLyft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLyft";
  };
 
  module FaMagento = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMagento";
  };
 
  module FaMailchimp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMailchimp";
  };
 
  module FaMandalorian = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMandalorian";
  };
 
  module FaMarkdown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarkdown";
  };
 
  module FaMastodon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMastodon";
  };
 
  module FaMaxcdn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMaxcdn";
  };
 
  module FaMdb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMdb";
  };
 
  module FaMedapps = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMedapps";
  };
 
  module FaMediumM = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMediumM";
  };
 
  module FaMedium = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMedium";
  };
 
  module FaMedrt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMedrt";
  };
 
  module FaMeetup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMeetup";
  };
 
  module FaMegaport = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMegaport";
  };
 
  module FaMendeley = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMendeley";
  };
 
  module FaMicroblog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicroblog";
  };
 
  module FaMicrosoft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrosoft";
  };
 
  module FaMix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMix";
  };
 
  module FaMixcloud = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMixcloud";
  };
 
  module FaMixer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMixer";
  };
 
  module FaMizuni = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMizuni";
  };
 
  module FaModx = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaModx";
  };
 
  module FaMonero = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMonero";
  };
 
  module FaNapster = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNapster";
  };
 
  module FaNeos = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNeos";
  };
 
  module FaNimblr = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNimblr";
  };
 
  module FaNodeJs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNodeJs";
  };
 
  module FaNode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNode";
  };
 
  module FaNpm = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNpm";
  };
 
  module FaNs8 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNs8";
  };
 
  module FaNutritionix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNutritionix";
  };
 
  module FaOdnoklassnikiSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOdnoklassnikiSquare";
  };
 
  module FaOdnoklassniki = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOdnoklassniki";
  };
 
  module FaOldRepublic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOldRepublic";
  };
 
  module FaOpencart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOpencart";
  };
 
  module FaOpenid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOpenid";
  };
 
  module FaOpera = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOpera";
  };
 
  module FaOptinMonster = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOptinMonster";
  };
 
  module FaOrcid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOrcid";
  };
 
  module FaOsi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOsi";
  };
 
  module FaPage4 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPage4";
  };
 
  module FaPagelines = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPagelines";
  };
 
  module FaPalfed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPalfed";
  };
 
  module FaPatreon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPatreon";
  };
 
  module FaPaypal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaypal";
  };
 
  module FaPennyArcade = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPennyArcade";
  };
 
  module FaPeriscope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPeriscope";
  };
 
  module FaPhabricator = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhabricator";
  };
 
  module FaPhoenixFramework = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoenixFramework";
  };
 
  module FaPhoenixSquadron = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoenixSquadron";
  };
 
  module FaPhp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhp";
  };
 
  module FaPiedPiperAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiedPiperAlt";
  };
 
  module FaPiedPiperHat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiedPiperHat";
  };
 
  module FaPiedPiperPp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiedPiperPp";
  };
 
  module FaPiedPiperSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiedPiperSquare";
  };
 
  module FaPiedPiper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiedPiper";
  };
 
  module FaPinterestP = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPinterestP";
  };
 
  module FaPinterestSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPinterestSquare";
  };
 
  module FaPinterest = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPinterest";
  };
 
  module FaPlaystation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlaystation";
  };
 
  module FaProductHunt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaProductHunt";
  };
 
  module FaPushed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPushed";
  };
 
  module FaPython = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPython";
  };
 
  module FaQq = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQq";
  };
 
  module FaQuinscape = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuinscape";
  };
 
  module FaQuora = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuora";
  };
 
  module FaRProject = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRProject";
  };
 
  module FaRaspberryPi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRaspberryPi";
  };
 
  module FaRavelry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRavelry";
  };
 
  module FaReact = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReact";
  };
 
  module FaReacteurope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReacteurope";
  };
 
  module FaReadme = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReadme";
  };
 
  module FaRebel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRebel";
  };
 
  module FaRedRiver = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedRiver";
  };
 
  module FaRedditAlien = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedditAlien";
  };
 
  module FaRedditSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedditSquare";
  };
 
  module FaReddit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReddit";
  };
 
  module FaRedhat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedhat";
  };
 
  module FaRenren = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRenren";
  };
 
  module FaReplyd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReplyd";
  };
 
  module FaResearchgate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaResearchgate";
  };
 
  module FaResolving = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaResolving";
  };
 
  module FaRev = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRev";
  };
 
  module FaRocketchat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRocketchat";
  };
 
  module FaRockrms = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRockrms";
  };
 
  module FaSafari = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSafari";
  };
 
  module FaSalesforce = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSalesforce";
  };
 
  module FaSass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSass";
  };
 
  module FaSchlix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSchlix";
  };
 
  module FaScribd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaScribd";
  };
 
  module FaSearchengin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearchengin";
  };
 
  module FaSellcast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSellcast";
  };
 
  module FaSellsy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSellsy";
  };
 
  module FaServicestack = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaServicestack";
  };
 
  module FaShirtsinbulk = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShirtsinbulk";
  };
 
  module FaShopify = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShopify";
  };
 
  module FaShopware = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShopware";
  };
 
  module FaSimplybuilt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSimplybuilt";
  };
 
  module FaSistrix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSistrix";
  };
 
  module FaSith = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSith";
  };
 
  module FaSketch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSketch";
  };
 
  module FaSkyatlas = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkyatlas";
  };
 
  module FaSkype = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkype";
  };
 
  module FaSlackHash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSlackHash";
  };
 
  module FaSlack = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSlack";
  };
 
  module FaSlideshare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSlideshare";
  };
 
  module FaSnapchatGhost = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnapchatGhost";
  };
 
  module FaSnapchatSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnapchatSquare";
  };
 
  module FaSnapchat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnapchat";
  };
 
  module FaSoundcloud = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSoundcloud";
  };
 
  module FaSourcetree = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSourcetree";
  };
 
  module FaSpeakap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpeakap";
  };
 
  module FaSpeakerDeck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpeakerDeck";
  };
 
  module FaSpotify = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpotify";
  };
 
  module FaSquarespace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSquarespace";
  };
 
  module FaStackExchange = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStackExchange";
  };
 
  module FaStackOverflow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStackOverflow";
  };
 
  module FaStackpath = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStackpath";
  };
 
  module FaStaylinked = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStaylinked";
  };
 
  module FaSteamSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSteamSquare";
  };
 
  module FaSteamSymbol = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSteamSymbol";
  };
 
  module FaSteam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSteam";
  };
 
  module FaStickerMule = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStickerMule";
  };
 
  module FaStrava = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStrava";
  };
 
  module FaStripeS = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStripeS";
  };
 
  module FaStripe = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStripe";
  };
 
  module FaStudiovinari = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStudiovinari";
  };
 
  module FaStumbleuponCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStumbleuponCircle";
  };
 
  module FaStumbleupon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStumbleupon";
  };
 
  module FaSuperpowers = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSuperpowers";
  };
 
  module FaSupple = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSupple";
  };
 
  module FaSuse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSuse";
  };
 
  module FaSwift = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSwift";
  };
 
  module FaSymfony = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSymfony";
  };
 
  module FaTeamspeak = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTeamspeak";
  };
 
  module FaTelegramPlane = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTelegramPlane";
  };
 
  module FaTelegram = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTelegram";
  };
 
  module FaTencentWeibo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTencentWeibo";
  };
 
  module FaTheRedYeti = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTheRedYeti";
  };
 
  module FaThemeco = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThemeco";
  };
 
  module FaThemeisle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThemeisle";
  };
 
  module FaThinkPeaks = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThinkPeaks";
  };
 
  module FaTradeFederation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTradeFederation";
  };
 
  module FaTrello = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrello";
  };
 
  module FaTripadvisor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTripadvisor";
  };
 
  module FaTumblrSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTumblrSquare";
  };
 
  module FaTumblr = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTumblr";
  };
 
  module FaTwitch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTwitch";
  };
 
  module FaTwitterSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTwitterSquare";
  };
 
  module FaTwitter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTwitter";
  };
 
  module FaTypo3 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTypo3";
  };
 
  module FaUber = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUber";
  };
 
  module FaUbuntu = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUbuntu";
  };
 
  module FaUikit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUikit";
  };
 
  module FaUmbraco = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUmbraco";
  };
 
  module FaUniregistry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUniregistry";
  };
 
  module FaUnity = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUnity";
  };
 
  module FaUntappd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUntappd";
  };
 
  module FaUps = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUps";
  };
 
  module FaUsb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUsb";
  };
 
  module FaUsps = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUsps";
  };
 
  module FaUssunnah = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUssunnah";
  };
 
  module FaVaadin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVaadin";
  };
 
  module FaViacoin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaViacoin";
  };
 
  module FaViadeoSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaViadeoSquare";
  };
 
  module FaViadeo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaViadeo";
  };
 
  module FaViber = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaViber";
  };
 
  module FaVimeoSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVimeoSquare";
  };
 
  module FaVimeoV = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVimeoV";
  };
 
  module FaVimeo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVimeo";
  };
 
  module FaVine = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVine";
  };
 
  module FaVk = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVk";
  };
 
  module FaVnv = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVnv";
  };
 
  module FaVuejs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVuejs";
  };
 
  module FaWaze = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWaze";
  };
 
  module FaWeebly = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWeebly";
  };
 
  module FaWeibo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWeibo";
  };
 
  module FaWeixin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWeixin";
  };
 
  module FaWhatsappSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWhatsappSquare";
  };
 
  module FaWhatsapp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWhatsapp";
  };
 
  module FaWhmcs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWhmcs";
  };
 
  module FaWikipediaW = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWikipediaW";
  };
 
  module FaWindows = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWindows";
  };
 
  module FaWix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWix";
  };
 
  module FaWizardsOfTheCoast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWizardsOfTheCoast";
  };
 
  module FaWolfPackBattalion = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWolfPackBattalion";
  };
 
  module FaWordpressSimple = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWordpressSimple";
  };
 
  module FaWordpress = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWordpress";
  };
 
  module FaWpbeginner = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWpbeginner";
  };
 
  module FaWpexplorer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWpexplorer";
  };
 
  module FaWpforms = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWpforms";
  };
 
  module FaWpressr = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWpressr";
  };
 
  module FaXbox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaXbox";
  };
 
  module FaXingSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaXingSquare";
  };
 
  module FaXing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaXing";
  };
 
  module FaYCombinator = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYCombinator";
  };
 
  module FaYahoo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYahoo";
  };
 
  module FaYammer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYammer";
  };
 
  module FaYandexInternational = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYandexInternational";
  };
 
  module FaYandex = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYandex";
  };
 
  module FaYarn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYarn";
  };
 
  module FaYelp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYelp";
  };
 
  module FaYoast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYoast";
  };
 
  module FaYoutubeSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYoutubeSquare";
  };
 
  module FaYoutube = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYoutube";
  };
 
  module FaZhihu = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaZhihu";
  };
 
  module FaAd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAd";
  };
 
  module FaAddressBook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAddressBook";
  };
 
  module FaAddressCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAddressCard";
  };
 
  module FaAdjust = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAdjust";
  };
 
  module FaAirFreshener = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAirFreshener";
  };
 
  module FaAlignCenter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlignCenter";
  };
 
  module FaAlignJustify = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlignJustify";
  };
 
  module FaAlignLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlignLeft";
  };
 
  module FaAlignRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAlignRight";
  };
 
  module FaAllergies = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAllergies";
  };
 
  module FaAmbulance = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAmbulance";
  };
 
  module FaAmericanSignLanguageInterpreting = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAmericanSignLanguageInterpreting";
  };
 
  module FaAnchor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAnchor";
  };
 
  module FaAngleDoubleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleDoubleDown";
  };
 
  module FaAngleDoubleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleDoubleLeft";
  };
 
  module FaAngleDoubleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleDoubleRight";
  };
 
  module FaAngleDoubleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleDoubleUp";
  };
 
  module FaAngleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleDown";
  };
 
  module FaAngleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleLeft";
  };
 
  module FaAngleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleRight";
  };
 
  module FaAngleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngleUp";
  };
 
  module FaAngry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAngry";
  };
 
  module FaAnkh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAnkh";
  };
 
  module FaAppleAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAppleAlt";
  };
 
  module FaArchive = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArchive";
  };
 
  module FaArchway = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArchway";
  };
 
  module FaArrowAltCircleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowAltCircleDown";
  };
 
  module FaArrowAltCircleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowAltCircleLeft";
  };
 
  module FaArrowAltCircleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowAltCircleRight";
  };
 
  module FaArrowAltCircleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowAltCircleUp";
  };
 
  module FaArrowCircleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowCircleDown";
  };
 
  module FaArrowCircleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowCircleLeft";
  };
 
  module FaArrowCircleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowCircleRight";
  };
 
  module FaArrowCircleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowCircleUp";
  };
 
  module FaArrowDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowDown";
  };
 
  module FaArrowLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowLeft";
  };
 
  module FaArrowRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowRight";
  };
 
  module FaArrowUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowUp";
  };
 
  module FaArrowsAltH = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowsAltH";
  };
 
  module FaArrowsAltV = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowsAltV";
  };
 
  module FaArrowsAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaArrowsAlt";
  };
 
  module FaAssistiveListeningSystems = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAssistiveListeningSystems";
  };
 
  module FaAsterisk = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAsterisk";
  };
 
  module FaAt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAt";
  };
 
  module FaAtlas = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAtlas";
  };
 
  module FaAtom = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAtom";
  };
 
  module FaAudioDescription = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAudioDescription";
  };
 
  module FaAward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaAward";
  };
 
  module FaBabyCarriage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBabyCarriage";
  };
 
  module FaBaby = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBaby";
  };
 
  module FaBackspace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBackspace";
  };
 
  module FaBackward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBackward";
  };
 
  module FaBacon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBacon";
  };
 
  module FaBahai = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBahai";
  };
 
  module FaBalanceScaleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBalanceScaleLeft";
  };
 
  module FaBalanceScaleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBalanceScaleRight";
  };
 
  module FaBalanceScale = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBalanceScale";
  };
 
  module FaBan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBan";
  };
 
  module FaBandAid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBandAid";
  };
 
  module FaBarcode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBarcode";
  };
 
  module FaBars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBars";
  };
 
  module FaBaseballBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBaseballBall";
  };
 
  module FaBasketballBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBasketballBall";
  };
 
  module FaBath = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBath";
  };
 
  module FaBatteryEmpty = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBatteryEmpty";
  };
 
  module FaBatteryFull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBatteryFull";
  };
 
  module FaBatteryHalf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBatteryHalf";
  };
 
  module FaBatteryQuarter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBatteryQuarter";
  };
 
  module FaBatteryThreeQuarters = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBatteryThreeQuarters";
  };
 
  module FaBed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBed";
  };
 
  module FaBeer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBeer";
  };
 
  module FaBellSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBellSlash";
  };
 
  module FaBell = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBell";
  };
 
  module FaBezierCurve = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBezierCurve";
  };
 
  module FaBible = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBible";
  };
 
  module FaBicycle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBicycle";
  };
 
  module FaBiking = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBiking";
  };
 
  module FaBinoculars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBinoculars";
  };
 
  module FaBiohazard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBiohazard";
  };
 
  module FaBirthdayCake = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBirthdayCake";
  };
 
  module FaBlenderPhone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlenderPhone";
  };
 
  module FaBlender = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlender";
  };
 
  module FaBlind = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlind";
  };
 
  module FaBlog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBlog";
  };
 
  module FaBold = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBold";
  };
 
  module FaBolt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBolt";
  };
 
  module FaBomb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBomb";
  };
 
  module FaBone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBone";
  };
 
  module FaBong = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBong";
  };
 
  module FaBookDead = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBookDead";
  };
 
  module FaBookMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBookMedical";
  };
 
  module FaBookOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBookOpen";
  };
 
  module FaBookReader = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBookReader";
  };
 
  module FaBook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBook";
  };
 
  module FaBookmark = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBookmark";
  };
 
  module FaBorderAll = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBorderAll";
  };
 
  module FaBorderNone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBorderNone";
  };
 
  module FaBorderStyle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBorderStyle";
  };
 
  module FaBowlingBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBowlingBall";
  };
 
  module FaBoxOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBoxOpen";
  };
 
  module FaBox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBox";
  };
 
  module FaBoxes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBoxes";
  };
 
  module FaBraille = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBraille";
  };
 
  module FaBrain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBrain";
  };
 
  module FaBreadSlice = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBreadSlice";
  };
 
  module FaBriefcaseMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBriefcaseMedical";
  };
 
  module FaBriefcase = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBriefcase";
  };
 
  module FaBroadcastTower = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBroadcastTower";
  };
 
  module FaBroom = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBroom";
  };
 
  module FaBrush = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBrush";
  };
 
  module FaBug = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBug";
  };
 
  module FaBuilding = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBuilding";
  };
 
  module FaBullhorn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBullhorn";
  };
 
  module FaBullseye = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBullseye";
  };
 
  module FaBurn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBurn";
  };
 
  module FaBusAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBusAlt";
  };
 
  module FaBus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBus";
  };
 
  module FaBusinessTime = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaBusinessTime";
  };
 
  module FaCalculator = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalculator";
  };
 
  module FaCalendarAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarAlt";
  };
 
  module FaCalendarCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarCheck";
  };
 
  module FaCalendarDay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarDay";
  };
 
  module FaCalendarMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarMinus";
  };
 
  module FaCalendarPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarPlus";
  };
 
  module FaCalendarTimes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarTimes";
  };
 
  module FaCalendarWeek = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendarWeek";
  };
 
  module FaCalendar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCalendar";
  };
 
  module FaCameraRetro = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCameraRetro";
  };
 
  module FaCamera = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCamera";
  };
 
  module FaCampground = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCampground";
  };
 
  module FaCandyCane = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCandyCane";
  };
 
  module FaCannabis = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCannabis";
  };
 
  module FaCapsules = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCapsules";
  };
 
  module FaCarAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCarAlt";
  };
 
  module FaCarBattery = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCarBattery";
  };
 
  module FaCarCrash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCarCrash";
  };
 
  module FaCarSide = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCarSide";
  };
 
  module FaCar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCar";
  };
 
  module FaCaravan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaravan";
  };
 
  module FaCaretDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretDown";
  };
 
  module FaCaretLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretLeft";
  };
 
  module FaCaretRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretRight";
  };
 
  module FaCaretSquareDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretSquareDown";
  };
 
  module FaCaretSquareLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretSquareLeft";
  };
 
  module FaCaretSquareRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretSquareRight";
  };
 
  module FaCaretSquareUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretSquareUp";
  };
 
  module FaCaretUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCaretUp";
  };
 
  module FaCarrot = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCarrot";
  };
 
  module FaCartArrowDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCartArrowDown";
  };
 
  module FaCartPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCartPlus";
  };
 
  module FaCashRegister = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCashRegister";
  };
 
  module FaCat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCat";
  };
 
  module FaCertificate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCertificate";
  };
 
  module FaChair = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChair";
  };
 
  module FaChalkboardTeacher = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChalkboardTeacher";
  };
 
  module FaChalkboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChalkboard";
  };
 
  module FaChargingStation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChargingStation";
  };
 
  module FaChartArea = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChartArea";
  };
 
  module FaChartBar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChartBar";
  };
 
  module FaChartLine = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChartLine";
  };
 
  module FaChartPie = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChartPie";
  };
 
  module FaCheckCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCheckCircle";
  };
 
  module FaCheckDouble = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCheckDouble";
  };
 
  module FaCheckSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCheckSquare";
  };
 
  module FaCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCheck";
  };
 
  module FaCheese = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCheese";
  };
 
  module FaChessBishop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessBishop";
  };
 
  module FaChessBoard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessBoard";
  };
 
  module FaChessKing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessKing";
  };
 
  module FaChessKnight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessKnight";
  };
 
  module FaChessPawn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessPawn";
  };
 
  module FaChessQueen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessQueen";
  };
 
  module FaChessRook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChessRook";
  };
 
  module FaChess = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChess";
  };
 
  module FaChevronCircleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronCircleDown";
  };
 
  module FaChevronCircleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronCircleLeft";
  };
 
  module FaChevronCircleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronCircleRight";
  };
 
  module FaChevronCircleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronCircleUp";
  };
 
  module FaChevronDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronDown";
  };
 
  module FaChevronLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronLeft";
  };
 
  module FaChevronRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronRight";
  };
 
  module FaChevronUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChevronUp";
  };
 
  module FaChild = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChild";
  };
 
  module FaChurch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaChurch";
  };
 
  module FaCircleNotch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCircleNotch";
  };
 
  module FaCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCircle";
  };
 
  module FaCity = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCity";
  };
 
  module FaClinicMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClinicMedical";
  };
 
  module FaClipboardCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClipboardCheck";
  };
 
  module FaClipboardList = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClipboardList";
  };
 
  module FaClipboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClipboard";
  };
 
  module FaClock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClock";
  };
 
  module FaClone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClone";
  };
 
  module FaClosedCaptioning = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaClosedCaptioning";
  };
 
  module FaCloudDownloadAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudDownloadAlt";
  };
 
  module FaCloudMeatball = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudMeatball";
  };
 
  module FaCloudMoonRain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudMoonRain";
  };
 
  module FaCloudMoon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudMoon";
  };
 
  module FaCloudRain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudRain";
  };
 
  module FaCloudShowersHeavy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudShowersHeavy";
  };
 
  module FaCloudSunRain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudSunRain";
  };
 
  module FaCloudSun = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudSun";
  };
 
  module FaCloudUploadAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloudUploadAlt";
  };
 
  module FaCloud = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCloud";
  };
 
  module FaCocktail = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCocktail";
  };
 
  module FaCodeBranch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCodeBranch";
  };
 
  module FaCode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCode";
  };
 
  module FaCoffee = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCoffee";
  };
 
  module FaCog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCog";
  };
 
  module FaCogs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCogs";
  };
 
  module FaCoins = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCoins";
  };
 
  module FaColumns = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaColumns";
  };
 
  module FaCommentAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentAlt";
  };
 
  module FaCommentDollar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentDollar";
  };
 
  module FaCommentDots = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentDots";
  };
 
  module FaCommentMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentMedical";
  };
 
  module FaCommentSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentSlash";
  };
 
  module FaComment = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaComment";
  };
 
  module FaCommentsDollar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCommentsDollar";
  };
 
  module FaComments = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaComments";
  };
 
  module FaCompactDisc = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCompactDisc";
  };
 
  module FaCompass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCompass";
  };
 
  module FaCompressAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCompressAlt";
  };
 
  module FaCompressArrowsAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCompressArrowsAlt";
  };
 
  module FaCompress = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCompress";
  };
 
  module FaConciergeBell = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaConciergeBell";
  };
 
  module FaCookieBite = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCookieBite";
  };
 
  module FaCookie = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCookie";
  };
 
  module FaCopy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCopy";
  };
 
  module FaCopyright = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCopyright";
  };
 
  module FaCouch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCouch";
  };
 
  module FaCreditCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCreditCard";
  };
 
  module FaCropAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCropAlt";
  };
 
  module FaCrop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCrop";
  };
 
  module FaCross = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCross";
  };
 
  module FaCrosshairs = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCrosshairs";
  };
 
  module FaCrow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCrow";
  };
 
  module FaCrown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCrown";
  };
 
  module FaCrutch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCrutch";
  };
 
  module FaCube = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCube";
  };
 
  module FaCubes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCubes";
  };
 
  module FaCut = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaCut";
  };
 
  module FaDatabase = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDatabase";
  };
 
  module FaDeaf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDeaf";
  };
 
  module FaDemocrat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDemocrat";
  };
 
  module FaDesktop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDesktop";
  };
 
  module FaDharmachakra = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDharmachakra";
  };
 
  module FaDiagnoses = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiagnoses";
  };
 
  module FaDiceD20 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceD20";
  };
 
  module FaDiceD6 = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceD6";
  };
 
  module FaDiceFive = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceFive";
  };
 
  module FaDiceFour = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceFour";
  };
 
  module FaDiceOne = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceOne";
  };
 
  module FaDiceSix = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceSix";
  };
 
  module FaDiceThree = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceThree";
  };
 
  module FaDiceTwo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDiceTwo";
  };
 
  module FaDice = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDice";
  };
 
  module FaDigitalTachograph = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDigitalTachograph";
  };
 
  module FaDirections = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDirections";
  };
 
  module FaDivide = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDivide";
  };
 
  module FaDizzy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDizzy";
  };
 
  module FaDna = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDna";
  };
 
  module FaDog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDog";
  };
 
  module FaDollarSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDollarSign";
  };
 
  module FaDollyFlatbed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDollyFlatbed";
  };
 
  module FaDolly = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDolly";
  };
 
  module FaDonate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDonate";
  };
 
  module FaDoorClosed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDoorClosed";
  };
 
  module FaDoorOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDoorOpen";
  };
 
  module FaDotCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDotCircle";
  };
 
  module FaDove = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDove";
  };
 
  module FaDownload = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDownload";
  };
 
  module FaDraftingCompass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDraftingCompass";
  };
 
  module FaDragon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDragon";
  };
 
  module FaDrawPolygon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDrawPolygon";
  };
 
  module FaDrumSteelpan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDrumSteelpan";
  };
 
  module FaDrum = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDrum";
  };
 
  module FaDrumstickBite = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDrumstickBite";
  };
 
  module FaDumbbell = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDumbbell";
  };
 
  module FaDumpsterFire = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDumpsterFire";
  };
 
  module FaDumpster = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDumpster";
  };
 
  module FaDungeon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaDungeon";
  };
 
  module FaEdit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEdit";
  };
 
  module FaEgg = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEgg";
  };
 
  module FaEject = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEject";
  };
 
  module FaEllipsisH = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEllipsisH";
  };
 
  module FaEllipsisV = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEllipsisV";
  };
 
  module FaEnvelopeOpenText = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEnvelopeOpenText";
  };
 
  module FaEnvelopeOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEnvelopeOpen";
  };
 
  module FaEnvelopeSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEnvelopeSquare";
  };
 
  module FaEnvelope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEnvelope";
  };
 
  module FaEquals = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEquals";
  };
 
  module FaEraser = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEraser";
  };
 
  module FaEthernet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEthernet";
  };
 
  module FaEuroSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEuroSign";
  };
 
  module FaExchangeAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExchangeAlt";
  };
 
  module FaExclamationCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExclamationCircle";
  };
 
  module FaExclamationTriangle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExclamationTriangle";
  };
 
  module FaExclamation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExclamation";
  };
 
  module FaExpandAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExpandAlt";
  };
 
  module FaExpandArrowsAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExpandArrowsAlt";
  };
 
  module FaExpand = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExpand";
  };
 
  module FaExternalLinkAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExternalLinkAlt";
  };
 
  module FaExternalLinkSquareAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaExternalLinkSquareAlt";
  };
 
  module FaEyeDropper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEyeDropper";
  };
 
  module FaEyeSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEyeSlash";
  };
 
  module FaEye = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaEye";
  };
 
  module FaFan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFan";
  };
 
  module FaFastBackward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFastBackward";
  };
 
  module FaFastForward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFastForward";
  };
 
  module FaFax = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFax";
  };
 
  module FaFeatherAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFeatherAlt";
  };
 
  module FaFeather = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFeather";
  };
 
  module FaFemale = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFemale";
  };
 
  module FaFighterJet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFighterJet";
  };
 
  module FaFileAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileAlt";
  };
 
  module FaFileArchive = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileArchive";
  };
 
  module FaFileAudio = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileAudio";
  };
 
  module FaFileCode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileCode";
  };
 
  module FaFileContract = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileContract";
  };
 
  module FaFileCsv = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileCsv";
  };
 
  module FaFileDownload = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileDownload";
  };
 
  module FaFileExcel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileExcel";
  };
 
  module FaFileExport = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileExport";
  };
 
  module FaFileImage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileImage";
  };
 
  module FaFileImport = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileImport";
  };
 
  module FaFileInvoiceDollar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileInvoiceDollar";
  };
 
  module FaFileInvoice = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileInvoice";
  };
 
  module FaFileMedicalAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileMedicalAlt";
  };
 
  module FaFileMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileMedical";
  };
 
  module FaFilePdf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFilePdf";
  };
 
  module FaFilePowerpoint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFilePowerpoint";
  };
 
  module FaFilePrescription = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFilePrescription";
  };
 
  module FaFileSignature = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileSignature";
  };
 
  module FaFileUpload = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileUpload";
  };
 
  module FaFileVideo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileVideo";
  };
 
  module FaFileWord = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFileWord";
  };
 
  module FaFile = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFile";
  };
 
  module FaFillDrip = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFillDrip";
  };
 
  module FaFill = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFill";
  };
 
  module FaFilm = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFilm";
  };
 
  module FaFilter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFilter";
  };
 
  module FaFingerprint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFingerprint";
  };
 
  module FaFireAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFireAlt";
  };
 
  module FaFireExtinguisher = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFireExtinguisher";
  };
 
  module FaFire = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFire";
  };
 
  module FaFirstAid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFirstAid";
  };
 
  module FaFish = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFish";
  };
 
  module FaFistRaised = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFistRaised";
  };
 
  module FaFlagCheckered = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlagCheckered";
  };
 
  module FaFlagUsa = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlagUsa";
  };
 
  module FaFlag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlag";
  };
 
  module FaFlask = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlask";
  };
 
  module FaFlushed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFlushed";
  };
 
  module FaFolderMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFolderMinus";
  };
 
  module FaFolderOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFolderOpen";
  };
 
  module FaFolderPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFolderPlus";
  };
 
  module FaFolder = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFolder";
  };
 
  module FaFont = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFont";
  };
 
  module FaFootballBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFootballBall";
  };
 
  module FaForward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaForward";
  };
 
  module FaFrog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFrog";
  };
 
  module FaFrownOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFrownOpen";
  };
 
  module FaFrown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFrown";
  };
 
  module FaFunnelDollar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFunnelDollar";
  };
 
  module FaFutbol = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaFutbol";
  };
 
  module FaGamepad = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGamepad";
  };
 
  module FaGasPump = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGasPump";
  };
 
  module FaGavel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGavel";
  };
 
  module FaGem = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGem";
  };
 
  module FaGenderless = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGenderless";
  };
 
  module FaGhost = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGhost";
  };
 
  module FaGift = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGift";
  };
 
  module FaGifts = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGifts";
  };
 
  module FaGlassCheers = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlassCheers";
  };
 
  module FaGlassMartiniAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlassMartiniAlt";
  };
 
  module FaGlassMartini = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlassMartini";
  };
 
  module FaGlassWhiskey = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlassWhiskey";
  };
 
  module FaGlasses = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlasses";
  };
 
  module FaGlobeAfrica = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlobeAfrica";
  };
 
  module FaGlobeAmericas = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlobeAmericas";
  };
 
  module FaGlobeAsia = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlobeAsia";
  };
 
  module FaGlobeEurope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlobeEurope";
  };
 
  module FaGlobe = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGlobe";
  };
 
  module FaGolfBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGolfBall";
  };
 
  module FaGopuram = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGopuram";
  };
 
  module FaGraduationCap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGraduationCap";
  };
 
  module FaGreaterThanEqual = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGreaterThanEqual";
  };
 
  module FaGreaterThan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGreaterThan";
  };
 
  module FaGrimace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrimace";
  };
 
  module FaGrinAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinAlt";
  };
 
  module FaGrinBeamSweat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinBeamSweat";
  };
 
  module FaGrinBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinBeam";
  };
 
  module FaGrinHearts = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinHearts";
  };
 
  module FaGrinSquintTears = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinSquintTears";
  };
 
  module FaGrinSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinSquint";
  };
 
  module FaGrinStars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinStars";
  };
 
  module FaGrinTears = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinTears";
  };
 
  module FaGrinTongueSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinTongueSquint";
  };
 
  module FaGrinTongueWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinTongueWink";
  };
 
  module FaGrinTongue = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinTongue";
  };
 
  module FaGrinWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrinWink";
  };
 
  module FaGrin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGrin";
  };
 
  module FaGripHorizontal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGripHorizontal";
  };
 
  module FaGripLinesVertical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGripLinesVertical";
  };
 
  module FaGripLines = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGripLines";
  };
 
  module FaGripVertical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGripVertical";
  };
 
  module FaGuitar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaGuitar";
  };
 
  module FaHSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHSquare";
  };
 
  module FaHamburger = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHamburger";
  };
 
  module FaHammer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHammer";
  };
 
  module FaHamsa = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHamsa";
  };
 
  module FaHandHoldingHeart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandHoldingHeart";
  };
 
  module FaHandHoldingUsd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandHoldingUsd";
  };
 
  module FaHandHolding = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandHolding";
  };
 
  module FaHandLizard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandLizard";
  };
 
  module FaHandMiddleFinger = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandMiddleFinger";
  };
 
  module FaHandPaper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPaper";
  };
 
  module FaHandPeace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPeace";
  };
 
  module FaHandPointDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPointDown";
  };
 
  module FaHandPointLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPointLeft";
  };
 
  module FaHandPointRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPointRight";
  };
 
  module FaHandPointUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPointUp";
  };
 
  module FaHandPointer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandPointer";
  };
 
  module FaHandRock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandRock";
  };
 
  module FaHandScissors = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandScissors";
  };
 
  module FaHandSpock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandSpock";
  };
 
  module FaHandsHelping = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandsHelping";
  };
 
  module FaHands = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHands";
  };
 
  module FaHandshake = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHandshake";
  };
 
  module FaHanukiah = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHanukiah";
  };
 
  module FaHardHat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHardHat";
  };
 
  module FaHashtag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHashtag";
  };
 
  module FaHatCowboySide = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHatCowboySide";
  };
 
  module FaHatCowboy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHatCowboy";
  };
 
  module FaHatWizard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHatWizard";
  };
 
  module FaHdd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHdd";
  };
 
  module FaHeading = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeading";
  };
 
  module FaHeadphonesAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeadphonesAlt";
  };
 
  module FaHeadphones = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeadphones";
  };
 
  module FaHeadset = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeadset";
  };
 
  module FaHeartBroken = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeartBroken";
  };
 
  module FaHeart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeart";
  };
 
  module FaHeartbeat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHeartbeat";
  };
 
  module FaHelicopter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHelicopter";
  };
 
  module FaHighlighter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHighlighter";
  };
 
  module FaHiking = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHiking";
  };
 
  module FaHippo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHippo";
  };
 
  module FaHistory = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHistory";
  };
 
  module FaHockeyPuck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHockeyPuck";
  };
 
  module FaHollyBerry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHollyBerry";
  };
 
  module FaHome = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHome";
  };
 
  module FaHorseHead = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHorseHead";
  };
 
  module FaHorse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHorse";
  };
 
  module FaHospitalAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHospitalAlt";
  };
 
  module FaHospitalSymbol = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHospitalSymbol";
  };
 
  module FaHospital = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHospital";
  };
 
  module FaHotTub = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHotTub";
  };
 
  module FaHotdog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHotdog";
  };
 
  module FaHotel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHotel";
  };
 
  module FaHourglassEnd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHourglassEnd";
  };
 
  module FaHourglassHalf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHourglassHalf";
  };
 
  module FaHourglassStart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHourglassStart";
  };
 
  module FaHourglass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHourglass";
  };
 
  module FaHouseDamage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHouseDamage";
  };
 
  module FaHryvnia = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaHryvnia";
  };
 
  module FaICursor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaICursor";
  };
 
  module FaIceCream = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIceCream";
  };
 
  module FaIcicles = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIcicles";
  };
 
  module FaIcons = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIcons";
  };
 
  module FaIdBadge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIdBadge";
  };
 
  module FaIdCardAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIdCardAlt";
  };
 
  module FaIdCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIdCard";
  };
 
  module FaIgloo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIgloo";
  };
 
  module FaImage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaImage";
  };
 
  module FaImages = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaImages";
  };
 
  module FaInbox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInbox";
  };
 
  module FaIndent = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIndent";
  };
 
  module FaIndustry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaIndustry";
  };
 
  module FaInfinity = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInfinity";
  };
 
  module FaInfoCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInfoCircle";
  };
 
  module FaInfo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaInfo";
  };
 
  module FaItalic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaItalic";
  };
 
  module FaJedi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJedi";
  };
 
  module FaJoint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJoint";
  };
 
  module FaJournalWhills = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaJournalWhills";
  };
 
  module FaKaaba = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKaaba";
  };
 
  module FaKey = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKey";
  };
 
  module FaKeyboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKeyboard";
  };
 
  module FaKhanda = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKhanda";
  };
 
  module FaKissBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKissBeam";
  };
 
  module FaKissWinkHeart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKissWinkHeart";
  };
 
  module FaKiss = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKiss";
  };
 
  module FaKiwiBird = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaKiwiBird";
  };
 
  module FaLandmark = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLandmark";
  };
 
  module FaLanguage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLanguage";
  };
 
  module FaLaptopCode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaptopCode";
  };
 
  module FaLaptopMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaptopMedical";
  };
 
  module FaLaptop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaptop";
  };
 
  module FaLaughBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaughBeam";
  };
 
  module FaLaughSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaughSquint";
  };
 
  module FaLaughWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaughWink";
  };
 
  module FaLaugh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLaugh";
  };
 
  module FaLayerGroup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLayerGroup";
  };
 
  module FaLeaf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLeaf";
  };
 
  module FaLemon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLemon";
  };
 
  module FaLessThanEqual = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLessThanEqual";
  };
 
  module FaLessThan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLessThan";
  };
 
  module FaLevelDownAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLevelDownAlt";
  };
 
  module FaLevelUpAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLevelUpAlt";
  };
 
  module FaLifeRing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLifeRing";
  };
 
  module FaLightbulb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLightbulb";
  };
 
  module FaLink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLink";
  };
 
  module FaLiraSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLiraSign";
  };
 
  module FaListAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaListAlt";
  };
 
  module FaListOl = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaListOl";
  };
 
  module FaListUl = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaListUl";
  };
 
  module FaList = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaList";
  };
 
  module FaLocationArrow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLocationArrow";
  };
 
  module FaLockOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLockOpen";
  };
 
  module FaLock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLock";
  };
 
  module FaLongArrowAltDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLongArrowAltDown";
  };
 
  module FaLongArrowAltLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLongArrowAltLeft";
  };
 
  module FaLongArrowAltRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLongArrowAltRight";
  };
 
  module FaLongArrowAltUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLongArrowAltUp";
  };
 
  module FaLowVision = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLowVision";
  };
 
  module FaLuggageCart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaLuggageCart";
  };
 
  module FaMagic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMagic";
  };
 
  module FaMagnet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMagnet";
  };
 
  module FaMailBulk = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMailBulk";
  };
 
  module FaMale = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMale";
  };
 
  module FaMapMarkedAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapMarkedAlt";
  };
 
  module FaMapMarked = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapMarked";
  };
 
  module FaMapMarkerAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapMarkerAlt";
  };
 
  module FaMapMarker = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapMarker";
  };
 
  module FaMapPin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapPin";
  };
 
  module FaMapSigns = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMapSigns";
  };
 
  module FaMap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMap";
  };
 
  module FaMarker = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarker";
  };
 
  module FaMarsDouble = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarsDouble";
  };
 
  module FaMarsStrokeH = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarsStrokeH";
  };
 
  module FaMarsStrokeV = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarsStrokeV";
  };
 
  module FaMarsStroke = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMarsStroke";
  };
 
  module FaMars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMars";
  };
 
  module FaMask = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMask";
  };
 
  module FaMedal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMedal";
  };
 
  module FaMedkit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMedkit";
  };
 
  module FaMehBlank = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMehBlank";
  };
 
  module FaMehRollingEyes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMehRollingEyes";
  };
 
  module FaMeh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMeh";
  };
 
  module FaMemory = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMemory";
  };
 
  module FaMenorah = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMenorah";
  };
 
  module FaMercury = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMercury";
  };
 
  module FaMeteor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMeteor";
  };
 
  module FaMicrochip = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrochip";
  };
 
  module FaMicrophoneAltSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrophoneAltSlash";
  };
 
  module FaMicrophoneAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrophoneAlt";
  };
 
  module FaMicrophoneSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrophoneSlash";
  };
 
  module FaMicrophone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicrophone";
  };
 
  module FaMicroscope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMicroscope";
  };
 
  module FaMinusCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMinusCircle";
  };
 
  module FaMinusSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMinusSquare";
  };
 
  module FaMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMinus";
  };
 
  module FaMitten = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMitten";
  };
 
  module FaMobileAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMobileAlt";
  };
 
  module FaMobile = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMobile";
  };
 
  module FaMoneyBillAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyBillAlt";
  };
 
  module FaMoneyBillWaveAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyBillWaveAlt";
  };
 
  module FaMoneyBillWave = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyBillWave";
  };
 
  module FaMoneyBill = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyBill";
  };
 
  module FaMoneyCheckAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyCheckAlt";
  };
 
  module FaMoneyCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoneyCheck";
  };
 
  module FaMonument = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMonument";
  };
 
  module FaMoon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMoon";
  };
 
  module FaMortarPestle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMortarPestle";
  };
 
  module FaMosque = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMosque";
  };
 
  module FaMotorcycle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMotorcycle";
  };
 
  module FaMountain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMountain";
  };
 
  module FaMousePointer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMousePointer";
  };
 
  module FaMouse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMouse";
  };
 
  module FaMugHot = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMugHot";
  };
 
  module FaMusic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaMusic";
  };
 
  module FaNetworkWired = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNetworkWired";
  };
 
  module FaNeuter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNeuter";
  };
 
  module FaNewspaper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNewspaper";
  };
 
  module FaNotEqual = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNotEqual";
  };
 
  module FaNotesMedical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaNotesMedical";
  };
 
  module FaObjectGroup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaObjectGroup";
  };
 
  module FaObjectUngroup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaObjectUngroup";
  };
 
  module FaOilCan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOilCan";
  };
 
  module FaOm = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOm";
  };
 
  module FaOtter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOtter";
  };
 
  module FaOutdent = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaOutdent";
  };
 
  module FaPager = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPager";
  };
 
  module FaPaintBrush = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaintBrush";
  };
 
  module FaPaintRoller = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaintRoller";
  };
 
  module FaPalette = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPalette";
  };
 
  module FaPallet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPallet";
  };
 
  module FaPaperPlane = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaperPlane";
  };
 
  module FaPaperclip = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaperclip";
  };
 
  module FaParachuteBox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaParachuteBox";
  };
 
  module FaParagraph = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaParagraph";
  };
 
  module FaParking = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaParking";
  };
 
  module FaPassport = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPassport";
  };
 
  module FaPastafarianism = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPastafarianism";
  };
 
  module FaPaste = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaste";
  };
 
  module FaPauseCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPauseCircle";
  };
 
  module FaPause = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPause";
  };
 
  module FaPaw = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPaw";
  };
 
  module FaPeace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPeace";
  };
 
  module FaPenAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPenAlt";
  };
 
  module FaPenFancy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPenFancy";
  };
 
  module FaPenNib = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPenNib";
  };
 
  module FaPenSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPenSquare";
  };
 
  module FaPen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPen";
  };
 
  module FaPencilAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPencilAlt";
  };
 
  module FaPencilRuler = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPencilRuler";
  };
 
  module FaPeopleCarry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPeopleCarry";
  };
 
  module FaPepperHot = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPepperHot";
  };
 
  module FaPercent = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPercent";
  };
 
  module FaPercentage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPercentage";
  };
 
  module FaPersonBooth = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPersonBooth";
  };
 
  module FaPhoneAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoneAlt";
  };
 
  module FaPhoneSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoneSlash";
  };
 
  module FaPhoneSquareAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoneSquareAlt";
  };
 
  module FaPhoneSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoneSquare";
  };
 
  module FaPhoneVolume = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhoneVolume";
  };
 
  module FaPhone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhone";
  };
 
  module FaPhotoVideo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPhotoVideo";
  };
 
  module FaPiggyBank = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPiggyBank";
  };
 
  module FaPills = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPills";
  };
 
  module FaPizzaSlice = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPizzaSlice";
  };
 
  module FaPlaceOfWorship = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlaceOfWorship";
  };
 
  module FaPlaneArrival = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlaneArrival";
  };
 
  module FaPlaneDeparture = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlaneDeparture";
  };
 
  module FaPlane = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlane";
  };
 
  module FaPlayCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlayCircle";
  };
 
  module FaPlay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlay";
  };
 
  module FaPlug = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlug";
  };
 
  module FaPlusCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlusCircle";
  };
 
  module FaPlusSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlusSquare";
  };
 
  module FaPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPlus";
  };
 
  module FaPodcast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPodcast";
  };
 
  module FaPollH = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPollH";
  };
 
  module FaPoll = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPoll";
  };
 
  module FaPooStorm = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPooStorm";
  };
 
  module FaPoo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPoo";
  };
 
  module FaPoop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPoop";
  };
 
  module FaPortrait = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPortrait";
  };
 
  module FaPoundSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPoundSign";
  };
 
  module FaPowerOff = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPowerOff";
  };
 
  module FaPray = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPray";
  };
 
  module FaPrayingHands = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPrayingHands";
  };
 
  module FaPrescriptionBottleAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPrescriptionBottleAlt";
  };
 
  module FaPrescriptionBottle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPrescriptionBottle";
  };
 
  module FaPrescription = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPrescription";
  };
 
  module FaPrint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPrint";
  };
 
  module FaProcedures = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaProcedures";
  };
 
  module FaProjectDiagram = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaProjectDiagram";
  };
 
  module FaPuzzlePiece = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaPuzzlePiece";
  };
 
  module FaQrcode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQrcode";
  };
 
  module FaQuestionCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuestionCircle";
  };
 
  module FaQuestion = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuestion";
  };
 
  module FaQuidditch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuidditch";
  };
 
  module FaQuoteLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuoteLeft";
  };
 
  module FaQuoteRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuoteRight";
  };
 
  module FaQuran = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaQuran";
  };
 
  module FaRadiationAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRadiationAlt";
  };
 
  module FaRadiation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRadiation";
  };
 
  module FaRainbow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRainbow";
  };
 
  module FaRandom = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRandom";
  };
 
  module FaReceipt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReceipt";
  };
 
  module FaRecordVinyl = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRecordVinyl";
  };
 
  module FaRecycle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRecycle";
  };
 
  module FaRedoAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedoAlt";
  };
 
  module FaRedo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRedo";
  };
 
  module FaRegistered = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegistered";
  };
 
  module FaRemoveFormat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRemoveFormat";
  };
 
  module FaReplyAll = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReplyAll";
  };
 
  module FaReply = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaReply";
  };
 
  module FaRepublican = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRepublican";
  };
 
  module FaRestroom = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRestroom";
  };
 
  module FaRetweet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRetweet";
  };
 
  module FaRibbon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRibbon";
  };
 
  module FaRing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRing";
  };
 
  module FaRoad = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRoad";
  };
 
  module FaRobot = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRobot";
  };
 
  module FaRocket = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRocket";
  };
 
  module FaRoute = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRoute";
  };
 
  module FaRssSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRssSquare";
  };
 
  module FaRss = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRss";
  };
 
  module FaRubleSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRubleSign";
  };
 
  module FaRulerCombined = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRulerCombined";
  };
 
  module FaRulerHorizontal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRulerHorizontal";
  };
 
  module FaRulerVertical = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRulerVertical";
  };
 
  module FaRuler = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRuler";
  };
 
  module FaRunning = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRunning";
  };
 
  module FaRupeeSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRupeeSign";
  };
 
  module FaSadCry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSadCry";
  };
 
  module FaSadTear = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSadTear";
  };
 
  module FaSatelliteDish = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSatelliteDish";
  };
 
  module FaSatellite = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSatellite";
  };
 
  module FaSave = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSave";
  };
 
  module FaSchool = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSchool";
  };
 
  module FaScrewdriver = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaScrewdriver";
  };
 
  module FaScroll = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaScroll";
  };
 
  module FaSdCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSdCard";
  };
 
  module FaSearchDollar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearchDollar";
  };
 
  module FaSearchLocation = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearchLocation";
  };
 
  module FaSearchMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearchMinus";
  };
 
  module FaSearchPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearchPlus";
  };
 
  module FaSearch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSearch";
  };
 
  module FaSeedling = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSeedling";
  };
 
  module FaServer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaServer";
  };
 
  module FaShapes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShapes";
  };
 
  module FaShareAltSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShareAltSquare";
  };
 
  module FaShareAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShareAlt";
  };
 
  module FaShareSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShareSquare";
  };
 
  module FaShare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShare";
  };
 
  module FaShekelSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShekelSign";
  };
 
  module FaShieldAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShieldAlt";
  };
 
  module FaShip = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShip";
  };
 
  module FaShippingFast = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShippingFast";
  };
 
  module FaShoePrints = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShoePrints";
  };
 
  module FaShoppingBag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShoppingBag";
  };
 
  module FaShoppingBasket = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShoppingBasket";
  };
 
  module FaShoppingCart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShoppingCart";
  };
 
  module FaShower = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShower";
  };
 
  module FaShuttleVan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaShuttleVan";
  };
 
  module FaSignInAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSignInAlt";
  };
 
  module FaSignLanguage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSignLanguage";
  };
 
  module FaSignOutAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSignOutAlt";
  };
 
  module FaSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSign";
  };
 
  module FaSignal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSignal";
  };
 
  module FaSignature = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSignature";
  };
 
  module FaSimCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSimCard";
  };
 
  module FaSitemap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSitemap";
  };
 
  module FaSkating = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkating";
  };
 
  module FaSkiingNordic = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkiingNordic";
  };
 
  module FaSkiing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkiing";
  };
 
  module FaSkullCrossbones = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkullCrossbones";
  };
 
  module FaSkull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSkull";
  };
 
  module FaSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSlash";
  };
 
  module FaSleigh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSleigh";
  };
 
  module FaSlidersH = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSlidersH";
  };
 
  module FaSmileBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmileBeam";
  };
 
  module FaSmileWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmileWink";
  };
 
  module FaSmile = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmile";
  };
 
  module FaSmog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmog";
  };
 
  module FaSmokingBan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmokingBan";
  };
 
  module FaSmoking = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSmoking";
  };
 
  module FaSms = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSms";
  };
 
  module FaSnowboarding = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnowboarding";
  };
 
  module FaSnowflake = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnowflake";
  };
 
  module FaSnowman = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnowman";
  };
 
  module FaSnowplow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSnowplow";
  };
 
  module FaSocks = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSocks";
  };
 
  module FaSolarPanel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSolarPanel";
  };
 
  module FaSortAlphaDownAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAlphaDownAlt";
  };
 
  module FaSortAlphaDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAlphaDown";
  };
 
  module FaSortAlphaUpAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAlphaUpAlt";
  };
 
  module FaSortAlphaUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAlphaUp";
  };
 
  module FaSortAmountDownAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAmountDownAlt";
  };
 
  module FaSortAmountDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAmountDown";
  };
 
  module FaSortAmountUpAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAmountUpAlt";
  };
 
  module FaSortAmountUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortAmountUp";
  };
 
  module FaSortDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortDown";
  };
 
  module FaSortNumericDownAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortNumericDownAlt";
  };
 
  module FaSortNumericDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortNumericDown";
  };
 
  module FaSortNumericUpAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortNumericUpAlt";
  };
 
  module FaSortNumericUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortNumericUp";
  };
 
  module FaSortUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSortUp";
  };
 
  module FaSort = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSort";
  };
 
  module FaSpa = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpa";
  };
 
  module FaSpaceShuttle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpaceShuttle";
  };
 
  module FaSpellCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpellCheck";
  };
 
  module FaSpider = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpider";
  };
 
  module FaSpinner = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSpinner";
  };
 
  module FaSplotch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSplotch";
  };
 
  module FaSprayCan = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSprayCan";
  };
 
  module FaSquareFull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSquareFull";
  };
 
  module FaSquareRootAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSquareRootAlt";
  };
 
  module FaSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSquare";
  };
 
  module FaStamp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStamp";
  };
 
  module FaStarAndCrescent = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStarAndCrescent";
  };
 
  module FaStarHalfAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStarHalfAlt";
  };
 
  module FaStarHalf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStarHalf";
  };
 
  module FaStarOfDavid = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStarOfDavid";
  };
 
  module FaStarOfLife = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStarOfLife";
  };
 
  module FaStar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStar";
  };
 
  module FaStepBackward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStepBackward";
  };
 
  module FaStepForward = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStepForward";
  };
 
  module FaStethoscope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStethoscope";
  };
 
  module FaStickyNote = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStickyNote";
  };
 
  module FaStopCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStopCircle";
  };
 
  module FaStop = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStop";
  };
 
  module FaStopwatch = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStopwatch";
  };
 
  module FaStoreAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStoreAlt";
  };
 
  module FaStore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStore";
  };
 
  module FaStream = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStream";
  };
 
  module FaStreetView = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStreetView";
  };
 
  module FaStrikethrough = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStrikethrough";
  };
 
  module FaStroopwafel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaStroopwafel";
  };
 
  module FaSubscript = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSubscript";
  };
 
  module FaSubway = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSubway";
  };
 
  module FaSuitcaseRolling = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSuitcaseRolling";
  };
 
  module FaSuitcase = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSuitcase";
  };
 
  module FaSun = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSun";
  };
 
  module FaSuperscript = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSuperscript";
  };
 
  module FaSurprise = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSurprise";
  };
 
  module FaSwatchbook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSwatchbook";
  };
 
  module FaSwimmer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSwimmer";
  };
 
  module FaSwimmingPool = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSwimmingPool";
  };
 
  module FaSynagogue = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSynagogue";
  };
 
  module FaSyncAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSyncAlt";
  };
 
  module FaSync = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSync";
  };
 
  module FaSyringe = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaSyringe";
  };
 
  module FaTableTennis = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTableTennis";
  };
 
  module FaTable = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTable";
  };
 
  module FaTabletAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTabletAlt";
  };
 
  module FaTablet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTablet";
  };
 
  module FaTablets = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTablets";
  };
 
  module FaTachometerAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTachometerAlt";
  };
 
  module FaTag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTag";
  };
 
  module FaTags = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTags";
  };
 
  module FaTape = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTape";
  };
 
  module FaTasks = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTasks";
  };
 
  module FaTaxi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTaxi";
  };
 
  module FaTeethOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTeethOpen";
  };
 
  module FaTeeth = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTeeth";
  };
 
  module FaTemperatureHigh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTemperatureHigh";
  };
 
  module FaTemperatureLow = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTemperatureLow";
  };
 
  module FaTenge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTenge";
  };
 
  module FaTerminal = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTerminal";
  };
 
  module FaTextHeight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTextHeight";
  };
 
  module FaTextWidth = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTextWidth";
  };
 
  module FaThLarge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThLarge";
  };
 
  module FaThList = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThList";
  };
 
  module FaTh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTh";
  };
 
  module FaTheaterMasks = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTheaterMasks";
  };
 
  module FaThermometerEmpty = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometerEmpty";
  };
 
  module FaThermometerFull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometerFull";
  };
 
  module FaThermometerHalf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometerHalf";
  };
 
  module FaThermometerQuarter = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometerQuarter";
  };
 
  module FaThermometerThreeQuarters = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometerThreeQuarters";
  };
 
  module FaThermometer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThermometer";
  };
 
  module FaThumbsDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThumbsDown";
  };
 
  module FaThumbsUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThumbsUp";
  };
 
  module FaThumbtack = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaThumbtack";
  };
 
  module FaTicketAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTicketAlt";
  };
 
  module FaTimesCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTimesCircle";
  };
 
  module FaTimes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTimes";
  };
 
  module FaTintSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTintSlash";
  };
 
  module FaTint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTint";
  };
 
  module FaTired = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTired";
  };
 
  module FaToggleOff = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToggleOff";
  };
 
  module FaToggleOn = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToggleOn";
  };
 
  module FaToiletPaper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToiletPaper";
  };
 
  module FaToilet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToilet";
  };
 
  module FaToolbox = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToolbox";
  };
 
  module FaTools = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTools";
  };
 
  module FaTooth = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTooth";
  };
 
  module FaTorah = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTorah";
  };
 
  module FaToriiGate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaToriiGate";
  };
 
  module FaTractor = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTractor";
  };
 
  module FaTrademark = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrademark";
  };
 
  module FaTrafficLight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrafficLight";
  };
 
  module FaTrailer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrailer";
  };
 
  module FaTrain = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrain";
  };
 
  module FaTram = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTram";
  };
 
  module FaTransgenderAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTransgenderAlt";
  };
 
  module FaTransgender = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTransgender";
  };
 
  module FaTrashAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrashAlt";
  };
 
  module FaTrashRestoreAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrashRestoreAlt";
  };
 
  module FaTrashRestore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrashRestore";
  };
 
  module FaTrash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrash";
  };
 
  module FaTree = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTree";
  };
 
  module FaTrophy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTrophy";
  };
 
  module FaTruckLoading = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTruckLoading";
  };
 
  module FaTruckMonster = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTruckMonster";
  };
 
  module FaTruckMoving = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTruckMoving";
  };
 
  module FaTruckPickup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTruckPickup";
  };
 
  module FaTruck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTruck";
  };
 
  module FaTshirt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTshirt";
  };
 
  module FaTty = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTty";
  };
 
  module FaTv = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaTv";
  };
 
  module FaUmbrellaBeach = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUmbrellaBeach";
  };
 
  module FaUmbrella = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUmbrella";
  };
 
  module FaUnderline = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUnderline";
  };
 
  module FaUndoAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUndoAlt";
  };
 
  module FaUndo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUndo";
  };
 
  module FaUniversalAccess = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUniversalAccess";
  };
 
  module FaUniversity = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUniversity";
  };
 
  module FaUnlink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUnlink";
  };
 
  module FaUnlockAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUnlockAlt";
  };
 
  module FaUnlock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUnlock";
  };
 
  module FaUpload = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUpload";
  };
 
  module FaUserAltSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserAltSlash";
  };
 
  module FaUserAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserAlt";
  };
 
  module FaUserAstronaut = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserAstronaut";
  };
 
  module FaUserCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserCheck";
  };
 
  module FaUserCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserCircle";
  };
 
  module FaUserClock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserClock";
  };
 
  module FaUserCog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserCog";
  };
 
  module FaUserEdit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserEdit";
  };
 
  module FaUserFriends = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserFriends";
  };
 
  module FaUserGraduate = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserGraduate";
  };
 
  module FaUserInjured = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserInjured";
  };
 
  module FaUserLock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserLock";
  };
 
  module FaUserMd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserMd";
  };
 
  module FaUserMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserMinus";
  };
 
  module FaUserNinja = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserNinja";
  };
 
  module FaUserNurse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserNurse";
  };
 
  module FaUserPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserPlus";
  };
 
  module FaUserSecret = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserSecret";
  };
 
  module FaUserShield = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserShield";
  };
 
  module FaUserSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserSlash";
  };
 
  module FaUserTag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserTag";
  };
 
  module FaUserTie = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserTie";
  };
 
  module FaUserTimes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUserTimes";
  };
 
  module FaUser = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUser";
  };
 
  module FaUsersCog = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUsersCog";
  };
 
  module FaUsers = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUsers";
  };
 
  module FaUtensilSpoon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUtensilSpoon";
  };
 
  module FaUtensils = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaUtensils";
  };
 
  module FaVectorSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVectorSquare";
  };
 
  module FaVenusDouble = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVenusDouble";
  };
 
  module FaVenusMars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVenusMars";
  };
 
  module FaVenus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVenus";
  };
 
  module FaVial = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVial";
  };
 
  module FaVials = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVials";
  };
 
  module FaVideoSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVideoSlash";
  };
 
  module FaVideo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVideo";
  };
 
  module FaVihara = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVihara";
  };
 
  module FaVoicemail = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVoicemail";
  };
 
  module FaVolleyballBall = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVolleyballBall";
  };
 
  module FaVolumeDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVolumeDown";
  };
 
  module FaVolumeMute = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVolumeMute";
  };
 
  module FaVolumeOff = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVolumeOff";
  };
 
  module FaVolumeUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVolumeUp";
  };
 
  module FaVoteYea = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVoteYea";
  };
 
  module FaVrCardboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaVrCardboard";
  };
 
  module FaWalking = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWalking";
  };
 
  module FaWallet = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWallet";
  };
 
  module FaWarehouse = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWarehouse";
  };
 
  module FaWater = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWater";
  };
 
  module FaWaveSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWaveSquare";
  };
 
  module FaWeightHanging = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWeightHanging";
  };
 
  module FaWeight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWeight";
  };
 
  module FaWheelchair = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWheelchair";
  };
 
  module FaWifi = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWifi";
  };
 
  module FaWind = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWind";
  };
 
  module FaWindowClose = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWindowClose";
  };
 
  module FaWindowMaximize = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWindowMaximize";
  };
 
  module FaWindowMinimize = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWindowMinimize";
  };
 
  module FaWindowRestore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWindowRestore";
  };
 
  module FaWineBottle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWineBottle";
  };
 
  module FaWineGlassAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWineGlassAlt";
  };
 
  module FaWineGlass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWineGlass";
  };
 
  module FaWonSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWonSign";
  };
 
  module FaWrench = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaWrench";
  };
 
  module FaXRay = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaXRay";
  };
 
  module FaYenSign = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYenSign";
  };
 
  module FaYinYang = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaYinYang";
  };
 
  module FaRegAddressBook = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegAddressBook";
  };
 
  module FaRegAddressCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegAddressCard";
  };
 
  module FaRegAngry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegAngry";
  };
 
  module FaRegArrowAltCircleDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegArrowAltCircleDown";
  };
 
  module FaRegArrowAltCircleLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegArrowAltCircleLeft";
  };
 
  module FaRegArrowAltCircleRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegArrowAltCircleRight";
  };
 
  module FaRegArrowAltCircleUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegArrowAltCircleUp";
  };
 
  module FaRegBellSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegBellSlash";
  };
 
  module FaRegBell = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegBell";
  };
 
  module FaRegBookmark = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegBookmark";
  };
 
  module FaRegBuilding = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegBuilding";
  };
 
  module FaRegCalendarAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendarAlt";
  };
 
  module FaRegCalendarCheck = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendarCheck";
  };
 
  module FaRegCalendarMinus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendarMinus";
  };
 
  module FaRegCalendarPlus = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendarPlus";
  };
 
  module FaRegCalendarTimes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendarTimes";
  };
 
  module FaRegCalendar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCalendar";
  };
 
  module FaRegCaretSquareDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCaretSquareDown";
  };
 
  module FaRegCaretSquareLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCaretSquareLeft";
  };
 
  module FaRegCaretSquareRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCaretSquareRight";
  };
 
  module FaRegCaretSquareUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCaretSquareUp";
  };
 
  module FaRegChartBar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegChartBar";
  };
 
  module FaRegCheckCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCheckCircle";
  };
 
  module FaRegCheckSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCheckSquare";
  };
 
  module FaRegCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCircle";
  };
 
  module FaRegClipboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegClipboard";
  };
 
  module FaRegClock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegClock";
  };
 
  module FaRegClone = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegClone";
  };
 
  module FaRegClosedCaptioning = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegClosedCaptioning";
  };
 
  module FaRegCommentAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCommentAlt";
  };
 
  module FaRegCommentDots = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCommentDots";
  };
 
  module FaRegComment = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegComment";
  };
 
  module FaRegComments = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegComments";
  };
 
  module FaRegCompass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCompass";
  };
 
  module FaRegCopy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCopy";
  };
 
  module FaRegCopyright = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCopyright";
  };
 
  module FaRegCreditCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegCreditCard";
  };
 
  module FaRegDizzy = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegDizzy";
  };
 
  module FaRegDotCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegDotCircle";
  };
 
  module FaRegEdit = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegEdit";
  };
 
  module FaRegEnvelopeOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegEnvelopeOpen";
  };
 
  module FaRegEnvelope = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegEnvelope";
  };
 
  module FaRegEyeSlash = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegEyeSlash";
  };
 
  module FaRegEye = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegEye";
  };
 
  module FaRegFileAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileAlt";
  };
 
  module FaRegFileArchive = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileArchive";
  };
 
  module FaRegFileAudio = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileAudio";
  };
 
  module FaRegFileCode = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileCode";
  };
 
  module FaRegFileExcel = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileExcel";
  };
 
  module FaRegFileImage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileImage";
  };
 
  module FaRegFilePdf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFilePdf";
  };
 
  module FaRegFilePowerpoint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFilePowerpoint";
  };
 
  module FaRegFileVideo = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileVideo";
  };
 
  module FaRegFileWord = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFileWord";
  };
 
  module FaRegFile = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFile";
  };
 
  module FaRegFlag = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFlag";
  };
 
  module FaRegFlushed = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFlushed";
  };
 
  module FaRegFolderOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFolderOpen";
  };
 
  module FaRegFolder = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFolder";
  };
 
  module FaRegFontAwesomeLogoFull = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFontAwesomeLogoFull";
  };
 
  module FaRegFrownOpen = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFrownOpen";
  };
 
  module FaRegFrown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFrown";
  };
 
  module FaRegFutbol = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegFutbol";
  };
 
  module FaRegGem = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGem";
  };
 
  module FaRegGrimace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrimace";
  };
 
  module FaRegGrinAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinAlt";
  };
 
  module FaRegGrinBeamSweat = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinBeamSweat";
  };
 
  module FaRegGrinBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinBeam";
  };
 
  module FaRegGrinHearts = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinHearts";
  };
 
  module FaRegGrinSquintTears = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinSquintTears";
  };
 
  module FaRegGrinSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinSquint";
  };
 
  module FaRegGrinStars = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinStars";
  };
 
  module FaRegGrinTears = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinTears";
  };
 
  module FaRegGrinTongueSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinTongueSquint";
  };
 
  module FaRegGrinTongueWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinTongueWink";
  };
 
  module FaRegGrinTongue = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinTongue";
  };
 
  module FaRegGrinWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrinWink";
  };
 
  module FaRegGrin = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegGrin";
  };
 
  module FaRegHandLizard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandLizard";
  };
 
  module FaRegHandPaper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPaper";
  };
 
  module FaRegHandPeace = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPeace";
  };
 
  module FaRegHandPointDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPointDown";
  };
 
  module FaRegHandPointLeft = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPointLeft";
  };
 
  module FaRegHandPointRight = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPointRight";
  };
 
  module FaRegHandPointUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPointUp";
  };
 
  module FaRegHandPointer = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandPointer";
  };
 
  module FaRegHandRock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandRock";
  };
 
  module FaRegHandScissors = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandScissors";
  };
 
  module FaRegHandSpock = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandSpock";
  };
 
  module FaRegHandshake = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHandshake";
  };
 
  module FaRegHdd = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHdd";
  };
 
  module FaRegHeart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHeart";
  };
 
  module FaRegHospital = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHospital";
  };
 
  module FaRegHourglass = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegHourglass";
  };
 
  module FaRegIdBadge = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegIdBadge";
  };
 
  module FaRegIdCard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegIdCard";
  };
 
  module FaRegImage = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegImage";
  };
 
  module FaRegImages = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegImages";
  };
 
  module FaRegKeyboard = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegKeyboard";
  };
 
  module FaRegKissBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegKissBeam";
  };
 
  module FaRegKissWinkHeart = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegKissWinkHeart";
  };
 
  module FaRegKiss = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegKiss";
  };
 
  module FaRegLaughBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLaughBeam";
  };
 
  module FaRegLaughSquint = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLaughSquint";
  };
 
  module FaRegLaughWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLaughWink";
  };
 
  module FaRegLaugh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLaugh";
  };
 
  module FaRegLemon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLemon";
  };
 
  module FaRegLifeRing = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLifeRing";
  };
 
  module FaRegLightbulb = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegLightbulb";
  };
 
  module FaRegListAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegListAlt";
  };
 
  module FaRegMap = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMap";
  };
 
  module FaRegMehBlank = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMehBlank";
  };
 
  module FaRegMehRollingEyes = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMehRollingEyes";
  };
 
  module FaRegMeh = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMeh";
  };
 
  module FaRegMinusSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMinusSquare";
  };
 
  module FaRegMoneyBillAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMoneyBillAlt";
  };
 
  module FaRegMoon = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegMoon";
  };
 
  module FaRegNewspaper = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegNewspaper";
  };
 
  module FaRegObjectGroup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegObjectGroup";
  };
 
  module FaRegObjectUngroup = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegObjectUngroup";
  };
 
  module FaRegPaperPlane = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegPaperPlane";
  };
 
  module FaRegPauseCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegPauseCircle";
  };
 
  module FaRegPlayCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegPlayCircle";
  };
 
  module FaRegPlusSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegPlusSquare";
  };
 
  module FaRegQuestionCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegQuestionCircle";
  };
 
  module FaRegRegistered = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegRegistered";
  };
 
  module FaRegSadCry = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSadCry";
  };
 
  module FaRegSadTear = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSadTear";
  };
 
  module FaRegSave = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSave";
  };
 
  module FaRegShareSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegShareSquare";
  };
 
  module FaRegSmileBeam = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSmileBeam";
  };
 
  module FaRegSmileWink = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSmileWink";
  };
 
  module FaRegSmile = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSmile";
  };
 
  module FaRegSnowflake = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSnowflake";
  };
 
  module FaRegSquare = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSquare";
  };
 
  module FaRegStarHalf = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegStarHalf";
  };
 
  module FaRegStar = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegStar";
  };
 
  module FaRegStickyNote = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegStickyNote";
  };
 
  module FaRegStopCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegStopCircle";
  };
 
  module FaRegSun = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSun";
  };
 
  module FaRegSurprise = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegSurprise";
  };
 
  module FaRegThumbsDown = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegThumbsDown";
  };
 
  module FaRegThumbsUp = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegThumbsUp";
  };
 
  module FaRegTimesCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegTimesCircle";
  };
 
  module FaRegTired = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegTired";
  };
 
  module FaRegTrashAlt = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegTrashAlt";
  };
 
  module FaRegUserCircle = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegUserCircle";
  };
 
  module FaRegUser = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegUser";
  };
 
  module FaRegWindowClose = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegWindowClose";
  };
 
  module FaRegWindowMaximize = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegWindowMaximize";
  };
 
  module FaRegWindowMinimize = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegWindowMinimize";
  };
 
  module FaRegWindowRestore = {
    @module("react-icons/fa") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FaRegWindowRestore";
  };
