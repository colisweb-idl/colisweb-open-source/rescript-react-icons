
  module RiAncientGateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAncientGateLine";
  };
 
  module RiAncientPavilionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAncientPavilionLine";
  };
 
  module RiBankLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankLine";
  };
 
  module RiBuilding2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding2Line";
  };
 
  module RiBuilding3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding3Line";
  };
 
  module RiBuilding4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding4Line";
  };
 
  module RiBuildingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuildingLine";
  };
 
  module RiCommunityLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCommunityLine";
  };
 
  module RiGovernmentLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGovernmentLine";
  };
 
  module RiHome2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome2Line";
  };
 
  module RiHome3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome3Line";
  };
 
  module RiHome4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome4Line";
  };
 
  module RiHome5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome5Line";
  };
 
  module RiHome6Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome6Line";
  };
 
  module RiHome7Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome7Line";
  };
 
  module RiHome8Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome8Line";
  };
 
  module RiHomeGearLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeGearLine";
  };
 
  module RiHomeHeartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeHeartLine";
  };
 
  module RiHomeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeLine";
  };
 
  module RiHomeSmile2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeSmile2Line";
  };
 
  module RiHomeSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeSmileLine";
  };
 
  module RiHomeWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeWifiLine";
  };
 
  module RiHospitalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHospitalLine";
  };
 
  module RiHotelLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotelLine";
  };
 
  module RiStore2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStore2Line";
  };
 
  module RiStore3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStore3Line";
  };
 
  module RiStoreLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStoreLine";
  };
 
  module RiAdvertisementLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAdvertisementLine";
  };
 
  module RiArchiveDrawerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArchiveDrawerLine";
  };
 
  module RiArchiveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArchiveLine";
  };
 
  module RiAtLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAtLine";
  };
 
  module RiAttachmentLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAttachmentLine";
  };
 
  module RiAwardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAwardLine";
  };
 
  module RiBarChart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChart2Line";
  };
 
  module RiBarChartBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartBoxLine";
  };
 
  module RiBarChartGroupedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartGroupedLine";
  };
 
  module RiBarChartHorizontalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartHorizontalLine";
  };
 
  module RiBarChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartLine";
  };
 
  module RiBookmark2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmark2Line";
  };
 
  module RiBookmark3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmark3Line";
  };
 
  module RiBookmarkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmarkLine";
  };
 
  module RiBriefcase2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase2Line";
  };
 
  module RiBriefcase3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase3Line";
  };
 
  module RiBriefcase4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase4Line";
  };
 
  module RiBriefcase5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase5Line";
  };
 
  module RiBriefcaseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcaseLine";
  };
 
  module RiBubbleChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBubbleChartLine";
  };
 
  module RiCalculatorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalculatorLine";
  };
 
  module RiCalendar2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendar2Line";
  };
 
  module RiCalendarCheckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarCheckLine";
  };
 
  module RiCalendarEventLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarEventLine";
  };
 
  module RiCalendarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarLine";
  };
 
  module RiCalendarTodoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarTodoLine";
  };
 
  module RiCloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudLine";
  };
 
  module RiCloudOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudOffLine";
  };
 
  module RiCopyleftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopyleftLine";
  };
 
  module RiCopyrightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopyrightLine";
  };
 
  module RiCreativeCommonsByLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsByLine";
  };
 
  module RiCreativeCommonsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsLine";
  };
 
  module RiCreativeCommonsNcLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsNcLine";
  };
 
  module RiCreativeCommonsNdLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsNdLine";
  };
 
  module RiCreativeCommonsSaLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsSaLine";
  };
 
  module RiCreativeCommonsZeroLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsZeroLine";
  };
 
  module RiCustomerService2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCustomerService2Line";
  };
 
  module RiCustomerServiceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCustomerServiceLine";
  };
 
  module RiDonutChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDonutChartLine";
  };
 
  module RiFlag2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlag2Line";
  };
 
  module RiFlagLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlagLine";
  };
 
  module RiGlobalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGlobalLine";
  };
 
  module RiHonourLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHonourLine";
  };
 
  module RiInboxArchiveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxArchiveLine";
  };
 
  module RiInboxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxLine";
  };
 
  module RiInboxUnarchiveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxUnarchiveLine";
  };
 
  module RiLineChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLineChartLine";
  };
 
  module RiLinksLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinksLine";
  };
 
  module RiMailAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailAddLine";
  };
 
  module RiMailCheckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailCheckLine";
  };
 
  module RiMailCloseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailCloseLine";
  };
 
  module RiMailDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailDownloadLine";
  };
 
  module RiMailForbidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailForbidLine";
  };
 
  module RiMailLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailLine";
  };
 
  module RiMailLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailLockLine";
  };
 
  module RiMailOpenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailOpenLine";
  };
 
  module RiMailSendLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailSendLine";
  };
 
  module RiMailSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailSettingsLine";
  };
 
  module RiMailStarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailStarLine";
  };
 
  module RiMailUnreadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailUnreadLine";
  };
 
  module RiMailVolumeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailVolumeLine";
  };
 
  module RiMedal2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedal2Line";
  };
 
  module RiMedalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedalLine";
  };
 
  module RiPieChart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChart2Line";
  };
 
  module RiPieChartBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChartBoxLine";
  };
 
  module RiPieChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChartLine";
  };
 
  module RiPrinterCloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPrinterCloudLine";
  };
 
  module RiPrinterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPrinterLine";
  };
 
  module RiProfileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProfileLine";
  };
 
  module RiProjector2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProjector2Line";
  };
 
  module RiProjectorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProjectorLine";
  };
 
  module RiRecordMailLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecordMailLine";
  };
 
  module RiRegisteredLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRegisteredLine";
  };
 
  module RiReplyAllLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReplyAllLine";
  };
 
  module RiReplyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReplyLine";
  };
 
  module RiSendPlane2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSendPlane2Line";
  };
 
  module RiSendPlaneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSendPlaneLine";
  };
 
  module RiServiceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiServiceLine";
  };
 
  module RiSlideshow2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow2Line";
  };
 
  module RiSlideshow3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow3Line";
  };
 
  module RiSlideshow4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow4Line";
  };
 
  module RiSlideshowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshowLine";
  };
 
  module RiStackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackLine";
  };
 
  module RiTrademarkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrademarkLine";
  };
 
  module RiWindow2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindow2Line";
  };
 
  module RiWindowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindowLine";
  };
 
  module RiChat1Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat1Line";
  };
 
  module RiChat2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat2Line";
  };
 
  module RiChat3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat3Line";
  };
 
  module RiChat4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat4Line";
  };
 
  module RiChatCheckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatCheckLine";
  };
 
  module RiChatDeleteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatDeleteLine";
  };
 
  module RiChatDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatDownloadLine";
  };
 
  module RiChatFollowUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatFollowUpLine";
  };
 
  module RiChatForwardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatForwardLine";
  };
 
  module RiChatHeartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatHeartLine";
  };
 
  module RiChatHistoryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatHistoryLine";
  };
 
  module RiChatNewLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatNewLine";
  };
 
  module RiChatOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatOffLine";
  };
 
  module RiChatPollLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatPollLine";
  };
 
  module RiChatPrivateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatPrivateLine";
  };
 
  module RiChatQuoteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatQuoteLine";
  };
 
  module RiChatSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSettingsLine";
  };
 
  module RiChatSmile2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmile2Line";
  };
 
  module RiChatSmile3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmile3Line";
  };
 
  module RiChatSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmileLine";
  };
 
  module RiChatUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatUploadLine";
  };
 
  module RiChatVoiceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatVoiceLine";
  };
 
  module RiDiscussLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscussLine";
  };
 
  module RiFeedbackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFeedbackLine";
  };
 
  module RiMessage2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessage2Line";
  };
 
  module RiMessage3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessage3Line";
  };
 
  module RiMessageLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessageLine";
  };
 
  module RiQuestionAnswerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionAnswerLine";
  };
 
  module RiQuestionnaireLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionnaireLine";
  };
 
  module RiVideoChatLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoChatLine";
  };
 
  module RiAnticlockwise2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnticlockwise2Line";
  };
 
  module RiAnticlockwiseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnticlockwiseLine";
  };
 
  module RiArtboard2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArtboard2Line";
  };
 
  module RiArtboardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArtboardLine";
  };
 
  module RiBallPenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBallPenLine";
  };
 
  module RiBlurOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBlurOffLine";
  };
 
  module RiBrush2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush2Line";
  };
 
  module RiBrush3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush3Line";
  };
 
  module RiBrush4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush4Line";
  };
 
  module RiBrushLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrushLine";
  };
 
  module RiClockwise2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClockwise2Line";
  };
 
  module RiClockwiseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClockwiseLine";
  };
 
  module RiCollageLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCollageLine";
  };
 
  module RiCompasses2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompasses2Line";
  };
 
  module RiCompassesLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassesLine";
  };
 
  module RiContrast2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrast2Line";
  };
 
  module RiContrastDrop2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastDrop2Line";
  };
 
  module RiContrastDropLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastDropLine";
  };
 
  module RiContrastLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastLine";
  };
 
  module RiCrop2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCrop2Line";
  };
 
  module RiCropLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCropLine";
  };
 
  module RiDragDropLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragDropLine";
  };
 
  module RiDragMove2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragMove2Line";
  };
 
  module RiDragMoveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragMoveLine";
  };
 
  module RiDropLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDropLine";
  };
 
  module RiEdit2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEdit2Line";
  };
 
  module RiEditBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditBoxLine";
  };
 
  module RiEditCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditCircleLine";
  };
 
  module RiEditLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditLine";
  };
 
  module RiEraserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEraserLine";
  };
 
  module RiFocus2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocus2Line";
  };
 
  module RiFocus3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocus3Line";
  };
 
  module RiFocusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocusLine";
  };
 
  module RiGridLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGridLine";
  };
 
  module RiHammerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHammerLine";
  };
 
  module RiInkBottleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInkBottleLine";
  };
 
  module RiInputMethodLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInputMethodLine";
  };
 
  module RiLayout2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout2Line";
  };
 
  module RiLayout3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout3Line";
  };
 
  module RiLayout4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout4Line";
  };
 
  module RiLayout5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout5Line";
  };
 
  module RiLayout6Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout6Line";
  };
 
  module RiLayoutBottom2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutBottom2Line";
  };
 
  module RiLayoutBottomLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutBottomLine";
  };
 
  module RiLayoutColumnLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutColumnLine";
  };
 
  module RiLayoutGridLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutGridLine";
  };
 
  module RiLayoutLeft2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutLeft2Line";
  };
 
  module RiLayoutLeftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutLeftLine";
  };
 
  module RiLayoutLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutLine";
  };
 
  module RiLayoutMasonryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutMasonryLine";
  };
 
  module RiLayoutRight2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRight2Line";
  };
 
  module RiLayoutRightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRightLine";
  };
 
  module RiLayoutRowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRowLine";
  };
 
  module RiLayoutTop2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutTop2Line";
  };
 
  module RiLayoutTopLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutTopLine";
  };
 
  module RiMagicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMagicLine";
  };
 
  module RiMarkPenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkPenLine";
  };
 
  module RiMarkupLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkupLine";
  };
 
  module RiPaintBrushLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaintBrushLine";
  };
 
  module RiPaintLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaintLine";
  };
 
  module RiPaletteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaletteLine";
  };
 
  module RiPantoneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPantoneLine";
  };
 
  module RiPenNibLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPenNibLine";
  };
 
  module RiPencilLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilLine";
  };
 
  module RiPencilRuler2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilRuler2Line";
  };
 
  module RiPencilRulerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilRulerLine";
  };
 
  module RiQuillPenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuillPenLine";
  };
 
  module RiRuler2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRuler2Line";
  };
 
  module RiRulerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRulerLine";
  };
 
  module RiScissors2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissors2Line";
  };
 
  module RiScissorsCutLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissorsCutLine";
  };
 
  module RiScissorsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissorsLine";
  };
 
  module RiScreenshot2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScreenshot2Line";
  };
 
  module RiScreenshotLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScreenshotLine";
  };
 
  module RiShape2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShape2Line";
  };
 
  module RiShapeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShapeLine";
  };
 
  module RiSipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSipLine";
  };
 
  module RiSliceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSliceLine";
  };
 
  module RiTBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTBoxLine";
  };
 
  module RiTableAltLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTableAltLine";
  };
 
  module RiTableLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTableLine";
  };
 
  module RiToolsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiToolsLine";
  };
 
  module RiBracesLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBracesLine";
  };
 
  module RiBracketsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBracketsLine";
  };
 
  module RiBug2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBug2Line";
  };
 
  module RiBugLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBugLine";
  };
 
  module RiCodeBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeBoxLine";
  };
 
  module RiCodeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeLine";
  };
 
  module RiCodeSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeSLine";
  };
 
  module RiCodeSSlashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeSSlashLine";
  };
 
  module RiCommandLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCommandLine";
  };
 
  module RiCss3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCss3Line";
  };
 
  module RiCursorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCursorLine";
  };
 
  module RiGitBranchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitBranchLine";
  };
 
  module RiGitCommitLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitCommitLine";
  };
 
  module RiGitMergeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitMergeLine";
  };
 
  module RiGitPullRequestLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitPullRequestLine";
  };
 
  module RiGitRepositoryCommitsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryCommitsLine";
  };
 
  module RiGitRepositoryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryLine";
  };
 
  module RiGitRepositoryPrivateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryPrivateLine";
  };
 
  module RiHtml5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHtml5Line";
  };
 
  module RiParenthesesLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParenthesesLine";
  };
 
  module RiTerminalBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalBoxLine";
  };
 
  module RiTerminalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalLine";
  };
 
  module RiTerminalWindowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalWindowLine";
  };
 
  module RiAirplayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAirplayLine";
  };
 
  module RiBarcodeBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarcodeBoxLine";
  };
 
  module RiBarcodeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarcodeLine";
  };
 
  module RiBaseStationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBaseStationLine";
  };
 
  module RiBattery2ChargeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBattery2ChargeLine";
  };
 
  module RiBattery2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBattery2Line";
  };
 
  module RiBatteryChargeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryChargeLine";
  };
 
  module RiBatteryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryLine";
  };
 
  module RiBatteryLowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryLowLine";
  };
 
  module RiBatterySaverLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatterySaverLine";
  };
 
  module RiBatteryShareLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryShareLine";
  };
 
  module RiBluetoothConnectLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBluetoothConnectLine";
  };
 
  module RiBluetoothLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBluetoothLine";
  };
 
  module RiCastLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCastLine";
  };
 
  module RiCellphoneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCellphoneLine";
  };
 
  module RiComputerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiComputerLine";
  };
 
  module RiCpuLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCpuLine";
  };
 
  module RiDashboard2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboard2Line";
  };
 
  module RiDashboard3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboard3Line";
  };
 
  module RiDatabase2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDatabase2Line";
  };
 
  module RiDatabaseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDatabaseLine";
  };
 
  module RiDeviceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeviceLine";
  };
 
  module RiDeviceRecoverLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeviceRecoverLine";
  };
 
  module RiDualSim1Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDualSim1Line";
  };
 
  module RiDualSim2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDualSim2Line";
  };
 
  module RiFingerprint2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFingerprint2Line";
  };
 
  module RiFingerprintLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFingerprintLine";
  };
 
  module RiGamepadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGamepadLine";
  };
 
  module RiGpsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGpsLine";
  };
 
  module RiGradienterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGradienterLine";
  };
 
  module RiHardDrive2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHardDrive2Line";
  };
 
  module RiHardDriveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHardDriveLine";
  };
 
  module RiHotspotLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotspotLine";
  };
 
  module RiInstallLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInstallLine";
  };
 
  module RiKeyboardBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyboardBoxLine";
  };
 
  module RiKeyboardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyboardLine";
  };
 
  module RiMacLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMacLine";
  };
 
  module RiMacbookLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMacbookLine";
  };
 
  module RiMouseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMouseLine";
  };
 
  module RiPhoneFindLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneFindLine";
  };
 
  module RiPhoneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneLine";
  };
 
  module RiPhoneLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneLockLine";
  };
 
  module RiQrCodeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrCodeLine";
  };
 
  module RiQrScan2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrScan2Line";
  };
 
  module RiQrScanLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrScanLine";
  };
 
  module RiRadarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadarLine";
  };
 
  module RiRemoteControl2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemoteControl2Line";
  };
 
  module RiRemoteControlLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemoteControlLine";
  };
 
  module RiRestartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestartLine";
  };
 
  module RiRotateLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRotateLockLine";
  };
 
  module RiRouterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRouterLine";
  };
 
  module RiRssLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRssLine";
  };
 
  module RiSave2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSave2Line";
  };
 
  module RiSave3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSave3Line";
  };
 
  module RiSaveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSaveLine";
  };
 
  module RiScan2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScan2Line";
  };
 
  module RiScanLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScanLine";
  };
 
  module RiSdCardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSdCardLine";
  };
 
  module RiSdCardMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSdCardMiniLine";
  };
 
  module RiSensorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSensorLine";
  };
 
  module RiServerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiServerLine";
  };
 
  module RiShutDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShutDownLine";
  };
 
  module RiSignalWifi1Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi1Line";
  };
 
  module RiSignalWifi2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi2Line";
  };
 
  module RiSignalWifi3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi3Line";
  };
 
  module RiSignalWifiErrorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiErrorLine";
  };
 
  module RiSignalWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiLine";
  };
 
  module RiSignalWifiOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiOffLine";
  };
 
  module RiSimCard2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSimCard2Line";
  };
 
  module RiSimCardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSimCardLine";
  };
 
  module RiSmartphoneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSmartphoneLine";
  };
 
  module RiTabletLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTabletLine";
  };
 
  module RiTv2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTv2Line";
  };
 
  module RiTvLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTvLine";
  };
 
  module RiUDiskLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUDiskLine";
  };
 
  module RiUninstallLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUninstallLine";
  };
 
  module RiUsbLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUsbLine";
  };
 
  module RiWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWifiLine";
  };
 
  module RiWifiOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWifiOffLine";
  };
 
  module RiWirelessChargingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWirelessChargingLine";
  };
 
  module RiArticleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArticleLine";
  };
 
  module RiBillLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBillLine";
  };
 
  module RiBook2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBook2Line";
  };
 
  module RiBook3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBook3Line";
  };
 
  module RiBookLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookLine";
  };
 
  module RiBookMarkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookMarkLine";
  };
 
  module RiBookOpenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookOpenLine";
  };
 
  module RiBookReadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookReadLine";
  };
 
  module RiBookletLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookletLine";
  };
 
  module RiClipboardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClipboardLine";
  };
 
  module RiContactsBook2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBook2Line";
  };
 
  module RiContactsBookLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBookLine";
  };
 
  module RiContactsBookUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBookUploadLine";
  };
 
  module RiDraftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDraftLine";
  };
 
  module RiFile2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile2Line";
  };
 
  module RiFile3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile3Line";
  };
 
  module RiFile4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile4Line";
  };
 
  module RiFileAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileAddLine";
  };
 
  module RiFileChart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileChart2Line";
  };
 
  module RiFileChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileChartLine";
  };
 
  module RiFileCloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCloudLine";
  };
 
  module RiFileCodeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCodeLine";
  };
 
  module RiFileCopy2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCopy2Line";
  };
 
  module RiFileCopyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCopyLine";
  };
 
  module RiFileDamageLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileDamageLine";
  };
 
  module RiFileDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileDownloadLine";
  };
 
  module RiFileEditLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileEditLine";
  };
 
  module RiFileExcel2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileExcel2Line";
  };
 
  module RiFileExcelLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileExcelLine";
  };
 
  module RiFileForbidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileForbidLine";
  };
 
  module RiFileGifLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileGifLine";
  };
 
  module RiFileHistoryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileHistoryLine";
  };
 
  module RiFileHwpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileHwpLine";
  };
 
  module RiFileInfoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileInfoLine";
  };
 
  module RiFileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileLine";
  };
 
  module RiFileList2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileList2Line";
  };
 
  module RiFileList3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileList3Line";
  };
 
  module RiFileListLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileListLine";
  };
 
  module RiFileLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileLockLine";
  };
 
  module RiFileMarkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileMarkLine";
  };
 
  module RiFileMusicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileMusicLine";
  };
 
  module RiFilePaper2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePaper2Line";
  };
 
  module RiFilePaperLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePaperLine";
  };
 
  module RiFilePdfLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePdfLine";
  };
 
  module RiFilePpt2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePpt2Line";
  };
 
  module RiFilePptLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePptLine";
  };
 
  module RiFileReduceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileReduceLine";
  };
 
  module RiFileSearchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileSearchLine";
  };
 
  module RiFileSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileSettingsLine";
  };
 
  module RiFileShield2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShield2Line";
  };
 
  module RiFileShieldLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShieldLine";
  };
 
  module RiFileShredLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShredLine";
  };
 
  module RiFileTextLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileTextLine";
  };
 
  module RiFileTransferLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileTransferLine";
  };
 
  module RiFileUnknowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUnknowLine";
  };
 
  module RiFileUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUploadLine";
  };
 
  module RiFileUserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUserLine";
  };
 
  module RiFileWarningLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWarningLine";
  };
 
  module RiFileWord2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWord2Line";
  };
 
  module RiFileWordLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWordLine";
  };
 
  module RiFileZipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileZipLine";
  };
 
  module RiFolder2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder2Line";
  };
 
  module RiFolder3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder3Line";
  };
 
  module RiFolder4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder4Line";
  };
 
  module RiFolder5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder5Line";
  };
 
  module RiFolderAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderAddLine";
  };
 
  module RiFolderChart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderChart2Line";
  };
 
  module RiFolderChartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderChartLine";
  };
 
  module RiFolderDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderDownloadLine";
  };
 
  module RiFolderForbidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderForbidLine";
  };
 
  module RiFolderHistoryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderHistoryLine";
  };
 
  module RiFolderInfoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderInfoLine";
  };
 
  module RiFolderKeyholeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderKeyholeLine";
  };
 
  module RiFolderLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderLine";
  };
 
  module RiFolderLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderLockLine";
  };
 
  module RiFolderMusicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderMusicLine";
  };
 
  module RiFolderOpenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderOpenLine";
  };
 
  module RiFolderReceivedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderReceivedLine";
  };
 
  module RiFolderReduceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderReduceLine";
  };
 
  module RiFolderSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderSettingsLine";
  };
 
  module RiFolderSharedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderSharedLine";
  };
 
  module RiFolderShield2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderShield2Line";
  };
 
  module RiFolderShieldLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderShieldLine";
  };
 
  module RiFolderTransferLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderTransferLine";
  };
 
  module RiFolderUnknowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUnknowLine";
  };
 
  module RiFolderUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUploadLine";
  };
 
  module RiFolderUserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUserLine";
  };
 
  module RiFolderWarningLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderWarningLine";
  };
 
  module RiFolderZipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderZipLine";
  };
 
  module RiFoldersLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFoldersLine";
  };
 
  module RiKeynoteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeynoteLine";
  };
 
  module RiMarkdownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkdownLine";
  };
 
  module RiNewspaperLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNewspaperLine";
  };
 
  module RiNumbersLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNumbersLine";
  };
 
  module RiPagesLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPagesLine";
  };
 
  module RiStickyNote2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStickyNote2Line";
  };
 
  module RiStickyNoteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStickyNoteLine";
  };
 
  module RiSurveyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurveyLine";
  };
 
  module RiTaskLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaskLine";
  };
 
  module RiTodoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTodoLine";
  };
 
  module Ri24HoursLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Ri24HoursLine";
  };
 
  module RiAuctionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAuctionLine";
  };
 
  module RiBankCard2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankCard2Line";
  };
 
  module RiBankCardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankCardLine";
  };
 
  module RiBitCoinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBitCoinLine";
  };
 
  module RiCoinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoinLine";
  };
 
  module RiCoinsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoinsLine";
  };
 
  module RiCopperCoinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopperCoinLine";
  };
 
  module RiCopperDiamondLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopperDiamondLine";
  };
 
  module RiCoupon2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon2Line";
  };
 
  module RiCoupon3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon3Line";
  };
 
  module RiCoupon4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon4Line";
  };
 
  module RiCoupon5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon5Line";
  };
 
  module RiCouponLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCouponLine";
  };
 
  module RiCurrencyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCurrencyLine";
  };
 
  module RiExchangeBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeBoxLine";
  };
 
  module RiExchangeCnyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeCnyLine";
  };
 
  module RiExchangeDollarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeDollarLine";
  };
 
  module RiExchangeFundsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeFundsLine";
  };
 
  module RiExchangeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeLine";
  };
 
  module RiFundsBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFundsBoxLine";
  };
 
  module RiFundsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFundsLine";
  };
 
  module RiGift2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGift2Line";
  };
 
  module RiGiftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGiftLine";
  };
 
  module RiHandCoinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandCoinLine";
  };
 
  module RiHandHeartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandHeartLine";
  };
 
  module RiIncreaseDecreaseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIncreaseDecreaseLine";
  };
 
  module RiMoneyCnyBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyCnyBoxLine";
  };
 
  module RiMoneyCnyCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyCnyCircleLine";
  };
 
  module RiMoneyDollarBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyDollarBoxLine";
  };
 
  module RiMoneyDollarCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyDollarCircleLine";
  };
 
  module RiMoneyEuroBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyEuroBoxLine";
  };
 
  module RiMoneyEuroCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyEuroCircleLine";
  };
 
  module RiMoneyPoundBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyPoundBoxLine";
  };
 
  module RiMoneyPoundCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyPoundCircleLine";
  };
 
  module RiPercentLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPercentLine";
  };
 
  module RiPriceTag2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTag2Line";
  };
 
  module RiPriceTag3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTag3Line";
  };
 
  module RiPriceTagLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTagLine";
  };
 
  module RiRedPacketLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRedPacketLine";
  };
 
  module RiRefund2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefund2Line";
  };
 
  module RiRefundLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefundLine";
  };
 
  module RiSafe2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafe2Line";
  };
 
  module RiSafeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafeLine";
  };
 
  module RiSecurePaymentLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSecurePaymentLine";
  };
 
  module RiShoppingBag2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBag2Line";
  };
 
  module RiShoppingBag3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBag3Line";
  };
 
  module RiShoppingBagLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBagLine";
  };
 
  module RiShoppingBasket2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBasket2Line";
  };
 
  module RiShoppingBasketLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBasketLine";
  };
 
  module RiShoppingCart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingCart2Line";
  };
 
  module RiShoppingCartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingCartLine";
  };
 
  module RiStockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStockLine";
  };
 
  module RiSwapBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwapBoxLine";
  };
 
  module RiSwapLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwapLine";
  };
 
  module RiTicket2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTicket2Line";
  };
 
  module RiTicketLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTicketLine";
  };
 
  module RiTrophyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrophyLine";
  };
 
  module RiVipCrown2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipCrown2Line";
  };
 
  module RiVipCrownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipCrownLine";
  };
 
  module RiVipDiamondLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipDiamondLine";
  };
 
  module RiVipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipLine";
  };
 
  module RiWallet2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWallet2Line";
  };
 
  module RiWallet3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWallet3Line";
  };
 
  module RiWalletLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWalletLine";
  };
 
  module RiWaterFlashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWaterFlashLine";
  };
 
  module RiCapsuleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCapsuleLine";
  };
 
  module RiDislikeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDislikeLine";
  };
 
  module RiDossierLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDossierLine";
  };
 
  module RiEmpathizeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmpathizeLine";
  };
 
  module RiFirstAidKitLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFirstAidKitLine";
  };
 
  module RiFlaskLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlaskLine";
  };
 
  module RiHandSanitizerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandSanitizerLine";
  };
 
  module RiHealthBookLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHealthBookLine";
  };
 
  module RiHeart2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeart2Line";
  };
 
  module RiHeart3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeart3Line";
  };
 
  module RiHeartAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartAddLine";
  };
 
  module RiHeartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartLine";
  };
 
  module RiHeartPulseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartPulseLine";
  };
 
  module RiHeartsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartsLine";
  };
 
  module RiInfraredThermometerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInfraredThermometerLine";
  };
 
  module RiLungsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLungsLine";
  };
 
  module RiMedicineBottleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedicineBottleLine";
  };
 
  module RiMentalHealthLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMentalHealthLine";
  };
 
  module RiMicroscopeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicroscopeLine";
  };
 
  module RiNurseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNurseLine";
  };
 
  module RiPsychotherapyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPsychotherapyLine";
  };
 
  module RiPulseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPulseLine";
  };
 
  module RiRestTimeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestTimeLine";
  };
 
  module RiStethoscopeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStethoscopeLine";
  };
 
  module RiSurgicalMaskLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurgicalMaskLine";
  };
 
  module RiSyringeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSyringeLine";
  };
 
  module RiTestTubeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTestTubeLine";
  };
 
  module RiThermometerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThermometerLine";
  };
 
  module RiVirusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVirusLine";
  };
 
  module RiZzzLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZzzLine";
  };
 
  module RiAlipayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlipayLine";
  };
 
  module RiAmazonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAmazonLine";
  };
 
  module RiAndroidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAndroidLine";
  };
 
  module RiAngularjsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAngularjsLine";
  };
 
  module RiAppStoreLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppStoreLine";
  };
 
  module RiAppleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppleLine";
  };
 
  module RiBaiduLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBaiduLine";
  };
 
  module RiBehanceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBehanceLine";
  };
 
  module RiBilibiliLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBilibiliLine";
  };
 
  module RiCentosLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCentosLine";
  };
 
  module RiChromeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChromeLine";
  };
 
  module RiCodepenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodepenLine";
  };
 
  module RiCoreosLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoreosLine";
  };
 
  module RiDingdingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDingdingLine";
  };
 
  module RiDiscordLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscordLine";
  };
 
  module RiDisqusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDisqusLine";
  };
 
  module RiDoubanLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoubanLine";
  };
 
  module RiDribbbleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDribbbleLine";
  };
 
  module RiDriveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDriveLine";
  };
 
  module RiDropboxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDropboxLine";
  };
 
  module RiEdgeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEdgeLine";
  };
 
  module RiEvernoteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEvernoteLine";
  };
 
  module RiFacebookBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookBoxLine";
  };
 
  module RiFacebookCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookCircleLine";
  };
 
  module RiFacebookLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookLine";
  };
 
  module RiFinderLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFinderLine";
  };
 
  module RiFirefoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFirefoxLine";
  };
 
  module RiFlutterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlutterLine";
  };
 
  module RiGatsbyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGatsbyLine";
  };
 
  module RiGithubLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGithubLine";
  };
 
  module RiGitlabLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitlabLine";
  };
 
  module RiGoogleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGoogleLine";
  };
 
  module RiGooglePlayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGooglePlayLine";
  };
 
  module RiHonorOfKingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHonorOfKingsLine";
  };
 
  module RiIeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIeLine";
  };
 
  module RiInstagramLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInstagramLine";
  };
 
  module RiInvisionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInvisionLine";
  };
 
  module RiKakaoTalkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKakaoTalkLine";
  };
 
  module RiLineLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLineLine";
  };
 
  module RiLinkedinBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinkedinBoxLine";
  };
 
  module RiLinkedinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinkedinLine";
  };
 
  module RiMastercardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMastercardLine";
  };
 
  module RiMastodonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMastodonLine";
  };
 
  module RiMediumLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMediumLine";
  };
 
  module RiMessengerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessengerLine";
  };
 
  module RiMicrosoftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicrosoftLine";
  };
 
  module RiMiniProgramLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMiniProgramLine";
  };
 
  module RiNeteaseCloudMusicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNeteaseCloudMusicLine";
  };
 
  module RiNetflixLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNetflixLine";
  };
 
  module RiNpmjsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNpmjsLine";
  };
 
  module RiOpenSourceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOpenSourceLine";
  };
 
  module RiOperaLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOperaLine";
  };
 
  module RiPatreonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPatreonLine";
  };
 
  module RiPaypalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaypalLine";
  };
 
  module RiPinterestLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPinterestLine";
  };
 
  module RiPixelfedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPixelfedLine";
  };
 
  module RiPlaystationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlaystationLine";
  };
 
  module RiProductHuntLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProductHuntLine";
  };
 
  module RiQqLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQqLine";
  };
 
  module RiReactjsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReactjsLine";
  };
 
  module RiRedditLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRedditLine";
  };
 
  module RiRemixiconLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemixiconLine";
  };
 
  module RiSafariLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafariLine";
  };
 
  module RiSkypeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkypeLine";
  };
 
  module RiSlackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlackLine";
  };
 
  module RiSnapchatLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSnapchatLine";
  };
 
  module RiSoundcloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSoundcloudLine";
  };
 
  module RiSpectrumLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpectrumLine";
  };
 
  module RiSpotifyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpotifyLine";
  };
 
  module RiStackOverflowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackOverflowLine";
  };
 
  module RiStackshareLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackshareLine";
  };
 
  module RiSteamLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteamLine";
  };
 
  module RiSwitchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwitchLine";
  };
 
  module RiTaobaoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaobaoLine";
  };
 
  module RiTelegramLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTelegramLine";
  };
 
  module RiTrelloLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrelloLine";
  };
 
  module RiTumblrLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTumblrLine";
  };
 
  module RiTwitchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTwitchLine";
  };
 
  module RiTwitterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTwitterLine";
  };
 
  module RiUbuntuLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUbuntuLine";
  };
 
  module RiUnsplashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUnsplashLine";
  };
 
  module RiVimeoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVimeoLine";
  };
 
  module RiVisaLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVisaLine";
  };
 
  module RiVuejsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVuejsLine";
  };
 
  module RiWechat2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechat2Line";
  };
 
  module RiWechatLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechatLine";
  };
 
  module RiWechatPayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechatPayLine";
  };
 
  module RiWeiboLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWeiboLine";
  };
 
  module RiWhatsappLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWhatsappLine";
  };
 
  module RiWindowsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindowsLine";
  };
 
  module RiXboxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiXboxLine";
  };
 
  module RiXingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiXingLine";
  };
 
  module RiYoutubeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiYoutubeLine";
  };
 
  module RiZcoolLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZcoolLine";
  };
 
  module RiZhihuLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZhihuLine";
  };
 
  module RiAnchorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnchorLine";
  };
 
  module RiBarricadeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarricadeLine";
  };
 
  module RiBikeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBikeLine";
  };
 
  module RiBus2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBus2Line";
  };
 
  module RiBusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBusLine";
  };
 
  module RiBusWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBusWifiLine";
  };
 
  module RiCarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCarLine";
  };
 
  module RiCarWashingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCarWashingLine";
  };
 
  module RiCaravanLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCaravanLine";
  };
 
  module RiChargingPile2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChargingPile2Line";
  };
 
  module RiChargingPileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChargingPileLine";
  };
 
  module RiChinaRailwayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChinaRailwayLine";
  };
 
  module RiCompass2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass2Line";
  };
 
  module RiCompass3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass3Line";
  };
 
  module RiCompass4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass4Line";
  };
 
  module RiCompassDiscoverLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassDiscoverLine";
  };
 
  module RiCompassLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassLine";
  };
 
  module RiCupLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCupLine";
  };
 
  module RiDirectionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDirectionLine";
  };
 
  module RiEBike2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEBike2Line";
  };
 
  module RiEBikeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEBikeLine";
  };
 
  module RiEarthLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEarthLine";
  };
 
  module RiFlightLandLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlightLandLine";
  };
 
  module RiFlightTakeoffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlightTakeoffLine";
  };
 
  module RiFootprintLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFootprintLine";
  };
 
  module RiGasStationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGasStationLine";
  };
 
  module RiGlobeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGlobeLine";
  };
 
  module RiGobletLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGobletLine";
  };
 
  module RiGuideLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGuideLine";
  };
 
  module RiHotelBedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotelBedLine";
  };
 
  module RiLifebuoyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLifebuoyLine";
  };
 
  module RiLuggageCartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLuggageCartLine";
  };
 
  module RiLuggageDepositLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLuggageDepositLine";
  };
 
  module RiMap2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMap2Line";
  };
 
  module RiMapLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapLine";
  };
 
  module RiMapPin2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin2Line";
  };
 
  module RiMapPin3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin3Line";
  };
 
  module RiMapPin4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin4Line";
  };
 
  module RiMapPin5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin5Line";
  };
 
  module RiMapPinAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinAddLine";
  };
 
  module RiMapPinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinLine";
  };
 
  module RiMapPinRangeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinRangeLine";
  };
 
  module RiMapPinTimeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinTimeLine";
  };
 
  module RiMapPinUserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinUserLine";
  };
 
  module RiMotorbikeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMotorbikeLine";
  };
 
  module RiNavigationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNavigationLine";
  };
 
  module RiOilLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOilLine";
  };
 
  module RiParkingBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParkingBoxLine";
  };
 
  module RiParkingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParkingLine";
  };
 
  module RiPassportLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPassportLine";
  };
 
  module RiPinDistanceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPinDistanceLine";
  };
 
  module RiPlaneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlaneLine";
  };
 
  module RiPoliceCarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPoliceCarLine";
  };
 
  module RiPushpin2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPushpin2Line";
  };
 
  module RiPushpinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPushpinLine";
  };
 
  module RiRestaurant2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestaurant2Line";
  };
 
  module RiRestaurantLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestaurantLine";
  };
 
  module RiRidingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRidingLine";
  };
 
  module RiRoadMapLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRoadMapLine";
  };
 
  module RiRoadsterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRoadsterLine";
  };
 
  module RiRocket2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRocket2Line";
  };
 
  module RiRocketLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRocketLine";
  };
 
  module RiRouteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRouteLine";
  };
 
  module RiRunLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRunLine";
  };
 
  module RiSailboatLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSailboatLine";
  };
 
  module RiShip2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShip2Line";
  };
 
  module RiShipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShipLine";
  };
 
  module RiSignalTowerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalTowerLine";
  };
 
  module RiSpaceShipLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpaceShipLine";
  };
 
  module RiSteering2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteering2Line";
  };
 
  module RiSteeringLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteeringLine";
  };
 
  module RiSubwayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubwayLine";
  };
 
  module RiSubwayWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubwayWifiLine";
  };
 
  module RiSuitcase2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcase2Line";
  };
 
  module RiSuitcase3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcase3Line";
  };
 
  module RiSuitcaseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcaseLine";
  };
 
  module RiTakeawayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTakeawayLine";
  };
 
  module RiTaxiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaxiLine";
  };
 
  module RiTaxiWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaxiWifiLine";
  };
 
  module RiTrafficLightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrafficLightLine";
  };
 
  module RiTrainLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrainLine";
  };
 
  module RiTrainWifiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrainWifiLine";
  };
 
  module RiTreasureMapLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTreasureMapLine";
  };
 
  module RiTruckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTruckLine";
  };
 
  module RiWalkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWalkLine";
  };
 
  module Ri4KLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Ri4KLine";
  };
 
  module RiAlbumLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlbumLine";
  };
 
  module RiAspectRatioLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAspectRatioLine";
  };
 
  module RiBroadcastLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBroadcastLine";
  };
 
  module RiCamera2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCamera2Line";
  };
 
  module RiCamera3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCamera3Line";
  };
 
  module RiCameraLensLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraLensLine";
  };
 
  module RiCameraLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraLine";
  };
 
  module RiCameraOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraOffLine";
  };
 
  module RiCameraSwitchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraSwitchLine";
  };
 
  module RiClapperboardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClapperboardLine";
  };
 
  module RiClosedCaptioningLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClosedCaptioningLine";
  };
 
  module RiDiscLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscLine";
  };
 
  module RiDvLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDvLine";
  };
 
  module RiDvdLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDvdLine";
  };
 
  module RiEjectLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEjectLine";
  };
 
  module RiEqualizerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEqualizerLine";
  };
 
  module RiFilmLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilmLine";
  };
 
  module RiFullscreenExitLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFullscreenExitLine";
  };
 
  module RiFullscreenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFullscreenLine";
  };
 
  module RiGalleryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGalleryLine";
  };
 
  module RiGalleryUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGalleryUploadLine";
  };
 
  module RiHdLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHdLine";
  };
 
  module RiHeadphoneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeadphoneLine";
  };
 
  module RiHqLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHqLine";
  };
 
  module RiImage2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImage2Line";
  };
 
  module RiImageAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageAddLine";
  };
 
  module RiImageEditLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageEditLine";
  };
 
  module RiImageLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageLine";
  };
 
  module RiLandscapeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLandscapeLine";
  };
 
  module RiLiveLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLiveLine";
  };
 
  module RiMic2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMic2Line";
  };
 
  module RiMicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicLine";
  };
 
  module RiMicOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicOffLine";
  };
 
  module RiMovie2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMovie2Line";
  };
 
  module RiMovieLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMovieLine";
  };
 
  module RiMusic2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMusic2Line";
  };
 
  module RiMusicLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMusicLine";
  };
 
  module RiMvLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMvLine";
  };
 
  module RiNotification2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification2Line";
  };
 
  module RiNotification3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification3Line";
  };
 
  module RiNotification4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification4Line";
  };
 
  module RiNotificationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationLine";
  };
 
  module RiNotificationOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationOffLine";
  };
 
  module RiOrderPlayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOrderPlayLine";
  };
 
  module RiPauseCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseCircleLine";
  };
 
  module RiPauseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseLine";
  };
 
  module RiPauseMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseMiniLine";
  };
 
  module RiPhoneCameraLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneCameraLine";
  };
 
  module RiPictureInPicture2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPicture2Line";
  };
 
  module RiPictureInPictureExitLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPictureExitLine";
  };
 
  module RiPictureInPictureLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPictureLine";
  };
 
  module RiPlayCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayCircleLine";
  };
 
  module RiPlayLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayLine";
  };
 
  module RiPlayList2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayList2Line";
  };
 
  module RiPlayListAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayListAddLine";
  };
 
  module RiPlayListLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayListLine";
  };
 
  module RiPlayMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayMiniLine";
  };
 
  module RiPolaroid2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPolaroid2Line";
  };
 
  module RiPolaroidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPolaroidLine";
  };
 
  module RiRadio2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadio2Line";
  };
 
  module RiRadioLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadioLine";
  };
 
  module RiRecordCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecordCircleLine";
  };
 
  module RiRepeat2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeat2Line";
  };
 
  module RiRepeatLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeatLine";
  };
 
  module RiRepeatOneLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeatOneLine";
  };
 
  module RiRewindLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRewindLine";
  };
 
  module RiRewindMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRewindMiniLine";
  };
 
  module RiRhythmLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRhythmLine";
  };
 
  module RiShuffleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShuffleLine";
  };
 
  module RiSkipBackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipBackLine";
  };
 
  module RiSkipBackMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipBackMiniLine";
  };
 
  module RiSkipForwardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipForwardLine";
  };
 
  module RiSkipForwardMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipForwardMiniLine";
  };
 
  module RiSoundModuleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSoundModuleLine";
  };
 
  module RiSpeaker2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeaker2Line";
  };
 
  module RiSpeaker3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeaker3Line";
  };
 
  module RiSpeakerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeakerLine";
  };
 
  module RiSpeedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeedLine";
  };
 
  module RiSpeedMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeedMiniLine";
  };
 
  module RiStopCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopCircleLine";
  };
 
  module RiStopLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopLine";
  };
 
  module RiStopMiniLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopMiniLine";
  };
 
  module RiSurroundSoundLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurroundSoundLine";
  };
 
  module RiTapeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTapeLine";
  };
 
  module RiVideoAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoAddLine";
  };
 
  module RiVideoDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoDownloadLine";
  };
 
  module RiVideoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoLine";
  };
 
  module RiVideoUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoUploadLine";
  };
 
  module RiVidicon2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVidicon2Line";
  };
 
  module RiVidiconLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVidiconLine";
  };
 
  module RiVoiceprintLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVoiceprintLine";
  };
 
  module RiVolumeDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeDownLine";
  };
 
  module RiVolumeMuteLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeMuteLine";
  };
 
  module RiVolumeOffVibrateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeOffVibrateLine";
  };
 
  module RiVolumeUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeUpLine";
  };
 
  module RiVolumeVibrateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeVibrateLine";
  };
 
  module RiWebcamLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWebcamLine";
  };
 
  module RiBasketballLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBasketballLine";
  };
 
  module RiBellLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBellLine";
  };
 
  module RiBilliardsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBilliardsLine";
  };
 
  module RiBoxingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBoxingLine";
  };
 
  module RiCactusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCactusLine";
  };
 
  module RiCake2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCake2Line";
  };
 
  module RiCake3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCake3Line";
  };
 
  module RiCakeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCakeLine";
  };
 
  module RiCharacterRecognitionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCharacterRecognitionLine";
  };
 
  module RiDoorClosedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorClosedLine";
  };
 
  module RiDoorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorLine";
  };
 
  module RiDoorLockBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorLockBoxLine";
  };
 
  module RiDoorLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorLockLine";
  };
 
  module RiDoorOpenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorOpenLine";
  };
 
  module RiFootballLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFootballLine";
  };
 
  module RiFridgeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFridgeLine";
  };
 
  module RiGameLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGameLine";
  };
 
  module RiHandbagLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandbagLine";
  };
 
  module RiKey2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKey2Line";
  };
 
  module RiKeyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyLine";
  };
 
  module RiKnifeBloodLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKnifeBloodLine";
  };
 
  module RiKnifeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKnifeLine";
  };
 
  module RiLeafLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLeafLine";
  };
 
  module RiLightbulbFlashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLightbulbFlashLine";
  };
 
  module RiLightbulbLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLightbulbLine";
  };
 
  module RiOutlet2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOutlet2Line";
  };
 
  module RiOutletLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOutletLine";
  };
 
  module RiPingPongLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPingPongLine";
  };
 
  module RiPlantLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlantLine";
  };
 
  module RiPlug2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlug2Line";
  };
 
  module RiPlugLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlugLine";
  };
 
  module RiRecycleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecycleLine";
  };
 
  module RiReservedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReservedLine";
  };
 
  module RiScales2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScales2Line";
  };
 
  module RiScales3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScales3Line";
  };
 
  module RiScalesLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScalesLine";
  };
 
  module RiSeedlingLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSeedlingLine";
  };
 
  module RiShirtLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShirtLine";
  };
 
  module RiSwordLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwordLine";
  };
 
  module RiTShirt2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirt2Line";
  };
 
  module RiTShirtAirLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirtAirLine";
  };
 
  module RiTShirtLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirtLine";
  };
 
  module RiUmbrellaLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUmbrellaLine";
  };
 
  module RiVoiceRecognitionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVoiceRecognitionLine";
  };
 
  module RiWheelchairLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWheelchairLine";
  };
 
  module RiAddBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddBoxLine";
  };
 
  module RiAddCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddCircleLine";
  };
 
  module RiAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddLine";
  };
 
  module RiAlarmLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlarmLine";
  };
 
  module RiAlarmWarningLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlarmWarningLine";
  };
 
  module RiAlertLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlertLine";
  };
 
  module RiApps2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiApps2Line";
  };
 
  module RiAppsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppsLine";
  };
 
  module RiArrowDownCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownCircleLine";
  };
 
  module RiArrowDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownLine";
  };
 
  module RiArrowDownSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownSLine";
  };
 
  module RiArrowDropDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropDownLine";
  };
 
  module RiArrowDropLeftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropLeftLine";
  };
 
  module RiArrowDropRightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropRightLine";
  };
 
  module RiArrowDropUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropUpLine";
  };
 
  module RiArrowGoBackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowGoBackLine";
  };
 
  module RiArrowGoForwardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowGoForwardLine";
  };
 
  module RiArrowLeftCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftCircleLine";
  };
 
  module RiArrowLeftDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftDownLine";
  };
 
  module RiArrowLeftLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftLine";
  };
 
  module RiArrowLeftRightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftRightLine";
  };
 
  module RiArrowLeftSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftSLine";
  };
 
  module RiArrowLeftUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftUpLine";
  };
 
  module RiArrowRightCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightCircleLine";
  };
 
  module RiArrowRightDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightDownLine";
  };
 
  module RiArrowRightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightLine";
  };
 
  module RiArrowRightSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightSLine";
  };
 
  module RiArrowRightUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightUpLine";
  };
 
  module RiArrowUpCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpCircleLine";
  };
 
  module RiArrowUpDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpDownLine";
  };
 
  module RiArrowUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpLine";
  };
 
  module RiArrowUpSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpSLine";
  };
 
  module RiCheckDoubleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckDoubleLine";
  };
 
  module RiCheckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckLine";
  };
 
  module RiCheckboxBlankCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxBlankCircleLine";
  };
 
  module RiCheckboxBlankLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxBlankLine";
  };
 
  module RiCheckboxCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxCircleLine";
  };
 
  module RiCheckboxIndeterminateLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxIndeterminateLine";
  };
 
  module RiCheckboxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxLine";
  };
 
  module RiCheckboxMultipleBlankLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxMultipleBlankLine";
  };
 
  module RiCheckboxMultipleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxMultipleLine";
  };
 
  module RiCloseCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloseCircleLine";
  };
 
  module RiCloseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloseLine";
  };
 
  module RiDashboardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboardLine";
  };
 
  module RiDeleteBack2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBack2Line";
  };
 
  module RiDeleteBackLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBackLine";
  };
 
  module RiDeleteBin2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin2Line";
  };
 
  module RiDeleteBin3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin3Line";
  };
 
  module RiDeleteBin4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin4Line";
  };
 
  module RiDeleteBin5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin5Line";
  };
 
  module RiDeleteBin6Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin6Line";
  };
 
  module RiDeleteBin7Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin7Line";
  };
 
  module RiDeleteBinLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBinLine";
  };
 
  module RiDivideLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDivideLine";
  };
 
  module RiDownload2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownload2Line";
  };
 
  module RiDownloadCloud2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadCloud2Line";
  };
 
  module RiDownloadCloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadCloudLine";
  };
 
  module RiDownloadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadLine";
  };
 
  module RiErrorWarningLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiErrorWarningLine";
  };
 
  module RiExternalLinkLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExternalLinkLine";
  };
 
  module RiEye2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEye2Line";
  };
 
  module RiEyeCloseLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeCloseLine";
  };
 
  module RiEyeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeLine";
  };
 
  module RiEyeOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeOffLine";
  };
 
  module RiFilter2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilter2Line";
  };
 
  module RiFilter3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilter3Line";
  };
 
  module RiFilterLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilterLine";
  };
 
  module RiFilterOffLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilterOffLine";
  };
 
  module RiFindReplaceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFindReplaceLine";
  };
 
  module RiForbid2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiForbid2Line";
  };
 
  module RiForbidLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiForbidLine";
  };
 
  module RiFunctionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFunctionLine";
  };
 
  module RiHistoryLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHistoryLine";
  };
 
  module RiIndeterminateCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIndeterminateCircleLine";
  };
 
  module RiInformationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInformationLine";
  };
 
  module RiListSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiListSettingsLine";
  };
 
  module RiLoader2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader2Line";
  };
 
  module RiLoader3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader3Line";
  };
 
  module RiLoader4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader4Line";
  };
 
  module RiLoader5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader5Line";
  };
 
  module RiLoaderLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoaderLine";
  };
 
  module RiLock2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLock2Line";
  };
 
  module RiLockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockLine";
  };
 
  module RiLockPasswordLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockPasswordLine";
  };
 
  module RiLockUnlockLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockUnlockLine";
  };
 
  module RiLoginBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoginBoxLine";
  };
 
  module RiLoginCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoginCircleLine";
  };
 
  module RiLogoutBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutBoxLine";
  };
 
  module RiLogoutBoxRLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutBoxRLine";
  };
 
  module RiLogoutCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutCircleLine";
  };
 
  module RiLogoutCircleRLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutCircleRLine";
  };
 
  module RiMenu2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu2Line";
  };
 
  module RiMenu3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu3Line";
  };
 
  module RiMenu4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu4Line";
  };
 
  module RiMenu5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu5Line";
  };
 
  module RiMenuAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuAddLine";
  };
 
  module RiMenuFoldLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuFoldLine";
  };
 
  module RiMenuLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuLine";
  };
 
  module RiMenuUnfoldLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuUnfoldLine";
  };
 
  module RiMore2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMore2Line";
  };
 
  module RiMoreLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoreLine";
  };
 
  module RiNotificationBadgeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationBadgeLine";
  };
 
  module RiQuestionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionLine";
  };
 
  module RiRadioButtonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadioButtonLine";
  };
 
  module RiRefreshLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefreshLine";
  };
 
  module RiSearch2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearch2Line";
  };
 
  module RiSearchEyeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearchEyeLine";
  };
 
  module RiSearchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearchLine";
  };
 
  module RiSettings2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings2Line";
  };
 
  module RiSettings3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings3Line";
  };
 
  module RiSettings4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings4Line";
  };
 
  module RiSettings5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings5Line";
  };
 
  module RiSettings6Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings6Line";
  };
 
  module RiSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettingsLine";
  };
 
  module RiShareBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareBoxLine";
  };
 
  module RiShareCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareCircleLine";
  };
 
  module RiShareForward2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForward2Line";
  };
 
  module RiShareForwardBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForwardBoxLine";
  };
 
  module RiShareForwardLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForwardLine";
  };
 
  module RiShareLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareLine";
  };
 
  module RiShieldCheckLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldCheckLine";
  };
 
  module RiShieldCrossLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldCrossLine";
  };
 
  module RiShieldFlashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldFlashLine";
  };
 
  module RiShieldKeyholeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldKeyholeLine";
  };
 
  module RiShieldLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldLine";
  };
 
  module RiShieldStarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldStarLine";
  };
 
  module RiShieldUserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldUserLine";
  };
 
  module RiSideBarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSideBarLine";
  };
 
  module RiSpam2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpam2Line";
  };
 
  module RiSpam3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpam3Line";
  };
 
  module RiSpamLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpamLine";
  };
 
  module RiStarHalfLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarHalfLine";
  };
 
  module RiStarHalfSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarHalfSLine";
  };
 
  module RiStarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarLine";
  };
 
  module RiStarSLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarSLine";
  };
 
  module RiSubtractLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubtractLine";
  };
 
  module RiThumbDownLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThumbDownLine";
  };
 
  module RiThumbUpLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThumbUpLine";
  };
 
  module RiTimeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimeLine";
  };
 
  module RiTimer2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimer2Line";
  };
 
  module RiTimerFlashLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimerFlashLine";
  };
 
  module RiTimerLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimerLine";
  };
 
  module RiToggleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiToggleLine";
  };
 
  module RiUpload2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUpload2Line";
  };
 
  module RiUploadCloud2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadCloud2Line";
  };
 
  module RiUploadCloudLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadCloudLine";
  };
 
  module RiUploadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadLine";
  };
 
  module RiZoomInLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZoomInLine";
  };
 
  module RiZoomOutLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZoomOutLine";
  };
 
  module RiAccountBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountBoxLine";
  };
 
  module RiAccountCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountCircleLine";
  };
 
  module RiAccountPinBoxLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountPinBoxLine";
  };
 
  module RiAccountPinCircleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountPinCircleLine";
  };
 
  module RiAdminLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAdminLine";
  };
 
  module RiAliensLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAliensLine";
  };
 
  module RiBearSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBearSmileLine";
  };
 
  module RiBodyScanLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBodyScanLine";
  };
 
  module RiContactsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsLine";
  };
 
  module RiCriminalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCriminalLine";
  };
 
  module RiEmotion2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotion2Line";
  };
 
  module RiEmotionHappyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionHappyLine";
  };
 
  module RiEmotionLaughLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionLaughLine";
  };
 
  module RiEmotionLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionLine";
  };
 
  module RiEmotionNormalLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionNormalLine";
  };
 
  module RiEmotionSadLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionSadLine";
  };
 
  module RiEmotionUnhappyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionUnhappyLine";
  };
 
  module RiGenderlessLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGenderlessLine";
  };
 
  module RiGhost2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhost2Line";
  };
 
  module RiGhostLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhostLine";
  };
 
  module RiGhostSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhostSmileLine";
  };
 
  module RiGroup2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGroup2Line";
  };
 
  module RiGroupLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGroupLine";
  };
 
  module RiMenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenLine";
  };
 
  module RiMickeyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMickeyLine";
  };
 
  module RiOpenArmLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOpenArmLine";
  };
 
  module RiParentLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParentLine";
  };
 
  module RiRobotLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRobotLine";
  };
 
  module RiSkull2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkull2Line";
  };
 
  module RiSkullLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkullLine";
  };
 
  module RiSpyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpyLine";
  };
 
  module RiStarSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarSmileLine";
  };
 
  module RiTeamLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTeamLine";
  };
 
  module RiTravestiLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTravestiLine";
  };
 
  module RiUser2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser2Line";
  };
 
  module RiUser3Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser3Line";
  };
 
  module RiUser4Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser4Line";
  };
 
  module RiUser5Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser5Line";
  };
 
  module RiUser6Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser6Line";
  };
 
  module RiUserAddLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserAddLine";
  };
 
  module RiUserFollowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserFollowLine";
  };
 
  module RiUserHeartLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserHeartLine";
  };
 
  module RiUserLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserLine";
  };
 
  module RiUserLocationLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserLocationLine";
  };
 
  module RiUserReceived2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserReceived2Line";
  };
 
  module RiUserReceivedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserReceivedLine";
  };
 
  module RiUserSearchLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSearchLine";
  };
 
  module RiUserSettingsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSettingsLine";
  };
 
  module RiUserShared2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserShared2Line";
  };
 
  module RiUserSharedLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSharedLine";
  };
 
  module RiUserSmileLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSmileLine";
  };
 
  module RiUserStarLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserStarLine";
  };
 
  module RiUserUnfollowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserUnfollowLine";
  };
 
  module RiUserVoiceLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserVoiceLine";
  };
 
  module RiWomenLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWomenLine";
  };
 
  module RiBlazeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBlazeLine";
  };
 
  module RiCelsiusLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCelsiusLine";
  };
 
  module RiCloudWindyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudWindyLine";
  };
 
  module RiCloudy2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudy2Line";
  };
 
  module RiCloudyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudyLine";
  };
 
  module RiDrizzleLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDrizzleLine";
  };
 
  module RiEarthquakeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEarthquakeLine";
  };
 
  module RiFahrenheitLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFahrenheitLine";
  };
 
  module RiFireLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFireLine";
  };
 
  module RiFlashlightLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlashlightLine";
  };
 
  module RiFloodLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFloodLine";
  };
 
  module RiFoggyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFoggyLine";
  };
 
  module RiHailLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHailLine";
  };
 
  module RiHaze2Line = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHaze2Line";
  };
 
  module RiHazeLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHazeLine";
  };
 
  module RiHeavyShowersLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeavyShowersLine";
  };
 
  module RiMeteorLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMeteorLine";
  };
 
  module RiMistLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMistLine";
  };
 
  module RiMoonClearLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonClearLine";
  };
 
  module RiMoonCloudyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonCloudyLine";
  };
 
  module RiMoonFoggyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonFoggyLine";
  };
 
  module RiMoonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonLine";
  };
 
  module RiRainbowLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRainbowLine";
  };
 
  module RiRainyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRainyLine";
  };
 
  module RiShowersLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShowersLine";
  };
 
  module RiSnowyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSnowyLine";
  };
 
  module RiSunCloudyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunCloudyLine";
  };
 
  module RiSunFoggyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunFoggyLine";
  };
 
  module RiSunLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunLine";
  };
 
  module RiTempColdLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTempColdLine";
  };
 
  module RiTempHotLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTempHotLine";
  };
 
  module RiThunderstormsLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThunderstormsLine";
  };
 
  module RiTornadoLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTornadoLine";
  };
 
  module RiTyphoonLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTyphoonLine";
  };
 
  module RiWindyLine = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindyLine";
  };
 
  module RiAncientGateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAncientGateFill";
  };
 
  module RiAncientPavilionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAncientPavilionFill";
  };
 
  module RiBankFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankFill";
  };
 
  module RiBuilding2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding2Fill";
  };
 
  module RiBuilding3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding3Fill";
  };
 
  module RiBuilding4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuilding4Fill";
  };
 
  module RiBuildingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBuildingFill";
  };
 
  module RiCommunityFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCommunityFill";
  };
 
  module RiGovernmentFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGovernmentFill";
  };
 
  module RiHome2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome2Fill";
  };
 
  module RiHome3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome3Fill";
  };
 
  module RiHome4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome4Fill";
  };
 
  module RiHome5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome5Fill";
  };
 
  module RiHome6Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome6Fill";
  };
 
  module RiHome7Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome7Fill";
  };
 
  module RiHome8Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHome8Fill";
  };
 
  module RiHomeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeFill";
  };
 
  module RiHomeGearFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeGearFill";
  };
 
  module RiHomeHeartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeHeartFill";
  };
 
  module RiHomeSmile2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeSmile2Fill";
  };
 
  module RiHomeSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeSmileFill";
  };
 
  module RiHomeWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHomeWifiFill";
  };
 
  module RiHospitalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHospitalFill";
  };
 
  module RiHotelFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotelFill";
  };
 
  module RiStore2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStore2Fill";
  };
 
  module RiStore3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStore3Fill";
  };
 
  module RiStoreFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStoreFill";
  };
 
  module RiAdvertisementFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAdvertisementFill";
  };
 
  module RiArchiveDrawerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArchiveDrawerFill";
  };
 
  module RiArchiveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArchiveFill";
  };
 
  module RiAtFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAtFill";
  };
 
  module RiAttachmentFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAttachmentFill";
  };
 
  module RiAwardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAwardFill";
  };
 
  module RiBarChart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChart2Fill";
  };
 
  module RiBarChartBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartBoxFill";
  };
 
  module RiBarChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartFill";
  };
 
  module RiBarChartGroupedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartGroupedFill";
  };
 
  module RiBarChartHorizontalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarChartHorizontalFill";
  };
 
  module RiBookmark2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmark2Fill";
  };
 
  module RiBookmark3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmark3Fill";
  };
 
  module RiBookmarkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookmarkFill";
  };
 
  module RiBriefcase2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase2Fill";
  };
 
  module RiBriefcase3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase3Fill";
  };
 
  module RiBriefcase4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase4Fill";
  };
 
  module RiBriefcase5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcase5Fill";
  };
 
  module RiBriefcaseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBriefcaseFill";
  };
 
  module RiBubbleChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBubbleChartFill";
  };
 
  module RiCalculatorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalculatorFill";
  };
 
  module RiCalendar2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendar2Fill";
  };
 
  module RiCalendarCheckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarCheckFill";
  };
 
  module RiCalendarEventFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarEventFill";
  };
 
  module RiCalendarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarFill";
  };
 
  module RiCalendarTodoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCalendarTodoFill";
  };
 
  module RiCloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudFill";
  };
 
  module RiCloudOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudOffFill";
  };
 
  module RiCopyleftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopyleftFill";
  };
 
  module RiCopyrightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopyrightFill";
  };
 
  module RiCreativeCommonsByFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsByFill";
  };
 
  module RiCreativeCommonsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsFill";
  };
 
  module RiCreativeCommonsNcFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsNcFill";
  };
 
  module RiCreativeCommonsNdFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsNdFill";
  };
 
  module RiCreativeCommonsSaFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsSaFill";
  };
 
  module RiCreativeCommonsZeroFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCreativeCommonsZeroFill";
  };
 
  module RiCustomerService2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCustomerService2Fill";
  };
 
  module RiCustomerServiceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCustomerServiceFill";
  };
 
  module RiDonutChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDonutChartFill";
  };
 
  module RiFlag2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlag2Fill";
  };
 
  module RiFlagFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlagFill";
  };
 
  module RiGlobalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGlobalFill";
  };
 
  module RiHonourFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHonourFill";
  };
 
  module RiInboxArchiveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxArchiveFill";
  };
 
  module RiInboxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxFill";
  };
 
  module RiInboxUnarchiveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInboxUnarchiveFill";
  };
 
  module RiLineChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLineChartFill";
  };
 
  module RiLinksFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinksFill";
  };
 
  module RiMailAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailAddFill";
  };
 
  module RiMailCheckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailCheckFill";
  };
 
  module RiMailCloseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailCloseFill";
  };
 
  module RiMailDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailDownloadFill";
  };
 
  module RiMailFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailFill";
  };
 
  module RiMailForbidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailForbidFill";
  };
 
  module RiMailLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailLockFill";
  };
 
  module RiMailOpenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailOpenFill";
  };
 
  module RiMailSendFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailSendFill";
  };
 
  module RiMailSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailSettingsFill";
  };
 
  module RiMailStarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailStarFill";
  };
 
  module RiMailUnreadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailUnreadFill";
  };
 
  module RiMailVolumeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMailVolumeFill";
  };
 
  module RiMedal2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedal2Fill";
  };
 
  module RiMedalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedalFill";
  };
 
  module RiPieChart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChart2Fill";
  };
 
  module RiPieChartBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChartBoxFill";
  };
 
  module RiPieChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPieChartFill";
  };
 
  module RiPrinterCloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPrinterCloudFill";
  };
 
  module RiPrinterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPrinterFill";
  };
 
  module RiProfileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProfileFill";
  };
 
  module RiProjector2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProjector2Fill";
  };
 
  module RiProjectorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProjectorFill";
  };
 
  module RiRecordMailFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecordMailFill";
  };
 
  module RiRegisteredFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRegisteredFill";
  };
 
  module RiReplyAllFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReplyAllFill";
  };
 
  module RiReplyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReplyFill";
  };
 
  module RiSendPlane2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSendPlane2Fill";
  };
 
  module RiSendPlaneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSendPlaneFill";
  };
 
  module RiServiceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiServiceFill";
  };
 
  module RiSlideshow2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow2Fill";
  };
 
  module RiSlideshow3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow3Fill";
  };
 
  module RiSlideshow4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshow4Fill";
  };
 
  module RiSlideshowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlideshowFill";
  };
 
  module RiStackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackFill";
  };
 
  module RiTrademarkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrademarkFill";
  };
 
  module RiWindow2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindow2Fill";
  };
 
  module RiWindowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindowFill";
  };
 
  module RiChat1Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat1Fill";
  };
 
  module RiChat2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat2Fill";
  };
 
  module RiChat3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat3Fill";
  };
 
  module RiChat4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChat4Fill";
  };
 
  module RiChatCheckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatCheckFill";
  };
 
  module RiChatDeleteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatDeleteFill";
  };
 
  module RiChatDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatDownloadFill";
  };
 
  module RiChatFollowUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatFollowUpFill";
  };
 
  module RiChatForwardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatForwardFill";
  };
 
  module RiChatHeartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatHeartFill";
  };
 
  module RiChatHistoryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatHistoryFill";
  };
 
  module RiChatNewFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatNewFill";
  };
 
  module RiChatOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatOffFill";
  };
 
  module RiChatPollFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatPollFill";
  };
 
  module RiChatPrivateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatPrivateFill";
  };
 
  module RiChatQuoteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatQuoteFill";
  };
 
  module RiChatSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSettingsFill";
  };
 
  module RiChatSmile2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmile2Fill";
  };
 
  module RiChatSmile3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmile3Fill";
  };
 
  module RiChatSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatSmileFill";
  };
 
  module RiChatUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatUploadFill";
  };
 
  module RiChatVoiceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChatVoiceFill";
  };
 
  module RiDiscussFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscussFill";
  };
 
  module RiFeedbackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFeedbackFill";
  };
 
  module RiMessage2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessage2Fill";
  };
 
  module RiMessage3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessage3Fill";
  };
 
  module RiMessageFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessageFill";
  };
 
  module RiQuestionAnswerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionAnswerFill";
  };
 
  module RiQuestionnaireFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionnaireFill";
  };
 
  module RiVideoChatFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoChatFill";
  };
 
  module RiAnticlockwise2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnticlockwise2Fill";
  };
 
  module RiAnticlockwiseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnticlockwiseFill";
  };
 
  module RiArtboard2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArtboard2Fill";
  };
 
  module RiArtboardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArtboardFill";
  };
 
  module RiBallPenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBallPenFill";
  };
 
  module RiBlurOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBlurOffFill";
  };
 
  module RiBrush2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush2Fill";
  };
 
  module RiBrush3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush3Fill";
  };
 
  module RiBrush4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrush4Fill";
  };
 
  module RiBrushFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBrushFill";
  };
 
  module RiClockwise2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClockwise2Fill";
  };
 
  module RiClockwiseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClockwiseFill";
  };
 
  module RiCollageFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCollageFill";
  };
 
  module RiCompasses2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompasses2Fill";
  };
 
  module RiCompassesFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassesFill";
  };
 
  module RiContrast2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrast2Fill";
  };
 
  module RiContrastDrop2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastDrop2Fill";
  };
 
  module RiContrastDropFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastDropFill";
  };
 
  module RiContrastFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContrastFill";
  };
 
  module RiCrop2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCrop2Fill";
  };
 
  module RiCropFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCropFill";
  };
 
  module RiDragDropFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragDropFill";
  };
 
  module RiDragMove2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragMove2Fill";
  };
 
  module RiDragMoveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDragMoveFill";
  };
 
  module RiDropFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDropFill";
  };
 
  module RiEdit2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEdit2Fill";
  };
 
  module RiEditBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditBoxFill";
  };
 
  module RiEditCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditCircleFill";
  };
 
  module RiEditFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEditFill";
  };
 
  module RiEraserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEraserFill";
  };
 
  module RiFocus2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocus2Fill";
  };
 
  module RiFocus3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocus3Fill";
  };
 
  module RiFocusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFocusFill";
  };
 
  module RiGridFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGridFill";
  };
 
  module RiHammerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHammerFill";
  };
 
  module RiInkBottleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInkBottleFill";
  };
 
  module RiInputMethodFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInputMethodFill";
  };
 
  module RiLayout2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout2Fill";
  };
 
  module RiLayout3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout3Fill";
  };
 
  module RiLayout4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout4Fill";
  };
 
  module RiLayout5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout5Fill";
  };
 
  module RiLayout6Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayout6Fill";
  };
 
  module RiLayoutBottom2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutBottom2Fill";
  };
 
  module RiLayoutBottomFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutBottomFill";
  };
 
  module RiLayoutColumnFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutColumnFill";
  };
 
  module RiLayoutFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutFill";
  };
 
  module RiLayoutGridFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutGridFill";
  };
 
  module RiLayoutLeft2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutLeft2Fill";
  };
 
  module RiLayoutLeftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutLeftFill";
  };
 
  module RiLayoutMasonryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutMasonryFill";
  };
 
  module RiLayoutRight2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRight2Fill";
  };
 
  module RiLayoutRightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRightFill";
  };
 
  module RiLayoutRowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutRowFill";
  };
 
  module RiLayoutTop2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutTop2Fill";
  };
 
  module RiLayoutTopFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLayoutTopFill";
  };
 
  module RiMagicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMagicFill";
  };
 
  module RiMarkPenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkPenFill";
  };
 
  module RiMarkupFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkupFill";
  };
 
  module RiPaintBrushFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaintBrushFill";
  };
 
  module RiPaintFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaintFill";
  };
 
  module RiPaletteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaletteFill";
  };
 
  module RiPantoneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPantoneFill";
  };
 
  module RiPenNibFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPenNibFill";
  };
 
  module RiPencilFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilFill";
  };
 
  module RiPencilRuler2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilRuler2Fill";
  };
 
  module RiPencilRulerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPencilRulerFill";
  };
 
  module RiQuillPenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuillPenFill";
  };
 
  module RiRuler2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRuler2Fill";
  };
 
  module RiRulerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRulerFill";
  };
 
  module RiScissors2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissors2Fill";
  };
 
  module RiScissorsCutFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissorsCutFill";
  };
 
  module RiScissorsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScissorsFill";
  };
 
  module RiScreenshot2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScreenshot2Fill";
  };
 
  module RiScreenshotFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScreenshotFill";
  };
 
  module RiShape2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShape2Fill";
  };
 
  module RiShapeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShapeFill";
  };
 
  module RiSipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSipFill";
  };
 
  module RiSliceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSliceFill";
  };
 
  module RiTBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTBoxFill";
  };
 
  module RiTableAltFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTableAltFill";
  };
 
  module RiTableFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTableFill";
  };
 
  module RiToolsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiToolsFill";
  };
 
  module RiBracesFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBracesFill";
  };
 
  module RiBracketsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBracketsFill";
  };
 
  module RiBug2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBug2Fill";
  };
 
  module RiBugFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBugFill";
  };
 
  module RiCodeBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeBoxFill";
  };
 
  module RiCodeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeFill";
  };
 
  module RiCodeSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeSFill";
  };
 
  module RiCodeSSlashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodeSSlashFill";
  };
 
  module RiCommandFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCommandFill";
  };
 
  module RiCss3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCss3Fill";
  };
 
  module RiCursorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCursorFill";
  };
 
  module RiGitBranchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitBranchFill";
  };
 
  module RiGitCommitFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitCommitFill";
  };
 
  module RiGitMergeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitMergeFill";
  };
 
  module RiGitPullRequestFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitPullRequestFill";
  };
 
  module RiGitRepositoryCommitsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryCommitsFill";
  };
 
  module RiGitRepositoryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryFill";
  };
 
  module RiGitRepositoryPrivateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitRepositoryPrivateFill";
  };
 
  module RiHtml5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHtml5Fill";
  };
 
  module RiParenthesesFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParenthesesFill";
  };
 
  module RiTerminalBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalBoxFill";
  };
 
  module RiTerminalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalFill";
  };
 
  module RiTerminalWindowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTerminalWindowFill";
  };
 
  module RiAirplayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAirplayFill";
  };
 
  module RiBarcodeBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarcodeBoxFill";
  };
 
  module RiBarcodeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarcodeFill";
  };
 
  module RiBaseStationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBaseStationFill";
  };
 
  module RiBattery2ChargeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBattery2ChargeFill";
  };
 
  module RiBattery2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBattery2Fill";
  };
 
  module RiBatteryChargeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryChargeFill";
  };
 
  module RiBatteryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryFill";
  };
 
  module RiBatteryLowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryLowFill";
  };
 
  module RiBatterySaverFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatterySaverFill";
  };
 
  module RiBatteryShareFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBatteryShareFill";
  };
 
  module RiBluetoothConnectFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBluetoothConnectFill";
  };
 
  module RiBluetoothFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBluetoothFill";
  };
 
  module RiCastFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCastFill";
  };
 
  module RiCellphoneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCellphoneFill";
  };
 
  module RiComputerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiComputerFill";
  };
 
  module RiCpuFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCpuFill";
  };
 
  module RiDashboard2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboard2Fill";
  };
 
  module RiDashboard3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboard3Fill";
  };
 
  module RiDatabase2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDatabase2Fill";
  };
 
  module RiDatabaseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDatabaseFill";
  };
 
  module RiDeviceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeviceFill";
  };
 
  module RiDeviceRecoverFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeviceRecoverFill";
  };
 
  module RiDualSim1Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDualSim1Fill";
  };
 
  module RiDualSim2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDualSim2Fill";
  };
 
  module RiFingerprint2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFingerprint2Fill";
  };
 
  module RiFingerprintFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFingerprintFill";
  };
 
  module RiGamepadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGamepadFill";
  };
 
  module RiGpsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGpsFill";
  };
 
  module RiGradienterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGradienterFill";
  };
 
  module RiHardDrive2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHardDrive2Fill";
  };
 
  module RiHardDriveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHardDriveFill";
  };
 
  module RiHotspotFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotspotFill";
  };
 
  module RiInstallFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInstallFill";
  };
 
  module RiKeyboardBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyboardBoxFill";
  };
 
  module RiKeyboardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyboardFill";
  };
 
  module RiMacFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMacFill";
  };
 
  module RiMacbookFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMacbookFill";
  };
 
  module RiMouseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMouseFill";
  };
 
  module RiPhoneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneFill";
  };
 
  module RiPhoneFindFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneFindFill";
  };
 
  module RiPhoneLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneLockFill";
  };
 
  module RiQrCodeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrCodeFill";
  };
 
  module RiQrScan2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrScan2Fill";
  };
 
  module RiQrScanFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQrScanFill";
  };
 
  module RiRadarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadarFill";
  };
 
  module RiRemoteControl2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemoteControl2Fill";
  };
 
  module RiRemoteControlFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemoteControlFill";
  };
 
  module RiRestartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestartFill";
  };
 
  module RiRotateLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRotateLockFill";
  };
 
  module RiRouterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRouterFill";
  };
 
  module RiRssFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRssFill";
  };
 
  module RiSave2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSave2Fill";
  };
 
  module RiSave3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSave3Fill";
  };
 
  module RiSaveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSaveFill";
  };
 
  module RiScan2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScan2Fill";
  };
 
  module RiScanFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScanFill";
  };
 
  module RiSdCardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSdCardFill";
  };
 
  module RiSdCardMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSdCardMiniFill";
  };
 
  module RiSensorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSensorFill";
  };
 
  module RiServerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiServerFill";
  };
 
  module RiShutDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShutDownFill";
  };
 
  module RiSignalWifi1Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi1Fill";
  };
 
  module RiSignalWifi2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi2Fill";
  };
 
  module RiSignalWifi3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifi3Fill";
  };
 
  module RiSignalWifiErrorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiErrorFill";
  };
 
  module RiSignalWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiFill";
  };
 
  module RiSignalWifiOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalWifiOffFill";
  };
 
  module RiSimCard2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSimCard2Fill";
  };
 
  module RiSimCardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSimCardFill";
  };
 
  module RiSmartphoneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSmartphoneFill";
  };
 
  module RiTabletFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTabletFill";
  };
 
  module RiTv2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTv2Fill";
  };
 
  module RiTvFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTvFill";
  };
 
  module RiUDiskFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUDiskFill";
  };
 
  module RiUninstallFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUninstallFill";
  };
 
  module RiUsbFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUsbFill";
  };
 
  module RiWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWifiFill";
  };
 
  module RiWifiOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWifiOffFill";
  };
 
  module RiWirelessChargingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWirelessChargingFill";
  };
 
  module RiArticleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArticleFill";
  };
 
  module RiBillFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBillFill";
  };
 
  module RiBook2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBook2Fill";
  };
 
  module RiBook3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBook3Fill";
  };
 
  module RiBookFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookFill";
  };
 
  module RiBookMarkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookMarkFill";
  };
 
  module RiBookOpenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookOpenFill";
  };
 
  module RiBookReadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookReadFill";
  };
 
  module RiBookletFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBookletFill";
  };
 
  module RiClipboardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClipboardFill";
  };
 
  module RiContactsBook2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBook2Fill";
  };
 
  module RiContactsBookFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBookFill";
  };
 
  module RiContactsBookUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsBookUploadFill";
  };
 
  module RiDraftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDraftFill";
  };
 
  module RiFile2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile2Fill";
  };
 
  module RiFile3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile3Fill";
  };
 
  module RiFile4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFile4Fill";
  };
 
  module RiFileAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileAddFill";
  };
 
  module RiFileChart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileChart2Fill";
  };
 
  module RiFileChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileChartFill";
  };
 
  module RiFileCloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCloudFill";
  };
 
  module RiFileCodeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCodeFill";
  };
 
  module RiFileCopy2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCopy2Fill";
  };
 
  module RiFileCopyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileCopyFill";
  };
 
  module RiFileDamageFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileDamageFill";
  };
 
  module RiFileDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileDownloadFill";
  };
 
  module RiFileEditFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileEditFill";
  };
 
  module RiFileExcel2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileExcel2Fill";
  };
 
  module RiFileExcelFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileExcelFill";
  };
 
  module RiFileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileFill";
  };
 
  module RiFileForbidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileForbidFill";
  };
 
  module RiFileGifFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileGifFill";
  };
 
  module RiFileHistoryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileHistoryFill";
  };
 
  module RiFileHwpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileHwpFill";
  };
 
  module RiFileInfoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileInfoFill";
  };
 
  module RiFileList2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileList2Fill";
  };
 
  module RiFileList3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileList3Fill";
  };
 
  module RiFileListFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileListFill";
  };
 
  module RiFileLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileLockFill";
  };
 
  module RiFileMarkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileMarkFill";
  };
 
  module RiFileMusicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileMusicFill";
  };
 
  module RiFilePaper2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePaper2Fill";
  };
 
  module RiFilePaperFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePaperFill";
  };
 
  module RiFilePdfFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePdfFill";
  };
 
  module RiFilePpt2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePpt2Fill";
  };
 
  module RiFilePptFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilePptFill";
  };
 
  module RiFileReduceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileReduceFill";
  };
 
  module RiFileSearchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileSearchFill";
  };
 
  module RiFileSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileSettingsFill";
  };
 
  module RiFileShield2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShield2Fill";
  };
 
  module RiFileShieldFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShieldFill";
  };
 
  module RiFileShredFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileShredFill";
  };
 
  module RiFileTextFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileTextFill";
  };
 
  module RiFileTransferFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileTransferFill";
  };
 
  module RiFileUnknowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUnknowFill";
  };
 
  module RiFileUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUploadFill";
  };
 
  module RiFileUserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileUserFill";
  };
 
  module RiFileWarningFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWarningFill";
  };
 
  module RiFileWord2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWord2Fill";
  };
 
  module RiFileWordFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileWordFill";
  };
 
  module RiFileZipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFileZipFill";
  };
 
  module RiFolder2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder2Fill";
  };
 
  module RiFolder3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder3Fill";
  };
 
  module RiFolder4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder4Fill";
  };
 
  module RiFolder5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolder5Fill";
  };
 
  module RiFolderAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderAddFill";
  };
 
  module RiFolderChart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderChart2Fill";
  };
 
  module RiFolderChartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderChartFill";
  };
 
  module RiFolderDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderDownloadFill";
  };
 
  module RiFolderFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderFill";
  };
 
  module RiFolderForbidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderForbidFill";
  };
 
  module RiFolderHistoryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderHistoryFill";
  };
 
  module RiFolderInfoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderInfoFill";
  };
 
  module RiFolderKeyholeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderKeyholeFill";
  };
 
  module RiFolderLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderLockFill";
  };
 
  module RiFolderMusicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderMusicFill";
  };
 
  module RiFolderOpenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderOpenFill";
  };
 
  module RiFolderReceivedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderReceivedFill";
  };
 
  module RiFolderReduceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderReduceFill";
  };
 
  module RiFolderSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderSettingsFill";
  };
 
  module RiFolderSharedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderSharedFill";
  };
 
  module RiFolderShield2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderShield2Fill";
  };
 
  module RiFolderShieldFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderShieldFill";
  };
 
  module RiFolderTransferFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderTransferFill";
  };
 
  module RiFolderUnknowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUnknowFill";
  };
 
  module RiFolderUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUploadFill";
  };
 
  module RiFolderUserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderUserFill";
  };
 
  module RiFolderWarningFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderWarningFill";
  };
 
  module RiFolderZipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFolderZipFill";
  };
 
  module RiFoldersFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFoldersFill";
  };
 
  module RiKeynoteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeynoteFill";
  };
 
  module RiMarkdownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMarkdownFill";
  };
 
  module RiNewspaperFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNewspaperFill";
  };
 
  module RiNumbersFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNumbersFill";
  };
 
  module RiPagesFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPagesFill";
  };
 
  module RiStickyNote2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStickyNote2Fill";
  };
 
  module RiStickyNoteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStickyNoteFill";
  };
 
  module RiSurveyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurveyFill";
  };
 
  module RiTaskFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaskFill";
  };
 
  module RiTodoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTodoFill";
  };
 
  module Ri24HoursFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Ri24HoursFill";
  };
 
  module RiAuctionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAuctionFill";
  };
 
  module RiBankCard2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankCard2Fill";
  };
 
  module RiBankCardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBankCardFill";
  };
 
  module RiBitCoinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBitCoinFill";
  };
 
  module RiCoinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoinFill";
  };
 
  module RiCoinsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoinsFill";
  };
 
  module RiCopperCoinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopperCoinFill";
  };
 
  module RiCopperDiamondFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCopperDiamondFill";
  };
 
  module RiCoupon2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon2Fill";
  };
 
  module RiCoupon3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon3Fill";
  };
 
  module RiCoupon4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon4Fill";
  };
 
  module RiCoupon5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoupon5Fill";
  };
 
  module RiCouponFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCouponFill";
  };
 
  module RiCurrencyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCurrencyFill";
  };
 
  module RiExchangeBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeBoxFill";
  };
 
  module RiExchangeCnyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeCnyFill";
  };
 
  module RiExchangeDollarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeDollarFill";
  };
 
  module RiExchangeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeFill";
  };
 
  module RiExchangeFundsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExchangeFundsFill";
  };
 
  module RiFundsBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFundsBoxFill";
  };
 
  module RiFundsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFundsFill";
  };
 
  module RiGift2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGift2Fill";
  };
 
  module RiGiftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGiftFill";
  };
 
  module RiHandCoinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandCoinFill";
  };
 
  module RiHandHeartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandHeartFill";
  };
 
  module RiIncreaseDecreaseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIncreaseDecreaseFill";
  };
 
  module RiMoneyCnyBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyCnyBoxFill";
  };
 
  module RiMoneyCnyCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyCnyCircleFill";
  };
 
  module RiMoneyDollarBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyDollarBoxFill";
  };
 
  module RiMoneyDollarCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyDollarCircleFill";
  };
 
  module RiMoneyEuroBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyEuroBoxFill";
  };
 
  module RiMoneyEuroCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyEuroCircleFill";
  };
 
  module RiMoneyPoundBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyPoundBoxFill";
  };
 
  module RiMoneyPoundCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoneyPoundCircleFill";
  };
 
  module RiPercentFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPercentFill";
  };
 
  module RiPriceTag2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTag2Fill";
  };
 
  module RiPriceTag3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTag3Fill";
  };
 
  module RiPriceTagFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPriceTagFill";
  };
 
  module RiRedPacketFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRedPacketFill";
  };
 
  module RiRefund2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefund2Fill";
  };
 
  module RiRefundFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefundFill";
  };
 
  module RiSafe2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafe2Fill";
  };
 
  module RiSafeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafeFill";
  };
 
  module RiSecurePaymentFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSecurePaymentFill";
  };
 
  module RiShoppingBag2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBag2Fill";
  };
 
  module RiShoppingBag3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBag3Fill";
  };
 
  module RiShoppingBagFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBagFill";
  };
 
  module RiShoppingBasket2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBasket2Fill";
  };
 
  module RiShoppingBasketFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingBasketFill";
  };
 
  module RiShoppingCart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingCart2Fill";
  };
 
  module RiShoppingCartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShoppingCartFill";
  };
 
  module RiStockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStockFill";
  };
 
  module RiSwapBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwapBoxFill";
  };
 
  module RiSwapFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwapFill";
  };
 
  module RiTicket2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTicket2Fill";
  };
 
  module RiTicketFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTicketFill";
  };
 
  module RiTrophyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrophyFill";
  };
 
  module RiVipCrown2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipCrown2Fill";
  };
 
  module RiVipCrownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipCrownFill";
  };
 
  module RiVipDiamondFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipDiamondFill";
  };
 
  module RiVipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVipFill";
  };
 
  module RiWallet2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWallet2Fill";
  };
 
  module RiWallet3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWallet3Fill";
  };
 
  module RiWalletFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWalletFill";
  };
 
  module RiWaterFlashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWaterFlashFill";
  };
 
  module RiCapsuleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCapsuleFill";
  };
 
  module RiDislikeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDislikeFill";
  };
 
  module RiDossierFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDossierFill";
  };
 
  module RiEmpathizeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmpathizeFill";
  };
 
  module RiFirstAidKitFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFirstAidKitFill";
  };
 
  module RiFlaskFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlaskFill";
  };
 
  module RiHandSanitizerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandSanitizerFill";
  };
 
  module RiHealthBookFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHealthBookFill";
  };
 
  module RiHeart2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeart2Fill";
  };
 
  module RiHeart3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeart3Fill";
  };
 
  module RiHeartAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartAddFill";
  };
 
  module RiHeartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartFill";
  };
 
  module RiHeartPulseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartPulseFill";
  };
 
  module RiHeartsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeartsFill";
  };
 
  module RiInfraredThermometerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInfraredThermometerFill";
  };
 
  module RiLungsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLungsFill";
  };
 
  module RiMedicineBottleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMedicineBottleFill";
  };
 
  module RiMentalHealthFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMentalHealthFill";
  };
 
  module RiMicroscopeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicroscopeFill";
  };
 
  module RiNurseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNurseFill";
  };
 
  module RiPsychotherapyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPsychotherapyFill";
  };
 
  module RiPulseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPulseFill";
  };
 
  module RiRestTimeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestTimeFill";
  };
 
  module RiStethoscopeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStethoscopeFill";
  };
 
  module RiSurgicalMaskFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurgicalMaskFill";
  };
 
  module RiSyringeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSyringeFill";
  };
 
  module RiTestTubeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTestTubeFill";
  };
 
  module RiThermometerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThermometerFill";
  };
 
  module RiVirusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVirusFill";
  };
 
  module RiZzzFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZzzFill";
  };
 
  module RiAlipayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlipayFill";
  };
 
  module RiAmazonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAmazonFill";
  };
 
  module RiAndroidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAndroidFill";
  };
 
  module RiAngularjsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAngularjsFill";
  };
 
  module RiAppStoreFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppStoreFill";
  };
 
  module RiAppleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppleFill";
  };
 
  module RiBaiduFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBaiduFill";
  };
 
  module RiBehanceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBehanceFill";
  };
 
  module RiBilibiliFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBilibiliFill";
  };
 
  module RiCentosFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCentosFill";
  };
 
  module RiChromeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChromeFill";
  };
 
  module RiCodepenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCodepenFill";
  };
 
  module RiCoreosFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCoreosFill";
  };
 
  module RiDingdingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDingdingFill";
  };
 
  module RiDiscordFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscordFill";
  };
 
  module RiDisqusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDisqusFill";
  };
 
  module RiDoubanFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoubanFill";
  };
 
  module RiDribbbleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDribbbleFill";
  };
 
  module RiDriveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDriveFill";
  };
 
  module RiDropboxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDropboxFill";
  };
 
  module RiEdgeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEdgeFill";
  };
 
  module RiEvernoteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEvernoteFill";
  };
 
  module RiFacebookBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookBoxFill";
  };
 
  module RiFacebookCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookCircleFill";
  };
 
  module RiFacebookFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFacebookFill";
  };
 
  module RiFinderFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFinderFill";
  };
 
  module RiFirefoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFirefoxFill";
  };
 
  module RiFlutterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlutterFill";
  };
 
  module RiGatsbyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGatsbyFill";
  };
 
  module RiGithubFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGithubFill";
  };
 
  module RiGitlabFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGitlabFill";
  };
 
  module RiGoogleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGoogleFill";
  };
 
  module RiGooglePlayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGooglePlayFill";
  };
 
  module RiHonorOfKingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHonorOfKingsFill";
  };
 
  module RiIeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIeFill";
  };
 
  module RiInstagramFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInstagramFill";
  };
 
  module RiInvisionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInvisionFill";
  };
 
  module RiKakaoTalkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKakaoTalkFill";
  };
 
  module RiLineFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLineFill";
  };
 
  module RiLinkedinBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinkedinBoxFill";
  };
 
  module RiLinkedinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLinkedinFill";
  };
 
  module RiMastercardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMastercardFill";
  };
 
  module RiMastodonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMastodonFill";
  };
 
  module RiMediumFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMediumFill";
  };
 
  module RiMessengerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMessengerFill";
  };
 
  module RiMicrosoftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicrosoftFill";
  };
 
  module RiMiniProgramFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMiniProgramFill";
  };
 
  module RiNeteaseCloudMusicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNeteaseCloudMusicFill";
  };
 
  module RiNetflixFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNetflixFill";
  };
 
  module RiNpmjsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNpmjsFill";
  };
 
  module RiOpenSourceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOpenSourceFill";
  };
 
  module RiOperaFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOperaFill";
  };
 
  module RiPatreonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPatreonFill";
  };
 
  module RiPaypalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPaypalFill";
  };
 
  module RiPinterestFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPinterestFill";
  };
 
  module RiPixelfedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPixelfedFill";
  };
 
  module RiPlaystationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlaystationFill";
  };
 
  module RiProductHuntFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiProductHuntFill";
  };
 
  module RiQqFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQqFill";
  };
 
  module RiReactjsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReactjsFill";
  };
 
  module RiRedditFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRedditFill";
  };
 
  module RiRemixiconFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRemixiconFill";
  };
 
  module RiSafariFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSafariFill";
  };
 
  module RiSkypeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkypeFill";
  };
 
  module RiSlackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSlackFill";
  };
 
  module RiSnapchatFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSnapchatFill";
  };
 
  module RiSoundcloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSoundcloudFill";
  };
 
  module RiSpectrumFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpectrumFill";
  };
 
  module RiSpotifyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpotifyFill";
  };
 
  module RiStackOverflowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackOverflowFill";
  };
 
  module RiStackshareFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStackshareFill";
  };
 
  module RiSteamFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteamFill";
  };
 
  module RiSwitchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwitchFill";
  };
 
  module RiTaobaoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaobaoFill";
  };
 
  module RiTelegramFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTelegramFill";
  };
 
  module RiTrelloFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrelloFill";
  };
 
  module RiTumblrFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTumblrFill";
  };
 
  module RiTwitchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTwitchFill";
  };
 
  module RiTwitterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTwitterFill";
  };
 
  module RiUbuntuFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUbuntuFill";
  };
 
  module RiUnsplashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUnsplashFill";
  };
 
  module RiVimeoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVimeoFill";
  };
 
  module RiVisaFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVisaFill";
  };
 
  module RiVuejsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVuejsFill";
  };
 
  module RiWechat2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechat2Fill";
  };
 
  module RiWechatFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechatFill";
  };
 
  module RiWechatPayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWechatPayFill";
  };
 
  module RiWeiboFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWeiboFill";
  };
 
  module RiWhatsappFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWhatsappFill";
  };
 
  module RiWindowsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindowsFill";
  };
 
  module RiXboxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiXboxFill";
  };
 
  module RiXingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiXingFill";
  };
 
  module RiYoutubeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiYoutubeFill";
  };
 
  module RiZcoolFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZcoolFill";
  };
 
  module RiZhihuFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZhihuFill";
  };
 
  module RiAnchorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAnchorFill";
  };
 
  module RiBarricadeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBarricadeFill";
  };
 
  module RiBikeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBikeFill";
  };
 
  module RiBus2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBus2Fill";
  };
 
  module RiBusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBusFill";
  };
 
  module RiBusWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBusWifiFill";
  };
 
  module RiCarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCarFill";
  };
 
  module RiCarWashingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCarWashingFill";
  };
 
  module RiCaravanFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCaravanFill";
  };
 
  module RiChargingPile2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChargingPile2Fill";
  };
 
  module RiChargingPileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChargingPileFill";
  };
 
  module RiChinaRailwayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiChinaRailwayFill";
  };
 
  module RiCompass2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass2Fill";
  };
 
  module RiCompass3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass3Fill";
  };
 
  module RiCompass4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompass4Fill";
  };
 
  module RiCompassDiscoverFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassDiscoverFill";
  };
 
  module RiCompassFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCompassFill";
  };
 
  module RiCupFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCupFill";
  };
 
  module RiDirectionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDirectionFill";
  };
 
  module RiEBike2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEBike2Fill";
  };
 
  module RiEBikeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEBikeFill";
  };
 
  module RiEarthFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEarthFill";
  };
 
  module RiFlightLandFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlightLandFill";
  };
 
  module RiFlightTakeoffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlightTakeoffFill";
  };
 
  module RiFootprintFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFootprintFill";
  };
 
  module RiGasStationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGasStationFill";
  };
 
  module RiGlobeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGlobeFill";
  };
 
  module RiGobletFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGobletFill";
  };
 
  module RiGuideFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGuideFill";
  };
 
  module RiHotelBedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHotelBedFill";
  };
 
  module RiLifebuoyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLifebuoyFill";
  };
 
  module RiLuggageCartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLuggageCartFill";
  };
 
  module RiLuggageDepositFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLuggageDepositFill";
  };
 
  module RiMap2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMap2Fill";
  };
 
  module RiMapFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapFill";
  };
 
  module RiMapPin2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin2Fill";
  };
 
  module RiMapPin3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin3Fill";
  };
 
  module RiMapPin4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin4Fill";
  };
 
  module RiMapPin5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPin5Fill";
  };
 
  module RiMapPinAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinAddFill";
  };
 
  module RiMapPinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinFill";
  };
 
  module RiMapPinRangeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinRangeFill";
  };
 
  module RiMapPinTimeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinTimeFill";
  };
 
  module RiMapPinUserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMapPinUserFill";
  };
 
  module RiMotorbikeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMotorbikeFill";
  };
 
  module RiNavigationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNavigationFill";
  };
 
  module RiOilFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOilFill";
  };
 
  module RiParkingBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParkingBoxFill";
  };
 
  module RiParkingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParkingFill";
  };
 
  module RiPassportFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPassportFill";
  };
 
  module RiPinDistanceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPinDistanceFill";
  };
 
  module RiPlaneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlaneFill";
  };
 
  module RiPoliceCarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPoliceCarFill";
  };
 
  module RiPushpin2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPushpin2Fill";
  };
 
  module RiPushpinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPushpinFill";
  };
 
  module RiRestaurant2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestaurant2Fill";
  };
 
  module RiRestaurantFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRestaurantFill";
  };
 
  module RiRidingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRidingFill";
  };
 
  module RiRoadMapFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRoadMapFill";
  };
 
  module RiRoadsterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRoadsterFill";
  };
 
  module RiRocket2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRocket2Fill";
  };
 
  module RiRocketFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRocketFill";
  };
 
  module RiRouteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRouteFill";
  };
 
  module RiRunFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRunFill";
  };
 
  module RiSailboatFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSailboatFill";
  };
 
  module RiShip2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShip2Fill";
  };
 
  module RiShipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShipFill";
  };
 
  module RiSignalTowerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSignalTowerFill";
  };
 
  module RiSpaceShipFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpaceShipFill";
  };
 
  module RiSteering2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteering2Fill";
  };
 
  module RiSteeringFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSteeringFill";
  };
 
  module RiSubwayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubwayFill";
  };
 
  module RiSubwayWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubwayWifiFill";
  };
 
  module RiSuitcase2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcase2Fill";
  };
 
  module RiSuitcase3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcase3Fill";
  };
 
  module RiSuitcaseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSuitcaseFill";
  };
 
  module RiTakeawayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTakeawayFill";
  };
 
  module RiTaxiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaxiFill";
  };
 
  module RiTaxiWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTaxiWifiFill";
  };
 
  module RiTrafficLightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrafficLightFill";
  };
 
  module RiTrainFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrainFill";
  };
 
  module RiTrainWifiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTrainWifiFill";
  };
 
  module RiTreasureMapFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTreasureMapFill";
  };
 
  module RiTruckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTruckFill";
  };
 
  module RiWalkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWalkFill";
  };
 
  module Ri4KFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Ri4KFill";
  };
 
  module RiAlbumFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlbumFill";
  };
 
  module RiAspectRatioFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAspectRatioFill";
  };
 
  module RiBroadcastFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBroadcastFill";
  };
 
  module RiCamera2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCamera2Fill";
  };
 
  module RiCamera3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCamera3Fill";
  };
 
  module RiCameraFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraFill";
  };
 
  module RiCameraLensFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraLensFill";
  };
 
  module RiCameraOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraOffFill";
  };
 
  module RiCameraSwitchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCameraSwitchFill";
  };
 
  module RiClapperboardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClapperboardFill";
  };
 
  module RiClosedCaptioningFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiClosedCaptioningFill";
  };
 
  module RiDiscFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDiscFill";
  };
 
  module RiDvFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDvFill";
  };
 
  module RiDvdFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDvdFill";
  };
 
  module RiEjectFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEjectFill";
  };
 
  module RiEqualizerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEqualizerFill";
  };
 
  module RiFilmFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilmFill";
  };
 
  module RiFullscreenExitFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFullscreenExitFill";
  };
 
  module RiFullscreenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFullscreenFill";
  };
 
  module RiGalleryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGalleryFill";
  };
 
  module RiGalleryUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGalleryUploadFill";
  };
 
  module RiHdFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHdFill";
  };
 
  module RiHeadphoneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeadphoneFill";
  };
 
  module RiHqFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHqFill";
  };
 
  module RiImage2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImage2Fill";
  };
 
  module RiImageAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageAddFill";
  };
 
  module RiImageEditFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageEditFill";
  };
 
  module RiImageFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiImageFill";
  };
 
  module RiLandscapeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLandscapeFill";
  };
 
  module RiLiveFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLiveFill";
  };
 
  module RiMic2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMic2Fill";
  };
 
  module RiMicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicFill";
  };
 
  module RiMicOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMicOffFill";
  };
 
  module RiMovie2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMovie2Fill";
  };
 
  module RiMovieFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMovieFill";
  };
 
  module RiMusic2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMusic2Fill";
  };
 
  module RiMusicFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMusicFill";
  };
 
  module RiMvFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMvFill";
  };
 
  module RiNotification2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification2Fill";
  };
 
  module RiNotification3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification3Fill";
  };
 
  module RiNotification4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotification4Fill";
  };
 
  module RiNotificationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationFill";
  };
 
  module RiNotificationOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationOffFill";
  };
 
  module RiOrderPlayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOrderPlayFill";
  };
 
  module RiPauseCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseCircleFill";
  };
 
  module RiPauseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseFill";
  };
 
  module RiPauseMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPauseMiniFill";
  };
 
  module RiPhoneCameraFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPhoneCameraFill";
  };
 
  module RiPictureInPicture2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPicture2Fill";
  };
 
  module RiPictureInPictureExitFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPictureExitFill";
  };
 
  module RiPictureInPictureFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPictureInPictureFill";
  };
 
  module RiPlayCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayCircleFill";
  };
 
  module RiPlayFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayFill";
  };
 
  module RiPlayList2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayList2Fill";
  };
 
  module RiPlayListAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayListAddFill";
  };
 
  module RiPlayListFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayListFill";
  };
 
  module RiPlayMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlayMiniFill";
  };
 
  module RiPolaroid2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPolaroid2Fill";
  };
 
  module RiPolaroidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPolaroidFill";
  };
 
  module RiRadio2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadio2Fill";
  };
 
  module RiRadioFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadioFill";
  };
 
  module RiRecordCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecordCircleFill";
  };
 
  module RiRepeat2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeat2Fill";
  };
 
  module RiRepeatFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeatFill";
  };
 
  module RiRepeatOneFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRepeatOneFill";
  };
 
  module RiRewindFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRewindFill";
  };
 
  module RiRewindMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRewindMiniFill";
  };
 
  module RiRhythmFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRhythmFill";
  };
 
  module RiShuffleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShuffleFill";
  };
 
  module RiSkipBackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipBackFill";
  };
 
  module RiSkipBackMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipBackMiniFill";
  };
 
  module RiSkipForwardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipForwardFill";
  };
 
  module RiSkipForwardMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkipForwardMiniFill";
  };
 
  module RiSoundModuleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSoundModuleFill";
  };
 
  module RiSpeaker2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeaker2Fill";
  };
 
  module RiSpeaker3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeaker3Fill";
  };
 
  module RiSpeakerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeakerFill";
  };
 
  module RiSpeedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeedFill";
  };
 
  module RiSpeedMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpeedMiniFill";
  };
 
  module RiStopCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopCircleFill";
  };
 
  module RiStopFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopFill";
  };
 
  module RiStopMiniFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStopMiniFill";
  };
 
  module RiSurroundSoundFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSurroundSoundFill";
  };
 
  module RiTapeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTapeFill";
  };
 
  module RiVideoAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoAddFill";
  };
 
  module RiVideoDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoDownloadFill";
  };
 
  module RiVideoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoFill";
  };
 
  module RiVideoUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVideoUploadFill";
  };
 
  module RiVidicon2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVidicon2Fill";
  };
 
  module RiVidiconFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVidiconFill";
  };
 
  module RiVoiceprintFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVoiceprintFill";
  };
 
  module RiVolumeDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeDownFill";
  };
 
  module RiVolumeMuteFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeMuteFill";
  };
 
  module RiVolumeOffVibrateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeOffVibrateFill";
  };
 
  module RiVolumeUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeUpFill";
  };
 
  module RiVolumeVibrateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVolumeVibrateFill";
  };
 
  module RiWebcamFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWebcamFill";
  };
 
  module RiBasketballFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBasketballFill";
  };
 
  module RiBellFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBellFill";
  };
 
  module RiBilliardsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBilliardsFill";
  };
 
  module RiBoxingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBoxingFill";
  };
 
  module RiCactusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCactusFill";
  };
 
  module RiCake2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCake2Fill";
  };
 
  module RiCake3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCake3Fill";
  };
 
  module RiCakeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCakeFill";
  };
 
  module RiCharacterRecognitionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCharacterRecognitionFill";
  };
 
  module RiDoorClosedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorClosedFill";
  };
 
  module RiDoorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorFill";
  };
 
  module RiDoorLockBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorLockBoxFill";
  };
 
  module RiDoorLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorLockFill";
  };
 
  module RiDoorOpenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDoorOpenFill";
  };
 
  module RiFootballFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFootballFill";
  };
 
  module RiFridgeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFridgeFill";
  };
 
  module RiGameFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGameFill";
  };
 
  module RiHandbagFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHandbagFill";
  };
 
  module RiKey2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKey2Fill";
  };
 
  module RiKeyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKeyFill";
  };
 
  module RiKnifeBloodFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKnifeBloodFill";
  };
 
  module RiKnifeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiKnifeFill";
  };
 
  module RiLeafFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLeafFill";
  };
 
  module RiLightbulbFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLightbulbFill";
  };
 
  module RiLightbulbFlashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLightbulbFlashFill";
  };
 
  module RiOutlet2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOutlet2Fill";
  };
 
  module RiOutletFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOutletFill";
  };
 
  module RiPingPongFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPingPongFill";
  };
 
  module RiPlantFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlantFill";
  };
 
  module RiPlug2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlug2Fill";
  };
 
  module RiPlugFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiPlugFill";
  };
 
  module RiRecycleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRecycleFill";
  };
 
  module RiReservedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiReservedFill";
  };
 
  module RiScales2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScales2Fill";
  };
 
  module RiScales3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScales3Fill";
  };
 
  module RiScalesFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiScalesFill";
  };
 
  module RiSeedlingFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSeedlingFill";
  };
 
  module RiShirtFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShirtFill";
  };
 
  module RiSwordFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSwordFill";
  };
 
  module RiTShirt2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirt2Fill";
  };
 
  module RiTShirtAirFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirtAirFill";
  };
 
  module RiTShirtFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTShirtFill";
  };
 
  module RiUmbrellaFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUmbrellaFill";
  };
 
  module RiVoiceRecognitionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiVoiceRecognitionFill";
  };
 
  module RiWheelchairFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWheelchairFill";
  };
 
  module RiAddBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddBoxFill";
  };
 
  module RiAddCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddCircleFill";
  };
 
  module RiAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAddFill";
  };
 
  module RiAlarmFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlarmFill";
  };
 
  module RiAlarmWarningFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlarmWarningFill";
  };
 
  module RiAlertFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAlertFill";
  };
 
  module RiApps2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiApps2Fill";
  };
 
  module RiAppsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAppsFill";
  };
 
  module RiArrowDownCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownCircleFill";
  };
 
  module RiArrowDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownFill";
  };
 
  module RiArrowDownSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDownSFill";
  };
 
  module RiArrowDropDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropDownFill";
  };
 
  module RiArrowDropLeftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropLeftFill";
  };
 
  module RiArrowDropRightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropRightFill";
  };
 
  module RiArrowDropUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowDropUpFill";
  };
 
  module RiArrowGoBackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowGoBackFill";
  };
 
  module RiArrowGoForwardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowGoForwardFill";
  };
 
  module RiArrowLeftCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftCircleFill";
  };
 
  module RiArrowLeftDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftDownFill";
  };
 
  module RiArrowLeftFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftFill";
  };
 
  module RiArrowLeftRightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftRightFill";
  };
 
  module RiArrowLeftSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftSFill";
  };
 
  module RiArrowLeftUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowLeftUpFill";
  };
 
  module RiArrowRightCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightCircleFill";
  };
 
  module RiArrowRightDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightDownFill";
  };
 
  module RiArrowRightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightFill";
  };
 
  module RiArrowRightSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightSFill";
  };
 
  module RiArrowRightUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowRightUpFill";
  };
 
  module RiArrowUpCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpCircleFill";
  };
 
  module RiArrowUpDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpDownFill";
  };
 
  module RiArrowUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpFill";
  };
 
  module RiArrowUpSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiArrowUpSFill";
  };
 
  module RiCheckDoubleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckDoubleFill";
  };
 
  module RiCheckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckFill";
  };
 
  module RiCheckboxBlankCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxBlankCircleFill";
  };
 
  module RiCheckboxBlankFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxBlankFill";
  };
 
  module RiCheckboxCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxCircleFill";
  };
 
  module RiCheckboxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxFill";
  };
 
  module RiCheckboxIndeterminateFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxIndeterminateFill";
  };
 
  module RiCheckboxMultipleBlankFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxMultipleBlankFill";
  };
 
  module RiCheckboxMultipleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCheckboxMultipleFill";
  };
 
  module RiCloseCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloseCircleFill";
  };
 
  module RiCloseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloseFill";
  };
 
  module RiDashboardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDashboardFill";
  };
 
  module RiDeleteBack2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBack2Fill";
  };
 
  module RiDeleteBackFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBackFill";
  };
 
  module RiDeleteBin2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin2Fill";
  };
 
  module RiDeleteBin3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin3Fill";
  };
 
  module RiDeleteBin4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin4Fill";
  };
 
  module RiDeleteBin5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin5Fill";
  };
 
  module RiDeleteBin6Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin6Fill";
  };
 
  module RiDeleteBin7Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBin7Fill";
  };
 
  module RiDeleteBinFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDeleteBinFill";
  };
 
  module RiDivideFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDivideFill";
  };
 
  module RiDownload2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownload2Fill";
  };
 
  module RiDownloadCloud2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadCloud2Fill";
  };
 
  module RiDownloadCloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadCloudFill";
  };
 
  module RiDownloadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDownloadFill";
  };
 
  module RiErrorWarningFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiErrorWarningFill";
  };
 
  module RiExternalLinkFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiExternalLinkFill";
  };
 
  module RiEye2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEye2Fill";
  };
 
  module RiEyeCloseFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeCloseFill";
  };
 
  module RiEyeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeFill";
  };
 
  module RiEyeOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEyeOffFill";
  };
 
  module RiFilter2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilter2Fill";
  };
 
  module RiFilter3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilter3Fill";
  };
 
  module RiFilterFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilterFill";
  };
 
  module RiFilterOffFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFilterOffFill";
  };
 
  module RiFindReplaceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFindReplaceFill";
  };
 
  module RiForbid2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiForbid2Fill";
  };
 
  module RiForbidFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiForbidFill";
  };
 
  module RiFunctionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFunctionFill";
  };
 
  module RiHistoryFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHistoryFill";
  };
 
  module RiIndeterminateCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiIndeterminateCircleFill";
  };
 
  module RiInformationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiInformationFill";
  };
 
  module RiListSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiListSettingsFill";
  };
 
  module RiLoader2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader2Fill";
  };
 
  module RiLoader3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader3Fill";
  };
 
  module RiLoader4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader4Fill";
  };
 
  module RiLoader5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoader5Fill";
  };
 
  module RiLoaderFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoaderFill";
  };
 
  module RiLock2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLock2Fill";
  };
 
  module RiLockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockFill";
  };
 
  module RiLockPasswordFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockPasswordFill";
  };
 
  module RiLockUnlockFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLockUnlockFill";
  };
 
  module RiLoginBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoginBoxFill";
  };
 
  module RiLoginCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLoginCircleFill";
  };
 
  module RiLogoutBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutBoxFill";
  };
 
  module RiLogoutBoxRFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutBoxRFill";
  };
 
  module RiLogoutCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutCircleFill";
  };
 
  module RiLogoutCircleRFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiLogoutCircleRFill";
  };
 
  module RiMenu2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu2Fill";
  };
 
  module RiMenu3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu3Fill";
  };
 
  module RiMenu4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu4Fill";
  };
 
  module RiMenu5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenu5Fill";
  };
 
  module RiMenuAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuAddFill";
  };
 
  module RiMenuFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuFill";
  };
 
  module RiMenuFoldFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuFoldFill";
  };
 
  module RiMenuUnfoldFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenuUnfoldFill";
  };
 
  module RiMore2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMore2Fill";
  };
 
  module RiMoreFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoreFill";
  };
 
  module RiNotificationBadgeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiNotificationBadgeFill";
  };
 
  module RiQuestionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiQuestionFill";
  };
 
  module RiRadioButtonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRadioButtonFill";
  };
 
  module RiRefreshFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRefreshFill";
  };
 
  module RiSearch2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearch2Fill";
  };
 
  module RiSearchEyeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearchEyeFill";
  };
 
  module RiSearchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSearchFill";
  };
 
  module RiSettings2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings2Fill";
  };
 
  module RiSettings3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings3Fill";
  };
 
  module RiSettings4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings4Fill";
  };
 
  module RiSettings5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings5Fill";
  };
 
  module RiSettings6Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettings6Fill";
  };
 
  module RiSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSettingsFill";
  };
 
  module RiShareBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareBoxFill";
  };
 
  module RiShareCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareCircleFill";
  };
 
  module RiShareFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareFill";
  };
 
  module RiShareForward2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForward2Fill";
  };
 
  module RiShareForwardBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForwardBoxFill";
  };
 
  module RiShareForwardFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShareForwardFill";
  };
 
  module RiShieldCheckFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldCheckFill";
  };
 
  module RiShieldCrossFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldCrossFill";
  };
 
  module RiShieldFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldFill";
  };
 
  module RiShieldFlashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldFlashFill";
  };
 
  module RiShieldKeyholeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldKeyholeFill";
  };
 
  module RiShieldStarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldStarFill";
  };
 
  module RiShieldUserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShieldUserFill";
  };
 
  module RiSideBarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSideBarFill";
  };
 
  module RiSpam2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpam2Fill";
  };
 
  module RiSpam3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpam3Fill";
  };
 
  module RiSpamFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpamFill";
  };
 
  module RiStarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarFill";
  };
 
  module RiStarHalfFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarHalfFill";
  };
 
  module RiStarHalfSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarHalfSFill";
  };
 
  module RiStarSFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarSFill";
  };
 
  module RiSubtractFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSubtractFill";
  };
 
  module RiThumbDownFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThumbDownFill";
  };
 
  module RiThumbUpFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThumbUpFill";
  };
 
  module RiTimeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimeFill";
  };
 
  module RiTimer2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimer2Fill";
  };
 
  module RiTimerFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimerFill";
  };
 
  module RiTimerFlashFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTimerFlashFill";
  };
 
  module RiToggleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiToggleFill";
  };
 
  module RiUpload2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUpload2Fill";
  };
 
  module RiUploadCloud2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadCloud2Fill";
  };
 
  module RiUploadCloudFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadCloudFill";
  };
 
  module RiUploadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUploadFill";
  };
 
  module RiZoomInFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZoomInFill";
  };
 
  module RiZoomOutFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiZoomOutFill";
  };
 
  module RiAccountBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountBoxFill";
  };
 
  module RiAccountCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountCircleFill";
  };
 
  module RiAccountPinBoxFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountPinBoxFill";
  };
 
  module RiAccountPinCircleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAccountPinCircleFill";
  };
 
  module RiAdminFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAdminFill";
  };
 
  module RiAliensFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiAliensFill";
  };
 
  module RiBearSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBearSmileFill";
  };
 
  module RiBodyScanFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBodyScanFill";
  };
 
  module RiContactsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiContactsFill";
  };
 
  module RiCriminalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCriminalFill";
  };
 
  module RiEmotion2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotion2Fill";
  };
 
  module RiEmotionFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionFill";
  };
 
  module RiEmotionHappyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionHappyFill";
  };
 
  module RiEmotionLaughFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionLaughFill";
  };
 
  module RiEmotionNormalFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionNormalFill";
  };
 
  module RiEmotionSadFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionSadFill";
  };
 
  module RiEmotionUnhappyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEmotionUnhappyFill";
  };
 
  module RiGenderlessFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGenderlessFill";
  };
 
  module RiGhost2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhost2Fill";
  };
 
  module RiGhostFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhostFill";
  };
 
  module RiGhostSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGhostSmileFill";
  };
 
  module RiGroup2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGroup2Fill";
  };
 
  module RiGroupFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiGroupFill";
  };
 
  module RiMenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMenFill";
  };
 
  module RiMickeyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMickeyFill";
  };
 
  module RiOpenArmFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiOpenArmFill";
  };
 
  module RiParentFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiParentFill";
  };
 
  module RiRobotFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRobotFill";
  };
 
  module RiSkull2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkull2Fill";
  };
 
  module RiSkullFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSkullFill";
  };
 
  module RiSpyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSpyFill";
  };
 
  module RiStarSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiStarSmileFill";
  };
 
  module RiTeamFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTeamFill";
  };
 
  module RiTravestiFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTravestiFill";
  };
 
  module RiUser2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser2Fill";
  };
 
  module RiUser3Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser3Fill";
  };
 
  module RiUser4Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser4Fill";
  };
 
  module RiUser5Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser5Fill";
  };
 
  module RiUser6Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUser6Fill";
  };
 
  module RiUserAddFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserAddFill";
  };
 
  module RiUserFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserFill";
  };
 
  module RiUserFollowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserFollowFill";
  };
 
  module RiUserHeartFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserHeartFill";
  };
 
  module RiUserLocationFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserLocationFill";
  };
 
  module RiUserReceived2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserReceived2Fill";
  };
 
  module RiUserReceivedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserReceivedFill";
  };
 
  module RiUserSearchFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSearchFill";
  };
 
  module RiUserSettingsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSettingsFill";
  };
 
  module RiUserShared2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserShared2Fill";
  };
 
  module RiUserSharedFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSharedFill";
  };
 
  module RiUserSmileFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserSmileFill";
  };
 
  module RiUserStarFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserStarFill";
  };
 
  module RiUserUnfollowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserUnfollowFill";
  };
 
  module RiUserVoiceFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiUserVoiceFill";
  };
 
  module RiWomenFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWomenFill";
  };
 
  module RiBlazeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiBlazeFill";
  };
 
  module RiCelsiusFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCelsiusFill";
  };
 
  module RiCloudWindyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudWindyFill";
  };
 
  module RiCloudy2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudy2Fill";
  };
 
  module RiCloudyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiCloudyFill";
  };
 
  module RiDrizzleFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiDrizzleFill";
  };
 
  module RiEarthquakeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiEarthquakeFill";
  };
 
  module RiFahrenheitFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFahrenheitFill";
  };
 
  module RiFireFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFireFill";
  };
 
  module RiFlashlightFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFlashlightFill";
  };
 
  module RiFloodFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFloodFill";
  };
 
  module RiFoggyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiFoggyFill";
  };
 
  module RiHailFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHailFill";
  };
 
  module RiHaze2Fill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHaze2Fill";
  };
 
  module RiHazeFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHazeFill";
  };
 
  module RiHeavyShowersFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiHeavyShowersFill";
  };
 
  module RiMeteorFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMeteorFill";
  };
 
  module RiMistFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMistFill";
  };
 
  module RiMoonClearFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonClearFill";
  };
 
  module RiMoonCloudyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonCloudyFill";
  };
 
  module RiMoonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonFill";
  };
 
  module RiMoonFoggyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiMoonFoggyFill";
  };
 
  module RiRainbowFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRainbowFill";
  };
 
  module RiRainyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiRainyFill";
  };
 
  module RiShowersFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiShowersFill";
  };
 
  module RiSnowyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSnowyFill";
  };
 
  module RiSunCloudyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunCloudyFill";
  };
 
  module RiSunFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunFill";
  };
 
  module RiSunFoggyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiSunFoggyFill";
  };
 
  module RiTempColdFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTempColdFill";
  };
 
  module RiTempHotFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTempHotFill";
  };
 
  module RiThunderstormsFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiThunderstormsFill";
  };
 
  module RiTornadoFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTornadoFill";
  };
 
  module RiTyphoonFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiTyphoonFill";
  };
 
  module RiWindyFill = {
    @module("react-icons/ri") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "RiWindyFill";
  };
