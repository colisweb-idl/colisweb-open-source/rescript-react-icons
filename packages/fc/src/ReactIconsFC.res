
  module FcAbout = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAbout";
  };
 
  module FcAcceptDatabase = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAcceptDatabase";
  };
 
  module FcAddColumn = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAddColumn";
  };
 
  module FcAddDatabase = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAddDatabase";
  };
 
  module FcAddImage = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAddImage";
  };
 
  module FcAddRow = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAddRow";
  };
 
  module FcAddressBook = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAddressBook";
  };
 
  module FcAdvance = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAdvance";
  };
 
  module FcAdvertising = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAdvertising";
  };
 
  module FcAlarmClock = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAlarmClock";
  };
 
  module FcAlphabeticalSortingAz = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAlphabeticalSortingAz";
  };
 
  module FcAlphabeticalSortingZa = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAlphabeticalSortingZa";
  };
 
  module FcAndroidOs = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAndroidOs";
  };
 
  module FcAnswers = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAnswers";
  };
 
  module FcApproval = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcApproval";
  };
 
  module FcApprove = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcApprove";
  };
 
  module FcAreaChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAreaChart";
  };
 
  module FcAssistant = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAssistant";
  };
 
  module FcAudioFile = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAudioFile";
  };
 
  module FcAutomatic = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAutomatic";
  };
 
  module FcAutomotive = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcAutomotive";
  };
 
  module FcBadDecision = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBadDecision";
  };
 
  module FcBarChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBarChart";
  };
 
  module FcBbc = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBbc";
  };
 
  module FcBearish = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBearish";
  };
 
  module FcBinoculars = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBinoculars";
  };
 
  module FcBiohazard = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBiohazard";
  };
 
  module FcBiomass = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBiomass";
  };
 
  module FcBiotech = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBiotech";
  };
 
  module FcBookmark = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBookmark";
  };
 
  module FcBriefcase = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBriefcase";
  };
 
  module FcBrokenLink = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBrokenLink";
  };
 
  module FcBullish = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBullish";
  };
 
  module FcBusinessContact = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBusinessContact";
  };
 
  module FcBusiness = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBusiness";
  };
 
  module FcBusinessman = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBusinessman";
  };
 
  module FcBusinesswoman = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcBusinesswoman";
  };
 
  module FcButtingIn = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcButtingIn";
  };
 
  module FcCableRelease = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCableRelease";
  };
 
  module FcCalculator = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCalculator";
  };
 
  module FcCalendar = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCalendar";
  };
 
  module FcCallTransfer = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCallTransfer";
  };
 
  module FcCallback = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCallback";
  };
 
  module FcCamcorderPro = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCamcorderPro";
  };
 
  module FcCamcorder = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCamcorder";
  };
 
  module FcCameraAddon = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCameraAddon";
  };
 
  module FcCameraIdentification = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCameraIdentification";
  };
 
  module FcCamera = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCamera";
  };
 
  module FcCancel = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCancel";
  };
 
  module FcCandleSticks = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCandleSticks";
  };
 
  module FcCapacitor = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCapacitor";
  };
 
  module FcCdLogo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCdLogo";
  };
 
  module FcCellPhone = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCellPhone";
  };
 
  module FcChargeBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcChargeBattery";
  };
 
  module FcCheckmark = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCheckmark";
  };
 
  module FcCircuit = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCircuit";
  };
 
  module FcClapperboard = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcClapperboard";
  };
 
  module FcClearFilters = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcClearFilters";
  };
 
  module FcClock = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcClock";
  };
 
  module FcCloseUpMode = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCloseUpMode";
  };
 
  module FcCloth = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCloth";
  };
 
  module FcCollaboration = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCollaboration";
  };
 
  module FcCollapse = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCollapse";
  };
 
  module FcCollect = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCollect";
  };
 
  module FcComboChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcComboChart";
  };
 
  module FcCommandLine = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCommandLine";
  };
 
  module FcComments = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcComments";
  };
 
  module FcCompactCamera = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCompactCamera";
  };
 
  module FcConferenceCall = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcConferenceCall";
  };
 
  module FcContacts = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcContacts";
  };
 
  module FcCopyleft = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCopyleft";
  };
 
  module FcCopyright = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCopyright";
  };
 
  module FcCrystalOscillator = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCrystalOscillator";
  };
 
  module FcCurrencyExchange = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCurrencyExchange";
  };
 
  module FcCursor = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCursor";
  };
 
  module FcCustomerSupport = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcCustomerSupport";
  };
 
  module FcDam = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDam";
  };
 
  module FcDataBackup = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataBackup";
  };
 
  module FcDataConfiguration = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataConfiguration";
  };
 
  module FcDataEncryption = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataEncryption";
  };
 
  module FcDataProtection = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataProtection";
  };
 
  module FcDataRecovery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataRecovery";
  };
 
  module FcDataSheet = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDataSheet";
  };
 
  module FcDatabase = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDatabase";
  };
 
  module FcDebian = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDebian";
  };
 
  module FcDebt = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDebt";
  };
 
  module FcDecision = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDecision";
  };
 
  module FcDeleteColumn = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDeleteColumn";
  };
 
  module FcDeleteDatabase = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDeleteDatabase";
  };
 
  module FcDeleteRow = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDeleteRow";
  };
 
  module FcDepartment = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDepartment";
  };
 
  module FcDeployment = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDeployment";
  };
 
  module FcDiploma1 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDiploma1";
  };
 
  module FcDiploma2 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDiploma2";
  };
 
  module FcDisapprove = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDisapprove";
  };
 
  module FcDisclaimer = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDisclaimer";
  };
 
  module FcDislike = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDislike";
  };
 
  module FcDisplay = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDisplay";
  };
 
  module FcDoNotInhale = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDoNotInhale";
  };
 
  module FcDoNotInsert = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDoNotInsert";
  };
 
  module FcDoNotMix = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDoNotMix";
  };
 
  module FcDocument = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDocument";
  };
 
  module FcDonate = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDonate";
  };
 
  module FcDoughnutChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDoughnutChart";
  };
 
  module FcDownLeft = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDownLeft";
  };
 
  module FcDownRight = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDownRight";
  };
 
  module FcDown = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDown";
  };
 
  module FcDownload = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDownload";
  };
 
  module FcDribbble = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDribbble";
  };
 
  module FcDvdLogo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcDvdLogo";
  };
 
  module FcEditImage = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEditImage";
  };
 
  module FcElectricalSensor = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcElectricalSensor";
  };
 
  module FcElectricalThreshold = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcElectricalThreshold";
  };
 
  module FcElectricity = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcElectricity";
  };
 
  module FcElectroDevices = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcElectroDevices";
  };
 
  module FcElectronics = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcElectronics";
  };
 
  module FcEmptyBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEmptyBattery";
  };
 
  module FcEmptyFilter = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEmptyFilter";
  };
 
  module FcEmptyTrash = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEmptyTrash";
  };
 
  module FcEndCall = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEndCall";
  };
 
  module FcEngineering = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEngineering";
  };
 
  module FcEnteringHeavenAlive = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcEnteringHeavenAlive";
  };
 
  module FcExpand = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcExpand";
  };
 
  module FcExpired = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcExpired";
  };
 
  module FcExport = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcExport";
  };
 
  module FcExternal = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcExternal";
  };
 
  module FcFactoryBreakdown = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFactoryBreakdown";
  };
 
  module FcFactory = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFactory";
  };
 
  module FcFaq = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFaq";
  };
 
  module FcFeedIn = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFeedIn";
  };
 
  module FcFeedback = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFeedback";
  };
 
  module FcFile = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFile";
  };
 
  module FcFilingCabinet = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFilingCabinet";
  };
 
  module FcFilledFilter = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFilledFilter";
  };
 
  module FcFilmReel = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFilmReel";
  };
 
  module FcFilm = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFilm";
  };
 
  module FcFinePrint = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFinePrint";
  };
 
  module FcFlashAuto = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFlashAuto";
  };
 
  module FcFlashOff = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFlashOff";
  };
 
  module FcFlashOn = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFlashOn";
  };
 
  module FcFlowChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFlowChart";
  };
 
  module FcFolder = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFolder";
  };
 
  module FcFrame = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFrame";
  };
 
  module FcFullBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFullBattery";
  };
 
  module FcFullTrash = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcFullTrash";
  };
 
  module FcGallery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGallery";
  };
 
  module FcGenealogy = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGenealogy";
  };
 
  module FcGenericSortingAsc = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGenericSortingAsc";
  };
 
  module FcGenericSortingDesc = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGenericSortingDesc";
  };
 
  module FcGlobe = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGlobe";
  };
 
  module FcGoodDecision = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGoodDecision";
  };
 
  module FcGoogle = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGoogle";
  };
 
  module FcGraduationCap = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGraduationCap";
  };
 
  module FcGrid = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcGrid";
  };
 
  module FcHeadset = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcHeadset";
  };
 
  module FcHeatMap = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcHeatMap";
  };
 
  module FcHighBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcHighBattery";
  };
 
  module FcHighPriority = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcHighPriority";
  };
 
  module FcHome = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcHome";
  };
 
  module FcIcons8Cup = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcIcons8Cup";
  };
 
  module FcIdea = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcIdea";
  };
 
  module FcImageFile = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcImageFile";
  };
 
  module FcImport = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcImport";
  };
 
  module FcInTransit = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcInTransit";
  };
 
  module FcInfo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcInfo";
  };
 
  module FcInspection = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcInspection";
  };
 
  module FcIntegratedWebcam = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcIntegratedWebcam";
  };
 
  module FcInternal = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcInternal";
  };
 
  module FcInvite = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcInvite";
  };
 
  module FcIpad = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcIpad";
  };
 
  module FcIphone = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcIphone";
  };
 
  module FcKey = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcKey";
  };
 
  module FcKindle = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcKindle";
  };
 
  module FcLandscape = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLandscape";
  };
 
  module FcLeave = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeave";
  };
 
  module FcLeftDown = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeftDown";
  };
 
  module FcLeftDown2 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeftDown2";
  };
 
  module FcLeftUp = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeftUp";
  };
 
  module FcLeftUp2 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeftUp2";
  };
 
  module FcLeft = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLeft";
  };
 
  module FcLibrary = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLibrary";
  };
 
  module FcLightAtTheEndOfTunnel = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLightAtTheEndOfTunnel";
  };
 
  module FcLikePlaceholder = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLikePlaceholder";
  };
 
  module FcLike = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLike";
  };
 
  module FcLineChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLineChart";
  };
 
  module FcLink = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLink";
  };
 
  module FcLinux = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLinux";
  };
 
  module FcList = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcList";
  };
 
  module FcLockLandscape = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLockLandscape";
  };
 
  module FcLockPortrait = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLockPortrait";
  };
 
  module FcLock = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLock";
  };
 
  module FcLowBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLowBattery";
  };
 
  module FcLowPriority = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcLowPriority";
  };
 
  module FcMakeDecision = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMakeDecision";
  };
 
  module FcManager = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcManager";
  };
 
  module FcMediumPriority = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMediumPriority";
  };
 
  module FcMenu = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMenu";
  };
 
  module FcMiddleBattery = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMiddleBattery";
  };
 
  module FcMindMap = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMindMap";
  };
 
  module FcMinus = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMinus";
  };
 
  module FcMissedCall = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMissedCall";
  };
 
  module FcMms = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMms";
  };
 
  module FcMoneyTransfer = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMoneyTransfer";
  };
 
  module FcMultipleCameras = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMultipleCameras";
  };
 
  module FcMultipleDevices = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMultipleDevices";
  };
 
  module FcMultipleInputs = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMultipleInputs";
  };
 
  module FcMultipleSmartphones = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMultipleSmartphones";
  };
 
  module FcMusic = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcMusic";
  };
 
  module FcNegativeDynamic = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNegativeDynamic";
  };
 
  module FcNeutralDecision = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNeutralDecision";
  };
 
  module FcNeutralTrading = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNeutralTrading";
  };
 
  module FcNews = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNews";
  };
 
  module FcNext = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNext";
  };
 
  module FcNfcSign = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNfcSign";
  };
 
  module FcNightLandscape = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNightLandscape";
  };
 
  module FcNightPortrait = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNightPortrait";
  };
 
  module FcNoIdea = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNoIdea";
  };
 
  module FcNoVideo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNoVideo";
  };
 
  module FcNook = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNook";
  };
 
  module FcNumericalSorting12 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNumericalSorting12";
  };
 
  module FcNumericalSorting21 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcNumericalSorting21";
  };
 
  module FcOk = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOk";
  };
 
  module FcOldTimeCamera = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOldTimeCamera";
  };
 
  module FcOnlineSupport = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOnlineSupport";
  };
 
  module FcOpenedFolder = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOpenedFolder";
  };
 
  module FcOrgUnit = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOrgUnit";
  };
 
  module FcOrganization = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOrganization";
  };
 
  module FcOvertime = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcOvertime";
  };
 
  module FcPackage = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPackage";
  };
 
  module FcPaid = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPaid";
  };
 
  module FcPanorama = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPanorama";
  };
 
  module FcParallelTasks = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcParallelTasks";
  };
 
  module FcPhoneAndroid = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPhoneAndroid";
  };
 
  module FcPhone = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPhone";
  };
 
  module FcPhotoReel = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPhotoReel";
  };
 
  module FcPicture = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPicture";
  };
 
  module FcPieChart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPieChart";
  };
 
  module FcPlanner = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPlanner";
  };
 
  module FcPlus = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPlus";
  };
 
  module FcPodiumWithAudience = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPodiumWithAudience";
  };
 
  module FcPodiumWithSpeaker = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPodiumWithSpeaker";
  };
 
  module FcPodiumWithoutSpeaker = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPodiumWithoutSpeaker";
  };
 
  module FcPortraitMode = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPortraitMode";
  };
 
  module FcPositiveDynamic = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPositiveDynamic";
  };
 
  module FcPrevious = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPrevious";
  };
 
  module FcPrint = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPrint";
  };
 
  module FcPrivacy = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPrivacy";
  };
 
  module FcProcess = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcProcess";
  };
 
  module FcPuzzle = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcPuzzle";
  };
 
  module FcQuestions = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcQuestions";
  };
 
  module FcRadarPlot = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRadarPlot";
  };
 
  module FcRating = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRating";
  };
 
  module FcRatings = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRatings";
  };
 
  module FcReadingEbook = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcReadingEbook";
  };
 
  module FcReading = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcReading";
  };
 
  module FcReddit = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcReddit";
  };
 
  module FcRedo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRedo";
  };
 
  module FcRefresh = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRefresh";
  };
 
  module FcRegisteredTrademark = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRegisteredTrademark";
  };
 
  module FcRemoveImage = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRemoveImage";
  };
 
  module FcReuse = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcReuse";
  };
 
  module FcRightDown = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRightDown";
  };
 
  module FcRightDown2 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRightDown2";
  };
 
  module FcRightUp = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRightUp";
  };
 
  module FcRightUp2 = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRightUp2";
  };
 
  module FcRight = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRight";
  };
 
  module FcRotateCamera = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRotateCamera";
  };
 
  module FcRotateToLandscape = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRotateToLandscape";
  };
 
  module FcRotateToPortrait = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRotateToPortrait";
  };
 
  module FcRuler = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRuler";
  };
 
  module FcRules = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcRules";
  };
 
  module FcSafe = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSafe";
  };
 
  module FcSalesPerformance = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSalesPerformance";
  };
 
  module FcScatterPlot = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcScatterPlot";
  };
 
  module FcSearch = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSearch";
  };
 
  module FcSelfServiceKiosk = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSelfServiceKiosk";
  };
 
  module FcSelfie = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSelfie";
  };
 
  module FcSerialTasks = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSerialTasks";
  };
 
  module FcServiceMark = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcServiceMark";
  };
 
  module FcServices = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcServices";
  };
 
  module FcSettings = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSettings";
  };
 
  module FcShare = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcShare";
  };
 
  module FcShipped = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcShipped";
  };
 
  module FcShop = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcShop";
  };
 
  module FcSignature = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSignature";
  };
 
  module FcSimCardChip = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSimCardChip";
  };
 
  module FcSimCard = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSimCard";
  };
 
  module FcSlrBackSide = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSlrBackSide";
  };
 
  module FcSmartphoneTablet = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSmartphoneTablet";
  };
 
  module FcSms = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSms";
  };
 
  module FcSoundRecordingCopyright = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSoundRecordingCopyright";
  };
 
  module FcSpeaker = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSpeaker";
  };
 
  module FcSportsMode = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSportsMode";
  };
 
  module FcStackOfPhotos = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcStackOfPhotos";
  };
 
  module FcStart = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcStart";
  };
 
  module FcStatistics = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcStatistics";
  };
 
  module FcSteam = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSteam";
  };
 
  module FcStumbleupon = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcStumbleupon";
  };
 
  module FcSupport = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSupport";
  };
 
  module FcSurvey = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSurvey";
  };
 
  module FcSwitchCamera = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSwitchCamera";
  };
 
  module FcSynchronize = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcSynchronize";
  };
 
  module FcTabletAndroid = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTabletAndroid";
  };
 
  module FcTemplate = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTemplate";
  };
 
  module FcTimeline = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTimeline";
  };
 
  module FcTodoList = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTodoList";
  };
 
  module FcTouchscreenSmartphone = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTouchscreenSmartphone";
  };
 
  module FcTrademark = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTrademark";
  };
 
  module FcTreeStructure = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTreeStructure";
  };
 
  module FcTwoSmartphones = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcTwoSmartphones";
  };
 
  module FcUndo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUndo";
  };
 
  module FcUnlock = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUnlock";
  };
 
  module FcUpLeft = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUpLeft";
  };
 
  module FcUpRight = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUpRight";
  };
 
  module FcUp = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUp";
  };
 
  module FcUpload = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUpload";
  };
 
  module FcUsb = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcUsb";
  };
 
  module FcVideoCall = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVideoCall";
  };
 
  module FcVideoFile = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVideoFile";
  };
 
  module FcVideoProjector = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVideoProjector";
  };
 
  module FcViewDetails = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcViewDetails";
  };
 
  module FcVip = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVip";
  };
 
  module FcVlc = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVlc";
  };
 
  module FcVoicePresentation = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVoicePresentation";
  };
 
  module FcVoicemail = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcVoicemail";
  };
 
  module FcWebcam = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcWebcam";
  };
 
  module FcWiFiLogo = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcWiFiLogo";
  };
 
  module FcWikipedia = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcWikipedia";
  };
 
  module FcWorkflow = {
    @module("react-icons/fc") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "FcWorkflow";
  };
