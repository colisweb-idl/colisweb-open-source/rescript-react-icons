
  module Md3DRotation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Md3DRotation";
  };
 
  module MdAccessibility = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccessibility";
  };
 
  module MdAccessible = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccessible";
  };
 
  module MdAccountBalance = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccountBalance";
  };
 
  module MdAccountBalanceWallet = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccountBalanceWallet";
  };
 
  module MdAccountBox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccountBox";
  };
 
  module MdAccountCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccountCircle";
  };
 
  module MdAddShoppingCart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddShoppingCart";
  };
 
  module MdAlarm = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAlarm";
  };
 
  module MdAlarmAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAlarmAdd";
  };
 
  module MdAlarmOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAlarmOff";
  };
 
  module MdAlarmOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAlarmOn";
  };
 
  module MdAllOut = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAllOut";
  };
 
  module MdAndroid = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAndroid";
  };
 
  module MdAnnouncement = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAnnouncement";
  };
 
  module MdAspectRatio = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAspectRatio";
  };
 
  module MdAssessment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssessment";
  };
 
  module MdAssignment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignment";
  };
 
  module MdAssignmentInd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignmentInd";
  };
 
  module MdAssignmentLate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignmentLate";
  };
 
  module MdAssignmentReturn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignmentReturn";
  };
 
  module MdAssignmentReturned = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignmentReturned";
  };
 
  module MdAssignmentTurnedIn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssignmentTurnedIn";
  };
 
  module MdAutorenew = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAutorenew";
  };
 
  module MdBackup = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBackup";
  };
 
  module MdBook = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBook";
  };
 
  module MdBookmark = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBookmark";
  };
 
  module MdBookmarkBorder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBookmarkBorder";
  };
 
  module MdBugReport = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBugReport";
  };
 
  module MdBuild = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBuild";
  };
 
  module MdCached = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCached";
  };
 
  module MdCameraEnhance = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCameraEnhance";
  };
 
  module MdCardGiftcard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCardGiftcard";
  };
 
  module MdCardMembership = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCardMembership";
  };
 
  module MdCardTravel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCardTravel";
  };
 
  module MdChangeHistory = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChangeHistory";
  };
 
  module MdCheckCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCheckCircle";
  };
 
  module MdChromeReaderMode = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChromeReaderMode";
  };
 
  module MdClass = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdClass";
  };
 
  module MdCode = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCode";
  };
 
  module MdCompareArrows = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCompareArrows";
  };
 
  module MdCopyright = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCopyright";
  };
 
  module MdCreditCard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCreditCard";
  };
 
  module MdDashboard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDashboard";
  };
 
  module MdDateRange = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDateRange";
  };
 
  module MdDelete = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDelete";
  };
 
  module MdDeleteForever = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDeleteForever";
  };
 
  module MdDescription = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDescription";
  };
 
  module MdDns = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDns";
  };
 
  module MdDone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDone";
  };
 
  module MdDoneAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDoneAll";
  };
 
  module MdDonutLarge = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDonutLarge";
  };
 
  module MdDonutSmall = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDonutSmall";
  };
 
  module MdEject = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEject";
  };
 
  module MdEuroSymbol = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEuroSymbol";
  };
 
  module MdEvent = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEvent";
  };
 
  module MdEventSeat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEventSeat";
  };
 
  module MdExitToApp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExitToApp";
  };
 
  module MdExplore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExplore";
  };
 
  module MdExtension = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExtension";
  };
 
  module MdFace = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFace";
  };
 
  module MdFavorite = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFavorite";
  };
 
  module MdFavoriteBorder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFavoriteBorder";
  };
 
  module MdFeedback = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFeedback";
  };
 
  module MdFindInPage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFindInPage";
  };
 
  module MdFindReplace = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFindReplace";
  };
 
  module MdFingerprint = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFingerprint";
  };
 
  module MdFlightLand = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlightLand";
  };
 
  module MdFlightTakeoff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlightTakeoff";
  };
 
  module MdFlipToBack = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlipToBack";
  };
 
  module MdFlipToFront = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlipToFront";
  };
 
  module MdGTranslate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGTranslate";
  };
 
  module MdGavel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGavel";
  };
 
  module MdGetApp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGetApp";
  };
 
  module MdGif = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGif";
  };
 
  module MdGrade = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGrade";
  };
 
  module MdGroupWork = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGroupWork";
  };
 
  module MdHelp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHelp";
  };
 
  module MdHelpOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHelpOutline";
  };
 
  module MdHighlightOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHighlightOff";
  };
 
  module MdHistory = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHistory";
  };
 
  module MdHome = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHome";
  };
 
  module MdHourglassEmpty = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHourglassEmpty";
  };
 
  module MdHourglassFull = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHourglassFull";
  };
 
  module MdHttp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHttp";
  };
 
  module MdHttps = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHttps";
  };
 
  module MdImportantDevices = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdImportantDevices";
  };
 
  module MdInfo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInfo";
  };
 
  module MdInfoOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInfoOutline";
  };
 
  module MdInput = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInput";
  };
 
  module MdInvertColors = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInvertColors";
  };
 
  module MdLabel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLabel";
  };
 
  module MdLabelOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLabelOutline";
  };
 
  module MdLanguage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLanguage";
  };
 
  module MdLaunch = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLaunch";
  };
 
  module MdLightbulbOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLightbulbOutline";
  };
 
  module MdLineStyle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLineStyle";
  };
 
  module MdLineWeight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLineWeight";
  };
 
  module MdList = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdList";
  };
 
  module MdLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLock";
  };
 
  module MdLockOpen = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLockOpen";
  };
 
  module MdLockOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLockOutline";
  };
 
  module MdLoyalty = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLoyalty";
  };
 
  module MdMarkunreadMailbox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMarkunreadMailbox";
  };
 
  module MdMotorcycle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMotorcycle";
  };
 
  module MdNoteAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNoteAdd";
  };
 
  module MdOfflinePin = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOfflinePin";
  };
 
  module MdOpacity = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOpacity";
  };
 
  module MdOpenInBrowser = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOpenInBrowser";
  };
 
  module MdOpenInNew = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOpenInNew";
  };
 
  module MdOpenWith = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOpenWith";
  };
 
  module MdPageview = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPageview";
  };
 
  module MdPanTool = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanTool";
  };
 
  module MdPayment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPayment";
  };
 
  module MdPermCameraMic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermCameraMic";
  };
 
  module MdPermContactCalendar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermContactCalendar";
  };
 
  module MdPermDataSetting = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermDataSetting";
  };
 
  module MdPermDeviceInformation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermDeviceInformation";
  };
 
  module MdPermIdentity = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermIdentity";
  };
 
  module MdPermMedia = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermMedia";
  };
 
  module MdPermPhoneMsg = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermPhoneMsg";
  };
 
  module MdPermScanWifi = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPermScanWifi";
  };
 
  module MdPets = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPets";
  };
 
  module MdPictureInPicture = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPictureInPicture";
  };
 
  module MdPictureInPictureAlt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPictureInPictureAlt";
  };
 
  module MdPlayForWork = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlayForWork";
  };
 
  module MdPolymer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPolymer";
  };
 
  module MdPowerSettingsNew = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPowerSettingsNew";
  };
 
  module MdPregnantWoman = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPregnantWoman";
  };
 
  module MdPrint = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPrint";
  };
 
  module MdQueryBuilder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdQueryBuilder";
  };
 
  module MdQuestionAnswer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdQuestionAnswer";
  };
 
  module MdReceipt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReceipt";
  };
 
  module MdRecordVoiceOver = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRecordVoiceOver";
  };
 
  module MdRedeem = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRedeem";
  };
 
  module MdRemoveShoppingCart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemoveShoppingCart";
  };
 
  module MdReorder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReorder";
  };
 
  module MdReportProblem = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReportProblem";
  };
 
  module MdRestore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRestore";
  };
 
  module MdRestorePage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRestorePage";
  };
 
  module MdRoom = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRoom";
  };
 
  module MdRoundedCorner = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRoundedCorner";
  };
 
  module MdRowing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRowing";
  };
 
  module MdSchedule = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSchedule";
  };
 
  module MdSearch = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSearch";
  };
 
  module MdSettings = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettings";
  };
 
  module MdSettingsApplications = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsApplications";
  };
 
  module MdSettingsBackupRestore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsBackupRestore";
  };
 
  module MdSettingsBluetooth = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsBluetooth";
  };
 
  module MdSettingsBrightness = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsBrightness";
  };
 
  module MdSettingsCell = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsCell";
  };
 
  module MdSettingsEthernet = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsEthernet";
  };
 
  module MdSettingsInputAntenna = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsInputAntenna";
  };
 
  module MdSettingsInputComponent = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsInputComponent";
  };
 
  module MdSettingsInputComposite = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsInputComposite";
  };
 
  module MdSettingsInputHdmi = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsInputHdmi";
  };
 
  module MdSettingsInputSvideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsInputSvideo";
  };
 
  module MdSettingsOverscan = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsOverscan";
  };
 
  module MdSettingsPhone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsPhone";
  };
 
  module MdSettingsPower = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsPower";
  };
 
  module MdSettingsRemote = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsRemote";
  };
 
  module MdSettingsVoice = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsVoice";
  };
 
  module MdShop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShop";
  };
 
  module MdShopTwo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShopTwo";
  };
 
  module MdShoppingBasket = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShoppingBasket";
  };
 
  module MdShoppingCart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShoppingCart";
  };
 
  module MdSpeakerNotes = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpeakerNotes";
  };
 
  module MdSpeakerNotesOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpeakerNotesOff";
  };
 
  module MdSpellcheck = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpellcheck";
  };
 
  module MdStars = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStars";
  };
 
  module MdStore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStore";
  };
 
  module MdSubject = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubject";
  };
 
  module MdSupervisorAccount = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSupervisorAccount";
  };
 
  module MdSwapHoriz = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwapHoriz";
  };
 
  module MdSwapVert = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwapVert";
  };
 
  module MdSwapVerticalCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwapVerticalCircle";
  };
 
  module MdSystemUpdateAlt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSystemUpdateAlt";
  };
 
  module MdTab = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTab";
  };
 
  module MdTabUnselected = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTabUnselected";
  };
 
  module MdTheaters = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTheaters";
  };
 
  module MdThumbDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdThumbDown";
  };
 
  module MdThumbUp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdThumbUp";
  };
 
  module MdThumbsUpDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdThumbsUpDown";
  };
 
  module MdTimeline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimeline";
  };
 
  module MdToc = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdToc";
  };
 
  module MdToday = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdToday";
  };
 
  module MdToll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdToll";
  };
 
  module MdTouchApp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTouchApp";
  };
 
  module MdTrackChanges = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTrackChanges";
  };
 
  module MdTranslate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTranslate";
  };
 
  module MdTrendingDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTrendingDown";
  };
 
  module MdTrendingFlat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTrendingFlat";
  };
 
  module MdTrendingUp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTrendingUp";
  };
 
  module MdTurnedIn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTurnedIn";
  };
 
  module MdTurnedInNot = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTurnedInNot";
  };
 
  module MdUpdate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUpdate";
  };
 
  module MdVerifiedUser = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVerifiedUser";
  };
 
  module MdViewAgenda = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewAgenda";
  };
 
  module MdViewArray = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewArray";
  };
 
  module MdViewCarousel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewCarousel";
  };
 
  module MdViewColumn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewColumn";
  };
 
  module MdViewDay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewDay";
  };
 
  module MdViewHeadline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewHeadline";
  };
 
  module MdViewList = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewList";
  };
 
  module MdViewModule = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewModule";
  };
 
  module MdViewQuilt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewQuilt";
  };
 
  module MdViewStream = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewStream";
  };
 
  module MdViewWeek = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewWeek";
  };
 
  module MdVisibility = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVisibility";
  };
 
  module MdVisibilityOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVisibilityOff";
  };
 
  module MdWatchLater = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWatchLater";
  };
 
  module MdWork = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWork";
  };
 
  module MdYoutubeSearchedFor = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdYoutubeSearchedFor";
  };
 
  module MdZoomIn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdZoomIn";
  };
 
  module MdZoomOut = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdZoomOut";
  };
 
  module MdAddAlert = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddAlert";
  };
 
  module MdError = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdError";
  };
 
  module MdErrorOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdErrorOutline";
  };
 
  module MdWarning = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWarning";
  };
 
  module MdAddToQueue = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddToQueue";
  };
 
  module MdAirplay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirplay";
  };
 
  module MdAlbum = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAlbum";
  };
 
  module MdArtTrack = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArtTrack";
  };
 
  module MdAvTimer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAvTimer";
  };
 
  module MdBrandingWatermark = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrandingWatermark";
  };
 
  module MdCallToAction = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallToAction";
  };
 
  module MdClosedCaption = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdClosedCaption";
  };
 
  module MdEqualizer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEqualizer";
  };
 
  module MdExplicit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExplicit";
  };
 
  module MdFastForward = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFastForward";
  };
 
  module MdFastRewind = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFastRewind";
  };
 
  module MdFeaturedPlayList = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFeaturedPlayList";
  };
 
  module MdFeaturedVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFeaturedVideo";
  };
 
  module MdFiberDvr = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFiberDvr";
  };
 
  module MdFiberManualRecord = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFiberManualRecord";
  };
 
  module MdFiberNew = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFiberNew";
  };
 
  module MdFiberPin = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFiberPin";
  };
 
  module MdFiberSmartRecord = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFiberSmartRecord";
  };
 
  module MdForward10 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdForward10";
  };
 
  module MdForward30 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdForward30";
  };
 
  module MdForward5 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdForward5";
  };
 
  module MdGames = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGames";
  };
 
  module MdHd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHd";
  };
 
  module MdHearing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHearing";
  };
 
  module MdHighQuality = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHighQuality";
  };
 
  module MdLibraryAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLibraryAdd";
  };
 
  module MdLibraryBooks = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLibraryBooks";
  };
 
  module MdLibraryMusic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLibraryMusic";
  };
 
  module MdLoop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLoop";
  };
 
  module MdMic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMic";
  };
 
  module MdMicNone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMicNone";
  };
 
  module MdMicOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMicOff";
  };
 
  module MdMovie = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMovie";
  };
 
  module MdMusicVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMusicVideo";
  };
 
  module MdNewReleases = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNewReleases";
  };
 
  module MdNotInterested = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotInterested";
  };
 
  module MdNote = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNote";
  };
 
  module MdPause = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPause";
  };
 
  module MdPauseCircleFilled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPauseCircleFilled";
  };
 
  module MdPauseCircleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPauseCircleOutline";
  };
 
  module MdPlayArrow = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlayArrow";
  };
 
  module MdPlayCircleFilled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlayCircleFilled";
  };
 
  module MdPlayCircleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlayCircleOutline";
  };
 
  module MdPlaylistAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlaylistAdd";
  };
 
  module MdPlaylistAddCheck = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlaylistAddCheck";
  };
 
  module MdPlaylistPlay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlaylistPlay";
  };
 
  module MdQueue = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdQueue";
  };
 
  module MdQueueMusic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdQueueMusic";
  };
 
  module MdQueuePlayNext = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdQueuePlayNext";
  };
 
  module MdRadio = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRadio";
  };
 
  module MdRecentActors = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRecentActors";
  };
 
  module MdRemoveFromQueue = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemoveFromQueue";
  };
 
  module MdRepeat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRepeat";
  };
 
  module MdRepeatOne = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRepeatOne";
  };
 
  module MdReplay10 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReplay10";
  };
 
  module MdReplay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReplay";
  };
 
  module MdReplay30 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReplay30";
  };
 
  module MdReplay5 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReplay5";
  };
 
  module MdShuffle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShuffle";
  };
 
  module MdSkipNext = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSkipNext";
  };
 
  module MdSkipPrevious = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSkipPrevious";
  };
 
  module MdSlowMotionVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSlowMotionVideo";
  };
 
  module MdSnooze = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSnooze";
  };
 
  module MdSortByAlpha = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSortByAlpha";
  };
 
  module MdStop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStop";
  };
 
  module MdSubscriptions = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubscriptions";
  };
 
  module MdSubtitles = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubtitles";
  };
 
  module MdSurroundSound = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSurroundSound";
  };
 
  module MdVideoCall = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideoCall";
  };
 
  module MdVideoLabel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideoLabel";
  };
 
  module MdVideoLibrary = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideoLibrary";
  };
 
  module MdVideocam = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideocam";
  };
 
  module MdVideocamOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideocamOff";
  };
 
  module MdVolumeDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVolumeDown";
  };
 
  module MdVolumeMute = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVolumeMute";
  };
 
  module MdVolumeOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVolumeOff";
  };
 
  module MdVolumeUp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVolumeUp";
  };
 
  module MdWeb = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWeb";
  };
 
  module MdWebAsset = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWebAsset";
  };
 
  module MdBusiness = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBusiness";
  };
 
  module MdCall = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCall";
  };
 
  module MdCallEnd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallEnd";
  };
 
  module MdCallMade = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallMade";
  };
 
  module MdCallMerge = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallMerge";
  };
 
  module MdCallMissed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallMissed";
  };
 
  module MdCallMissedOutgoing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallMissedOutgoing";
  };
 
  module MdCallReceived = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallReceived";
  };
 
  module MdCallSplit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCallSplit";
  };
 
  module MdChat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChat";
  };
 
  module MdChatBubble = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChatBubble";
  };
 
  module MdChatBubbleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChatBubbleOutline";
  };
 
  module MdClearAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdClearAll";
  };
 
  module MdComment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdComment";
  };
 
  module MdContactMail = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContactMail";
  };
 
  module MdContactPhone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContactPhone";
  };
 
  module MdContacts = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContacts";
  };
 
  module MdDialerSip = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDialerSip";
  };
 
  module MdDialpad = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDialpad";
  };
 
  module MdEmail = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEmail";
  };
 
  module MdForum = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdForum";
  };
 
  module MdImportContacts = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdImportContacts";
  };
 
  module MdImportExport = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdImportExport";
  };
 
  module MdInvertColorsOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInvertColorsOff";
  };
 
  module MdLiveHelp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLiveHelp";
  };
 
  module MdLocationOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocationOff";
  };
 
  module MdLocationOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocationOn";
  };
 
  module MdMailOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMailOutline";
  };
 
  module MdMessage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMessage";
  };
 
  module MdNoSim = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNoSim";
  };
 
  module MdPhone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhone";
  };
 
  module MdPhonelinkErase = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelinkErase";
  };
 
  module MdPhonelinkLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelinkLock";
  };
 
  module MdPhonelinkRing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelinkRing";
  };
 
  module MdPhonelinkSetup = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelinkSetup";
  };
 
  module MdPortableWifiOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPortableWifiOff";
  };
 
  module MdPresentToAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPresentToAll";
  };
 
  module MdRingVolume = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRingVolume";
  };
 
  module MdRssFeed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRssFeed";
  };
 
  module MdScreenShare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScreenShare";
  };
 
  module MdSpeakerPhone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpeakerPhone";
  };
 
  module MdStayCurrentLandscape = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStayCurrentLandscape";
  };
 
  module MdStayCurrentPortrait = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStayCurrentPortrait";
  };
 
  module MdStayPrimaryLandscape = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStayPrimaryLandscape";
  };
 
  module MdStayPrimaryPortrait = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStayPrimaryPortrait";
  };
 
  module MdStopScreenShare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStopScreenShare";
  };
 
  module MdSwapCalls = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwapCalls";
  };
 
  module MdTextsms = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTextsms";
  };
 
  module MdVoicemail = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVoicemail";
  };
 
  module MdVpnKey = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVpnKey";
  };
 
  module MdAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAdd";
  };
 
  module MdAddBox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddBox";
  };
 
  module MdAddCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddCircle";
  };
 
  module MdAddCircleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddCircleOutline";
  };
 
  module MdArchive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArchive";
  };
 
  module MdBackspace = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBackspace";
  };
 
  module MdBlock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBlock";
  };
 
  module MdClear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdClear";
  };
 
  module MdContentCopy = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContentCopy";
  };
 
  module MdContentCut = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContentCut";
  };
 
  module MdContentPaste = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdContentPaste";
  };
 
  module MdCreate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCreate";
  };
 
  module MdDeleteSweep = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDeleteSweep";
  };
 
  module MdDrafts = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDrafts";
  };
 
  module MdFilterList = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterList";
  };
 
  module MdFlag = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlag";
  };
 
  module MdFontDownload = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFontDownload";
  };
 
  module MdForward = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdForward";
  };
 
  module MdGesture = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGesture";
  };
 
  module MdInbox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInbox";
  };
 
  module MdLink = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLink";
  };
 
  module MdLowPriority = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLowPriority";
  };
 
  module MdMail = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMail";
  };
 
  module MdMarkunread = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMarkunread";
  };
 
  module MdMoveToInbox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMoveToInbox";
  };
 
  module MdNextWeek = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNextWeek";
  };
 
  module MdRedo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRedo";
  };
 
  module MdRemove = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemove";
  };
 
  module MdRemoveCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemoveCircle";
  };
 
  module MdRemoveCircleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemoveCircleOutline";
  };
 
  module MdReply = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReply";
  };
 
  module MdReplyAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReplyAll";
  };
 
  module MdReport = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdReport";
  };
 
  module MdSave = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSave";
  };
 
  module MdSelectAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSelectAll";
  };
 
  module MdSend = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSend";
  };
 
  module MdSort = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSort";
  };
 
  module MdTextFormat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTextFormat";
  };
 
  module MdUnarchive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUnarchive";
  };
 
  module MdUndo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUndo";
  };
 
  module MdWeekend = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWeekend";
  };
 
  module MdAccessAlarm = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccessAlarm";
  };
 
  module MdAccessAlarms = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccessAlarms";
  };
 
  module MdAccessTime = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAccessTime";
  };
 
  module MdAddAlarm = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddAlarm";
  };
 
  module MdAirplanemodeActive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirplanemodeActive";
  };
 
  module MdAirplanemodeInactive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirplanemodeInactive";
  };
 
  module MdBattery20 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery20";
  };
 
  module MdBattery30 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery30";
  };
 
  module MdBattery50 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery50";
  };
 
  module MdBattery60 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery60";
  };
 
  module MdBattery80 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery80";
  };
 
  module MdBattery90 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBattery90";
  };
 
  module MdBatteryAlert = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryAlert";
  };
 
  module MdBatteryCharging20 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging20";
  };
 
  module MdBatteryCharging30 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging30";
  };
 
  module MdBatteryCharging50 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging50";
  };
 
  module MdBatteryCharging60 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging60";
  };
 
  module MdBatteryCharging80 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging80";
  };
 
  module MdBatteryCharging90 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryCharging90";
  };
 
  module MdBatteryChargingFull = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryChargingFull";
  };
 
  module MdBatteryFull = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryFull";
  };
 
  module MdBatteryStd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryStd";
  };
 
  module MdBatteryUnknown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBatteryUnknown";
  };
 
  module MdBluetooth = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBluetooth";
  };
 
  module MdBluetoothConnected = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBluetoothConnected";
  };
 
  module MdBluetoothDisabled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBluetoothDisabled";
  };
 
  module MdBluetoothSearching = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBluetoothSearching";
  };
 
  module MdBrightnessAuto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightnessAuto";
  };
 
  module MdBrightnessHigh = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightnessHigh";
  };
 
  module MdBrightnessLow = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightnessLow";
  };
 
  module MdBrightnessMedium = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightnessMedium";
  };
 
  module MdDataUsage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDataUsage";
  };
 
  module MdDeveloperMode = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDeveloperMode";
  };
 
  module MdDevices = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDevices";
  };
 
  module MdDvr = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDvr";
  };
 
  module MdGpsFixed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGpsFixed";
  };
 
  module MdGpsNotFixed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGpsNotFixed";
  };
 
  module MdGpsOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGpsOff";
  };
 
  module MdGraphicEq = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGraphicEq";
  };
 
  module MdLocationDisabled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocationDisabled";
  };
 
  module MdLocationSearching = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocationSearching";
  };
 
  module MdNetworkCell = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNetworkCell";
  };
 
  module MdNetworkWifi = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNetworkWifi";
  };
 
  module MdNfc = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNfc";
  };
 
  module MdScreenLockLandscape = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScreenLockLandscape";
  };
 
  module MdScreenLockPortrait = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScreenLockPortrait";
  };
 
  module MdScreenLockRotation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScreenLockRotation";
  };
 
  module MdScreenRotation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScreenRotation";
  };
 
  module MdSdStorage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSdStorage";
  };
 
  module MdSettingsSystemDaydream = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSettingsSystemDaydream";
  };
 
  module MdSignalCellular0Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellular0Bar";
  };
 
  module MdSignalCellular1Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellular1Bar";
  };
 
  module MdSignalCellular2Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellular2Bar";
  };
 
  module MdSignalCellular3Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellular3Bar";
  };
 
  module MdSignalCellular4Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellular4Bar";
  };
 
  module MdSignalCellularConnectedNoInternet0Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularConnectedNoInternet0Bar";
  };
 
  module MdSignalCellularConnectedNoInternet1Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularConnectedNoInternet1Bar";
  };
 
  module MdSignalCellularConnectedNoInternet2Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularConnectedNoInternet2Bar";
  };
 
  module MdSignalCellularConnectedNoInternet3Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularConnectedNoInternet3Bar";
  };
 
  module MdSignalCellularConnectedNoInternet4Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularConnectedNoInternet4Bar";
  };
 
  module MdSignalCellularNoSim = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularNoSim";
  };
 
  module MdSignalCellularNull = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularNull";
  };
 
  module MdSignalCellularOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalCellularOff";
  };
 
  module MdSignalWifi0Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi0Bar";
  };
 
  module MdSignalWifi1Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi1Bar";
  };
 
  module MdSignalWifi1BarLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi1BarLock";
  };
 
  module MdSignalWifi2Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi2Bar";
  };
 
  module MdSignalWifi2BarLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi2BarLock";
  };
 
  module MdSignalWifi3Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi3Bar";
  };
 
  module MdSignalWifi3BarLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi3BarLock";
  };
 
  module MdSignalWifi4Bar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi4Bar";
  };
 
  module MdSignalWifi4BarLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifi4BarLock";
  };
 
  module MdSignalWifiOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSignalWifiOff";
  };
 
  module MdStorage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStorage";
  };
 
  module MdUsb = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUsb";
  };
 
  module MdWallpaper = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWallpaper";
  };
 
  module MdWidgets = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWidgets";
  };
 
  module MdWifiLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWifiLock";
  };
 
  module MdWifiTethering = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWifiTethering";
  };
 
  module MdAttachFile = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAttachFile";
  };
 
  module MdAttachMoney = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAttachMoney";
  };
 
  module MdBorderAll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderAll";
  };
 
  module MdBorderBottom = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderBottom";
  };
 
  module MdBorderClear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderClear";
  };
 
  module MdBorderColor = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderColor";
  };
 
  module MdBorderHorizontal = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderHorizontal";
  };
 
  module MdBorderInner = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderInner";
  };
 
  module MdBorderLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderLeft";
  };
 
  module MdBorderOuter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderOuter";
  };
 
  module MdBorderRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderRight";
  };
 
  module MdBorderStyle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderStyle";
  };
 
  module MdBorderTop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderTop";
  };
 
  module MdBorderVertical = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBorderVertical";
  };
 
  module MdBubbleChart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBubbleChart";
  };
 
  module MdDragHandle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDragHandle";
  };
 
  module MdFormatAlignCenter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatAlignCenter";
  };
 
  module MdFormatAlignJustify = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatAlignJustify";
  };
 
  module MdFormatAlignLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatAlignLeft";
  };
 
  module MdFormatAlignRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatAlignRight";
  };
 
  module MdFormatBold = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatBold";
  };
 
  module MdFormatClear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatClear";
  };
 
  module MdFormatColorFill = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatColorFill";
  };
 
  module MdFormatColorReset = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatColorReset";
  };
 
  module MdFormatColorText = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatColorText";
  };
 
  module MdFormatIndentDecrease = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatIndentDecrease";
  };
 
  module MdFormatIndentIncrease = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatIndentIncrease";
  };
 
  module MdFormatItalic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatItalic";
  };
 
  module MdFormatLineSpacing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatLineSpacing";
  };
 
  module MdFormatListBulleted = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatListBulleted";
  };
 
  module MdFormatListNumbered = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatListNumbered";
  };
 
  module MdFormatPaint = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatPaint";
  };
 
  module MdFormatQuote = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatQuote";
  };
 
  module MdFormatShapes = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatShapes";
  };
 
  module MdFormatSize = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatSize";
  };
 
  module MdFormatStrikethrough = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatStrikethrough";
  };
 
  module MdFormatTextdirectionLToR = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatTextdirectionLToR";
  };
 
  module MdFormatTextdirectionRToL = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatTextdirectionRToL";
  };
 
  module MdFormatUnderlined = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFormatUnderlined";
  };
 
  module MdFunctions = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFunctions";
  };
 
  module MdHighlight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHighlight";
  };
 
  module MdInsertChart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertChart";
  };
 
  module MdInsertComment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertComment";
  };
 
  module MdInsertDriveFile = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertDriveFile";
  };
 
  module MdInsertEmoticon = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertEmoticon";
  };
 
  module MdInsertInvitation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertInvitation";
  };
 
  module MdInsertLink = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertLink";
  };
 
  module MdInsertPhoto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdInsertPhoto";
  };
 
  module MdLinearScale = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLinearScale";
  };
 
  module MdMergeType = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMergeType";
  };
 
  module MdModeComment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdModeComment";
  };
 
  module MdModeEdit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdModeEdit";
  };
 
  module MdMonetizationOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMonetizationOn";
  };
 
  module MdMoneyOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMoneyOff";
  };
 
  module MdMultilineChart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMultilineChart";
  };
 
  module MdPieChart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPieChart";
  };
 
  module MdPieChartOutlined = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPieChartOutlined";
  };
 
  module MdPublish = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPublish";
  };
 
  module MdShortText = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShortText";
  };
 
  module MdShowChart = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShowChart";
  };
 
  module MdSpaceBar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpaceBar";
  };
 
  module MdStrikethroughS = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStrikethroughS";
  };
 
  module MdTextFields = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTextFields";
  };
 
  module MdTitle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTitle";
  };
 
  module MdVerticalAlignBottom = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVerticalAlignBottom";
  };
 
  module MdVerticalAlignCenter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVerticalAlignCenter";
  };
 
  module MdVerticalAlignTop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVerticalAlignTop";
  };
 
  module MdWrapText = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWrapText";
  };
 
  module MdAttachment = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAttachment";
  };
 
  module MdCloud = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloud";
  };
 
  module MdCloudCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudCircle";
  };
 
  module MdCloudDone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudDone";
  };
 
  module MdCloudDownload = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudDownload";
  };
 
  module MdCloudOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudOff";
  };
 
  module MdCloudQueue = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudQueue";
  };
 
  module MdCloudUpload = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCloudUpload";
  };
 
  module MdCreateNewFolder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCreateNewFolder";
  };
 
  module MdFileDownload = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFileDownload";
  };
 
  module MdFileUpload = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFileUpload";
  };
 
  module MdFolder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFolder";
  };
 
  module MdFolderOpen = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFolderOpen";
  };
 
  module MdFolderShared = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFolderShared";
  };
 
  module MdCast = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCast";
  };
 
  module MdCastConnected = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCastConnected";
  };
 
  module MdComputer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdComputer";
  };
 
  module MdDesktopMac = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDesktopMac";
  };
 
  module MdDesktopWindows = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDesktopWindows";
  };
 
  module MdDeveloperBoard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDeveloperBoard";
  };
 
  module MdDeviceHub = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDeviceHub";
  };
 
  module MdDevicesOther = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDevicesOther";
  };
 
  module MdDock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDock";
  };
 
  module MdGamepad = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGamepad";
  };
 
  module MdHeadset = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHeadset";
  };
 
  module MdHeadsetMic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHeadsetMic";
  };
 
  module MdKeyboard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboard";
  };
 
  module MdKeyboardArrowDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardArrowDown";
  };
 
  module MdKeyboardArrowLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardArrowLeft";
  };
 
  module MdKeyboardArrowRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardArrowRight";
  };
 
  module MdKeyboardArrowUp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardArrowUp";
  };
 
  module MdKeyboardBackspace = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardBackspace";
  };
 
  module MdKeyboardCapslock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardCapslock";
  };
 
  module MdKeyboardHide = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardHide";
  };
 
  module MdKeyboardReturn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardReturn";
  };
 
  module MdKeyboardTab = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardTab";
  };
 
  module MdKeyboardVoice = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKeyboardVoice";
  };
 
  module MdLaptop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLaptop";
  };
 
  module MdLaptopChromebook = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLaptopChromebook";
  };
 
  module MdLaptopMac = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLaptopMac";
  };
 
  module MdLaptopWindows = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLaptopWindows";
  };
 
  module MdMemory = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMemory";
  };
 
  module MdMouse = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMouse";
  };
 
  module MdPhoneAndroid = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneAndroid";
  };
 
  module MdPhoneIphone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneIphone";
  };
 
  module MdPhonelink = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelink";
  };
 
  module MdPhonelinkOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonelinkOff";
  };
 
  module MdPowerInput = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPowerInput";
  };
 
  module MdRouter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRouter";
  };
 
  module MdScanner = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdScanner";
  };
 
  module MdSecurity = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSecurity";
  };
 
  module MdSimCard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSimCard";
  };
 
  module MdSmartphone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSmartphone";
  };
 
  module MdSpeaker = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpeaker";
  };
 
  module MdSpeakerGroup = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpeakerGroup";
  };
 
  module MdTablet = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTablet";
  };
 
  module MdTabletAndroid = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTabletAndroid";
  };
 
  module MdTabletMac = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTabletMac";
  };
 
  module MdToys = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdToys";
  };
 
  module MdTv = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTv";
  };
 
  module MdVideogameAsset = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVideogameAsset";
  };
 
  module MdWatch = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWatch";
  };
 
  module MdAddAPhoto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddAPhoto";
  };
 
  module MdAddToPhotos = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddToPhotos";
  };
 
  module MdAdjust = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAdjust";
  };
 
  module MdAssistant = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssistant";
  };
 
  module MdAssistantPhoto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAssistantPhoto";
  };
 
  module MdAudiotrack = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAudiotrack";
  };
 
  module MdBlurCircular = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBlurCircular";
  };
 
  module MdBlurLinear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBlurLinear";
  };
 
  module MdBlurOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBlurOff";
  };
 
  module MdBlurOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBlurOn";
  };
 
  module MdBrightness1 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness1";
  };
 
  module MdBrightness2 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness2";
  };
 
  module MdBrightness3 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness3";
  };
 
  module MdBrightness4 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness4";
  };
 
  module MdBrightness5 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness5";
  };
 
  module MdBrightness6 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness6";
  };
 
  module MdBrightness7 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrightness7";
  };
 
  module MdBrokenImage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrokenImage";
  };
 
  module MdBrush = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBrush";
  };
 
  module MdBurstMode = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBurstMode";
  };
 
  module MdCamera = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCamera";
  };
 
  module MdCameraAlt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCameraAlt";
  };
 
  module MdCameraFront = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCameraFront";
  };
 
  module MdCameraRear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCameraRear";
  };
 
  module MdCameraRoll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCameraRoll";
  };
 
  module MdCenterFocusStrong = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCenterFocusStrong";
  };
 
  module MdCenterFocusWeak = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCenterFocusWeak";
  };
 
  module MdCollections = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCollections";
  };
 
  module MdCollectionsBookmark = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCollectionsBookmark";
  };
 
  module MdColorLens = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdColorLens";
  };
 
  module MdColorize = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdColorize";
  };
 
  module MdCompare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCompare";
  };
 
  module MdControlPoint = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdControlPoint";
  };
 
  module MdControlPointDuplicate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdControlPointDuplicate";
  };
 
  module MdCrop169 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCrop169";
  };
 
  module MdCrop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCrop";
  };
 
  module MdCrop32 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCrop32";
  };
 
  module MdCrop54 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCrop54";
  };
 
  module MdCrop75 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCrop75";
  };
 
  module MdCropDin = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropDin";
  };
 
  module MdCropFree = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropFree";
  };
 
  module MdCropLandscape = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropLandscape";
  };
 
  module MdCropOriginal = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropOriginal";
  };
 
  module MdCropPortrait = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropPortrait";
  };
 
  module MdCropRotate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropRotate";
  };
 
  module MdCropSquare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCropSquare";
  };
 
  module MdDehaze = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDehaze";
  };
 
  module MdDetails = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDetails";
  };
 
  module MdEdit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEdit";
  };
 
  module MdExposure = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposure";
  };
 
  module MdExposureNeg1 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposureNeg1";
  };
 
  module MdExposureNeg2 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposureNeg2";
  };
 
  module MdExposurePlus1 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposurePlus1";
  };
 
  module MdExposurePlus2 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposurePlus2";
  };
 
  module MdExposureZero = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExposureZero";
  };
 
  module MdFilter1 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter1";
  };
 
  module MdFilter2 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter2";
  };
 
  module MdFilter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter";
  };
 
  module MdFilter3 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter3";
  };
 
  module MdFilter4 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter4";
  };
 
  module MdFilter5 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter5";
  };
 
  module MdFilter6 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter6";
  };
 
  module MdFilter7 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter7";
  };
 
  module MdFilter8 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter8";
  };
 
  module MdFilter9 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter9";
  };
 
  module MdFilter9Plus = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilter9Plus";
  };
 
  module MdFilterBAndW = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterBAndW";
  };
 
  module MdFilterCenterFocus = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterCenterFocus";
  };
 
  module MdFilterDrama = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterDrama";
  };
 
  module MdFilterFrames = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterFrames";
  };
 
  module MdFilterHdr = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterHdr";
  };
 
  module MdFilterNone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterNone";
  };
 
  module MdFilterTiltShift = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterTiltShift";
  };
 
  module MdFilterVintage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFilterVintage";
  };
 
  module MdFlare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlare";
  };
 
  module MdFlashAuto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlashAuto";
  };
 
  module MdFlashOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlashOff";
  };
 
  module MdFlashOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlashOn";
  };
 
  module MdFlip = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlip";
  };
 
  module MdGradient = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGradient";
  };
 
  module MdGrain = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGrain";
  };
 
  module MdGridOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGridOff";
  };
 
  module MdGridOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGridOn";
  };
 
  module MdHdrOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHdrOff";
  };
 
  module MdHdrOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHdrOn";
  };
 
  module MdHdrStrong = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHdrStrong";
  };
 
  module MdHdrWeak = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHdrWeak";
  };
 
  module MdHealing = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHealing";
  };
 
  module MdImage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdImage";
  };
 
  module MdImageAspectRatio = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdImageAspectRatio";
  };
 
  module MdIso = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdIso";
  };
 
  module MdLandscape = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLandscape";
  };
 
  module MdLeakAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLeakAdd";
  };
 
  module MdLeakRemove = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLeakRemove";
  };
 
  module MdLens = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLens";
  };
 
  module MdLinkedCamera = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLinkedCamera";
  };
 
  module MdLooks = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooks";
  };
 
  module MdLooks3 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooks3";
  };
 
  module MdLooks4 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooks4";
  };
 
  module MdLooks5 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooks5";
  };
 
  module MdLooks6 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooks6";
  };
 
  module MdLooksOne = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooksOne";
  };
 
  module MdLooksTwo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLooksTwo";
  };
 
  module MdLoupe = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLoupe";
  };
 
  module MdMonochromePhotos = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMonochromePhotos";
  };
 
  module MdMovieCreation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMovieCreation";
  };
 
  module MdMovieFilter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMovieFilter";
  };
 
  module MdMusicNote = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMusicNote";
  };
 
  module MdNature = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNature";
  };
 
  module MdNaturePeople = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNaturePeople";
  };
 
  module MdNavigateBefore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNavigateBefore";
  };
 
  module MdNavigateNext = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNavigateNext";
  };
 
  module MdPalette = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPalette";
  };
 
  module MdPanorama = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanorama";
  };
 
  module MdPanoramaFishEye = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanoramaFishEye";
  };
 
  module MdPanoramaHorizontal = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanoramaHorizontal";
  };
 
  module MdPanoramaVertical = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanoramaVertical";
  };
 
  module MdPanoramaWideAngle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPanoramaWideAngle";
  };
 
  module MdPhoto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoto";
  };
 
  module MdPhotoAlbum = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoAlbum";
  };
 
  module MdPhotoCamera = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoCamera";
  };
 
  module MdPhotoFilter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoFilter";
  };
 
  module MdPhotoLibrary = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoLibrary";
  };
 
  module MdPhotoSizeSelectActual = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoSizeSelectActual";
  };
 
  module MdPhotoSizeSelectLarge = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoSizeSelectLarge";
  };
 
  module MdPhotoSizeSelectSmall = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhotoSizeSelectSmall";
  };
 
  module MdPictureAsPdf = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPictureAsPdf";
  };
 
  module MdPortrait = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPortrait";
  };
 
  module MdRemoveRedEye = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRemoveRedEye";
  };
 
  module MdRotate90DegreesCcw = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRotate90DegreesCcw";
  };
 
  module MdRotateLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRotateLeft";
  };
 
  module MdRotateRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRotateRight";
  };
 
  module MdSlideshow = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSlideshow";
  };
 
  module MdStraighten = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStraighten";
  };
 
  module MdStyle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStyle";
  };
 
  module MdSwitchCamera = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwitchCamera";
  };
 
  module MdSwitchVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSwitchVideo";
  };
 
  module MdTagFaces = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTagFaces";
  };
 
  module MdTexture = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTexture";
  };
 
  module MdTimelapse = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimelapse";
  };
 
  module MdTimer10 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimer10";
  };
 
  module MdTimer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimer";
  };
 
  module MdTimer3 = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimer3";
  };
 
  module MdTimerOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimerOff";
  };
 
  module MdTonality = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTonality";
  };
 
  module MdTransform = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTransform";
  };
 
  module MdTune = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTune";
  };
 
  module MdViewComfy = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewComfy";
  };
 
  module MdViewCompact = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdViewCompact";
  };
 
  module MdVignette = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVignette";
  };
 
  module MdWbAuto = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWbAuto";
  };
 
  module MdWbCloudy = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWbCloudy";
  };
 
  module MdWbIncandescent = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWbIncandescent";
  };
 
  module MdWbIridescent = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWbIridescent";
  };
 
  module MdWbSunny = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWbSunny";
  };
 
  module MdAddLocation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAddLocation";
  };
 
  module MdBeenhere = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBeenhere";
  };
 
  module MdDirections = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirections";
  };
 
  module MdDirectionsBike = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsBike";
  };
 
  module MdDirectionsBoat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsBoat";
  };
 
  module MdDirectionsBus = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsBus";
  };
 
  module MdDirectionsCar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsCar";
  };
 
  module MdDirectionsRailway = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsRailway";
  };
 
  module MdDirectionsRun = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsRun";
  };
 
  module MdDirectionsSubway = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsSubway";
  };
 
  module MdDirectionsTransit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsTransit";
  };
 
  module MdDirectionsWalk = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDirectionsWalk";
  };
 
  module MdEditLocation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEditLocation";
  };
 
  module MdEvStation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEvStation";
  };
 
  module MdFlight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFlight";
  };
 
  module MdHotel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHotel";
  };
 
  module MdLayers = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLayers";
  };
 
  module MdLayersClear = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLayersClear";
  };
 
  module MdLocalActivity = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalActivity";
  };
 
  module MdLocalAirport = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalAirport";
  };
 
  module MdLocalAtm = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalAtm";
  };
 
  module MdLocalBar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalBar";
  };
 
  module MdLocalCafe = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalCafe";
  };
 
  module MdLocalCarWash = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalCarWash";
  };
 
  module MdLocalConvenienceStore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalConvenienceStore";
  };
 
  module MdLocalDining = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalDining";
  };
 
  module MdLocalDrink = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalDrink";
  };
 
  module MdLocalFlorist = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalFlorist";
  };
 
  module MdLocalGasStation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalGasStation";
  };
 
  module MdLocalGroceryStore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalGroceryStore";
  };
 
  module MdLocalHospital = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalHospital";
  };
 
  module MdLocalHotel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalHotel";
  };
 
  module MdLocalLaundryService = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalLaundryService";
  };
 
  module MdLocalLibrary = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalLibrary";
  };
 
  module MdLocalMall = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalMall";
  };
 
  module MdLocalMovies = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalMovies";
  };
 
  module MdLocalOffer = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalOffer";
  };
 
  module MdLocalParking = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalParking";
  };
 
  module MdLocalPharmacy = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPharmacy";
  };
 
  module MdLocalPhone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPhone";
  };
 
  module MdLocalPizza = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPizza";
  };
 
  module MdLocalPlay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPlay";
  };
 
  module MdLocalPostOffice = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPostOffice";
  };
 
  module MdLocalPrintshop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalPrintshop";
  };
 
  module MdLocalSee = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalSee";
  };
 
  module MdLocalShipping = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalShipping";
  };
 
  module MdLocalTaxi = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocalTaxi";
  };
 
  module MdMap = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMap";
  };
 
  module MdMyLocation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMyLocation";
  };
 
  module MdNavigation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNavigation";
  };
 
  module MdNearMe = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNearMe";
  };
 
  module MdPersonPin = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPersonPin";
  };
 
  module MdPersonPinCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPersonPinCircle";
  };
 
  module MdPinDrop = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPinDrop";
  };
 
  module MdPlace = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlace";
  };
 
  module MdRateReview = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRateReview";
  };
 
  module MdRestaurant = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRestaurant";
  };
 
  module MdRestaurantMenu = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRestaurantMenu";
  };
 
  module MdSatellite = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSatellite";
  };
 
  module MdStoreMallDirectory = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStoreMallDirectory";
  };
 
  module MdStreetview = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStreetview";
  };
 
  module MdSubway = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubway";
  };
 
  module MdTerrain = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTerrain";
  };
 
  module MdTraffic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTraffic";
  };
 
  module MdTrain = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTrain";
  };
 
  module MdTram = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTram";
  };
 
  module MdTransferWithinAStation = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTransferWithinAStation";
  };
 
  module MdZoomOutMap = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdZoomOutMap";
  };
 
  module MdApps = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdApps";
  };
 
  module MdArrowBack = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowBack";
  };
 
  module MdArrowDownward = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowDownward";
  };
 
  module MdArrowDropDown = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowDropDown";
  };
 
  module MdArrowDropDownCircle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowDropDownCircle";
  };
 
  module MdArrowDropUp = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowDropUp";
  };
 
  module MdArrowForward = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowForward";
  };
 
  module MdArrowUpward = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdArrowUpward";
  };
 
  module MdCancel = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCancel";
  };
 
  module MdCheck = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCheck";
  };
 
  module MdChevronLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChevronLeft";
  };
 
  module MdChevronRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChevronRight";
  };
 
  module MdClose = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdClose";
  };
 
  module MdExpandLess = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExpandLess";
  };
 
  module MdExpandMore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdExpandMore";
  };
 
  module MdFirstPage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFirstPage";
  };
 
  module MdFullscreen = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFullscreen";
  };
 
  module MdFullscreenExit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFullscreenExit";
  };
 
  module MdLastPage = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLastPage";
  };
 
  module MdMenu = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMenu";
  };
 
  module MdMoreHoriz = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMoreHoriz";
  };
 
  module MdMoreVert = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMoreVert";
  };
 
  module MdRefresh = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRefresh";
  };
 
  module MdSubdirectoryArrowLeft = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubdirectoryArrowLeft";
  };
 
  module MdSubdirectoryArrowRight = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSubdirectoryArrowRight";
  };
 
  module MdUnfoldLess = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUnfoldLess";
  };
 
  module MdUnfoldMore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdUnfoldMore";
  };
 
  module MdAdb = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAdb";
  };
 
  module MdAirlineSeatFlat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatFlat";
  };
 
  module MdAirlineSeatFlatAngled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatFlatAngled";
  };
 
  module MdAirlineSeatIndividualSuite = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatIndividualSuite";
  };
 
  module MdAirlineSeatLegroomExtra = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatLegroomExtra";
  };
 
  module MdAirlineSeatLegroomNormal = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatLegroomNormal";
  };
 
  module MdAirlineSeatLegroomReduced = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatLegroomReduced";
  };
 
  module MdAirlineSeatReclineExtra = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatReclineExtra";
  };
 
  module MdAirlineSeatReclineNormal = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirlineSeatReclineNormal";
  };
 
  module MdBluetoothAudio = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBluetoothAudio";
  };
 
  module MdConfirmationNumber = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdConfirmationNumber";
  };
 
  module MdDiscFull = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDiscFull";
  };
 
  module MdDoNotDisturb = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDoNotDisturb";
  };
 
  module MdDoNotDisturbAlt = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDoNotDisturbAlt";
  };
 
  module MdDoNotDisturbOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDoNotDisturbOff";
  };
 
  module MdDoNotDisturbOn = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDoNotDisturbOn";
  };
 
  module MdDriveEta = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDriveEta";
  };
 
  module MdEnhancedEncryption = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEnhancedEncryption";
  };
 
  module MdEventAvailable = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEventAvailable";
  };
 
  module MdEventBusy = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEventBusy";
  };
 
  module MdEventNote = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdEventNote";
  };
 
  module MdFolderSpecial = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFolderSpecial";
  };
 
  module MdLiveTv = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLiveTv";
  };
 
  module MdMms = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMms";
  };
 
  module MdMore = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMore";
  };
 
  module MdNetworkCheck = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNetworkCheck";
  };
 
  module MdNetworkLocked = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNetworkLocked";
  };
 
  module MdNoEncryption = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNoEncryption";
  };
 
  module MdOndemandVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdOndemandVideo";
  };
 
  module MdPersonalVideo = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPersonalVideo";
  };
 
  module MdPhoneBluetoothSpeaker = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneBluetoothSpeaker";
  };
 
  module MdPhoneForwarded = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneForwarded";
  };
 
  module MdPhoneInTalk = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneInTalk";
  };
 
  module MdPhoneLocked = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneLocked";
  };
 
  module MdPhoneMissed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhoneMissed";
  };
 
  module MdPhonePaused = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPhonePaused";
  };
 
  module MdPower = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPower";
  };
 
  module MdPriorityHigh = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPriorityHigh";
  };
 
  module MdRvHookup = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRvHookup";
  };
 
  module MdSdCard = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSdCard";
  };
 
  module MdSimCardAlert = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSimCardAlert";
  };
 
  module MdSms = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSms";
  };
 
  module MdSmsFailed = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSmsFailed";
  };
 
  module MdSync = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSync";
  };
 
  module MdSyncDisabled = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSyncDisabled";
  };
 
  module MdSyncProblem = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSyncProblem";
  };
 
  module MdSystemUpdate = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSystemUpdate";
  };
 
  module MdTapAndPlay = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTapAndPlay";
  };
 
  module MdTimeToLeave = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdTimeToLeave";
  };
 
  module MdVibration = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVibration";
  };
 
  module MdVoiceChat = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVoiceChat";
  };
 
  module MdVpnLock = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdVpnLock";
  };
 
  module MdWc = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWc";
  };
 
  module MdWifi = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWifi";
  };
 
  module MdAcUnit = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAcUnit";
  };
 
  module MdAirportShuttle = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAirportShuttle";
  };
 
  module MdAllInclusive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdAllInclusive";
  };
 
  module MdBeachAccess = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBeachAccess";
  };
 
  module MdBusinessCenter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdBusinessCenter";
  };
 
  module MdCasino = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCasino";
  };
 
  module MdChildCare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChildCare";
  };
 
  module MdChildFriendly = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdChildFriendly";
  };
 
  module MdFitnessCenter = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFitnessCenter";
  };
 
  module MdFreeBreakfast = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdFreeBreakfast";
  };
 
  module MdGolfCourse = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGolfCourse";
  };
 
  module MdHotTub = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdHotTub";
  };
 
  module MdKitchen = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdKitchen";
  };
 
  module MdPool = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPool";
  };
 
  module MdRoomService = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRoomService";
  };
 
  module MdSmokeFree = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSmokeFree";
  };
 
  module MdSmokingRooms = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSmokingRooms";
  };
 
  module MdSpa = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSpa";
  };
 
  module MdCake = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCake";
  };
 
  module MdDomain = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdDomain";
  };
 
  module MdGroup = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGroup";
  };
 
  module MdGroupAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdGroupAdd";
  };
 
  module MdLocationCity = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdLocationCity";
  };
 
  module MdMood = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMood";
  };
 
  module MdMoodBad = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdMoodBad";
  };
 
  module MdNotifications = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotifications";
  };
 
  module MdNotificationsActive = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotificationsActive";
  };
 
  module MdNotificationsNone = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotificationsNone";
  };
 
  module MdNotificationsOff = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotificationsOff";
  };
 
  module MdNotificationsPaused = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdNotificationsPaused";
  };
 
  module MdPages = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPages";
  };
 
  module MdPartyMode = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPartyMode";
  };
 
  module MdPeople = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPeople";
  };
 
  module MdPeopleOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPeopleOutline";
  };
 
  module MdPerson = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPerson";
  };
 
  module MdPersonAdd = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPersonAdd";
  };
 
  module MdPersonOutline = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPersonOutline";
  };
 
  module MdPlusOne = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPlusOne";
  };
 
  module MdPoll = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPoll";
  };
 
  module MdPublic = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdPublic";
  };
 
  module MdSchool = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSchool";
  };
 
  module MdSentimentDissatisfied = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSentimentDissatisfied";
  };
 
  module MdSentimentNeutral = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSentimentNeutral";
  };
 
  module MdSentimentSatisfied = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSentimentSatisfied";
  };
 
  module MdSentimentVeryDissatisfied = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSentimentVeryDissatisfied";
  };
 
  module MdSentimentVerySatisfied = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdSentimentVerySatisfied";
  };
 
  module MdShare = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdShare";
  };
 
  module MdWhatshot = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdWhatshot";
  };
 
  module MdCheckBox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCheckBox";
  };
 
  module MdCheckBoxOutlineBlank = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdCheckBoxOutlineBlank";
  };
 
  module MdIndeterminateCheckBox = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdIndeterminateCheckBox";
  };
 
  module MdRadioButtonChecked = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRadioButtonChecked";
  };
 
  module MdRadioButtonUnchecked = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdRadioButtonUnchecked";
  };
 
  module MdStar = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStar";
  };
 
  module MdStarBorder = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStarBorder";
  };
 
  module MdStarHalf = {
    @module("react-icons/md") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "MdStarHalf";
  };
