
  module Gi3DGlasses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Gi3DGlasses";
  };
 
  module Gi3DHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Gi3DHammer";
  };
 
  module Gi3DMeeple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Gi3DMeeple";
  };
 
  module Gi3DStairs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "Gi3DStairs";
  };
 
  module GiAbacus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbacus";
  };
 
  module GiAbbotMeeple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbbotMeeple";
  };
 
  module GiAbstract001 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract001";
  };
 
  module GiAbstract002 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract002";
  };
 
  module GiAbstract003 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract003";
  };
 
  module GiAbstract004 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract004";
  };
 
  module GiAbstract005 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract005";
  };
 
  module GiAbstract006 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract006";
  };
 
  module GiAbstract007 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract007";
  };
 
  module GiAbstract008 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract008";
  };
 
  module GiAbstract009 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract009";
  };
 
  module GiAbstract010 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract010";
  };
 
  module GiAbstract011 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract011";
  };
 
  module GiAbstract012 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract012";
  };
 
  module GiAbstract013 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract013";
  };
 
  module GiAbstract014 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract014";
  };
 
  module GiAbstract015 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract015";
  };
 
  module GiAbstract016 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract016";
  };
 
  module GiAbstract017 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract017";
  };
 
  module GiAbstract018 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract018";
  };
 
  module GiAbstract019 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract019";
  };
 
  module GiAbstract020 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract020";
  };
 
  module GiAbstract021 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract021";
  };
 
  module GiAbstract022 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract022";
  };
 
  module GiAbstract023 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract023";
  };
 
  module GiAbstract024 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract024";
  };
 
  module GiAbstract025 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract025";
  };
 
  module GiAbstract026 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract026";
  };
 
  module GiAbstract027 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract027";
  };
 
  module GiAbstract028 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract028";
  };
 
  module GiAbstract029 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract029";
  };
 
  module GiAbstract030 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract030";
  };
 
  module GiAbstract031 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract031";
  };
 
  module GiAbstract032 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract032";
  };
 
  module GiAbstract033 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract033";
  };
 
  module GiAbstract034 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract034";
  };
 
  module GiAbstract035 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract035";
  };
 
  module GiAbstract036 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract036";
  };
 
  module GiAbstract037 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract037";
  };
 
  module GiAbstract038 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract038";
  };
 
  module GiAbstract039 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract039";
  };
 
  module GiAbstract040 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract040";
  };
 
  module GiAbstract041 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract041";
  };
 
  module GiAbstract042 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract042";
  };
 
  module GiAbstract043 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract043";
  };
 
  module GiAbstract044 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract044";
  };
 
  module GiAbstract045 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract045";
  };
 
  module GiAbstract046 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract046";
  };
 
  module GiAbstract047 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract047";
  };
 
  module GiAbstract048 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract048";
  };
 
  module GiAbstract049 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract049";
  };
 
  module GiAbstract050 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract050";
  };
 
  module GiAbstract051 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract051";
  };
 
  module GiAbstract052 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract052";
  };
 
  module GiAbstract053 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract053";
  };
 
  module GiAbstract054 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract054";
  };
 
  module GiAbstract055 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract055";
  };
 
  module GiAbstract056 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract056";
  };
 
  module GiAbstract057 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract057";
  };
 
  module GiAbstract058 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract058";
  };
 
  module GiAbstract059 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract059";
  };
 
  module GiAbstract060 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract060";
  };
 
  module GiAbstract061 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract061";
  };
 
  module GiAbstract062 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract062";
  };
 
  module GiAbstract063 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract063";
  };
 
  module GiAbstract064 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract064";
  };
 
  module GiAbstract065 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract065";
  };
 
  module GiAbstract066 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract066";
  };
 
  module GiAbstract067 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract067";
  };
 
  module GiAbstract068 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract068";
  };
 
  module GiAbstract069 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract069";
  };
 
  module GiAbstract070 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract070";
  };
 
  module GiAbstract071 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract071";
  };
 
  module GiAbstract072 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract072";
  };
 
  module GiAbstract073 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract073";
  };
 
  module GiAbstract074 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract074";
  };
 
  module GiAbstract075 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract075";
  };
 
  module GiAbstract076 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract076";
  };
 
  module GiAbstract077 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract077";
  };
 
  module GiAbstract078 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract078";
  };
 
  module GiAbstract079 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract079";
  };
 
  module GiAbstract080 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract080";
  };
 
  module GiAbstract081 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract081";
  };
 
  module GiAbstract082 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract082";
  };
 
  module GiAbstract083 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract083";
  };
 
  module GiAbstract084 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract084";
  };
 
  module GiAbstract085 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract085";
  };
 
  module GiAbstract086 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract086";
  };
 
  module GiAbstract087 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract087";
  };
 
  module GiAbstract088 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract088";
  };
 
  module GiAbstract089 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract089";
  };
 
  module GiAbstract090 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract090";
  };
 
  module GiAbstract091 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract091";
  };
 
  module GiAbstract092 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract092";
  };
 
  module GiAbstract093 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract093";
  };
 
  module GiAbstract094 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract094";
  };
 
  module GiAbstract095 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract095";
  };
 
  module GiAbstract096 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract096";
  };
 
  module GiAbstract097 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract097";
  };
 
  module GiAbstract098 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract098";
  };
 
  module GiAbstract099 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract099";
  };
 
  module GiAbstract100 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract100";
  };
 
  module GiAbstract101 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract101";
  };
 
  module GiAbstract102 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract102";
  };
 
  module GiAbstract103 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract103";
  };
 
  module GiAbstract104 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract104";
  };
 
  module GiAbstract105 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract105";
  };
 
  module GiAbstract106 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract106";
  };
 
  module GiAbstract107 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract107";
  };
 
  module GiAbstract108 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract108";
  };
 
  module GiAbstract109 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract109";
  };
 
  module GiAbstract110 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract110";
  };
 
  module GiAbstract111 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract111";
  };
 
  module GiAbstract112 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract112";
  };
 
  module GiAbstract113 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract113";
  };
 
  module GiAbstract114 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract114";
  };
 
  module GiAbstract115 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract115";
  };
 
  module GiAbstract116 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract116";
  };
 
  module GiAbstract117 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract117";
  };
 
  module GiAbstract118 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract118";
  };
 
  module GiAbstract119 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract119";
  };
 
  module GiAbstract120 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract120";
  };
 
  module GiAbstract121 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAbstract121";
  };
 
  module GiAce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAce";
  };
 
  module GiAchievement = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAchievement";
  };
 
  module GiAchillesHeel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAchillesHeel";
  };
 
  module GiAcidBlob = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcidBlob";
  };
 
  module GiAcidTube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcidTube";
  };
 
  module GiAcid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcid";
  };
 
  module GiAcorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcorn";
  };
 
  module GiAcousticMegaphone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcousticMegaphone";
  };
 
  module GiAcrobatic = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAcrobatic";
  };
 
  module GiAerialSignal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAerialSignal";
  };
 
  module GiAerodynamicHarpoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAerodynamicHarpoon";
  };
 
  module GiAerosol = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAerosol";
  };
 
  module GiAfrica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAfrica";
  };
 
  module GiAfterburn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAfterburn";
  };
 
  module GiAgave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAgave";
  };
 
  module GiAges = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAges";
  };
 
  module GiAirBalloon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirBalloon";
  };
 
  module GiAirForce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirForce";
  };
 
  module GiAirZigzag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirZigzag";
  };
 
  module GiAirplaneArrival = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirplaneArrival";
  };
 
  module GiAirplaneDeparture = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirplaneDeparture";
  };
 
  module GiAirplane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirplane";
  };
 
  module GiAirtightHatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAirtightHatch";
  };
 
  module GiAk47 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAk47";
  };
 
  module GiAk47U = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAk47U";
  };
 
  module GiAkhet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAkhet";
  };
 
  module GiAlarmClock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlarmClock";
  };
 
  module GiAlgae = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlgae";
  };
 
  module GiAlgeria = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlgeria";
  };
 
  module GiAlienBug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlienBug";
  };
 
  module GiAlienEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlienEgg";
  };
 
  module GiAlienFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlienFire";
  };
 
  module GiAlienSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlienSkull";
  };
 
  module GiAlienStare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlienStare";
  };
 
  module GiAllForOne = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAllForOne";
  };
 
  module GiAllSeeingEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAllSeeingEye";
  };
 
  module GiAlliedStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlliedStar";
  };
 
  module GiAlligatorClip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlligatorClip";
  };
 
  module GiAlmond = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAlmond";
  };
 
  module GiAmberMosquito = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmberMosquito";
  };
 
  module GiAmbulance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmbulance";
  };
 
  module GiAmericanFootballBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmericanFootballBall";
  };
 
  module GiAmericanFootballHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmericanFootballHelmet";
  };
 
  module GiAmericanFootballPlayer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmericanFootballPlayer";
  };
 
  module GiAmericanShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmericanShield";
  };
 
  module GiAmethyst = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmethyst";
  };
 
  module GiAmmoBox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmmoBox";
  };
 
  module GiAmmoniteFossil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmmoniteFossil";
  };
 
  module GiAmmonite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmmonite";
  };
 
  module GiAmphora = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmphora";
  };
 
  module GiAmpleDress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmpleDress";
  };
 
  module GiAmplitude = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmplitude";
  };
 
  module GiAmputation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAmputation";
  };
 
  module GiAnarchy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnarchy";
  };
 
  module GiAnatomy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnatomy";
  };
 
  module GiAnchor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnchor";
  };
 
  module GiAncientColumns = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAncientColumns";
  };
 
  module GiAncientRuins = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAncientRuins";
  };
 
  module GiAncientScrew = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAncientScrew";
  };
 
  module GiAncientSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAncientSword";
  };
 
  module GiAndroidMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAndroidMask";
  };
 
  module GiAndromedaChain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAndromedaChain";
  };
 
  module GiAngelOutfit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAngelOutfit";
  };
 
  module GiAngelWings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAngelWings";
  };
 
  module GiAnglerFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnglerFish";
  };
 
  module GiAngola = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAngola";
  };
 
  module GiAngryEyes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAngryEyes";
  };
 
  module GiAngularSpider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAngularSpider";
  };
 
  module GiAnimalHide = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnimalHide";
  };
 
  module GiAnimalSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnimalSkull";
  };
 
  module GiAnkh = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnkh";
  };
 
  module GiAnnexation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnnexation";
  };
 
  module GiAnt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnt";
  };
 
  module GiAntarctica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAntarctica";
  };
 
  module GiAnthem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnthem";
  };
 
  module GiAntiAircraftGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAntiAircraftGun";
  };
 
  module GiAntibody = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAntibody";
  };
 
  module GiAnticlockwiseRotation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnticlockwiseRotation";
  };
 
  module GiAnts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnts";
  };
 
  module GiAnubis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnubis";
  };
 
  module GiAnvilImpact = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnvilImpact";
  };
 
  module GiAnvil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAnvil";
  };
 
  module GiApc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiApc";
  };
 
  module GiApolloCapsule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiApolloCapsule";
  };
 
  module GiApothecary = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiApothecary";
  };
 
  module GiAppleCore = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAppleCore";
  };
 
  module GiAppleMaggot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAppleMaggot";
  };
 
  module GiAppleSeeds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAppleSeeds";
  };
 
  module GiAquarium = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAquarium";
  };
 
  module GiAquarius = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAquarius";
  };
 
  module GiAqueduct = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAqueduct";
  };
 
  module GiArabicDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArabicDoor";
  };
 
  module GiArcTriomphe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArcTriomphe";
  };
 
  module GiArchBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArchBridge";
  };
 
  module GiArcher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArcher";
  };
 
  module GiArcheryTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArcheryTarget";
  };
 
  module GiArchitectMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArchitectMask";
  };
 
  module GiArchiveResearch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArchiveResearch";
  };
 
  module GiArcingBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArcingBolt";
  };
 
  module GiArena = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArena";
  };
 
  module GiAries = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAries";
  };
 
  module GiArmBandage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmBandage";
  };
 
  module GiArmSling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmSling";
  };
 
  module GiArm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArm";
  };
 
  module GiArmadilloTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmadilloTail";
  };
 
  module GiArmadillo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmadillo";
  };
 
  module GiArmorDowngrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmorDowngrade";
  };
 
  module GiArmorPunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmorPunch";
  };
 
  module GiArmorUpgrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmorUpgrade";
  };
 
  module GiArmorVest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmorVest";
  };
 
  module GiArmoredBoomerang = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmoredBoomerang";
  };
 
  module GiArmoredPants = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmoredPants";
  };
 
  module GiArmouredShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArmouredShell";
  };
 
  module GiArrest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrest";
  };
 
  module GiArrowCluster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowCluster";
  };
 
  module GiArrowCursor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowCursor";
  };
 
  module GiArrowDunk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowDunk";
  };
 
  module GiArrowFlights = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowFlights";
  };
 
  module GiArrowScope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowScope";
  };
 
  module GiArrowWings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowWings";
  };
 
  module GiArrowed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowed";
  };
 
  module GiArrowhead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowhead";
  };
 
  module GiArrowsShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArrowsShield";
  };
 
  module GiArson = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArson";
  };
 
  module GiArtificialHive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArtificialHive";
  };
 
  module GiArtificialIntelligence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArtificialIntelligence";
  };
 
  module GiArtilleryShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiArtilleryShell";
  };
 
  module GiAscendingBlock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAscendingBlock";
  };
 
  module GiAsianLantern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAsianLantern";
  };
 
  module GiAsparagus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAsparagus";
  };
 
  module GiAspergillum = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAspergillum";
  };
 
  module GiAssassinPocket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAssassinPocket";
  };
 
  module GiAsteroid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAsteroid";
  };
 
  module GiAstronautHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAstronautHelmet";
  };
 
  module GiAtSea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAtSea";
  };
 
  module GiAtlas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAtlas";
  };
 
  module GiAtomCore = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAtomCore";
  };
 
  module GiAtom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAtom";
  };
 
  module GiAtomicSlashes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAtomicSlashes";
  };
 
  module GiAttachedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAttachedShield";
  };
 
  module GiAubergine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAubergine";
  };
 
  module GiAudioCassette = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAudioCassette";
  };
 
  module GiAura = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAura";
  };
 
  module GiAustralia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAustralia";
  };
 
  module GiAutoRepair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAutoRepair";
  };
 
  module GiAutogun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAutogun";
  };
 
  module GiAutomaticSas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAutomaticSas";
  };
 
  module GiAvocado = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAvocado";
  };
 
  module GiAvoidance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAvoidance";
  };
 
  module GiAwareness = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAwareness";
  };
 
  module GiAxeInLog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAxeInLog";
  };
 
  module GiAxeInStump = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAxeInStump";
  };
 
  module GiAxeSwing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAxeSwing";
  };
 
  module GiAxeSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAxeSword";
  };
 
  module GiAzulFlake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiAzulFlake";
  };
 
  module GiBabyBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBabyBottle";
  };
 
  module GiBabyFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBabyFace";
  };
 
  module GiBabyfootPlayers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBabyfootPlayers";
  };
 
  module GiBackForth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackForth";
  };
 
  module GiBackPain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackPain";
  };
 
  module GiBackboneShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackboneShell";
  };
 
  module GiBackgammon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackgammon";
  };
 
  module GiBackpack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackpack";
  };
 
  module GiBackstab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackstab";
  };
 
  module GiBackup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackup";
  };
 
  module GiBackwardTime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBackwardTime";
  };
 
  module GiBacon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBacon";
  };
 
  module GiBadBreath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBadBreath";
  };
 
  module GiBadGnome = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBadGnome";
  };
 
  module GiBalaclava = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBalaclava";
  };
 
  module GiBalkenkreuz = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBalkenkreuz";
  };
 
  module GiBallGlow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBallGlow";
  };
 
  module GiBallHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBallHeart";
  };
 
  module GiBallPyramid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBallPyramid";
  };
 
  module GiBallerinaShoes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBallerinaShoes";
  };
 
  module GiBallista = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBallista";
  };
 
  module GiBalloonDog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBalloonDog";
  };
 
  module GiBalloons = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBalloons";
  };
 
  module GiBambooFountain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBambooFountain";
  };
 
  module GiBamboo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBamboo";
  };
 
  module GiBananaBunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBananaBunch";
  };
 
  module GiBananaPeel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBananaPeel";
  };
 
  module GiBananaPeeled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBananaPeeled";
  };
 
  module GiBanana = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBanana";
  };
 
  module GiBandageRoll = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBandageRoll";
  };
 
  module GiBandaged = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBandaged";
  };
 
  module GiBandit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBandit";
  };
 
  module GiBangingGavel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBangingGavel";
  };
 
  module GiBank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBank";
  };
 
  module GiBanknote = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBanknote";
  };
 
  module GiBarbarian = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbarian";
  };
 
  module GiBarbecue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbecue";
  };
 
  module GiBarbedArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedArrow";
  };
 
  module GiBarbedCoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedCoil";
  };
 
  module GiBarbedNails = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedNails";
  };
 
  module GiBarbedSpear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedSpear";
  };
 
  module GiBarbedStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedStar";
  };
 
  module GiBarbedSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedSun";
  };
 
  module GiBarbedWire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbedWire";
  };
 
  module GiBarbute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarbute";
  };
 
  module GiBarefoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarefoot";
  };
 
  module GiBarn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarn";
  };
 
  module GiBarracksTent = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarracksTent";
  };
 
  module GiBarracks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarracks";
  };
 
  module GiBarrelLeak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarrelLeak";
  };
 
  module GiBarrel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarrel";
  };
 
  module GiBarricade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarricade";
  };
 
  module GiBarrier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBarrier";
  };
 
  module GiBaseballBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBaseballBat";
  };
 
  module GiBaseballGlove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBaseballGlove";
  };
 
  module GiBasketballBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBasketballBall";
  };
 
  module GiBasketballBasket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBasketballBasket";
  };
 
  module GiBasketballJersey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBasketballJersey";
  };
 
  module GiBastet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBastet";
  };
 
  module GiBatBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatBlade";
  };
 
  module GiBatMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatMask";
  };
 
  module GiBatWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatWing";
  };
 
  module GiBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBat";
  };
 
  module GiBathtub = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBathtub";
  };
 
  module GiBaton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBaton";
  };
 
  module GiBatteredAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteredAxe";
  };
 
  module GiBatteries = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteries";
  };
 
  module GiBattery0 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattery0";
  };
 
  module GiBattery100 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattery100";
  };
 
  module GiBattery25 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattery25";
  };
 
  module GiBattery50 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattery50";
  };
 
  module GiBattery75 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattery75";
  };
 
  module GiBatteryMinus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteryMinus";
  };
 
  module GiBatteryPackAlt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteryPackAlt";
  };
 
  module GiBatteryPack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteryPack";
  };
 
  module GiBatteryPlus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatteryPlus";
  };
 
  module GiBattleAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattleAxe";
  };
 
  module GiBattleGear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattleGear";
  };
 
  module GiBattleMech = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattleMech";
  };
 
  module GiBattleTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattleTank";
  };
 
  module GiBattleship = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBattleship";
  };
 
  module GiBatwingEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBatwingEmblem";
  };
 
  module GiBayonet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBayonet";
  };
 
  module GiBeachBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeachBall";
  };
 
  module GiBeachBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeachBucket";
  };
 
  module GiBeamSatellite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeamSatellite";
  };
 
  module GiBeamWake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeamWake";
  };
 
  module GiBeamsAura = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeamsAura";
  };
 
  module GiBeanstalk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeanstalk";
  };
 
  module GiBearFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBearFace";
  };
 
  module GiBearHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBearHead";
  };
 
  module GiBeard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeard";
  };
 
  module GiBeastEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeastEye";
  };
 
  module GiBeaver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeaver";
  };
 
  module GiBedLamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBedLamp";
  };
 
  module GiBed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBed";
  };
 
  module GiBee = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBee";
  };
 
  module GiBeech = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeech";
  };
 
  module GiBeehive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeehive";
  };
 
  module GiBeerBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeerBottle";
  };
 
  module GiBeerHorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeerHorn";
  };
 
  module GiBeerStein = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeerStein";
  };
 
  module GiBeet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeet";
  };
 
  module GiBeetleShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeetleShell";
  };
 
  module GiBehold = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBehold";
  };
 
  module GiBelgium = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBelgium";
  };
 
  module GiBellShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBellShield";
  };
 
  module GiBellows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBellows";
  };
 
  module GiBeltArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeltArmor";
  };
 
  module GiBeltBuckles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeltBuckles";
  };
 
  module GiBelt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBelt";
  };
 
  module GiBerriesBowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBerriesBowl";
  };
 
  module GiBerryBush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBerryBush";
  };
 
  module GiBestialFangs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBestialFangs";
  };
 
  module GiBeveledStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBeveledStar";
  };
 
  module GiBiceps = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBiceps";
  };
 
  module GiBigDiamondRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBigDiamondRing";
  };
 
  module GiBigEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBigEgg";
  };
 
  module GiBigGear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBigGear";
  };
 
  module GiBigWave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBigWave";
  };
 
  module GiBilledCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBilledCap";
  };
 
  module GiBindle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBindle";
  };
 
  module GiBinoculars = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBinoculars";
  };
 
  module GiBiohazard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBiohazard";
  };
 
  module GiBiplane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBiplane";
  };
 
  module GiBirdClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBirdClaw";
  };
 
  module GiBirdHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBirdHouse";
  };
 
  module GiBirdLimb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBirdLimb";
  };
 
  module GiBirdMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBirdMask";
  };
 
  module GiBirdTwitter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBirdTwitter";
  };
 
  module GiBison = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBison";
  };
 
  module GiBlackBar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackBar";
  };
 
  module GiBlackBelt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackBelt";
  };
 
  module GiBlackBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackBook";
  };
 
  module GiBlackBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackBridge";
  };
 
  module GiBlackFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackFlag";
  };
 
  module GiBlackHandShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackHandShield";
  };
 
  module GiBlackHoleBolas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackHoleBolas";
  };
 
  module GiBlackKnightHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackKnightHelm";
  };
 
  module GiBlackSea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackSea";
  };
 
  module GiBlackball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackball";
  };
 
  module GiBlackcurrant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlackcurrant";
  };
 
  module GiBlacksmith = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlacksmith";
  };
 
  module GiBladeBite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBladeBite";
  };
 
  module GiBladeDrag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBladeDrag";
  };
 
  module GiBladeFall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBladeFall";
  };
 
  module GiBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlast";
  };
 
  module GiBlaster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlaster";
  };
 
  module GiBleedingEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBleedingEye";
  };
 
  module GiBleedingHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBleedingHeart";
  };
 
  module GiBleedingWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBleedingWound";
  };
 
  module GiBlindfold = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlindfold";
  };
 
  module GiBlockHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlockHouse";
  };
 
  module GiBlood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlood";
  };
 
  module GiBloodyStash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBloodyStash";
  };
 
  module GiBloodySword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBloodySword";
  };
 
  module GiBlunderbuss = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBlunderbuss";
  };
 
  module GiBo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBo";
  };
 
  module GiBoarEnsign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoarEnsign";
  };
 
  module GiBoarTusks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoarTusks";
  };
 
  module GiBoatEngine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoatEngine";
  };
 
  module GiBoatFishing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoatFishing";
  };
 
  module GiBoatHorizon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoatHorizon";
  };
 
  module GiBoatPropeller = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoatPropeller";
  };
 
  module GiBodyBalance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBodyBalance";
  };
 
  module GiBodyHeight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBodyHeight";
  };
 
  module GiBodySwapping = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBodySwapping";
  };
 
  module GiBoilingBubbles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoilingBubbles";
  };
 
  module GiBolas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBolas";
  };
 
  module GiBolivia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBolivia";
  };
 
  module GiBoltBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltBomb";
  };
 
  module GiBoltCutter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltCutter";
  };
 
  module GiBoltDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltDrop";
  };
 
  module GiBoltEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltEye";
  };
 
  module GiBoltSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltSaw";
  };
 
  module GiBoltShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltShield";
  };
 
  module GiBoltSpellCast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoltSpellCast";
  };
 
  module GiBolterGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBolterGun";
  };
 
  module GiBomber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBomber";
  };
 
  module GiBombingRun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBombingRun";
  };
 
  module GiBoneGnawer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoneGnawer";
  };
 
  module GiBoneKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoneKnife";
  };
 
  module GiBoneMace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoneMace";
  };
 
  module GiBonsaiTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBonsaiTree";
  };
 
  module GiBookAura = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookAura";
  };
 
  module GiBookCover = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookCover";
  };
 
  module GiBookPile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookPile";
  };
 
  module GiBookStorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookStorm";
  };
 
  module GiBookmark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookmark";
  };
 
  module GiBookmarklet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookmarklet";
  };
 
  module GiBookshelf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBookshelf";
  };
 
  module GiBoombox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoombox";
  };
 
  module GiBoomerangCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoomerangCross";
  };
 
  module GiBoomerangSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoomerangSun";
  };
 
  module GiBoomerang = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoomerang";
  };
 
  module GiBootKick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBootKick";
  };
 
  module GiBootPrints = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBootPrints";
  };
 
  module GiBootStomp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBootStomp";
  };
 
  module GiBoots = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoots";
  };
 
  module GiBooze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBooze";
  };
 
  module GiBorderedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBorderedShield";
  };
 
  module GiBossKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBossKey";
  };
 
  module GiBottleCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBottleCap";
  };
 
  module GiBottleVapors = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBottleVapors";
  };
 
  module GiBottledBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBottledBolt";
  };
 
  module GiBottledShadow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBottledShadow";
  };
 
  module GiBottomRight3DArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBottomRight3DArrow";
  };
 
  module GiBoulderDash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoulderDash";
  };
 
  module GiBouncingSpring = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBouncingSpring";
  };
 
  module GiBouncingSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBouncingSword";
  };
 
  module GiBowArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowArrow";
  };
 
  module GiBowString = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowString";
  };
 
  module GiBowTieRibbon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowTieRibbon";
  };
 
  module GiBowTie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowTie";
  };
 
  module GiBowels = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowels";
  };
 
  module GiBowenKnot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowenKnot";
  };
 
  module GiBowieKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowieKnife";
  };
 
  module GiBowlSpiral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowlSpiral";
  };
 
  module GiBowlingPin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowlingPin";
  };
 
  module GiBowlingPropulsion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowlingPropulsion";
  };
 
  module GiBowlingStrike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowlingStrike";
  };
 
  module GiBowman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBowman";
  };
 
  module GiBoxCutter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxCutter";
  };
 
  module GiBoxTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxTrap";
  };
 
  module GiBoxUnpacking = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxUnpacking";
  };
 
  module GiBoxingGloveSurprise = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxingGloveSurprise";
  };
 
  module GiBoxingGlove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxingGlove";
  };
 
  module GiBoxingRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBoxingRing";
  };
 
  module GiBracer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBracer";
  };
 
  module GiBracers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBracers";
  };
 
  module GiBrainFreeze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrainFreeze";
  };
 
  module GiBrainLeak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrainLeak";
  };
 
  module GiBrainStem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrainStem";
  };
 
  module GiBrainTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrainTentacle";
  };
 
  module GiBrain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrain";
  };
 
  module GiBrainstorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrainstorm";
  };
 
  module GiBranchArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBranchArrow";
  };
 
  module GiBrandyBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrandyBottle";
  };
 
  module GiBrasero = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrasero";
  };
 
  module GiBrassEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrassEye";
  };
 
  module GiBrassKnuckles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrassKnuckles";
  };
 
  module GiBrazilFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrazilFlag";
  };
 
  module GiBrazil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrazil";
  };
 
  module GiBreadSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBreadSlice";
  };
 
  module GiBread = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBread";
  };
 
  module GiBreakingChain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBreakingChain";
  };
 
  module GiBreastplate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBreastplate";
  };
 
  module GiBrickPile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrickPile";
  };
 
  module GiBrickWall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrickWall";
  };
 
  module GiBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBridge";
  };
 
  module GiBriefcase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBriefcase";
  };
 
  module GiBrightExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrightExplosion";
  };
 
  module GiBroadDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBroadDagger";
  };
 
  module GiBroadheadArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBroadheadArrow";
  };
 
  module GiBroadsword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBroadsword";
  };
 
  module GiBroccoli = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBroccoli";
  };
 
  module GiBrodieHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrodieHelmet";
  };
 
  module GiBrokenArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenArrow";
  };
 
  module GiBrokenAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenAxe";
  };
 
  module GiBrokenBone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenBone";
  };
 
  module GiBrokenBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenBottle";
  };
 
  module GiBrokenHeartZone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenHeartZone";
  };
 
  module GiBrokenHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenHeart";
  };
 
  module GiBrokenPottery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenPottery";
  };
 
  module GiBrokenRibbon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenRibbon";
  };
 
  module GiBrokenShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenShield";
  };
 
  module GiBrokenSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenSkull";
  };
 
  module GiBrokenTablet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenTablet";
  };
 
  module GiBrokenWall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrokenWall";
  };
 
  module GiBroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBroom";
  };
 
  module GiBrutalHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrutalHelm";
  };
 
  module GiBrute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBrute";
  };
 
  module GiBubbleField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBubbleField";
  };
 
  module GiBubbles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBubbles";
  };
 
  module GiBubblingBeam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBubblingBeam";
  };
 
  module GiBubblingBowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBubblingBowl";
  };
 
  module GiBubblingFlask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBubblingFlask";
  };
 
  module GiBud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBud";
  };
 
  module GiBuffaloHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBuffaloHead";
  };
 
  module GiBugleCall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBugleCall";
  };
 
  module GiBulb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBulb";
  };
 
  module GiBullHorns = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBullHorns";
  };
 
  module GiBull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBull";
  };
 
  module GiBulldozer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBulldozer";
  };
 
  module GiBulletImpacts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBulletImpacts";
  };
 
  module GiBullets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBullets";
  };
 
  module GiBullseye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBullseye";
  };
 
  module GiBullyMinion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBullyMinion";
  };
 
  module GiBundleGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBundleGrenade";
  };
 
  module GiBunkerAssault = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBunkerAssault";
  };
 
  module GiBunker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBunker";
  };
 
  module GiBuoy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBuoy";
  };
 
  module GiBurn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurn";
  };
 
  module GiBurningBlobs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningBlobs";
  };
 
  module GiBurningBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningBook";
  };
 
  module GiBurningDot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningDot";
  };
 
  module GiBurningEmbers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningEmbers";
  };
 
  module GiBurningEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningEye";
  };
 
  module GiBurningForest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningForest";
  };
 
  module GiBurningMeteor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningMeteor";
  };
 
  module GiBurningPassion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningPassion";
  };
 
  module GiBurningRoundShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningRoundShot";
  };
 
  module GiBurningSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningSkull";
  };
 
  module GiBurningTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurningTree";
  };
 
  module GiBurstBlob = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBurstBlob";
  };
 
  module GiBus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBus";
  };
 
  module GiButter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiButter";
  };
 
  module GiButterflyKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiButterflyKnife";
  };
 
  module GiButterflyWarning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiButterflyWarning";
  };
 
  module GiButterfly = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiButterfly";
  };
 
  module GiButtonFinger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiButtonFinger";
  };
 
  module GiBuyCard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiBuyCard";
  };
 
  module GiByzantinTemple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiByzantinTemple";
  };
 
  module GiC96 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiC96";
  };
 
  module GiCableStayedBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCableStayedBridge";
  };
 
  module GiCactusPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCactusPot";
  };
 
  module GiCactus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCactus";
  };
 
  module GiCadillacHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCadillacHelm";
  };
 
  module GiCaduceus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaduceus";
  };
 
  module GiCaesar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaesar";
  };
 
  module GiCage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCage";
  };
 
  module GiCagedBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCagedBall";
  };
 
  module GiCakeSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCakeSlice";
  };
 
  module GiCalavera = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCalavera";
  };
 
  module GiCaldera = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaldera";
  };
 
  module GiCalendar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCalendar";
  };
 
  module GiCaltrops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaltrops";
  };
 
  module GiCamargueCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCamargueCross";
  };
 
  module GiCambodia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCambodia";
  };
 
  module GiCamelHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCamelHead";
  };
 
  module GiCamel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCamel";
  };
 
  module GiCampCookingPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCampCookingPot";
  };
 
  module GiCampfire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCampfire";
  };
 
  module GiCampingTent = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCampingTent";
  };
 
  module GiCancel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCancel";
  };
 
  module GiCancer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCancer";
  };
 
  module GiCandleFlame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandleFlame";
  };
 
  module GiCandleHolder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandleHolder";
  };
 
  module GiCandleLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandleLight";
  };
 
  module GiCandleSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandleSkull";
  };
 
  module GiCandlebright = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandlebright";
  };
 
  module GiCandles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandles";
  };
 
  module GiCandlestickPhone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandlestickPhone";
  };
 
  module GiCandyCanes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCandyCanes";
  };
 
  module GiCannedFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCannedFish";
  };
 
  module GiCannister = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCannister";
  };
 
  module GiCannonBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCannonBall";
  };
 
  module GiCannonShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCannonShot";
  };
 
  module GiCannon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCannon";
  };
 
  module GiCanoe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCanoe";
  };
 
  module GiCapeArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCapeArmor";
  };
 
  module GiCape = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCape";
  };
 
  module GiCapitol = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCapitol";
  };
 
  module GiCapricorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCapricorn";
  };
 
  module GiCaptainHatProfile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaptainHatProfile";
  };
 
  module GiCarBattery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarBattery";
  };
 
  module GiCarDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarDoor";
  };
 
  module GiCarKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarKey";
  };
 
  module GiCarSeat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarSeat";
  };
 
  module GiCarWheel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarWheel";
  };
 
  module GiCarambola = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarambola";
  };
 
  module GiCaravan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaravan";
  };
 
  module GiCaravel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaravel";
  };
 
  module GiCard10Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard10Clubs";
  };
 
  module GiCard10Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard10Diamonds";
  };
 
  module GiCard10Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard10Hearts";
  };
 
  module GiCard10Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard10Spades";
  };
 
  module GiCard2Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard2Clubs";
  };
 
  module GiCard2Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard2Diamonds";
  };
 
  module GiCard2Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard2Hearts";
  };
 
  module GiCard2Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard2Spades";
  };
 
  module GiCard3Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard3Clubs";
  };
 
  module GiCard3Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard3Diamonds";
  };
 
  module GiCard3Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard3Hearts";
  };
 
  module GiCard3Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard3Spades";
  };
 
  module GiCard4Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard4Clubs";
  };
 
  module GiCard4Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard4Diamonds";
  };
 
  module GiCard4Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard4Hearts";
  };
 
  module GiCard4Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard4Spades";
  };
 
  module GiCard5Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard5Clubs";
  };
 
  module GiCard5Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard5Diamonds";
  };
 
  module GiCard5Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard5Hearts";
  };
 
  module GiCard5Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard5Spades";
  };
 
  module GiCard6Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard6Clubs";
  };
 
  module GiCard6Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard6Diamonds";
  };
 
  module GiCard6Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard6Hearts";
  };
 
  module GiCard6Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard6Spades";
  };
 
  module GiCard7Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard7Clubs";
  };
 
  module GiCard7Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard7Diamonds";
  };
 
  module GiCard7Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard7Hearts";
  };
 
  module GiCard7Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard7Spades";
  };
 
  module GiCard8Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard8Clubs";
  };
 
  module GiCard8Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard8Diamonds";
  };
 
  module GiCard8Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard8Hearts";
  };
 
  module GiCard8Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard8Spades";
  };
 
  module GiCard9Clubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard9Clubs";
  };
 
  module GiCard9Diamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard9Diamonds";
  };
 
  module GiCard9Hearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard9Hearts";
  };
 
  module GiCard9Spades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCard9Spades";
  };
 
  module GiCardAceClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardAceClubs";
  };
 
  module GiCardAceDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardAceDiamonds";
  };
 
  module GiCardAceHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardAceHearts";
  };
 
  module GiCardAceSpades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardAceSpades";
  };
 
  module GiCardBurn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardBurn";
  };
 
  module GiCardDiscard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardDiscard";
  };
 
  module GiCardDraw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardDraw";
  };
 
  module GiCardExchange = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardExchange";
  };
 
  module GiCardJackClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardJackClubs";
  };
 
  module GiCardJackDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardJackDiamonds";
  };
 
  module GiCardJackHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardJackHearts";
  };
 
  module GiCardJackSpades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardJackSpades";
  };
 
  module GiCardJoker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardJoker";
  };
 
  module GiCardKingClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardKingClubs";
  };
 
  module GiCardKingDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardKingDiamonds";
  };
 
  module GiCardKingHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardKingHearts";
  };
 
  module GiCardKingSpades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardKingSpades";
  };
 
  module GiCardPick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardPick";
  };
 
  module GiCardPickup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardPickup";
  };
 
  module GiCardPlay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardPlay";
  };
 
  module GiCardQueenClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardQueenClubs";
  };
 
  module GiCardQueenDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardQueenDiamonds";
  };
 
  module GiCardQueenHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardQueenHearts";
  };
 
  module GiCardQueenSpades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardQueenSpades";
  };
 
  module GiCardRandom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardRandom";
  };
 
  module GiCardboardBox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCardboardBox";
  };
 
  module GiCargoCrane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCargoCrane";
  };
 
  module GiCargoCrate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCargoCrate";
  };
 
  module GiCargoShip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCargoShip";
  };
 
  module GiCarillon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarillon";
  };
 
  module GiCarnivalMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarnivalMask";
  };
 
  module GiCarnivoreMouth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarnivoreMouth";
  };
 
  module GiCarnivorousPlant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarnivorousPlant";
  };
 
  module GiCarnyx = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarnyx";
  };
 
  module GiCarpetBombing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarpetBombing";
  };
 
  module GiCarrier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarrier";
  };
 
  module GiCarrion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarrion";
  };
 
  module GiCarrot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCarrot";
  };
 
  module GiCartwheel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCartwheel";
  };
 
  module GiCash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCash";
  };
 
  module GiCassowaryHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCassowaryHead";
  };
 
  module GiCastleRuins = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCastleRuins";
  };
 
  module GiCastle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCastle";
  };
 
  module GiCat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCat";
  };
 
  module GiCatapult = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCatapult";
  };
 
  module GiCatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCatch";
  };
 
  module GiCaterpillar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaterpillar";
  };
 
  module GiCauldron = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCauldron";
  };
 
  module GiCavalry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCavalry";
  };
 
  module GiCaveEntrance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaveEntrance";
  };
 
  module GiCaveman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCaveman";
  };
 
  module GiCctvCamera = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCctvCamera";
  };
 
  module GiCeilingBarnacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCeilingBarnacle";
  };
 
  module GiCeilingLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCeilingLight";
  };
 
  module GiCelebrationFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCelebrationFire";
  };
 
  module GiCellarBarrels = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCellarBarrels";
  };
 
  module GiCementShoes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCementShoes";
  };
 
  module GiCentaur = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCentaur";
  };
 
  module GiCentipede = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCentipede";
  };
 
  module GiCeremonialMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCeremonialMask";
  };
 
  module GiChainLightning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChainLightning";
  };
 
  module GiChainMail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChainMail";
  };
 
  module GiChainedArrowHeads = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChainedArrowHeads";
  };
 
  module GiChainedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChainedHeart";
  };
 
  module GiChaingun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChaingun";
  };
 
  module GiChainsaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChainsaw";
  };
 
  module GiChaliceDrops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChaliceDrops";
  };
 
  module GiChalkOutlineMurder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChalkOutlineMurder";
  };
 
  module GiChameleonGlyph = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChameleonGlyph";
  };
 
  module GiChampions = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChampions";
  };
 
  module GiChanterelles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChanterelles";
  };
 
  module GiChargedArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChargedArrow";
  };
 
  module GiChargingBull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChargingBull";
  };
 
  module GiCharging = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCharging";
  };
 
  module GiChariot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChariot";
  };
 
  module GiCharm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCharm";
  };
 
  module GiChart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChart";
  };
 
  module GiChatBubble = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChatBubble";
  };
 
  module GiCheckMark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheckMark";
  };
 
  module GiCheckboxTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheckboxTree";
  };
 
  module GiCheckedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheckedShield";
  };
 
  module GiCheckeredDiamond = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheckeredDiamond";
  };
 
  module GiCheckeredFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheckeredFlag";
  };
 
  module GiChecklist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChecklist";
  };
 
  module GiCheerful = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheerful";
  };
 
  module GiCheeseWedge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCheeseWedge";
  };
 
  module GiChefToque = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChefToque";
  };
 
  module GiChelseaBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChelseaBoot";
  };
 
  module GiChemicalArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChemicalArrow";
  };
 
  module GiChemicalBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChemicalBolt";
  };
 
  module GiChemicalDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChemicalDrop";
  };
 
  module GiChemicalTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChemicalTank";
  };
 
  module GiCherish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCherish";
  };
 
  module GiCherry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCherry";
  };
 
  module GiChessBishop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessBishop";
  };
 
  module GiChessKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessKing";
  };
 
  module GiChessKnight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessKnight";
  };
 
  module GiChessPawn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessPawn";
  };
 
  module GiChessQueen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessQueen";
  };
 
  module GiChessRook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChessRook";
  };
 
  module GiChestArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChestArmor";
  };
 
  module GiChest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChest";
  };
 
  module GiChestnutLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChestnutLeaf";
  };
 
  module GiChewedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChewedHeart";
  };
 
  module GiChewedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChewedSkull";
  };
 
  module GiChickenLeg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChickenLeg";
  };
 
  module GiChickenOven = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChickenOven";
  };
 
  module GiChicken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChicken";
  };
 
  module GiChiliPepper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChiliPepper";
  };
 
  module GiChimney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChimney";
  };
 
  module GiChipsBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChipsBag";
  };
 
  module GiChisel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChisel";
  };
 
  module GiChocolateBar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChocolateBar";
  };
 
  module GiChoice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChoice";
  };
 
  module GiChoppedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChoppedSkull";
  };
 
  module GiChurch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiChurch";
  };
 
  module GiCigale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCigale";
  };
 
  module GiCigar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCigar";
  };
 
  module GiCigarette = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCigarette";
  };
 
  module GiCircleCage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircleCage";
  };
 
  module GiCircleClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircleClaws";
  };
 
  module GiCircleSparks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircleSparks";
  };
 
  module GiCircle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircle";
  };
 
  module GiCirclingFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCirclingFish";
  };
 
  module GiCircuitry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircuitry";
  };
 
  module GiCircularSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircularSaw";
  };
 
  module GiCircularSawblade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCircularSawblade";
  };
 
  module GiCityCar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCityCar";
  };
 
  module GiClamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClamp";
  };
 
  module GiClapperboard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClapperboard";
  };
 
  module GiClawHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClawHammer";
  };
 
  module GiClawSlashes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClawSlashes";
  };
 
  module GiClawString = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClawString";
  };
 
  module GiClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClaw";
  };
 
  module GiClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClaws";
  };
 
  module GiClayBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClayBrick";
  };
 
  module GiClaymoreExplosive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClaymoreExplosive";
  };
 
  module GiCleaver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCleaver";
  };
 
  module GiClick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClick";
  };
 
  module GiCliffCrossing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCliffCrossing";
  };
 
  module GiCloakDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloakDagger";
  };
 
  module GiCloak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloak";
  };
 
  module GiClockwiseRotation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClockwiseRotation";
  };
 
  module GiClockwork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClockwork";
  };
 
  module GiClosedBarbute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClosedBarbute";
  };
 
  module GiClosedDoors = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClosedDoors";
  };
 
  module GiClothJar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClothJar";
  };
 
  module GiClothes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClothes";
  };
 
  module GiClothespin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClothespin";
  };
 
  module GiCloudDownload = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloudDownload";
  };
 
  module GiCloudRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloudRing";
  };
 
  module GiCloudUpload = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloudUpload";
  };
 
  module GiCloudyFork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloudyFork";
  };
 
  module GiClout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClout";
  };
 
  module GiCloverSpiked = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCloverSpiked";
  };
 
  module GiClover = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClover";
  };
 
  module GiClown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClown";
  };
 
  module GiClownfish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClownfish";
  };
 
  module GiClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClubs";
  };
 
  module GiClusterBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiClusterBomb";
  };
 
  module GiCoalWagon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoalWagon";
  };
 
  module GiCobra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCobra";
  };
 
  module GiCobweb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCobweb";
  };
 
  module GiCoconuts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoconuts";
  };
 
  module GiCoffeeBeans = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoffeeBeans";
  };
 
  module GiCoffeeCup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoffeeCup";
  };
 
  module GiCoffeeMug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoffeeMug";
  };
 
  module GiCoffeePot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoffeePot";
  };
 
  module GiCoffin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoffin";
  };
 
  module GiCogLock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCogLock";
  };
 
  module GiCog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCog";
  };
 
  module GiCogsplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCogsplosion";
  };
 
  module GiCoilingCurl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoilingCurl";
  };
 
  module GiCoinsPile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoinsPile";
  };
 
  module GiCoins = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoins";
  };
 
  module GiColdHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColdHeart";
  };
 
  module GiColiseum = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColiseum";
  };
 
  module GiColombia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColombia";
  };
 
  module GiColombianStatue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColombianStatue";
  };
 
  module GiColtM1911 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColtM1911";
  };
 
  module GiColumnVase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiColumnVase";
  };
 
  module GiComa = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiComa";
  };
 
  module GiComb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiComb";
  };
 
  module GiCombinationLock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCombinationLock";
  };
 
  module GiCometSpark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCometSpark";
  };
 
  module GiCommercialAirplane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCommercialAirplane";
  };
 
  module GiCompactDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCompactDisc";
  };
 
  module GiCompanionCube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCompanionCube";
  };
 
  module GiCompass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCompass";
  };
 
  module GiComputerFan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiComputerFan";
  };
 
  module GiComputing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiComputing";
  };
 
  module GiConcentrationOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConcentrationOrb";
  };
 
  module GiConcentricCrescents = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConcentricCrescents";
  };
 
  module GiConcreteBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConcreteBag";
  };
 
  module GiCondorEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCondorEmblem";
  };
 
  module GiCondyluraSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCondyluraSkull";
  };
 
  module GiConfirmed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConfirmed";
  };
 
  module GiConfrontation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConfrontation";
  };
 
  module GiCongress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCongress";
  };
 
  module GiConqueror = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConqueror";
  };
 
  module GiConsoleController = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConsoleController";
  };
 
  module GiContortionist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiContortionist";
  };
 
  module GiContract = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiContract";
  };
 
  module GiControlTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiControlTower";
  };
 
  module GiConvergenceTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConvergenceTarget";
  };
 
  module GiConversation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConversation";
  };
 
  module GiConverseShoe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConverseShoe";
  };
 
  module GiConvict = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConvict";
  };
 
  module GiConvince = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConvince";
  };
 
  module GiConwayLifeGlider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiConwayLifeGlider";
  };
 
  module GiCook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCook";
  };
 
  module GiCookie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCookie";
  };
 
  module GiCookingGlove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCookingGlove";
  };
 
  module GiCookingPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCookingPot";
  };
 
  module GiCoolSpices = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoolSpices";
  };
 
  module GiCoral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoral";
  };
 
  module GiCorkedTube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorkedTube";
  };
 
  module GiCorkscrew = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorkscrew";
  };
 
  module GiCorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorn";
  };
 
  module GiCornerExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCornerExplosion";
  };
 
  module GiCornerFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCornerFlag";
  };
 
  module GiCornucopia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCornucopia";
  };
 
  module GiCoronation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoronation";
  };
 
  module GiCorporal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorporal";
  };
 
  module GiCorset = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorset";
  };
 
  module GiCorsica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCorsica";
  };
 
  module GiCottonFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCottonFlower";
  };
 
  module GiCoveredJar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCoveredJar";
  };
 
  module GiCow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCow";
  };
 
  module GiCowboyBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCowboyBoot";
  };
 
  module GiCowboyHolster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCowboyHolster";
  };
 
  module GiCowled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCowled";
  };
 
  module GiCpuShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCpuShot";
  };
 
  module GiCpu = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCpu";
  };
 
  module GiCrabClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrabClaw";
  };
 
  module GiCrab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrab";
  };
 
  module GiCrackedAlienSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedAlienSkull";
  };
 
  module GiCrackedBallDunk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedBallDunk";
  };
 
  module GiCrackedDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedDisc";
  };
 
  module GiCrackedGlass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedGlass";
  };
 
  module GiCrackedHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedHelm";
  };
 
  module GiCrackedMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedMask";
  };
 
  module GiCrackedSaber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedSaber";
  };
 
  module GiCrackedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrackedShield";
  };
 
  module GiCrafting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrafting";
  };
 
  module GiCrags = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrags";
  };
 
  module GiCrane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrane";
  };
 
  module GiCreditsCurrency = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCreditsCurrency";
  };
 
  module GiCrenelCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrenelCrown";
  };
 
  module GiCrenulatedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrenulatedShield";
  };
 
  module GiCrescentBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrescentBlade";
  };
 
  module GiCrescentStaff = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrescentStaff";
  };
 
  module GiCrestedHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrestedHelmet";
  };
 
  module GiCricketBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCricketBat";
  };
 
  module GiCricket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCricket";
  };
 
  module GiCrimeSceneTape = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrimeSceneTape";
  };
 
  module GiCrocJaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrocJaws";
  };
 
  module GiCrocSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrocSword";
  };
 
  module GiCroissant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCroissant";
  };
 
  module GiCroissantsPupil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCroissantsPupil";
  };
 
  module GiCrookFlail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrookFlail";
  };
 
  module GiCrossFlare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossFlare";
  };
 
  module GiCrossMark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossMark";
  };
 
  module GiCrossShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossShield";
  };
 
  module GiCrossbow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossbow";
  };
 
  module GiCrosscutSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrosscutSaw";
  };
 
  module GiCrossedAirFlows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedAirFlows";
  };
 
  module GiCrossedAxes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedAxes";
  };
 
  module GiCrossedBones = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedBones";
  };
 
  module GiCrossedChains = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedChains";
  };
 
  module GiCrossedClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedClaws";
  };
 
  module GiCrossedPistols = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedPistols";
  };
 
  module GiCrossedSabres = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedSabres";
  };
 
  module GiCrossedSlashes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedSlashes";
  };
 
  module GiCrossedSwords = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossedSwords";
  };
 
  module GiCrosshairArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrosshairArrow";
  };
 
  module GiCrosshair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrosshair";
  };
 
  module GiCrossroad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrossroad";
  };
 
  module GiCrowDive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrowDive";
  };
 
  module GiCrowNest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrowNest";
  };
 
  module GiCrowbar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrowbar";
  };
 
  module GiCrownCoin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrownCoin";
  };
 
  module GiCrownOfThorns = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrownOfThorns";
  };
 
  module GiCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrown";
  };
 
  module GiCrownedExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrownedExplosion";
  };
 
  module GiCrownedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrownedHeart";
  };
 
  module GiCrownedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrownedSkull";
  };
 
  module GiCrucifix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrucifix";
  };
 
  module GiCruiser = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCruiser";
  };
 
  module GiCrumblingBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrumblingBall";
  };
 
  module GiCrush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrush";
  };
 
  module GiCryoChamber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCryoChamber";
  };
 
  module GiCrystalBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalBall";
  };
 
  module GiCrystalBars = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalBars";
  };
 
  module GiCrystalCluster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalCluster";
  };
 
  module GiCrystalEarrings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalEarrings";
  };
 
  module GiCrystalEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalEye";
  };
 
  module GiCrystalGrowth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalGrowth";
  };
 
  module GiCrystalShine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalShine";
  };
 
  module GiCrystalShrine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalShrine";
  };
 
  module GiCrystalWand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalWand";
  };
 
  module GiCrystalize = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCrystalize";
  };
 
  module GiCuauhtli = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCuauhtli";
  };
 
  module GiCube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCube";
  };
 
  module GiCubeforce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCubeforce";
  };
 
  module GiCubes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCubes";
  };
 
  module GiCultist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCultist";
  };
 
  module GiCupcake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCupcake";
  };
 
  module GiCupidonArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCupidonArrow";
  };
 
  module GiCurledLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurledLeaf";
  };
 
  module GiCurledTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurledTentacle";
  };
 
  module GiCurlingStone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurlingStone";
  };
 
  module GiCurlingVines = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurlingVines";
  };
 
  module GiCurlyMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurlyMask";
  };
 
  module GiCurlyWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurlyWing";
  };
 
  module GiCursedStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCursedStar";
  };
 
  module GiCurvyKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCurvyKnife";
  };
 
  module GiCustodianHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCustodianHelmet";
  };
 
  module GiCutDiamond = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCutDiamond";
  };
 
  module GiCutLemon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCutLemon";
  };
 
  module GiCutPalm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCutPalm";
  };
 
  module GiCyberEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCyberEye";
  };
 
  module GiCyborgFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCyborgFace";
  };
 
  module GiCycle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCycle";
  };
 
  module GiCycling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCycling";
  };
 
  module GiCyclops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCyclops";
  };
 
  module GiCzSkorpion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiCzSkorpion";
  };
 
  module GiD10 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiD10";
  };
 
  module GiD12 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiD12";
  };
 
  module GiD4 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiD4";
  };
 
  module GiDaemonPull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDaemonPull";
  };
 
  module GiDaemonSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDaemonSkull";
  };
 
  module GiDaggerRose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDaggerRose";
  };
 
  module GiDaggers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDaggers";
  };
 
  module GiDaisy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDaisy";
  };
 
  module GiDam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDam";
  };
 
  module GiDamagedHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDamagedHouse";
  };
 
  module GiDandelionFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDandelionFlower";
  };
 
  module GiDango = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDango";
  };
 
  module GiDarkSquad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDarkSquad";
  };
 
  module GiDart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDart";
  };
 
  module GiDatabase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDatabase";
  };
 
  module GiDeadEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeadEye";
  };
 
  module GiDeadHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeadHead";
  };
 
  module GiDeadWood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeadWood";
  };
 
  module GiDeadlyStrike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeadlyStrike";
  };
 
  module GiDeathJuice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeathJuice";
  };
 
  module GiDeathNote = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeathNote";
  };
 
  module GiDeathSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeathSkull";
  };
 
  module GiDeathZone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeathZone";
  };
 
  module GiDeathcab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeathcab";
  };
 
  module GiDecapitation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDecapitation";
  };
 
  module GiDeerTrack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeerTrack";
  };
 
  module GiDefenseSatellite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDefenseSatellite";
  };
 
  module GiDefensiveWall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDefensiveWall";
  };
 
  module GiDefibrilate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDefibrilate";
  };
 
  module GiDelighted = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDelighted";
  };
 
  module GiDeliveryDrone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeliveryDrone";
  };
 
  module GiDemolish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDemolish";
  };
 
  module GiDervishSwords = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDervishSwords";
  };
 
  module GiDesertEagle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDesertEagle";
  };
 
  module GiDesertSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDesertSkull";
  };
 
  module GiDesert = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDesert";
  };
 
  module GiDeshretRedCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeshretRedCrown";
  };
 
  module GiDeskLamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDeskLamp";
  };
 
  module GiDesk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDesk";
  };
 
  module GiDespair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDespair";
  };
 
  module GiDetour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDetour";
  };
 
  module GiDevilMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDevilMask";
  };
 
  module GiDew = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDew";
  };
 
  module GiDiabloSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiabloSkull";
  };
 
  module GiDiagram = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiagram";
  };
 
  module GiDiamondHard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiamondHard";
  };
 
  module GiDiamondHilt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiamondHilt";
  };
 
  module GiDiamondRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiamondRing";
  };
 
  module GiDiamondsSmile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiamondsSmile";
  };
 
  module GiDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiamonds";
  };
 
  module GiDiceEightFacesEight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceEightFacesEight";
  };
 
  module GiDiceFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceFire";
  };
 
  module GiDiceShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceShield";
  };
 
  module GiDiceSixFacesFive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesFive";
  };
 
  module GiDiceSixFacesFour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesFour";
  };
 
  module GiDiceSixFacesOne = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesOne";
  };
 
  module GiDiceSixFacesSix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesSix";
  };
 
  module GiDiceSixFacesThree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesThree";
  };
 
  module GiDiceSixFacesTwo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceSixFacesTwo";
  };
 
  module GiDiceTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceTarget";
  };
 
  module GiDiceTwentyFacesOne = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceTwentyFacesOne";
  };
 
  module GiDiceTwentyFacesTwenty = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiceTwentyFacesTwenty";
  };
 
  module GiDigDug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDigDug";
  };
 
  module GiDigHole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDigHole";
  };
 
  module GiDigitalTrace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDigitalTrace";
  };
 
  module GiDimetrodon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDimetrodon";
  };
 
  module GiDinosaurBones = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDinosaurBones";
  };
 
  module GiDinosaurEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDinosaurEgg";
  };
 
  module GiDinosaurRex = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDinosaurRex";
  };
 
  module GiDiplodocus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiplodocus";
  };
 
  module GiDiploma = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiploma";
  };
 
  module GiDirectionSign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDirectionSign";
  };
 
  module GiDirectionSigns = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDirectionSigns";
  };
 
  module GiDirectorChair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDirectorChair";
  };
 
  module GiDirewolf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDirewolf";
  };
 
  module GiDiscGolfBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiscGolfBag";
  };
 
  module GiDiscGolfBasket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiscGolfBasket";
  };
 
  module GiDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDisc";
  };
 
  module GiDiscussion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDiscussion";
  };
 
  module GiDisintegrate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDisintegrate";
  };
 
  module GiDistraction = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDistraction";
  };
 
  module GiDistressSignal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDistressSignal";
  };
 
  module GiDivergence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDivergence";
  };
 
  module GiDivert = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDivert";
  };
 
  module GiDividedSpiral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDividedSpiral";
  };
 
  module GiDividedSquare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDividedSquare";
  };
 
  module GiDivingDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDivingDagger";
  };
 
  module GiDivingHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDivingHelmet";
  };
 
  module GiDjinn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDjinn";
  };
 
  module GiDna1 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDna1";
  };
 
  module GiDna2 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDna2";
  };
 
  module GiDoctorFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoctorFace";
  };
 
  module GiDodge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDodge";
  };
 
  module GiDodging = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDodging";
  };
 
  module GiDogBowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDogBowl";
  };
 
  module GiDogHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDogHouse";
  };
 
  module GiDolmen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDolmen";
  };
 
  module GiDolphin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDolphin";
  };
 
  module GiDominoMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDominoMask";
  };
 
  module GiDominoTiles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDominoTiles";
  };
 
  module GiDonkey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDonkey";
  };
 
  module GiDonut = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDonut";
  };
 
  module GiDoorHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoorHandle";
  };
 
  module GiDoorRingHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoorRingHandle";
  };
 
  module GiDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoor";
  };
 
  module GiDoorway = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoorway";
  };
 
  module GiDorsalScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDorsalScales";
  };
 
  module GiDoubleDiaphragm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleDiaphragm";
  };
 
  module GiDoubleDragon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleDragon";
  };
 
  module GiDoubleFaceMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleFaceMask";
  };
 
  module GiDoubleFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleFish";
  };
 
  module GiDoubleQuaver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleQuaver";
  };
 
  module GiDoubleRingedOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleRingedOrb";
  };
 
  module GiDoubleShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleShot";
  };
 
  module GiDoubleStreetLights = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubleStreetLights";
  };
 
  module GiDoubled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoubled";
  };
 
  module GiDoughRoller = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDoughRoller";
  };
 
  module GiDove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDove";
  };
 
  module GiDozen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDozen";
  };
 
  module GiDragonBalls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonBalls";
  };
 
  module GiDragonBreath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonBreath";
  };
 
  module GiDragonHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonHead";
  };
 
  module GiDragonOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonOrb";
  };
 
  module GiDragonShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonShield";
  };
 
  module GiDragonSpiral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonSpiral";
  };
 
  module GiDragonfly = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDragonfly";
  };
 
  module GiDrakkarDragon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrakkarDragon";
  };
 
  module GiDrakkar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrakkar";
  };
 
  module GiDramaMasks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDramaMasks";
  };
 
  module GiDrawbridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrawbridge";
  };
 
  module GiDreadSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDreadSkull";
  };
 
  module GiDread = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDread";
  };
 
  module GiDreadnought = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDreadnought";
  };
 
  module GiDreamCatcher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDreamCatcher";
  };
 
  module GiDress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDress";
  };
 
  module GiDrill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrill";
  };
 
  module GiDrinkMe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrinkMe";
  };
 
  module GiDrinking = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrinking";
  };
 
  module GiDrippingBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingBlade";
  };
 
  module GiDrippingGoo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingGoo";
  };
 
  module GiDrippingHoney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingHoney";
  };
 
  module GiDrippingKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingKnife";
  };
 
  module GiDrippingStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingStar";
  };
 
  module GiDrippingStone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingStone";
  };
 
  module GiDrippingSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingSword";
  };
 
  module GiDrippingTube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrippingTube";
  };
 
  module GiDropEarrings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDropEarrings";
  };
 
  module GiDropWeapon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDropWeapon";
  };
 
  module GiDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrop";
  };
 
  module GiDropletSplash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDropletSplash";
  };
 
  module GiDroplets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDroplets";
  };
 
  module GiDrowning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrowning";
  };
 
  module GiDrum = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDrum";
  };
 
  module GiDualityMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDualityMask";
  };
 
  module GiDuality = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuality";
  };
 
  module GiDuckPalm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuckPalm";
  };
 
  module GiDuck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuck";
  };
 
  module GiDuel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuel";
  };
 
  module GiDuffelBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuffelBag";
  };
 
  module GiDunceCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDunceCap";
  };
 
  module GiDungeonGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDungeonGate";
  };
 
  module GiDungeonLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDungeonLight";
  };
 
  module GiDuration = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDuration";
  };
 
  module GiDustCloud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDustCloud";
  };
 
  module GiDutchBike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDutchBike";
  };
 
  module GiDwarfFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDwarfFace";
  };
 
  module GiDwarfHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDwarfHelmet";
  };
 
  module GiDwarfKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDwarfKing";
  };
 
  module GiDwennimmen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDwennimmen";
  };
 
  module GiDynamite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiDynamite";
  };
 
  module GiEagleEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEagleEmblem";
  };
 
  module GiEagleHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEagleHead";
  };
 
  module GiEarrings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarrings";
  };
 
  module GiEarthAfricaEurope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthAfricaEurope";
  };
 
  module GiEarthAmerica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthAmerica";
  };
 
  module GiEarthAsiaOceania = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthAsiaOceania";
  };
 
  module GiEarthCrack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthCrack";
  };
 
  module GiEarthSpit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthSpit";
  };
 
  module GiEarthWorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarthWorm";
  };
 
  module GiEarwig = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEarwig";
  };
 
  module GiEasterEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEasterEgg";
  };
 
  module GiEatingPelican = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEatingPelican";
  };
 
  module GiEating = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEating";
  };
 
  module GiEchoRipples = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEchoRipples";
  };
 
  module GiEclipseFlare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEclipseFlare";
  };
 
  module GiEclipseSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEclipseSaw";
  };
 
  module GiEclipse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEclipse";
  };
 
  module GiEcology = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEcology";
  };
 
  module GiEdgeCrack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEdgeCrack";
  };
 
  module GiEdgedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEdgedShield";
  };
 
  module GiEel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEel";
  };
 
  module GiEggClutch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEggClutch";
  };
 
  module GiEggDefense = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEggDefense";
  };
 
  module GiEggEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEggEye";
  };
 
  module GiEggPod = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEggPod";
  };
 
  module GiEgypt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgypt";
  };
 
  module GiEgyptianBird = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianBird";
  };
 
  module GiEgyptianProfile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianProfile";
  };
 
  module GiEgyptianPyramids = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianPyramids";
  };
 
  module GiEgyptianSphinx = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianSphinx";
  };
 
  module GiEgyptianTemple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianTemple";
  };
 
  module GiEgyptianUrns = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianUrns";
  };
 
  module GiEgyptianWalk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEgyptianWalk";
  };
 
  module GiEightBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEightBall";
  };
 
  module GiElbowPad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElbowPad";
  };
 
  module GiElderberry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElderberry";
  };
 
  module GiElectricWhip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElectricWhip";
  };
 
  module GiElectric = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElectric";
  };
 
  module GiElectricalCrescent = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElectricalCrescent";
  };
 
  module GiElectricalResistance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElectricalResistance";
  };
 
  module GiElectricalSocket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElectricalSocket";
  };
 
  module GiElephantHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElephantHead";
  };
 
  module GiElephant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElephant";
  };
 
  module GiElevator = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElevator";
  };
 
  module GiElfEar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElfEar";
  };
 
  module GiElfHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElfHelmet";
  };
 
  module GiElvenCastle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiElvenCastle";
  };
 
  module GiEmberShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmberShot";
  };
 
  module GiEmbrassedEnergy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmbrassedEnergy";
  };
 
  module GiEmbryo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmbryo";
  };
 
  module GiEmeraldNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmeraldNecklace";
  };
 
  module GiEmerald = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmerald";
  };
 
  module GiEmptyChessboard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyChessboard";
  };
 
  module GiEmptyHourglass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyHourglass";
  };
 
  module GiEmptyMetalBucketHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyMetalBucketHandle";
  };
 
  module GiEmptyMetalBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyMetalBucket";
  };
 
  module GiEmptyWoodBucketHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyWoodBucketHandle";
  };
 
  module GiEmptyWoodBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEmptyWoodBucket";
  };
 
  module GiEncirclement = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEncirclement";
  };
 
  module GiEnergise = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergise";
  };
 
  module GiEnergyArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergyArrow";
  };
 
  module GiEnergyBreath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergyBreath";
  };
 
  module GiEnergyShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergyShield";
  };
 
  module GiEnergySword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergySword";
  };
 
  module GiEnergyTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnergyTank";
  };
 
  module GiEngagementRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEngagementRing";
  };
 
  module GiEnlightenment = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnlightenment";
  };
 
  module GiEnrage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnrage";
  };
 
  module GiEntMouth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEntMouth";
  };
 
  module GiEntangledTyphoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEntangledTyphoon";
  };
 
  module GiEntryDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEntryDoor";
  };
 
  module GiEnvelope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEnvelope";
  };
 
  module GiErlenmeyer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiErlenmeyer";
  };
 
  module GiErmine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiErmine";
  };
 
  module GiEruption = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEruption";
  };
 
  module GiEscalator = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEscalator";
  };
 
  module GiEskimo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEskimo";
  };
 
  module GiEternalLove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEternalLove";
  };
 
  module GiEuropeanFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEuropeanFlag";
  };
 
  module GiEvasion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvasion";
  };
 
  module GiEvilBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilBat";
  };
 
  module GiEvilBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilBook";
  };
 
  module GiEvilBud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilBud";
  };
 
  module GiEvilComet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilComet";
  };
 
  module GiEvilEyes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilEyes";
  };
 
  module GiEvilFork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilFork";
  };
 
  module GiEvilHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilHand";
  };
 
  module GiEvilLove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilLove";
  };
 
  module GiEvilMinion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilMinion";
  };
 
  module GiEvilMoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilMoon";
  };
 
  module GiEvilTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilTower";
  };
 
  module GiEvilTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilTree";
  };
 
  module GiEvilWings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEvilWings";
  };
 
  module GiExecutionerHood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExecutionerHood";
  };
 
  module GiExitDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExitDoor";
  };
 
  module GiExpand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExpand";
  };
 
  module GiExpandedRays = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExpandedRays";
  };
 
  module GiExpander = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExpander";
  };
 
  module GiExpense = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExpense";
  };
 
  module GiExplodingPlanet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExplodingPlanet";
  };
 
  module GiExplosionRays = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExplosionRays";
  };
 
  module GiExplosiveMaterials = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExplosiveMaterials";
  };
 
  module GiExplosiveMeeting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExplosiveMeeting";
  };
 
  module GiExtraLucid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExtraLucid";
  };
 
  module GiExtraTime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExtraTime";
  };
 
  module GiExtractionOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiExtractionOrb";
  };
 
  module GiEyeOfHorus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyeOfHorus";
  };
 
  module GiEyeShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyeShield";
  };
 
  module GiEyeTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyeTarget";
  };
 
  module GiEyeball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyeball";
  };
 
  module GiEyedropper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyedropper";
  };
 
  module GiEyelashes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyelashes";
  };
 
  module GiEyepatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyepatch";
  };
 
  module GiEyestalk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiEyestalk";
  };
 
  module GiFClef = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFClef";
  };
 
  module GiF1Car = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiF1Car";
  };
 
  module GiFaceToFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFaceToFace";
  };
 
  module GiFactoryArm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFactoryArm";
  };
 
  module GiFactory = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFactory";
  };
 
  module GiFairyWand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFairyWand";
  };
 
  module GiFairyWings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFairyWings";
  };
 
  module GiFairy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFairy";
  };
 
  module GiFalconMoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFalconMoon";
  };
 
  module GiFallDown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallDown";
  };
 
  module GiFallingBlob = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingBlob";
  };
 
  module GiFallingBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingBomb";
  };
 
  module GiFallingBoulder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingBoulder";
  };
 
  module GiFallingEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingEye";
  };
 
  module GiFallingLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingLeaf";
  };
 
  module GiFallingOvoid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingOvoid";
  };
 
  module GiFallingRocks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingRocks";
  };
 
  module GiFallingStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFallingStar";
  };
 
  module GiFalling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFalling";
  };
 
  module GiFalloutShelter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFalloutShelter";
  };
 
  module GiFamas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFamas";
  };
 
  module GiFamilyHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFamilyHouse";
  };
 
  module GiFamilyTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFamilyTree";
  };
 
  module GiFangedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFangedSkull";
  };
 
  module GiFangsCircle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFangsCircle";
  };
 
  module GiFangs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFangs";
  };
 
  module GiFarmTractor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFarmTractor";
  };
 
  module GiFarmer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFarmer";
  };
 
  module GiFastArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFastArrow";
  };
 
  module GiFastBackwardButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFastBackwardButton";
  };
 
  module GiFastForwardButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFastForwardButton";
  };
 
  module GiFat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFat";
  };
 
  module GiFeatherNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFeatherNecklace";
  };
 
  module GiFeatherWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFeatherWound";
  };
 
  module GiFeather = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFeather";
  };
 
  module GiFeatheredWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFeatheredWing";
  };
 
  module GiFedora = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFedora";
  };
 
  module GiFeline = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFeline";
  };
 
  module GiFemaleLegs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFemaleLegs";
  };
 
  module GiFemaleVampire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFemaleVampire";
  };
 
  module GiFemale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFemale";
  };
 
  module GiFencer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFencer";
  };
 
  module GiFern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFern";
  };
 
  module GiFertilizerBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFertilizerBag";
  };
 
  module GiFetus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFetus";
  };
 
  module GiFez = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFez";
  };
 
  module GiFieldGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFieldGun";
  };
 
  module GiField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiField";
  };
 
  module GiFigurehead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFigurehead";
  };
 
  module GiFiles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFiles";
  };
 
  module GiFilmProjector = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFilmProjector";
  };
 
  module GiFilmSpool = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFilmSpool";
  };
 
  module GiFilmStrip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFilmStrip";
  };
 
  module GiFinch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFinch";
  };
 
  module GiFingerPrint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFingerPrint";
  };
 
  module GiFingernail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFingernail";
  };
 
  module GiFingersCrossed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFingersCrossed";
  };
 
  module GiFinishLine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFinishLine";
  };
 
  module GiFireAce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireAce";
  };
 
  module GiFireAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireAxe";
  };
 
  module GiFireBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireBomb";
  };
 
  module GiFireBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireBottle";
  };
 
  module GiFireBowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireBowl";
  };
 
  module GiFireBreath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireBreath";
  };
 
  module GiFireDash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireDash";
  };
 
  module GiFireExtinguisher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireExtinguisher";
  };
 
  module GiFireFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireFlower";
  };
 
  module GiFireGem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireGem";
  };
 
  module GiFirePunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFirePunch";
  };
 
  module GiFireRay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireRay";
  };
 
  module GiFireRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireRing";
  };
 
  module GiFireShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireShield";
  };
 
  module GiFireShrine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireShrine";
  };
 
  module GiFireSilhouette = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireSilhouette";
  };
 
  module GiFireSpellCast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireSpellCast";
  };
 
  module GiFireTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireTail";
  };
 
  module GiFireWave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireWave";
  };
 
  module GiFireZone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireZone";
  };
 
  module GiFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFire";
  };
 
  module GiFireball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireball";
  };
 
  module GiFireflake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireflake";
  };
 
  module GiFireplace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireplace";
  };
 
  module GiFirewall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFirewall";
  };
 
  module GiFireworkRocket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFireworkRocket";
  };
 
  module GiFirstAidKit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFirstAidKit";
  };
 
  module GiFishBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishBucket";
  };
 
  module GiFishCooked = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishCooked";
  };
 
  module GiFishCorpse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishCorpse";
  };
 
  module GiFishEggs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishEggs";
  };
 
  module GiFishEscape = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishEscape";
  };
 
  module GiFishMonster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishMonster";
  };
 
  module GiFishScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishScales";
  };
 
  module GiFishSmoking = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishSmoking";
  };
 
  module GiFishbone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishbone";
  };
 
  module GiFishhookFork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishhookFork";
  };
 
  module GiFishingBoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingBoat";
  };
 
  module GiFishingHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingHook";
  };
 
  module GiFishingJig = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingJig";
  };
 
  module GiFishingLure = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingLure";
  };
 
  module GiFishingNet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingNet";
  };
 
  module GiFishingPole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingPole";
  };
 
  module GiFishingSpoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishingSpoon";
  };
 
  module GiFishing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFishing";
  };
 
  module GiFission = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFission";
  };
 
  module GiFist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFist";
  };
 
  module GiFizzingFlask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFizzingFlask";
  };
 
  module GiFlagObjective = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlagObjective";
  };
 
  module GiFlail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlail";
  };
 
  module GiFlake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlake";
  };
 
  module GiFlameClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlameClaws";
  };
 
  module GiFlameSpin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlameSpin";
  };
 
  module GiFlameTunnel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlameTunnel";
  };
 
  module GiFlame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlame";
  };
 
  module GiFlamedLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamedLeaf";
  };
 
  module GiFlamer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamer";
  };
 
  module GiFlamethrowerSoldier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamethrowerSoldier";
  };
 
  module GiFlamethrower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamethrower";
  };
 
  module GiFlamingArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamingArrow";
  };
 
  module GiFlamingClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamingClaw";
  };
 
  module GiFlamingSheet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamingSheet";
  };
 
  module GiFlamingTrident = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamingTrident";
  };
 
  module GiFlamingo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlamingo";
  };
 
  module GiFlangedMace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlangedMace";
  };
 
  module GiFlashGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlashGrenade";
  };
 
  module GiFlashlight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlashlight";
  };
 
  module GiFlatHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatHammer";
  };
 
  module GiFlatPawPrint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatPawPrint";
  };
 
  module GiFlatPlatform = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatPlatform";
  };
 
  module GiFlatStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatStar";
  };
 
  module GiFlatTire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatTire";
  };
 
  module GiFlatbedCovered = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatbedCovered";
  };
 
  module GiFlatbed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatbed";
  };
 
  module GiFlatfish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlatfish";
  };
 
  module GiFlax = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlax";
  };
 
  module GiFleshyMass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFleshyMass";
  };
 
  module GiFleurDeLys = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFleurDeLys";
  };
 
  module GiFlexibleLamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlexibleLamp";
  };
 
  module GiFlexibleStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlexibleStar";
  };
 
  module GiFlintSpark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlintSpark";
  };
 
  module GiFlipFlops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlipFlops";
  };
 
  module GiFloatingCrystal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloatingCrystal";
  };
 
  module GiFloatingGhost = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloatingGhost";
  };
 
  module GiFloatingPlatforms = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloatingPlatforms";
  };
 
  module GiFloatingTentacles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloatingTentacles";
  };
 
  module GiFlood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlood";
  };
 
  module GiFloorHatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloorHatch";
  };
 
  module GiFloorPolisher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFloorPolisher";
  };
 
  module GiFlowerEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowerEmblem";
  };
 
  module GiFlowerHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowerHat";
  };
 
  module GiFlowerPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowerPot";
  };
 
  module GiFlowerStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowerStar";
  };
 
  module GiFlowerTwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowerTwirl";
  };
 
  module GiFlowers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlowers";
  };
 
  module GiFluffyCloud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFluffyCloud";
  };
 
  module GiFluffyFlame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFluffyFlame";
  };
 
  module GiFluffySwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFluffySwirl";
  };
 
  module GiFluffyTrefoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFluffyTrefoil";
  };
 
  module GiFluffyWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFluffyWing";
  };
 
  module GiFlute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlute";
  };
 
  module GiFly = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFly";
  };
 
  module GiFlyingBeetle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingBeetle";
  };
 
  module GiFlyingDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingDagger";
  };
 
  module GiFlyingFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingFlag";
  };
 
  module GiFlyingFox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingFox";
  };
 
  module GiFlyingShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingShuriken";
  };
 
  module GiFlyingTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingTarget";
  };
 
  module GiFlyingTrout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFlyingTrout";
  };
 
  module GiFnFal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFnFal";
  };
 
  module GiFoam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoam";
  };
 
  module GiFoamyDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoamyDisc";
  };
 
  module GiFocusedLightning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFocusedLightning";
  };
 
  module GiFogLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFogLight";
  };
 
  module GiFog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFog";
  };
 
  module GiFoldedPaper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoldedPaper";
  };
 
  module GiFomorian = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFomorian";
  };
 
  module GiFoodChain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoodChain";
  };
 
  module GiFoodTruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoodTruck";
  };
 
  module GiFootPlaster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFootPlaster";
  };
 
  module GiFootTrip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFootTrip";
  };
 
  module GiFootprint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFootprint";
  };
 
  module GiFootsteps = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFootsteps";
  };
 
  module GiFootyField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFootyField";
  };
 
  module GiForearm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForearm";
  };
 
  module GiForestCamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForestCamp";
  };
 
  module GiForest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForest";
  };
 
  module GiForkKnifeSpoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForkKnifeSpoon";
  };
 
  module GiForklift = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForklift";
  };
 
  module GiForwardField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForwardField";
  };
 
  module GiForwardSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiForwardSun";
  };
 
  module GiFossil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFossil";
  };
 
  module GiFoundryBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoundryBucket";
  };
 
  module GiFountainPen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFountainPen";
  };
 
  module GiFountain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFountain";
  };
 
  module GiFoxHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoxHead";
  };
 
  module GiFoxTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFoxTail";
  };
 
  module GiFragmentedMeteor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFragmentedMeteor";
  };
 
  module GiFragmentedSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFragmentedSword";
  };
 
  module GiFragrance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFragrance";
  };
 
  module GiFrance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrance";
  };
 
  module GiFrankensteinCreature = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrankensteinCreature";
  };
 
  module GiFrayedArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrayedArrow";
  };
 
  module GiFreedomDove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFreedomDove";
  };
 
  module GiFreemasonry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFreemasonry";
  };
 
  module GiFrenchFries = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrenchFries";
  };
 
  module GiFriedEggs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFriedEggs";
  };
 
  module GiFriedFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFriedFish";
  };
 
  module GiFrisbee = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrisbee";
  };
 
  module GiFrogFoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrogFoot";
  };
 
  module GiFrogPrince = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrogPrince";
  };
 
  module GiFrog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrog";
  };
 
  module GiFrontTeeth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrontTeeth";
  };
 
  module GiFrontalLobe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrontalLobe";
  };
 
  module GiFrostfire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrostfire";
  };
 
  module GiFrozenArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrozenArrow";
  };
 
  module GiFrozenBlock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrozenBlock";
  };
 
  module GiFrozenBody = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrozenBody";
  };
 
  module GiFrozenOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrozenOrb";
  };
 
  module GiFrozenRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFrozenRing";
  };
 
  module GiFruitBowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFruitBowl";
  };
 
  module GiFruitTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFruitTree";
  };
 
  module GiFruiting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFruiting";
  };
 
  module GiFuelTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFuelTank";
  };
 
  module GiFuji = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFuji";
  };
 
  module GiFulguroPunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFulguroPunch";
  };
 
  module GiFullFolder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullFolder";
  };
 
  module GiFullMetalBucketHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullMetalBucketHandle";
  };
 
  module GiFullMetalBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullMetalBucket";
  };
 
  module GiFullMotorcycleHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullMotorcycleHelmet";
  };
 
  module GiFullPizza = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullPizza";
  };
 
  module GiFullWoodBucketHandle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullWoodBucketHandle";
  };
 
  module GiFullWoodBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFullWoodBucket";
  };
 
  module GiFunnel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFunnel";
  };
 
  module GiFurBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFurBoot";
  };
 
  module GiFurShirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFurShirt";
  };
 
  module GiFurnace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiFurnace";
  };
 
  module GiGClef = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGClef";
  };
 
  module GiGalea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGalea";
  };
 
  module GiGalleon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGalleon";
  };
 
  module GiGalley = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGalley";
  };
 
  module GiGameConsole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGameConsole";
  };
 
  module GiGamepadCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGamepadCross";
  };
 
  module GiGamepad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGamepad";
  };
 
  module GiGargoyle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGargoyle";
  };
 
  module GiGarlic = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGarlic";
  };
 
  module GiGasMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGasMask";
  };
 
  module GiGasPump = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGasPump";
  };
 
  module GiGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGate";
  };
 
  module GiGaulsHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGaulsHelm";
  };
 
  module GiGauntlet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGauntlet";
  };
 
  module GiGavel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGavel";
  };
 
  module GiGaze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGaze";
  };
 
  module GiGearHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGearHammer";
  };
 
  module GiGears = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGears";
  };
 
  module GiGecko = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGecko";
  };
 
  module GiGemChain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGemChain";
  };
 
  module GiGemNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGemNecklace";
  };
 
  module GiGemPendant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGemPendant";
  };
 
  module GiGemini = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGemini";
  };
 
  module GiGems = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGems";
  };
 
  module GiGhostAlly = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGhostAlly";
  };
 
  module GiGhost = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGhost";
  };
 
  module GiGiantSquid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGiantSquid";
  };
 
  module GiGiant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGiant";
  };
 
  module GiGibbet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGibbet";
  };
 
  module GiGiftOfKnowledge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGiftOfKnowledge";
  };
 
  module GiGiftTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGiftTrap";
  };
 
  module GiGingerbreadMan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGingerbreadMan";
  };
 
  module GiGinkgoLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGinkgoLeaf";
  };
 
  module GiGladius = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGladius";
  };
 
  module GiGlaive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlaive";
  };
 
  module GiGlassBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlassBall";
  };
 
  module GiGlassCelebration = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlassCelebration";
  };
 
  module GiGlassHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlassHeart";
  };
 
  module GiGlassShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlassShot";
  };
 
  module GiGlider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlider";
  };
 
  module GiGlobeRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlobeRing";
  };
 
  module GiGlobe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlobe";
  };
 
  module GiGlock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlock";
  };
 
  module GiGloop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGloop";
  };
 
  module GiGloves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGloves";
  };
 
  module GiGlowingHands = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGlowingHands";
  };
 
  module GiGluttonousSmile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGluttonousSmile";
  };
 
  module GiGluttony = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGluttony";
  };
 
  module GiGoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoat";
  };
 
  module GiGoblinCamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoblinCamp";
  };
 
  module GiGoblinHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoblinHead";
  };
 
  module GiGoldBar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldBar";
  };
 
  module GiGoldMine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldMine";
  };
 
  module GiGoldNuggets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldNuggets";
  };
 
  module GiGoldScarab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldScarab";
  };
 
  module GiGoldShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldShell";
  };
 
  module GiGoldStack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoldStack";
  };
 
  module GiGolemHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGolemHead";
  };
 
  module GiGolfFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGolfFlag";
  };
 
  module GiGolfTee = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGolfTee";
  };
 
  module GiGong = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGong";
  };
 
  module GiGooExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooExplosion";
  };
 
  module GiGooSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooSkull";
  };
 
  module GiGooSpurt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooSpurt";
  };
 
  module GiGooeyDaemon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooeyDaemon";
  };
 
  module GiGooeyEyedSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooeyEyedSun";
  };
 
  module GiGooeyImpact = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooeyImpact";
  };
 
  module GiGooeyMolecule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooeyMolecule";
  };
 
  module GiGooeySword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGooeySword";
  };
 
  module GiGoose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGoose";
  };
 
  module GiGorilla = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGorilla";
  };
 
  module GiGothicCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGothicCross";
  };
 
  module GiGps = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGps";
  };
 
  module GiGrab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrab";
  };
 
  module GiGraduateCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGraduateCap";
  };
 
  module GiGrainBundle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrainBundle";
  };
 
  module GiGrain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrain";
  };
 
  module GiGranary = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGranary";
  };
 
  module GiGrapes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrapes";
  };
 
  module GiGrapple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrapple";
  };
 
  module GiGraspingClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGraspingClaws";
  };
 
  module GiGraspingSlug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGraspingSlug";
  };
 
  module GiGrass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrass";
  };
 
  module GiGraveFlowers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGraveFlowers";
  };
 
  module GiGraveyard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGraveyard";
  };
 
  module GiGreaseTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreaseTrap";
  };
 
  module GiGreatPyramid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreatPyramid";
  };
 
  module GiGreatWarTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreatWarTank";
  };
 
  module GiGreaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreaves";
  };
 
  module GiGreekSphinx = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreekSphinx";
  };
 
  module GiGreekTemple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreekTemple";
  };
 
  module GiGreenPower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreenPower";
  };
 
  module GiGreenhouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGreenhouse";
  };
 
  module GiGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrenade";
  };
 
  module GiGriffinShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGriffinShield";
  };
 
  module GiGriffinSymbol = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGriffinSymbol";
  };
 
  module GiGrimReaper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrimReaper";
  };
 
  module GiGroundSprout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGroundSprout";
  };
 
  module GiGroundbreaker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGroundbreaker";
  };
 
  module GiGroupedDrops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGroupedDrops";
  };
 
  module GiGrowth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGrowth";
  };
 
  module GiGuardedTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuardedTower";
  };
 
  module GiGuards = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuards";
  };
 
  module GiGuillotine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuillotine";
  };
 
  module GiGuitarBassHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuitarBassHead";
  };
 
  module GiGuitarHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuitarHead";
  };
 
  module GiGuitar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGuitar";
  };
 
  module GiGunRose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGunRose";
  };
 
  module GiGunStock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGunStock";
  };
 
  module GiGunshot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGunshot";
  };
 
  module GiGymBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiGymBag";
  };
 
  module GiH2O = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiH2O";
  };
 
  module GiHabitatDome = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHabitatDome";
  };
 
  module GiHairStrands = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHairStrands";
  };
 
  module GiHalberdShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalberdShuriken";
  };
 
  module GiHalberd = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalberd";
  };
 
  module GiHalfBodyCrawling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalfBodyCrawling";
  };
 
  module GiHalfDead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalfDead";
  };
 
  module GiHalfHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalfHeart";
  };
 
  module GiHalfLog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalfLog";
  };
 
  module GiHalfTornado = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalfTornado";
  };
 
  module GiHalt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHalt";
  };
 
  module GiHamShank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHamShank";
  };
 
  module GiHamburgerMenu = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHamburgerMenu";
  };
 
  module GiHamburger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHamburger";
  };
 
  module GiHammerBreak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHammerBreak";
  };
 
  module GiHammerDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHammerDrop";
  };
 
  module GiHammerNails = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHammerNails";
  };
 
  module GiHammerSickle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHammerSickle";
  };
 
  module GiHandBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandBag";
  };
 
  module GiHandBandage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandBandage";
  };
 
  module GiHandGrip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandGrip";
  };
 
  module GiHandOfGod = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandOfGod";
  };
 
  module GiHandOk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandOk";
  };
 
  module GiHandSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandSaw";
  };
 
  module GiHandWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandWing";
  };
 
  module GiHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHand";
  };
 
  module GiHandcuffed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandcuffed";
  };
 
  module GiHandcuffs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandcuffs";
  };
 
  module GiHandheldFan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHandheldFan";
  };
 
  module GiHangGlider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHangGlider";
  };
 
  module GiHanger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHanger";
  };
 
  module GiHangingSign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHangingSign";
  };
 
  module GiHangingSpider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHangingSpider";
  };
 
  module GiHappySkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHappySkull";
  };
 
  module GiHarborDock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHarborDock";
  };
 
  module GiHarpoonChain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHarpoonChain";
  };
 
  module GiHarpoonTrident = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHarpoonTrident";
  };
 
  module GiHarpy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHarpy";
  };
 
  module GiHarryPotterSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHarryPotterSkull";
  };
 
  module GiHastyGrave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHastyGrave";
  };
 
  module GiHatchet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHatchet";
  };
 
  module GiHatchets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHatchets";
  };
 
  module GiHaunting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHaunting";
  };
 
  module GiHawkEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHawkEmblem";
  };
 
  module GiHazardSign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHazardSign";
  };
 
  module GiHazmatSuit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHazmatSuit";
  };
 
  module GiHeadShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeadShot";
  };
 
  module GiHeadbandKnot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeadbandKnot";
  };
 
  module GiHeadphones = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeadphones";
  };
 
  module GiHeadshot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeadshot";
  };
 
  module GiHealing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealing";
  };
 
  module GiHealthCapsule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealthCapsule";
  };
 
  module GiHealthDecrease = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealthDecrease";
  };
 
  module GiHealthIncrease = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealthIncrease";
  };
 
  module GiHealthNormal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealthNormal";
  };
 
  module GiHealthPotion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHealthPotion";
  };
 
  module GiHearingDisabled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHearingDisabled";
  };
 
  module GiHeartArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartArmor";
  };
 
  module GiHeartBattery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartBattery";
  };
 
  module GiHeartBeats = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartBeats";
  };
 
  module GiHeartBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartBottle";
  };
 
  module GiHeartDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartDrop";
  };
 
  module GiHeartEarrings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartEarrings";
  };
 
  module GiHeartInside = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartInside";
  };
 
  module GiHeartKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartKey";
  };
 
  module GiHeartMinus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartMinus";
  };
 
  module GiHeartNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartNecklace";
  };
 
  module GiHeartOrgan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartOrgan";
  };
 
  module GiHeartPlus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartPlus";
  };
 
  module GiHeartStake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartStake";
  };
 
  module GiHeartTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartTower";
  };
 
  module GiHeartWings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartWings";
  };
 
  module GiHeartburn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeartburn";
  };
 
  module GiHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHearts";
  };
 
  module GiHeatHaze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeatHaze";
  };
 
  module GiHeavyArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyArrow";
  };
 
  module GiHeavyBullets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyBullets";
  };
 
  module GiHeavyFall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyFall";
  };
 
  module GiHeavyFighter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyFighter";
  };
 
  module GiHeavyHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyHelm";
  };
 
  module GiHeavyLightning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyLightning";
  };
 
  module GiHeavyRain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyRain";
  };
 
  module GiHeavyThornyTriskelion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyThornyTriskelion";
  };
 
  module GiHeavyTimer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeavyTimer";
  };
 
  module GiHedjetWhiteCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHedjetWhiteCrown";
  };
 
  module GiHelicoprion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelicoprion";
  };
 
  module GiHelicopterTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelicopterTail";
  };
 
  module GiHelicopter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelicopter";
  };
 
  module GiHellCrosses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHellCrosses";
  };
 
  module GiHelmetHeadShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelmetHeadShot";
  };
 
  module GiHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelmet";
  };
 
  module GiHelp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHelp";
  };
 
  module GiHemp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHemp";
  };
 
  module GiHeptagram = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHeptagram";
  };
 
  module GiHerbsBundle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHerbsBundle";
  };
 
  module GiHexagonalNut = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHexagonalNut";
  };
 
  module GiHexes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHexes";
  };
 
  module GiHidden = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHidden";
  };
 
  module GiHieroglyphY = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHieroglyphY";
  };
 
  module GiHighFive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighFive";
  };
 
  module GiHighGrass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighGrass";
  };
 
  module GiHighHeel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighHeel";
  };
 
  module GiHighKick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighKick";
  };
 
  module GiHighPunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighPunch";
  };
 
  module GiHighShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighShot";
  };
 
  module GiHighTide = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHighTide";
  };
 
  module GiHillConquest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHillConquest";
  };
 
  module GiHillFort = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHillFort";
  };
 
  module GiHills = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHills";
  };
 
  module GiHistogram = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHistogram";
  };
 
  module GiHiveMind = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHiveMind";
  };
 
  module GiHive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHive";
  };
 
  module GiHobbitDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHobbitDoor";
  };
 
  module GiHobbitDwelling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHobbitDwelling";
  };
 
  module GiHockey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHockey";
  };
 
  module GiHoleLadder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoleLadder";
  };
 
  module GiHole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHole";
  };
 
  module GiHollowCat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHollowCat";
  };
 
  module GiHolosphere = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolosphere";
  };
 
  module GiHolyGrail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolyGrail";
  };
 
  module GiHolyHandGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolyHandGrenade";
  };
 
  module GiHolyOak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolyOak";
  };
 
  module GiHolySymbol = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolySymbol";
  };
 
  module GiHolyWater = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHolyWater";
  };
 
  module GiHomeGarage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHomeGarage";
  };
 
  module GiHoneyJar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoneyJar";
  };
 
  module GiHoneycomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoneycomb";
  };
 
  module GiHoneypot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoneypot";
  };
 
  module GiHood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHood";
  };
 
  module GiHoodedAssassin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoodedAssassin";
  };
 
  module GiHoodedFigure = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoodedFigure";
  };
 
  module GiHoodie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoodie";
  };
 
  module GiHoof = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHoof";
  };
 
  module GiHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHook";
  };
 
  module GiHops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHops";
  };
 
  module GiHorizontalFlip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHorizontalFlip";
  };
 
  module GiHornInternal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHornInternal";
  };
 
  module GiHornedHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHornedHelm";
  };
 
  module GiHornedReptile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHornedReptile";
  };
 
  module GiHornedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHornedSkull";
  };
 
  module GiHorseHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHorseHead";
  };
 
  module GiHorseshoe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHorseshoe";
  };
 
  module GiHorus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHorus";
  };
 
  module GiHospitalCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHospitalCross";
  };
 
  module GiHospital = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHospital";
  };
 
  module GiHotMeal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHotMeal";
  };
 
  module GiHotSpices = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHotSpices";
  };
 
  module GiHotSurface = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHotSurface";
  };
 
  module GiHound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHound";
  };
 
  module GiHourglass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHourglass";
  };
 
  module GiHouseKeys = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHouseKeys";
  };
 
  module GiHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHouse";
  };
 
  module GiHumanCannonball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHumanCannonball";
  };
 
  module GiHumanEar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHumanEar";
  };
 
  module GiHumanPyramid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHumanPyramid";
  };
 
  module GiHumanTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHumanTarget";
  };
 
  module GiHummingbird = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHummingbird";
  };
 
  module GiHungary = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHungary";
  };
 
  module GiHuntingBolas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHuntingBolas";
  };
 
  module GiHuntingHorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHuntingHorn";
  };
 
  module GiHut = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHut";
  };
 
  module GiHutsVillage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHutsVillage";
  };
 
  module GiHydraShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHydraShot";
  };
 
  module GiHydra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHydra";
  };
 
  module GiHypersonicBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHypersonicBolt";
  };
 
  module GiHypersonicMelon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHypersonicMelon";
  };
 
  module GiHypodermicTest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiHypodermicTest";
  };
 
  module GiIBeam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIBeam";
  };
 
  module GiIBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIBrick";
  };
 
  module GiIbis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIbis";
  };
 
  module GiIcarus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIcarus";
  };
 
  module GiIceBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceBolt";
  };
 
  module GiIceBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceBomb";
  };
 
  module GiIceCreamCone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceCreamCone";
  };
 
  module GiIceCreamScoop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceCreamScoop";
  };
 
  module GiIceCube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceCube";
  };
 
  module GiIceCubes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceCubes";
  };
 
  module GiIceGolem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceGolem";
  };
 
  module GiIceIris = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceIris";
  };
 
  module GiIcePop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIcePop";
  };
 
  module GiIceShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceShield";
  };
 
  module GiIceSkate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceSkate";
  };
 
  module GiIceSpear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceSpear";
  };
 
  module GiIceSpellCast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceSpellCast";
  };
 
  module GiIceberg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceberg";
  };
 
  module GiIcebergs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIcebergs";
  };
 
  module GiIceland = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIceland";
  };
 
  module GiIciclesAura = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIciclesAura";
  };
 
  module GiIciclesFence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIciclesFence";
  };
 
  module GiIdCard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIdCard";
  };
 
  module GiIdea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIdea";
  };
 
  module GiIfrit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIfrit";
  };
 
  module GiIgloo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIgloo";
  };
 
  module GiImbricatedArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImbricatedArrows";
  };
 
  module GiImpLaugh = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImpLaugh";
  };
 
  module GiImp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImp";
  };
 
  module GiImpactPoint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImpactPoint";
  };
 
  module GiImperialCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImperialCrown";
  };
 
  module GiImplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImplosion";
  };
 
  module GiImprisoned = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiImprisoned";
  };
 
  module GiIncense = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIncense";
  };
 
  module GiIncisors = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIncisors";
  };
 
  module GiIncomingRocket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIncomingRocket";
  };
 
  module GiIncubator = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIncubator";
  };
 
  module GiIndianPalace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIndianPalace";
  };
 
  module GiInfestedMass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInfestedMass";
  };
 
  module GiInfinity = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInfinity";
  };
 
  module GiInfo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInfo";
  };
 
  module GiInjustice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInjustice";
  };
 
  module GiInkSwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInkSwirl";
  };
 
  module GiInnerSelf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInnerSelf";
  };
 
  module GiInsectJaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInsectJaws";
  };
 
  module GiInspiration = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInspiration";
  };
 
  module GiInterceptorShip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterceptorShip";
  };
 
  module GiInterdiction = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterdiction";
  };
 
  module GiInterlacedTentacles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterlacedTentacles";
  };
 
  module GiInterleavedArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterleavedArrows";
  };
 
  module GiInterleavedClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterleavedClaws";
  };
 
  module GiInternalInjury = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInternalInjury";
  };
 
  module GiInternalOrgan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInternalOrgan";
  };
 
  module GiInterstellarPath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInterstellarPath";
  };
 
  module GiIntricateNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIntricateNecklace";
  };
 
  module GiInvertedDice1 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice1";
  };
 
  module GiInvertedDice2 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice2";
  };
 
  module GiInvertedDice3 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice3";
  };
 
  module GiInvertedDice4 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice4";
  };
 
  module GiInvertedDice5 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice5";
  };
 
  module GiInvertedDice6 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvertedDice6";
  };
 
  module GiInvisibleFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvisibleFace";
  };
 
  module GiInvisible = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiInvisible";
  };
 
  module GiIonCannonBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIonCannonBlast";
  };
 
  module GiIonicColumn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIonicColumn";
  };
 
  module GiIraq = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIraq";
  };
 
  module GiIronCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIronCross";
  };
 
  module GiIronHulledWarship = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIronHulledWarship";
  };
 
  module GiIronMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIronMask";
  };
 
  module GiIsland = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIsland";
  };
 
  module GiItalia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiItalia";
  };
 
  module GiIvoryTusks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiIvoryTusks";
  };
 
  module GiJBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJBrick";
  };
 
  module GiJackPlug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJackPlug";
  };
 
  module GiJamesBondAperture = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJamesBondAperture";
  };
 
  module GiJapan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJapan";
  };
 
  module GiJapaneseBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJapaneseBridge";
  };
 
  module GiJasmine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJasmine";
  };
 
  module GiJasonMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJasonMask";
  };
 
  module GiJawbone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJawbone";
  };
 
  module GiJawlessCyclop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJawlessCyclop";
  };
 
  module GiJeep = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJeep";
  };
 
  module GiJellyfish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJellyfish";
  };
 
  module GiJerrycan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJerrycan";
  };
 
  module GiJerusalemCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJerusalemCross";
  };
 
  module GiJesterHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJesterHat";
  };
 
  module GiJetFighter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJetFighter";
  };
 
  module GiJetPack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJetPack";
  };
 
  module GiJetpack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJetpack";
  };
 
  module GiJewelCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJewelCrown";
  };
 
  module GiJeweledChalice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJeweledChalice";
  };
 
  module GiJigsawBox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJigsawBox";
  };
 
  module GiJigsawPiece = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJigsawPiece";
  };
 
  module GiJoin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJoin";
  };
 
  module GiJoint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJoint";
  };
 
  module GiJourney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJourney";
  };
 
  module GiJoystick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJoystick";
  };
 
  module GiJug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJug";
  };
 
  module GiJuggler = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJuggler";
  };
 
  module GiJugglingClubs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJugglingClubs";
  };
 
  module GiJugglingSeal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJugglingSeal";
  };
 
  module GiJumpAcross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJumpAcross";
  };
 
  module GiJumpingDog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJumpingDog";
  };
 
  module GiJumpingRope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJumpingRope";
  };
 
  module GiJungle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJungle";
  };
 
  module GiJupiter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJupiter";
  };
 
  module GiJusticeStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiJusticeStar";
  };
 
  module GiKaleidoscopePearls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKaleidoscopePearls";
  };
 
  module GiKangaroo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKangaroo";
  };
 
  module GiKatana = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKatana";
  };
 
  module GiKebabSpit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKebabSpit";
  };
 
  module GiKenkuHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKenkuHead";
  };
 
  module GiKenya = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKenya";
  };
 
  module GiKevlarVest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKevlarVest";
  };
 
  module GiKevlar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKevlar";
  };
 
  module GiKeyCard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKeyCard";
  };
 
  module GiKeyLock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKeyLock";
  };
 
  module GiKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKey";
  };
 
  module GiKeyboard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKeyboard";
  };
 
  module GiKeyring = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKeyring";
  };
 
  module GiKickScooter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKickScooter";
  };
 
  module GiKidneys = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKidneys";
  };
 
  module GiKimono = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKimono";
  };
 
  module GiKindle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKindle";
  };
 
  module GiKingJuMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKingJuMask";
  };
 
  module GiKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKing";
  };
 
  module GiKitchenKnives = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKitchenKnives";
  };
 
  module GiKitchenScale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKitchenScale";
  };
 
  module GiKite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKite";
  };
 
  module GiKiwiBird = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKiwiBird";
  };
 
  module GiKiwiFruit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKiwiFruit";
  };
 
  module GiKlingon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKlingon";
  };
 
  module GiKnapsack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnapsack";
  };
 
  module GiKneeCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKneeCap";
  };
 
  module GiKneePad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKneePad";
  };
 
  module GiKneeling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKneeling";
  };
 
  module GiKnifeFork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnifeFork";
  };
 
  module GiKnifeThrust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnifeThrust";
  };
 
  module GiKnightBanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnightBanner";
  };
 
  module GiKnockout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnockout";
  };
 
  module GiKnot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKnot";
  };
 
  module GiKoala = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKoala";
  };
 
  module GiKrakenTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKrakenTentacle";
  };
 
  module GiKusarigama = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiKusarigama";
  };
 
  module GiLBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLBrick";
  };
 
  module GiLabCoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLabCoat";
  };
 
  module GiLabradorHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLabradorHead";
  };
 
  module GiLadder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLadder";
  };
 
  module GiLaddersPlatform = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaddersPlatform";
  };
 
  module GiLadle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLadle";
  };
 
  module GiLadybug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLadybug";
  };
 
  module GiLamellar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLamellar";
  };
 
  module GiLampreyMouth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLampreyMouth";
  };
 
  module GiLandMine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLandMine";
  };
 
  module GiLanternFlame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLanternFlame";
  };
 
  module GiLantern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLantern";
  };
 
  module GiLaptop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaptop";
  };
 
  module GiLargeDress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLargeDress";
  };
 
  module GiLargePaintBrush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLargePaintBrush";
  };
 
  module GiLargeWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLargeWound";
  };
 
  module GiLaserBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserBlast";
  };
 
  module GiLaserBurst = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserBurst";
  };
 
  module GiLaserGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserGun";
  };
 
  module GiLaserPrecision = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserPrecision";
  };
 
  module GiLaserSparks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserSparks";
  };
 
  module GiLaserTurret = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserTurret";
  };
 
  module GiLaserWarning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserWarning";
  };
 
  module GiLaserburn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaserburn";
  };
 
  module GiLasso = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLasso";
  };
 
  module GiLatvia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLatvia";
  };
 
  module GiLaurelCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaurelCrown";
  };
 
  module GiLaurelsTrophy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaurelsTrophy";
  };
 
  module GiLaurels = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLaurels";
  };
 
  module GiLava = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLava";
  };
 
  module GiLawStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLawStar";
  };
 
  module GiLayeredArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLayeredArmor";
  };
 
  module GiLeadPipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeadPipe";
  };
 
  module GiLeafSkeleton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeafSkeleton";
  };
 
  module GiLeafSwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeafSwirl";
  };
 
  module GiLeak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeak";
  };
 
  module GiLeakySkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeakySkull";
  };
 
  module GiLeapfrog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeapfrog";
  };
 
  module GiLeatherArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeatherArmor";
  };
 
  module GiLeatherBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeatherBoot";
  };
 
  module GiLeatherVest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeatherVest";
  };
 
  module GiLed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLed";
  };
 
  module GiLeeEnfield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeeEnfield";
  };
 
  module GiLeechingWorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeechingWorm";
  };
 
  module GiLeek = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeek";
  };
 
  module GiLegArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLegArmor";
  };
 
  module GiLeg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeg";
  };
 
  module GiLemon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLemon";
  };
 
  module GiLeo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLeo";
  };
 
  module GiLetterBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLetterBomb";
  };
 
  module GiLevelCrossing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelCrossing";
  };
 
  module GiLevelFourAdvanced = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelFourAdvanced";
  };
 
  module GiLevelFour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelFour";
  };
 
  module GiLevelThreeAdvanced = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelThreeAdvanced";
  };
 
  module GiLevelThree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelThree";
  };
 
  module GiLevelTwoAdvanced = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelTwoAdvanced";
  };
 
  module GiLevelTwo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLevelTwo";
  };
 
  module GiLever = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLever";
  };
 
  module GiLiar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLiar";
  };
 
  module GiLibertyWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLibertyWing";
  };
 
  module GiLibra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLibra";
  };
 
  module GiLibya = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLibya";
  };
 
  module GiLifeBar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeBar";
  };
 
  module GiLifeBuoy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeBuoy";
  };
 
  module GiLifeInTheBalance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeInTheBalance";
  };
 
  module GiLifeJacket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeJacket";
  };
 
  module GiLifeSupport = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeSupport";
  };
 
  module GiLifeTap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLifeTap";
  };
 
  module GiLift = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLift";
  };
 
  module GiLightBackpack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightBackpack";
  };
 
  module GiLightBulb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightBulb";
  };
 
  module GiLightFighter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightFighter";
  };
 
  module GiLightHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightHelm";
  };
 
  module GiLightProjector = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightProjector";
  };
 
  module GiLightSabers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightSabers";
  };
 
  module GiLightThornyTriskelion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightThornyTriskelion";
  };
 
  module GiLighter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLighter";
  };
 
  module GiLighthouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLighthouse";
  };
 
  module GiLightningArc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningArc";
  };
 
  module GiLightningBow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningBow";
  };
 
  module GiLightningBranches = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningBranches";
  };
 
  module GiLightningDissipation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningDissipation";
  };
 
  module GiLightningDome = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningDome";
  };
 
  module GiLightningElectron = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningElectron";
  };
 
  module GiLightningFlame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningFlame";
  };
 
  module GiLightningFrequency = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningFrequency";
  };
 
  module GiLightningHelix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningHelix";
  };
 
  module GiLightningMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningMask";
  };
 
  module GiLightningSaber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningSaber";
  };
 
  module GiLightningShadow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningShadow";
  };
 
  module GiLightningShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningShield";
  };
 
  module GiLightningShout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningShout";
  };
 
  module GiLightningSlashes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningSlashes";
  };
 
  module GiLightningSpanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningSpanner";
  };
 
  module GiLightningStorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningStorm";
  };
 
  module GiLightningTear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningTear";
  };
 
  module GiLightningTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningTree";
  };
 
  module GiLightningTrio = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLightningTrio";
  };
 
  module GiLilyPads = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLilyPads";
  };
 
  module GiLindenLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLindenLeaf";
  };
 
  module GiLinkedRings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLinkedRings";
  };
 
  module GiLion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLion";
  };
 
  module GiLips = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLips";
  };
 
  module GiLipstick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLipstick";
  };
 
  module GiLitCandelabra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLitCandelabra";
  };
 
  module GiLiver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLiver";
  };
 
  module GiLizardTongue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLizardTongue";
  };
 
  module GiLizardman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLizardman";
  };
 
  module GiLoad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoad";
  };
 
  module GiLobArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLobArrow";
  };
 
  module GiLockPicking = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockPicking";
  };
 
  module GiLockSpy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockSpy";
  };
 
  module GiLockedBox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockedBox";
  };
 
  module GiLockedChest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockedChest";
  };
 
  module GiLockedDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockedDoor";
  };
 
  module GiLockedFortress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockedFortress";
  };
 
  module GiLockedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockedHeart";
  };
 
  module GiLockpicks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLockpicks";
  };
 
  module GiLog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLog";
  };
 
  module GiLogging = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogging";
  };
 
  module GiLogicGateAnd = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateAnd";
  };
 
  module GiLogicGateNand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateNand";
  };
 
  module GiLogicGateNor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateNor";
  };
 
  module GiLogicGateNot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateNot";
  };
 
  module GiLogicGateNxor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateNxor";
  };
 
  module GiLogicGateOr = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateOr";
  };
 
  module GiLogicGateXor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLogicGateXor";
  };
 
  module GiLoincloth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoincloth";
  };
 
  module GiLongAntennaeBug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLongAntennaeBug";
  };
 
  module GiLongLeggedSpider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLongLeggedSpider";
  };
 
  module GiLookAt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLookAt";
  };
 
  module GiLorgnette = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLorgnette";
  };
 
  module GiLostLimb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLostLimb";
  };
 
  module GiLotusFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLotusFlower";
  };
 
  module GiLotus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLotus";
  };
 
  module GiLouvrePyramid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLouvrePyramid";
  };
 
  module GiLoveHowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoveHowl";
  };
 
  module GiLoveInjection = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoveInjection";
  };
 
  module GiLoveLetter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoveLetter";
  };
 
  module GiLoveMystery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoveMystery";
  };
 
  module GiLoveSong = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLoveSong";
  };
 
  module GiLovers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLovers";
  };
 
  module GiLowTide = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLowTide";
  };
 
  module GiLuchador = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLuchador";
  };
 
  module GiLuciferCannon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLuciferCannon";
  };
 
  module GiLuckyFisherman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLuckyFisherman";
  };
 
  module GiLuger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLuger";
  };
 
  module GiLunarModule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLunarModule";
  };
 
  module GiLunarWand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLunarWand";
  };
 
  module GiLungs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLungs";
  };
 
  module GiLynxHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLynxHead";
  };
 
  module GiLyre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiLyre";
  };
 
  module GiM3GreaseGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiM3GreaseGun";
  };
 
  module GiMac10 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMac10";
  };
 
  module GiMaceHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaceHead";
  };
 
  module GiMachete = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMachete";
  };
 
  module GiMachineGunMagazine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMachineGunMagazine";
  };
 
  module GiMachineGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMachineGun";
  };
 
  module GiMadScientist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMadScientist";
  };
 
  module GiMaggot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaggot";
  };
 
  module GiMagicAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicAxe";
  };
 
  module GiMagicGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicGate";
  };
 
  module GiMagicHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicHat";
  };
 
  module GiMagicLamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicLamp";
  };
 
  module GiMagicPalm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicPalm";
  };
 
  module GiMagicPortal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicPortal";
  };
 
  module GiMagicPotion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicPotion";
  };
 
  module GiMagicShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicShield";
  };
 
  module GiMagicSwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicSwirl";
  };
 
  module GiMagicTrident = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagicTrident";
  };
 
  module GiMagickTrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagickTrick";
  };
 
  module GiMagnetBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagnetBlast";
  };
 
  module GiMagnet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagnet";
  };
 
  module GiMagnifyingGlass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMagnifyingGlass";
  };
 
  module GiMailShirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMailShirt";
  };
 
  module GiMailbox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMailbox";
  };
 
  module GiMailedFist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMailedFist";
  };
 
  module GiMale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMale";
  };
 
  module GiMammoth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMammoth";
  };
 
  module GiManacles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiManacles";
  };
 
  module GiMandrillHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMandrillHead";
  };
 
  module GiMantaRay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMantaRay";
  };
 
  module GiMantrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMantrap";
  };
 
  module GiManualJuicer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiManualJuicer";
  };
 
  module GiManualMeatGrinder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiManualMeatGrinder";
  };
 
  module GiMapleLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMapleLeaf";
  };
 
  module GiMaracas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaracas";
  };
 
  module GiMarbles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMarbles";
  };
 
  module GiMarrowDrain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMarrowDrain";
  };
 
  module GiMarsCuriosity = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMarsCuriosity";
  };
 
  module GiMarsPathfinder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMarsPathfinder";
  };
 
  module GiMarshmallows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMarshmallows";
  };
 
  module GiMartini = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMartini";
  };
 
  module GiMaskedSpider = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaskedSpider";
  };
 
  module GiMassDriver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMassDriver";
  };
 
  module GiMasterOfArms = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMasterOfArms";
  };
 
  module GiMatchHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMatchHead";
  };
 
  module GiMatchTip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMatchTip";
  };
 
  module GiMatchbox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMatchbox";
  };
 
  module GiMaterialsScience = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaterialsScience";
  };
 
  module GiMatryoshkaDolls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMatryoshkaDolls";
  };
 
  module GiMatterStates = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMatterStates";
  };
 
  module GiMayanPyramid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMayanPyramid";
  };
 
  module GiMazeCornea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMazeCornea";
  };
 
  module GiMazeSaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMazeSaw";
  };
 
  module GiMaze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMaze";
  };
 
  module GiMeal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeal";
  };
 
  module GiMeatCleaver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeatCleaver";
  };
 
  module GiMeatHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeatHook";
  };
 
  module GiMeat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeat";
  };
 
  module GiMechaHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMechaHead";
  };
 
  module GiMechaMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMechaMask";
  };
 
  module GiMechanicGarage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMechanicGarage";
  };
 
  module GiMechanicalArm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMechanicalArm";
  };
 
  module GiMedalSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedalSkull";
  };
 
  module GiMedal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedal";
  };
 
  module GiMedallist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedallist";
  };
 
  module GiMedicalPackAlt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedicalPackAlt";
  };
 
  module GiMedicalPack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedicalPack";
  };
 
  module GiMedicalThermometer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedicalThermometer";
  };
 
  module GiMedicinePills = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedicinePills";
  };
 
  module GiMedicines = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedicines";
  };
 
  module GiMedievalBarracks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedievalBarracks";
  };
 
  module GiMedievalGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedievalGate";
  };
 
  module GiMedievalPavilion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedievalPavilion";
  };
 
  module GiMeditation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeditation";
  };
 
  module GiMedusaHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMedusaHead";
  };
 
  module GiMeepleGroup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeepleGroup";
  };
 
  module GiMeepleKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeepleKing";
  };
 
  module GiMeeple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeeple";
  };
 
  module GiMegabot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMegabot";
  };
 
  module GiMegaphone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMegaphone";
  };
 
  module GiMeltingIceCube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeltingIceCube";
  };
 
  module GiMeltingMetal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeltingMetal";
  };
 
  module GiMenhir = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMenhir";
  };
 
  module GiMermaid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMermaid";
  };
 
  module GiMeshBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeshBall";
  };
 
  module GiMeshNetwork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeshNetwork";
  };
 
  module GiMetalBar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalBar";
  };
 
  module GiMetalDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalDisc";
  };
 
  module GiMetalHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalHand";
  };
 
  module GiMetalPlate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalPlate";
  };
 
  module GiMetalScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalScales";
  };
 
  module GiMetalSkirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetalSkirt";
  };
 
  module GiMeteorImpact = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMeteorImpact";
  };
 
  module GiMetroid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetroid";
  };
 
  module GiMetronome = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMetronome";
  };
 
  module GiMexico = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMexico";
  };
 
  module GiMicrochip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMicrochip";
  };
 
  module GiMicrophone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMicrophone";
  };
 
  module GiMicroscopeLens = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMicroscopeLens";
  };
 
  module GiMicroscope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMicroscope";
  };
 
  module GiMiddleArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiddleArrow";
  };
 
  module GiMidnightClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMidnightClaw";
  };
 
  module GiMightyBoosh = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMightyBoosh";
  };
 
  module GiMightyForce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMightyForce";
  };
 
  module GiMightySpanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMightySpanner";
  };
 
  module GiMilitaryAmbulance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMilitaryAmbulance";
  };
 
  module GiMilitaryFort = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMilitaryFort";
  };
 
  module GiMilkCarton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMilkCarton";
  };
 
  module GiMilleniumKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMilleniumKey";
  };
 
  module GiMimicChest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMimicChest";
  };
 
  module GiMineExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMineExplosion";
  };
 
  module GiMineTruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMineTruck";
  };
 
  module GiMineWagon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMineWagon";
  };
 
  module GiMinefield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMinefield";
  };
 
  module GiMiner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiner";
  };
 
  module GiMineralHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMineralHeart";
  };
 
  module GiMinerals = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMinerals";
  };
 
  module GiMiniSubmarine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiniSubmarine";
  };
 
  module GiMinigun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMinigun";
  };
 
  module GiMiningHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiningHelmet";
  };
 
  module GiMining = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMining";
  };
 
  module GiMinions = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMinions";
  };
 
  module GiMinotaur = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMinotaur";
  };
 
  module GiMiracleMedecine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiracleMedecine";
  };
 
  module GiMirrorMirror = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMirrorMirror";
  };
 
  module GiMissileLauncher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMissileLauncher";
  };
 
  module GiMissileMech = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMissileMech";
  };
 
  module GiMissilePod = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMissilePod";
  };
 
  module GiMissileSwarm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMissileSwarm";
  };
 
  module GiMiteAlt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMiteAlt";
  };
 
  module GiMite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMite";
  };
 
  module GiMoai = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoai";
  };
 
  module GiModernCity = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiModernCity";
  };
 
  module GiMoebiusStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoebiusStar";
  };
 
  module GiMoebiusTrefoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoebiusTrefoil";
  };
 
  module GiMoebiusTriangle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoebiusTriangle";
  };
 
  module GiMokaPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMokaPot";
  };
 
  module GiMoldova = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoldova";
  };
 
  module GiMolecule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMolecule";
  };
 
  module GiMolotov = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMolotov";
  };
 
  module GiMonaLisa = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonaLisa";
  };
 
  module GiMonclerJacket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonclerJacket";
  };
 
  module GiMoneyStack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoneyStack";
  };
 
  module GiMonkFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonkFace";
  };
 
  module GiMonkeyWrench = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonkeyWrench";
  };
 
  module GiMonkey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonkey";
  };
 
  module GiMonoWheelRobot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonoWheelRobot";
  };
 
  module GiMonsterGrasp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonsterGrasp";
  };
 
  module GiMonsteraLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonsteraLeaf";
  };
 
  module GiMonumentValley = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMonumentValley";
  };
 
  module GiMoonBats = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoonBats";
  };
 
  module GiMoonClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoonClaws";
  };
 
  module GiMoonOrbit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoonOrbit";
  };
 
  module GiMoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMoon";
  };
 
  module GiMooringBollard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMooringBollard";
  };
 
  module GiMorbidHumour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMorbidHumour";
  };
 
  module GiMorgueFeet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMorgueFeet";
  };
 
  module GiMorphBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMorphBall";
  };
 
  module GiMortar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMortar";
  };
 
  module GiMountainCave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMountainCave";
  };
 
  module GiMountainRoad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMountainRoad";
  };
 
  module GiMountains = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMountains";
  };
 
  module GiMountaintop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMountaintop";
  };
 
  module GiMountedKnight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMountedKnight";
  };
 
  module GiMouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMouse";
  };
 
  module GiMouthWatering = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMouthWatering";
  };
 
  module GiMove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMove";
  };
 
  module GiMovementSensor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMovementSensor";
  };
 
  module GiMp40 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMp40";
  };
 
  module GiMp5 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMp5";
  };
 
  module GiMp5K = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMp5K";
  };
 
  module GiMucousPillar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMucousPillar";
  };
 
  module GiMugShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMugShot";
  };
 
  module GiMultipleTargets = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMultipleTargets";
  };
 
  module GiMummyHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMummyHead";
  };
 
  module GiMuscleFat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMuscleFat";
  };
 
  module GiMuscleUp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMuscleUp";
  };
 
  module GiMuscularTorso = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMuscularTorso";
  };
 
  module GiMushroomCloud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushroomCloud";
  };
 
  module GiMushroomGills = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushroomGills";
  };
 
  module GiMushroomHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushroomHouse";
  };
 
  module GiMushroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushroom";
  };
 
  module GiMushroomsCluster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushroomsCluster";
  };
 
  module GiMushrooms = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMushrooms";
  };
 
  module GiMusicSpell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMusicSpell";
  };
 
  module GiMusicalKeyboard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMusicalKeyboard";
  };
 
  module GiMusicalNotes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMusicalNotes";
  };
 
  module GiMusicalScore = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMusicalScore";
  };
 
  module GiMusket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMusket";
  };
 
  module GiMussel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMussel";
  };
 
  module GiMustache = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMustache";
  };
 
  module GiMute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiMute";
  };
 
  module GiNachos = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNachos";
  };
 
  module GiNailedFoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNailedFoot";
  };
 
  module GiNailedHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNailedHead";
  };
 
  module GiNails = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNails";
  };
 
  module GiNanoBot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNanoBot";
  };
 
  module GiNautilusShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNautilusShell";
  };
 
  module GiNeckBite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNeckBite";
  };
 
  module GiNecklaceDisplay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNecklaceDisplay";
  };
 
  module GiNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNecklace";
  };
 
  module GiNeedleDrill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNeedleDrill";
  };
 
  module GiNeedleJaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNeedleJaws";
  };
 
  module GiNefertiti = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNefertiti";
  };
 
  module GiNestBirds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNestBirds";
  };
 
  module GiNestEggs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNestEggs";
  };
 
  module GiNestedEclipses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNestedEclipses";
  };
 
  module GiNestedHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNestedHearts";
  };
 
  module GiNestedHexagons = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNestedHexagons";
  };
 
  module GiNetworkBars = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNetworkBars";
  };
 
  module GiNewBorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNewBorn";
  };
 
  module GiNewShoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNewShoot";
  };
 
  module GiNewspaper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNewspaper";
  };
 
  module GiNextButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNextButton";
  };
 
  module GiNightSky = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNightSky";
  };
 
  module GiNightSleep = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNightSleep";
  };
 
  module GiNightVision = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNightVision";
  };
 
  module GiNinjaArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaArmor";
  };
 
  module GiNinjaHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaHead";
  };
 
  module GiNinjaHeroicStance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaHeroicStance";
  };
 
  module GiNinjaMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaMask";
  };
 
  module GiNinjaStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaStar";
  };
 
  module GiNinjaVelociraptor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNinjaVelociraptor";
  };
 
  module GiNodular = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNodular";
  };
 
  module GiNoodleBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNoodleBall";
  };
 
  module GiNoodles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNoodles";
  };
 
  module GiNorthStarShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNorthStarShuriken";
  };
 
  module GiNoseFront = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNoseFront";
  };
 
  module GiNoseSide = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNoseSide";
  };
 
  module GiNothingToSay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNothingToSay";
  };
 
  module GiNuclearBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNuclearBomb";
  };
 
  module GiNuclearPlant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNuclearPlant";
  };
 
  module GiNuclearWaste = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNuclearWaste";
  };
 
  module GiNuclear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNuclear";
  };
 
  module GiNunFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNunFace";
  };
 
  module GiNunchaku = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNunchaku";
  };
 
  module GiNurseFemale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNurseFemale";
  };
 
  module GiNurseMale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiNurseMale";
  };
 
  module GiOBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOBrick";
  };
 
  module GiOakLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOakLeaf";
  };
 
  module GiOak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOak";
  };
 
  module GiOasis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOasis";
  };
 
  module GiOat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOat";
  };
 
  module GiObelisk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiObelisk";
  };
 
  module GiObservatory = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiObservatory";
  };
 
  module GiOcarina = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOcarina";
  };
 
  module GiOccupy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOccupy";
  };
 
  module GiOctogonalEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOctogonalEye";
  };
 
  module GiOctoman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOctoman";
  };
 
  module GiOctopus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOctopus";
  };
 
  module GiOden = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOden";
  };
 
  module GiOfficeChair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOfficeChair";
  };
 
  module GiOffshorePlatform = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOffshorePlatform";
  };
 
  module GiOgre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOgre";
  };
 
  module GiOilDrum = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOilDrum";
  };
 
  module GiOilPump = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOilPump";
  };
 
  module GiOilRig = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOilRig";
  };
 
  module GiOilySpiral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOilySpiral";
  };
 
  module GiOldKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOldKing";
  };
 
  module GiOldLantern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOldLantern";
  };
 
  module GiOldMicrophone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOldMicrophone";
  };
 
  module GiOldWagon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOldWagon";
  };
 
  module GiOlive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOlive";
  };
 
  module GiOmega = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOmega";
  };
 
  module GiOnSight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOnSight";
  };
 
  module GiOnTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOnTarget";
  };
 
  module GiOneEyed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOneEyed";
  };
 
  module GiOni = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOni";
  };
 
  module GiOpenBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenBook";
  };
 
  module GiOpenChest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenChest";
  };
 
  module GiOpenFolder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenFolder";
  };
 
  module GiOpenGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenGate";
  };
 
  module GiOpenPalm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenPalm";
  };
 
  module GiOpenTreasureChest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenTreasureChest";
  };
 
  module GiOpenWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenWound";
  };
 
  module GiOpenedFoodCan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpenedFoodCan";
  };
 
  module GiOpeningShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOpeningShell";
  };
 
  module GiOphiuchus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOphiuchus";
  };
 
  module GiOppidum = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOppidum";
  };
 
  module GiOppositeHearts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOppositeHearts";
  };
 
  module GiOppression = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOppression";
  };
 
  module GiOrangeSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrangeSlice";
  };
 
  module GiOrange = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrange";
  };
 
  module GiOrbDirection = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrbDirection";
  };
 
  module GiOrbWand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrbWand";
  };
 
  module GiOrbit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrbit";
  };
 
  module GiOrbitalRays = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrbitalRays";
  };
 
  module GiOrbital = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrbital";
  };
 
  module GiOrcHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrcHead";
  };
 
  module GiOre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOre";
  };
 
  module GiOrganigram = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOrganigram";
  };
 
  module GiOstrich = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOstrich";
  };
 
  module GiOuroboros = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOuroboros";
  };
 
  module GiOutbackHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOutbackHat";
  };
 
  module GiOverInfinity = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverInfinity";
  };
 
  module GiOverdose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverdose";
  };
 
  module GiOverdrive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverdrive";
  };
 
  module GiOverhead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverhead";
  };
 
  module GiOverkill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverkill";
  };
 
  module GiOverlordHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOverlordHelm";
  };
 
  module GiOvermind = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOvermind";
  };
 
  module GiOwl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOwl";
  };
 
  module GiOysterPearl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiOysterPearl";
  };
 
  module GiP90 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiP90";
  };
 
  module GiPackedPlanks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPackedPlanks";
  };
 
  module GiPaddles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaddles";
  };
 
  module GiPadlockOpen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPadlockOpen";
  };
 
  module GiPadlock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPadlock";
  };
 
  module GiPagoda = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPagoda";
  };
 
  module GiPaintBrush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaintBrush";
  };
 
  module GiPaintBucket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaintBucket";
  };
 
  module GiPaintRoller = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaintRoller";
  };
 
  module GiPaintedPottery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaintedPottery";
  };
 
  module GiPalette = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPalette";
  };
 
  module GiPalisade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPalisade";
  };
 
  module GiPalmTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPalmTree";
  };
 
  module GiPalm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPalm";
  };
 
  module GiPanFlute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPanFlute";
  };
 
  module GiPanda = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPanda";
  };
 
  module GiPanzerfaust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPanzerfaust";
  };
 
  module GiPaperArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperArrow";
  };
 
  module GiPaperBoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperBoat";
  };
 
  module GiPaperBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperBomb";
  };
 
  module GiPaperClip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperClip";
  };
 
  module GiPaperLantern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperLantern";
  };
 
  module GiPaperPlane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperPlane";
  };
 
  module GiPaperTray = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperTray";
  };
 
  module GiPaperWindmill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaperWindmill";
  };
 
  module GiPaper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaper";
  };
 
  module GiPapers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPapers";
  };
 
  module GiPapyrus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPapyrus";
  };
 
  module GiParachute = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParachute";
  };
 
  module GiParaguay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParaguay";
  };
 
  module GiParanoia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParanoia";
  };
 
  module GiParasaurolophus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParasaurolophus";
  };
 
  module GiParkBench = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParkBench";
  };
 
  module GiParmecia = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParmecia";
  };
 
  module GiParrotHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiParrotHead";
  };
 
  module GiPartyFlags = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPartyFlags";
  };
 
  module GiPartyHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPartyHat";
  };
 
  module GiPartyPopper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPartyPopper";
  };
 
  module GiPassport = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPassport";
  };
 
  module GiPathDistance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPathDistance";
  };
 
  module GiPathTile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPathTile";
  };
 
  module GiPauldrons = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPauldrons";
  };
 
  module GiPauseButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPauseButton";
  };
 
  module GiPawFront = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPawFront";
  };
 
  module GiPawHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPawHeart";
  };
 
  module GiPawPrint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPawPrint";
  };
 
  module GiPaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPaw";
  };
 
  module GiPawn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPawn";
  };
 
  module GiPayMoney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPayMoney";
  };
 
  module GiPc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPc";
  };
 
  module GiPeaceDove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeaceDove";
  };
 
  module GiPeach = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeach";
  };
 
  module GiPeaks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeaks";
  };
 
  module GiPeanut = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeanut";
  };
 
  module GiPear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPear";
  };
 
  module GiPearlEarring = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPearlEarring";
  };
 
  module GiPearlNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPearlNecklace";
  };
 
  module GiPeas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeas";
  };
 
  module GiPegasus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPegasus";
  };
 
  module GiPelvisBone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPelvisBone";
  };
 
  module GiPencilBrush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPencilBrush";
  };
 
  module GiPencilRuler = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPencilRuler";
  };
 
  module GiPencil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPencil";
  };
 
  module GiPendantKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPendantKey";
  };
 
  module GiPendulumSwing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPendulumSwing";
  };
 
  module GiPenguin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPenguin";
  };
 
  module GiPentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPentacle";
  };
 
  module GiPentagramRose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPentagramRose";
  };
 
  module GiPentarrowsTornado = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPentarrowsTornado";
  };
 
  module GiPerfumeBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerfumeBottle";
  };
 
  module GiPeriscope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeriscope";
  };
 
  module GiPerpendicularRings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerpendicularRings";
  };
 
  module GiPersonInBed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPersonInBed";
  };
 
  module GiPerson = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerson";
  };
 
  module GiPerspectiveDiceFive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceFive";
  };
 
  module GiPerspectiveDiceFour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceFour";
  };
 
  module GiPerspectiveDiceOne = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceOne";
  };
 
  module GiPerspectiveDiceSixFacesFive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesFive";
  };
 
  module GiPerspectiveDiceSixFacesFour = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesFour";
  };
 
  module GiPerspectiveDiceSixFacesOne = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesOne";
  };
 
  module GiPerspectiveDiceSixFacesRandom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesRandom";
  };
 
  module GiPerspectiveDiceSixFacesSix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesSix";
  };
 
  module GiPerspectiveDiceSixFacesThree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesThree";
  };
 
  module GiPerspectiveDiceSixFacesTwo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSixFacesTwo";
  };
 
  module GiPerspectiveDiceSix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceSix";
  };
 
  module GiPerspectiveDiceThree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceThree";
  };
 
  module GiPerspectiveDiceTwo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPerspectiveDiceTwo";
  };
 
  module GiPeru = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPeru";
  };
 
  module GiPestleMortar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPestleMortar";
  };
 
  module GiPharoah = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPharoah";
  };
 
  module GiPhone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPhone";
  };
 
  module GiPhotoCamera = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPhotoCamera";
  };
 
  module GiPhrygianCap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPhrygianCap";
  };
 
  module GiPianoKeys = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPianoKeys";
  };
 
  module GiPickOfDestiny = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPickOfDestiny";
  };
 
  module GiPickelhaube = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPickelhaube";
  };
 
  module GiPieChart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPieChart";
  };
 
  module GiPieSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPieSlice";
  };
 
  module GiPieceSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPieceSkull";
  };
 
  module GiPiercedBody = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPiercedBody";
  };
 
  module GiPiercedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPiercedHeart";
  };
 
  module GiPiercingSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPiercingSword";
  };
 
  module GiPigFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPigFace";
  };
 
  module GiPig = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPig";
  };
 
  module GiPiggyBank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPiggyBank";
  };
 
  module GiPikeman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPikeman";
  };
 
  module GiPilgrimHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPilgrimHat";
  };
 
  module GiPillDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPillDrop";
  };
 
  module GiPill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPill";
  };
 
  module GiPillow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPillow";
  };
 
  module GiPimiento = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPimiento";
  };
 
  module GiPin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPin";
  };
 
  module GiPinata = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPinata";
  };
 
  module GiPinballFlipper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPinballFlipper";
  };
 
  module GiPincers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPincers";
  };
 
  module GiPineTree = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPineTree";
  };
 
  module GiPineapple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPineapple";
  };
 
  module GiPingPongBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPingPongBat";
  };
 
  module GiPipes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPipes";
  };
 
  module GiPiranha = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPiranha";
  };
 
  module GiPirateCannon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateCannon";
  };
 
  module GiPirateCaptain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateCaptain";
  };
 
  module GiPirateCoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateCoat";
  };
 
  module GiPirateFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateFlag";
  };
 
  module GiPirateGrave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateGrave";
  };
 
  module GiPirateHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateHat";
  };
 
  module GiPirateHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateHook";
  };
 
  module GiPirateSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPirateSkull";
  };
 
  module GiPisaTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPisaTower";
  };
 
  module GiPisces = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPisces";
  };
 
  module GiPistolGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPistolGun";
  };
 
  module GiPitchfork = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPitchfork";
  };
 
  module GiPizzaCutter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPizzaCutter";
  };
 
  module GiPizzaSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPizzaSlice";
  };
 
  module GiPlagueDoctorProfile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlagueDoctorProfile";
  };
 
  module GiPlainArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlainArrow";
  };
 
  module GiPlainCircle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlainCircle";
  };
 
  module GiPlainDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlainDagger";
  };
 
  module GiPlainSquare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlainSquare";
  };
 
  module GiPlanePilot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlanePilot";
  };
 
  module GiPlaneWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlaneWing";
  };
 
  module GiPlanetConquest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlanetConquest";
  };
 
  module GiPlanetCore = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlanetCore";
  };
 
  module GiPlanks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlanks";
  };
 
  module GiPlantRoots = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlantRoots";
  };
 
  module GiPlantWatering = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlantWatering";
  };
 
  module GiPlantsAndAnimals = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlantsAndAnimals";
  };
 
  module GiPlasmaBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlasmaBolt";
  };
 
  module GiPlasticDuck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlasticDuck";
  };
 
  module GiPlastron = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlastron";
  };
 
  module GiPlateClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlateClaw";
  };
 
  module GiPlatform = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlatform";
  };
 
  module GiPlayButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlayButton";
  };
 
  module GiPlayerBase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlayerBase";
  };
 
  module GiPlayerNext = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlayerNext";
  };
 
  module GiPlayerPrevious = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlayerPrevious";
  };
 
  module GiPlayerTime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlayerTime";
  };
 
  module GiPlesiosaurus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlesiosaurus";
  };
 
  module GiPlow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlow";
  };
 
  module GiPlug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlug";
  };
 
  module GiPlunger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPlunger";
  };
 
  module GiPocketBow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPocketBow";
  };
 
  module GiPocketRadio = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPocketRadio";
  };
 
  module GiPocketWatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPocketWatch";
  };
 
  module GiPodiumSecond = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPodiumSecond";
  };
 
  module GiPodiumThird = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPodiumThird";
  };
 
  module GiPodiumWinner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPodiumWinner";
  };
 
  module GiPodium = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPodium";
  };
 
  module GiPointing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPointing";
  };
 
  module GiPointyHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPointyHat";
  };
 
  module GiPointySword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPointySword";
  };
 
  module GiPoisonBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoisonBottle";
  };
 
  module GiPoisonCloud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoisonCloud";
  };
 
  module GiPoisonGas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoisonGas";
  };
 
  module GiPoison = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoison";
  };
 
  module GiPokecog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPokecog";
  };
 
  module GiPokerHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPokerHand";
  };
 
  module GiPolarBear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPolarBear";
  };
 
  module GiPoliceBadge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoliceBadge";
  };
 
  module GiPoliceCar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoliceCar";
  };
 
  module GiPoliceOfficerHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoliceOfficerHead";
  };
 
  module GiPoliceTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoliceTarget";
  };
 
  module GiPollenDust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPollenDust";
  };
 
  module GiPoloShirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoloShirt";
  };
 
  module GiPoncho = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoncho";
  };
 
  module GiPoolDive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoolDive";
  };
 
  module GiPopcorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPopcorn";
  };
 
  module GiPopeCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPopeCrown";
  };
 
  module GiPoppy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPoppy";
  };
 
  module GiPorcelainVase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPorcelainVase";
  };
 
  module GiPortal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPortal";
  };
 
  module GiPortculis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPortculis";
  };
 
  module GiPortrait = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPortrait";
  };
 
  module GiPortugal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPortugal";
  };
 
  module GiPositionMarker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPositionMarker";
  };
 
  module GiPostStamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPostStamp";
  };
 
  module GiPotato = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPotato";
  };
 
  module GiPotionBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPotionBall";
  };
 
  module GiPotionOfMadness = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPotionOfMadness";
  };
 
  module GiPounce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPounce";
  };
 
  module GiPouringChalice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPouringChalice";
  };
 
  module GiPouringPot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPouringPot";
  };
 
  module GiPowderBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowderBag";
  };
 
  module GiPowder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowder";
  };
 
  module GiPowerButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowerButton";
  };
 
  module GiPowerGenerator = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowerGenerator";
  };
 
  module GiPowerLightning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowerLightning";
  };
 
  module GiPowerRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPowerRing";
  };
 
  module GiPrayerBeads = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrayerBeads";
  };
 
  module GiPrayer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrayer";
  };
 
  module GiPrayingMantis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrayingMantis";
  };
 
  module GiPresent = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPresent";
  };
 
  module GiPrettyFangs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrettyFangs";
  };
 
  module GiPretzel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPretzel";
  };
 
  module GiPreviousButton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPreviousButton";
  };
 
  module GiPriceTag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPriceTag";
  };
 
  module GiPrimitiveNecklace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrimitiveNecklace";
  };
 
  module GiPrimitiveTorch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrimitiveTorch";
  };
 
  module GiPrism = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrism";
  };
 
  module GiPrisoner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrisoner";
  };
 
  module GiPrivateFirstClass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrivateFirstClass";
  };
 
  module GiPrivate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPrivate";
  };
 
  module GiProcessor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiProcessor";
  };
 
  module GiProfit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiProfit";
  };
 
  module GiProgression = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiProgression";
  };
 
  module GiPropellerBeanie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPropellerBeanie";
  };
 
  module GiProtectionGlasses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiProtectionGlasses";
  };
 
  module GiPschentDoubleCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPschentDoubleCrown";
  };
 
  module GiPsychicWaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPsychicWaves";
  };
 
  module GiPterodactylus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPterodactylus";
  };
 
  module GiPteruges = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPteruges";
  };
 
  module GiPublicSpeaker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPublicSpeaker";
  };
 
  module GiPull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPull";
  };
 
  module GiPulleyHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPulleyHook";
  };
 
  module GiPulse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPulse";
  };
 
  module GiPummeled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPummeled";
  };
 
  module GiPumpkinLantern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPumpkinLantern";
  };
 
  module GiPumpkinMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPumpkinMask";
  };
 
  module GiPumpkin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPumpkin";
  };
 
  module GiPunchBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPunchBlast";
  };
 
  module GiPunch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPunch";
  };
 
  module GiPunchingBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPunchingBag";
  };
 
  module GiPuppet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPuppet";
  };
 
  module GiPurpleTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPurpleTentacle";
  };
 
  module GiPush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPush";
  };
 
  module GiPuzzle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPuzzle";
  };
 
  module GiPylon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPylon";
  };
 
  module GiPyre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPyre";
  };
 
  module GiPyromaniac = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiPyromaniac";
  };
 
  module GiQuakeStomp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuakeStomp";
  };
 
  module GiQueenCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQueenCrown";
  };
 
  module GiQuickSlash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuickSlash";
  };
 
  module GiQuicksand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuicksand";
  };
 
  module GiQuillInk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuillInk";
  };
 
  module GiQuill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuill";
  };
 
  module GiQuiver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiQuiver";
  };
 
  module GiRabbitHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRabbitHead";
  };
 
  module GiRabbit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRabbit";
  };
 
  module GiRaccoonHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaccoonHead";
  };
 
  module GiRaceCar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaceCar";
  };
 
  module GiRadarCrossSection = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadarCrossSection";
  };
 
  module GiRadarDish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadarDish";
  };
 
  module GiRadarSweep = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadarSweep";
  };
 
  module GiRadialBalance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadialBalance";
  };
 
  module GiRadiations = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadiations";
  };
 
  module GiRadioTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadioTower";
  };
 
  module GiRadioactive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRadioactive";
  };
 
  module GiRaft = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaft";
  };
 
  module GiRaggedWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaggedWound";
  };
 
  module GiRailway = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRailway";
  };
 
  module GiRainbowStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRainbowStar";
  };
 
  module GiRaining = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaining";
  };
 
  module GiRaiseSkeleton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaiseSkeleton";
  };
 
  module GiRaiseZombie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaiseZombie";
  };
 
  module GiRake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRake";
  };
 
  module GiRallyTheTroops = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRallyTheTroops";
  };
 
  module GiRamProfile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRamProfile";
  };
 
  module GiRam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRam";
  };
 
  module GiRanchGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRanchGate";
  };
 
  module GiRank1 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRank1";
  };
 
  module GiRank2 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRank2";
  };
 
  module GiRank3 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRank3";
  };
 
  module GiRapidshareArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRapidshareArrow";
  };
 
  module GiRaspberry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaspberry";
  };
 
  module GiRat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRat";
  };
 
  module GiRattlesnake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRattlesnake";
  };
 
  module GiRaven = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRaven";
  };
 
  module GiRawEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRawEgg";
  };
 
  module GiRayGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRayGun";
  };
 
  module GiRazorBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRazorBlade";
  };
 
  module GiReactor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReactor";
  };
 
  module GiRead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRead";
  };
 
  module GiReaperScythe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReaperScythe";
  };
 
  module GiRearAura = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRearAura";
  };
 
  module GiReceiveMoney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReceiveMoney";
  };
 
  module GiRecycle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRecycle";
  };
 
  module GiReed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReed";
  };
 
  module GiRefinery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRefinery";
  };
 
  module GiRegeneration = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRegeneration";
  };
 
  module GiRelationshipBounds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRelationshipBounds";
  };
 
  module GiRelicBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRelicBlade";
  };
 
  module GiReloadGunBarrel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReloadGunBarrel";
  };
 
  module GiRemedy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRemedy";
  };
 
  module GiRempart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRempart";
  };
 
  module GiReptileTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReptileTail";
  };
 
  module GiResize = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiResize";
  };
 
  module GiResonance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiResonance";
  };
 
  module GiRestingVampire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRestingVampire";
  };
 
  module GiReticule = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReticule";
  };
 
  module GiRetroController = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRetroController";
  };
 
  module GiReturnArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiReturnArrow";
  };
 
  module GiRevolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRevolt";
  };
 
  module GiRevolver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRevolver";
  };
 
  module GiRhinocerosHorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRhinocerosHorn";
  };
 
  module GiRialtoBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRialtoBridge";
  };
 
  module GiRibbonMedal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRibbonMedal";
  };
 
  module GiRibbonShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRibbonShield";
  };
 
  module GiRibbon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRibbon";
  };
 
  module GiRibcage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRibcage";
  };
 
  module GiRifle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRifle";
  };
 
  module GiRingBox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingBox";
  };
 
  module GiRingMould = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingMould";
  };
 
  module GiRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRing";
  };
 
  module GiRingedBeam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingedBeam";
  };
 
  module GiRingedPlanet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingedPlanet";
  };
 
  module GiRingingAlarm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingingAlarm";
  };
 
  module GiRingingBell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingingBell";
  };
 
  module GiRingmaster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRingmaster";
  };
 
  module GiRiotShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRiotShield";
  };
 
  module GiRiver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRiver";
  };
 
  module GiRoad = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoad";
  };
 
  module GiRoastChicken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoastChicken";
  };
 
  module GiRobberHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobberHand";
  };
 
  module GiRobberMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobberMask";
  };
 
  module GiRobber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobber";
  };
 
  module GiRobe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobe";
  };
 
  module GiRobinHoodHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobinHoodHat";
  };
 
  module GiRobotAntennas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobotAntennas";
  };
 
  module GiRobotGolem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobotGolem";
  };
 
  module GiRobotGrab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobotGrab";
  };
 
  module GiRobotHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobotHelmet";
  };
 
  module GiRobotLeg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRobotLeg";
  };
 
  module GiRockGolem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRockGolem";
  };
 
  module GiRock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRock";
  };
 
  module GiRocketFlight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRocketFlight";
  };
 
  module GiRocketThruster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRocketThruster";
  };
 
  module GiRocket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRocket";
  };
 
  module GiRockingChair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRockingChair";
  };
 
  module GiRodOfAsclepius = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRodOfAsclepius";
  };
 
  module GiRogue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRogue";
  };
 
  module GiRolledCloth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRolledCloth";
  };
 
  module GiRollerSkate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRollerSkate";
  };
 
  module GiRollingBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRollingBomb";
  };
 
  module GiRollingDiceCup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRollingDiceCup";
  };
 
  module GiRollingDices = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRollingDices";
  };
 
  module GiRollingEnergy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRollingEnergy";
  };
 
  module GiRomanShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRomanShield";
  };
 
  module GiRomanToga = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRomanToga";
  };
 
  module GiRooster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRooster";
  };
 
  module GiRootTip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRootTip";
  };
 
  module GiRopeBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRopeBridge";
  };
 
  module GiRopeCoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRopeCoil";
  };
 
  module GiRopeDart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRopeDart";
  };
 
  module GiRopeway = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRopeway";
  };
 
  module GiRosaShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRosaShield";
  };
 
  module GiRose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRose";
  };
 
  module GiRoughWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoughWound";
  };
 
  module GiRoundBottomFlask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundBottomFlask";
  };
 
  module GiRoundKnob = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundKnob";
  };
 
  module GiRoundShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundShield";
  };
 
  module GiRoundSilo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundSilo";
  };
 
  module GiRoundStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundStar";
  };
 
  module GiRoundStrawBale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundStrawBale";
  };
 
  module GiRoundStruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundStruck";
  };
 
  module GiRoundTable = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoundTable";
  };
 
  module GiRoyalLove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRoyalLove";
  };
 
  module GiRss = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRss";
  };
 
  module GiRubberBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRubberBoot";
  };
 
  module GiRugbyConversion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRugbyConversion";
  };
 
  module GiRuleBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRuleBook";
  };
 
  module GiRun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRun";
  };
 
  module GiRuneStone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRuneStone";
  };
 
  module GiRuneSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRuneSword";
  };
 
  module GiRunningNinja = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRunningNinja";
  };
 
  module GiRunningShoe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRunningShoe";
  };
 
  module GiRupee = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRupee";
  };
 
  module GiRustySword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiRustySword";
  };
 
  module GiSBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSBrick";
  };
 
  module GiSaberAndPistol = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaberAndPistol";
  };
 
  module GiSaberSlash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaberSlash";
  };
 
  module GiSaberTooth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaberTooth";
  };
 
  module GiSaberToothedCatHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaberToothedCatHead";
  };
 
  module GiSabersChoc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSabersChoc";
  };
 
  module GiSacrificialDagger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSacrificialDagger";
  };
 
  module GiSadCrab = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSadCrab";
  };
 
  module GiSaddle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaddle";
  };
 
  module GiSafetyPin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSafetyPin";
  };
 
  module GiSagittarius = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSagittarius";
  };
 
  module GiSai = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSai";
  };
 
  module GiSail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSail";
  };
 
  module GiSailboat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSailboat";
  };
 
  module GiSaintBasilCathedral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaintBasilCathedral";
  };
 
  module GiSaiyanSuit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaiyanSuit";
  };
 
  module GiSalamander = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSalamander";
  };
 
  module GiSalmon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSalmon";
  };
 
  module GiSaloonDoors = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaloonDoors";
  };
 
  module GiSaloon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaloon";
  };
 
  module GiSaltShaker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaltShaker";
  };
 
  module GiSamaraMosque = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSamaraMosque";
  };
 
  module GiSamuraiHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSamuraiHelmet";
  };
 
  module GiSamusHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSamusHelmet";
  };
 
  module GiSandCastle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSandCastle";
  };
 
  module GiSandSnake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSandSnake";
  };
 
  module GiSandsOfTime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSandsOfTime";
  };
 
  module GiSandstorm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSandstorm";
  };
 
  module GiSandwich = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSandwich";
  };
 
  module GiSaphir = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaphir";
  };
 
  module GiSarcophagus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSarcophagus";
  };
 
  module GiSasquatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSasquatch";
  };
 
  module GiSatelliteCommunication = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSatelliteCommunication";
  };
 
  module GiSattelite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSattelite";
  };
 
  module GiSauropodHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSauropodHead";
  };
 
  module GiSausage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSausage";
  };
 
  module GiSausagesRibbon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSausagesRibbon";
  };
 
  module GiSaveArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaveArrow";
  };
 
  module GiSave = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSave";
  };
 
  module GiSawClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSawClaw";
  };
 
  module GiSawedOffShotgun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSawedOffShotgun";
  };
 
  module GiSaxophone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSaxophone";
  };
 
  module GiScabbard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScabbard";
  };
 
  module GiScaleMail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScaleMail";
  };
 
  module GiScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScales";
  };
 
  module GiScallop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScallop";
  };
 
  module GiScalpelStrike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScalpelStrike";
  };
 
  module GiScalpel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScalpel";
  };
 
  module GiScarWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScarWound";
  };
 
  module GiScarabBeetle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScarabBeetle";
  };
 
  module GiScarecrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScarecrow";
  };
 
  module GiSchoolBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSchoolBag";
  };
 
  module GiSchoolOfFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSchoolOfFish";
  };
 
  module GiScissors = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScissors";
  };
 
  module GiScooter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScooter";
  };
 
  module GiScorpio = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScorpio";
  };
 
  module GiScorpionTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScorpionTail";
  };
 
  module GiScorpion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScorpion";
  };
 
  module GiScoutShip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScoutShip";
  };
 
  module GiScreaming = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScreaming";
  };
 
  module GiScreenImpact = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScreenImpact";
  };
 
  module GiScrew = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScrew";
  };
 
  module GiScrewdriver = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScrewdriver";
  };
 
  module GiScrollQuill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScrollQuill";
  };
 
  module GiScrollUnfurled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScrollUnfurled";
  };
 
  module GiScubaMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScubaMask";
  };
 
  module GiScubaTanks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScubaTanks";
  };
 
  module GiScythe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiScythe";
  };
 
  module GiSeaCreature = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeaCreature";
  };
 
  module GiSeaDragon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeaDragon";
  };
 
  module GiSeaSerpent = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeaSerpent";
  };
 
  module GiSeaStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeaStar";
  };
 
  module GiSeagull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeagull";
  };
 
  module GiSeahorse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeahorse";
  };
 
  module GiSeatedMouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeatedMouse";
  };
 
  module GiSecretBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSecretBook";
  };
 
  module GiSecretDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSecretDoor";
  };
 
  module GiSecurityGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSecurityGate";
  };
 
  module GiSeedling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeedling";
  };
 
  module GiSelect = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSelect";
  };
 
  module GiSelfLove = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSelfLove";
  };
 
  module GiSellCard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSellCard";
  };
 
  module GiSemiClosedEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSemiClosedEye";
  };
 
  module GiSensuousness = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSensuousness";
  };
 
  module GiSentryGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSentryGun";
  };
 
  module GiSergeant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSergeant";
  };
 
  module GiSerratedSlash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSerratedSlash";
  };
 
  module GiServerRack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiServerRack";
  };
 
  module GiSesame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSesame";
  };
 
  module GiSettingsKnobs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSettingsKnobs";
  };
 
  module GiSevenPointedStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSevenPointedStar";
  };
 
  module GiSeveredHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSeveredHand";
  };
 
  module GiSewedShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSewedShell";
  };
 
  module GiSewingNeedle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSewingNeedle";
  };
 
  module GiSewingString = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSewingString";
  };
 
  module GiSextant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSextant";
  };
 
  module GiShadowFollower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShadowFollower";
  };
 
  module GiShadowGrasp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShadowGrasp";
  };
 
  module GiShakingHands = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShakingHands";
  };
 
  module GiShamblingMound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShamblingMound";
  };
 
  module GiShamblingZombie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShamblingZombie";
  };
 
  module GiShamrock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShamrock";
  };
 
  module GiShardSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShardSword";
  };
 
  module GiShare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShare";
  };
 
  module GiSharkBite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharkBite";
  };
 
  module GiSharkFin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharkFin";
  };
 
  module GiSharkJaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharkJaws";
  };
 
  module GiSharpAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpAxe";
  };
 
  module GiSharpCrown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpCrown";
  };
 
  module GiSharpHalberd = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpHalberd";
  };
 
  module GiSharpLips = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpLips";
  };
 
  module GiSharpShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpShuriken";
  };
 
  module GiSharpSmile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpSmile";
  };
 
  module GiSharpedTeethSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSharpedTeethSkull";
  };
 
  module GiShatter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShatter";
  };
 
  module GiShatteredGlass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShatteredGlass";
  };
 
  module GiShatteredHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShatteredHeart";
  };
 
  module GiShatteredSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShatteredSword";
  };
 
  module GiSheep = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSheep";
  };
 
  module GiSheikahEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSheikahEye";
  };
 
  module GiShepherdsCrook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShepherdsCrook";
  };
 
  module GiSherlockHolmes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSherlockHolmes";
  };
 
  module GiShieldBash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldBash";
  };
 
  module GiShieldBounces = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldBounces";
  };
 
  module GiShieldDisabled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldDisabled";
  };
 
  module GiShieldEchoes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldEchoes";
  };
 
  module GiShieldImpact = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldImpact";
  };
 
  module GiShieldReflect = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldReflect";
  };
 
  module GiShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShield";
  };
 
  module GiShieldcomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShieldcomb";
  };
 
  module GiShiningClaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShiningClaw";
  };
 
  module GiShiningHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShiningHeart";
  };
 
  module GiShiningSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShiningSword";
  };
 
  module GiShintoShrineMirror = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShintoShrineMirror";
  };
 
  module GiShintoShrine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShintoShrine";
  };
 
  module GiShinyApple = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShinyApple";
  };
 
  module GiShinyEntrance = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShinyEntrance";
  };
 
  module GiShinyIris = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShinyIris";
  };
 
  module GiShinyOmega = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShinyOmega";
  };
 
  module GiShinyPurse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShinyPurse";
  };
 
  module GiShipBow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShipBow";
  };
 
  module GiShipWheel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShipWheel";
  };
 
  module GiShipWreck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShipWreck";
  };
 
  module GiShirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShirt";
  };
 
  module GiShoonerSailboat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoonerSailboat";
  };
 
  module GiShop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShop";
  };
 
  module GiShoppingBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoppingBag";
  };
 
  module GiShoppingCart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoppingCart";
  };
 
  module GiShorts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShorts";
  };
 
  module GiShotgunRounds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShotgunRounds";
  };
 
  module GiShotgun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShotgun";
  };
 
  module GiShoulderArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoulderArmor";
  };
 
  module GiShoulderBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoulderBag";
  };
 
  module GiShoulderScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShoulderScales";
  };
 
  module GiShouting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShouting";
  };
 
  module GiShower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShower";
  };
 
  module GiShrimp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShrimp";
  };
 
  module GiShrug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShrug";
  };
 
  module GiShurikenAperture = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShurikenAperture";
  };
 
  module GiShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShuriken";
  };
 
  module GiShutRose = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShutRose";
  };
 
  module GiShuttlecock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiShuttlecock";
  };
 
  module GiSickle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSickle";
  };
 
  module GiSideswipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSideswipe";
  };
 
  module GiSiegeRam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSiegeRam";
  };
 
  module GiSiegeTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSiegeTower";
  };
 
  module GiSightDisabled = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSightDisabled";
  };
 
  module GiSilence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSilence";
  };
 
  module GiSilenced = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSilenced";
  };
 
  module GiSilex = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSilex";
  };
 
  module GiSilverBullet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSilverBullet";
  };
 
  module GiSinagot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSinagot";
  };
 
  module GiSing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSing";
  };
 
  module GiSinkingShip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSinkingShip";
  };
 
  module GiSinkingTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSinkingTrap";
  };
 
  module GiSinusoidalBeam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSinusoidalBeam";
  };
 
  module GiSiren = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSiren";
  };
 
  module GiSittingDog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSittingDog";
  };
 
  module GiSixEyes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSixEyes";
  };
 
  module GiSkateboard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkateboard";
  };
 
  module GiSkeletalHand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkeletalHand";
  };
 
  module GiSkeletonInside = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkeletonInside";
  };
 
  module GiSkeletonKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkeletonKey";
  };
 
  module GiSkeleton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkeleton";
  };
 
  module GiSkiBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkiBoot";
  };
 
  module GiSkidMark = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkidMark";
  };
 
  module GiSkier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkier";
  };
 
  module GiSkills = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkills";
  };
 
  module GiSkirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkirt";
  };
 
  module GiSkis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkis";
  };
 
  module GiSkullBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullBolt";
  };
 
  module GiSkullCrack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullCrack";
  };
 
  module GiSkullCrossedBones = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullCrossedBones";
  };
 
  module GiSkullInJar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullInJar";
  };
 
  module GiSkullMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullMask";
  };
 
  module GiSkullRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullRing";
  };
 
  module GiSkullSabertooth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullSabertooth";
  };
 
  module GiSkullShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullShield";
  };
 
  module GiSkullSignet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullSignet";
  };
 
  module GiSkullSlices = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullSlices";
  };
 
  module GiSkullStaff = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullStaff";
  };
 
  module GiSkullWithSyringe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSkullWithSyringe";
  };
 
  module GiSlalom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlalom";
  };
 
  module GiSlap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlap";
  };
 
  module GiSlashedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlashedShield";
  };
 
  module GiSlaveryWhip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlaveryWhip";
  };
 
  module GiSleepy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSleepy";
  };
 
  module GiSleevelessJacket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSleevelessJacket";
  };
 
  module GiSleevelessTop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSleevelessTop";
  };
 
  module GiSlicedBread = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlicedBread";
  };
 
  module GiSlicedMushroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlicedMushroom";
  };
 
  module GiSlicedSausage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlicedSausage";
  };
 
  module GiSlicingArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlicingArrow";
  };
 
  module GiSlime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlime";
  };
 
  module GiSling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSling";
  };
 
  module GiSlingshot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlingshot";
  };
 
  module GiSlowBlob = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlowBlob";
  };
 
  module GiSlumberingSanctuary = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSlumberingSanctuary";
  };
 
  module GiSly = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSly";
  };
 
  module GiSmallFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmallFire";
  };
 
  module GiSmallFishingSailboat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmallFishingSailboat";
  };
 
  module GiSmart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmart";
  };
 
  module GiSmartphone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmartphone";
  };
 
  module GiSmashArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmashArrows";
  };
 
  module GiSmitten = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmitten";
  };
 
  module GiSmokeBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmokeBomb";
  };
 
  module GiSmokingFinger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmokingFinger";
  };
 
  module GiSmokingOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmokingOrb";
  };
 
  module GiSmokingPipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmokingPipe";
  };
 
  module GiSmokingVolcano = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSmokingVolcano";
  };
 
  module GiSnailEyes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnailEyes";
  };
 
  module GiSnail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnail";
  };
 
  module GiSnakeBite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeBite";
  };
 
  module GiSnakeEgg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeEgg";
  };
 
  module GiSnakeJar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeJar";
  };
 
  module GiSnakeSpiral = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeSpiral";
  };
 
  module GiSnakeTongue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeTongue";
  };
 
  module GiSnakeTotem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnakeTotem";
  };
 
  module GiSnake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnake";
  };
 
  module GiSnatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnatch";
  };
 
  module GiSniffingDog = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSniffingDog";
  };
 
  module GiSnitchQuidditchBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnitchQuidditchBall";
  };
 
  module GiSnorkel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnorkel";
  };
 
  module GiSnout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnout";
  };
 
  module GiSnowBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnowBottle";
  };
 
  module GiSnowflake1 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnowflake1";
  };
 
  module GiSnowflake2 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnowflake2";
  };
 
  module GiSnowing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnowing";
  };
 
  module GiSnowman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSnowman";
  };
 
  module GiSoapExperiment = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoapExperiment";
  };
 
  module GiSoap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoap";
  };
 
  module GiSoccerBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoccerBall";
  };
 
  module GiSoccerField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoccerField";
  };
 
  module GiSoccerKick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoccerKick";
  };
 
  module GiSocks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSocks";
  };
 
  module GiSodaCan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSodaCan";
  };
 
  module GiSofa = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSofa";
  };
 
  module GiSolarPower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSolarPower";
  };
 
  module GiSolarSystem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSolarSystem";
  };
 
  module GiSolarTime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSolarTime";
  };
 
  module GiSolidLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSolidLeaf";
  };
 
  module GiSombrero = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSombrero";
  };
 
  module GiSonicBoom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSonicBoom";
  };
 
  module GiSonicLightning = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSonicLightning";
  };
 
  module GiSonicScreech = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSonicScreech";
  };
 
  module GiSonicShoes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSonicShoes";
  };
 
  module GiSonicShout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSonicShout";
  };
 
  module GiSoundOff = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoundOff";
  };
 
  module GiSoundOn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoundOn";
  };
 
  module GiSoundWaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSoundWaves";
  };
 
  module GiSouthAfricaFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSouthAfricaFlag";
  };
 
  module GiSouthAfrica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSouthAfrica";
  };
 
  module GiSouthAmerica = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSouthAmerica";
  };
 
  module GiSouthKorea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSouthKorea";
  };
 
  module GiSpaceShuttle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpaceShuttle";
  };
 
  module GiSpaceSuit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpaceSuit";
  };
 
  module GiSpaceship = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpaceship";
  };
 
  module GiSpadeSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpadeSkull";
  };
 
  module GiSpade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpade";
  };
 
  module GiSpades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpades";
  };
 
  module GiSpain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpain";
  };
 
  module GiSpanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpanner";
  };
 
  module GiSparkSpirit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSparkSpirit";
  };
 
  module GiSparkles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSparkles";
  };
 
  module GiSparklingSabre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSparklingSabre";
  };
 
  module GiSparkyBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSparkyBomb";
  };
 
  module GiSparrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSparrow";
  };
 
  module GiSpartanHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpartanHelmet";
  };
 
  module GiSpartan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpartan";
  };
 
  module GiSpatter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpatter";
  };
 
  module GiSpawnNode = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpawnNode";
  };
 
  module GiSpeakerOff = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpeakerOff";
  };
 
  module GiSpeaker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpeaker";
  };
 
  module GiSpearFeather = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpearFeather";
  };
 
  module GiSpearHook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpearHook";
  };
 
  module GiSpearfishing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpearfishing";
  };
 
  module GiSpears = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpears";
  };
 
  module GiSpectacleLenses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpectacleLenses";
  };
 
  module GiSpectacles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpectacles";
  };
 
  module GiSpectreM4 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpectreM4";
  };
 
  module GiSpectre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpectre";
  };
 
  module GiSpeedBoat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpeedBoat";
  };
 
  module GiSpeedometer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpeedometer";
  };
 
  module GiSpellBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpellBook";
  };
 
  module GiSpermWhale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpermWhale";
  };
 
  module GiSpiderAlt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderAlt";
  };
 
  module GiSpiderBot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderBot";
  };
 
  module GiSpiderEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderEye";
  };
 
  module GiSpiderFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderFace";
  };
 
  module GiSpiderMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderMask";
  };
 
  module GiSpiderWeb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiderWeb";
  };
 
  module GiSpikeball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikeball";
  };
 
  module GiSpikedArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedArmor";
  };
 
  module GiSpikedBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedBall";
  };
 
  module GiSpikedBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedBat";
  };
 
  module GiSpikedCollar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedCollar";
  };
 
  module GiSpikedDragonHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedDragonHead";
  };
 
  module GiSpikedFence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedFence";
  };
 
  module GiSpikedHalo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedHalo";
  };
 
  module GiSpikedMace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedMace";
  };
 
  module GiSpikedShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedShell";
  };
 
  module GiSpikedShoulderArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedShoulderArmor";
  };
 
  module GiSpikedSnail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedSnail";
  };
 
  module GiSpikedTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedTail";
  };
 
  module GiSpikedTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedTentacle";
  };
 
  module GiSpikedTrunk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikedTrunk";
  };
 
  module GiSpikesFull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikesFull";
  };
 
  module GiSpikesHalf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikesHalf";
  };
 
  module GiSpikesInit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikesInit";
  };
 
  module GiSpikes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikes";
  };
 
  module GiSpikyEclipse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikyEclipse";
  };
 
  module GiSpikyExplosion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikyExplosion";
  };
 
  module GiSpikyField = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikyField";
  };
 
  module GiSpikyPit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikyPit";
  };
 
  module GiSpikyWing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpikyWing";
  };
 
  module GiSpill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpill";
  };
 
  module GiSpinalCoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpinalCoil";
  };
 
  module GiSpineArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpineArrow";
  };
 
  module GiSpinningBlades = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpinningBlades";
  };
 
  module GiSpinningRibbons = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpinningRibbons";
  };
 
  module GiSpinningSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpinningSword";
  };
 
  module GiSpinningTop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpinningTop";
  };
 
  module GiSpiralArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralArrow";
  };
 
  module GiSpiralBloom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralBloom";
  };
 
  module GiSpiralBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralBottle";
  };
 
  module GiSpiralHilt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralHilt";
  };
 
  module GiSpiralLollipop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralLollipop";
  };
 
  module GiSpiralShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralShell";
  };
 
  module GiSpiralTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralTentacle";
  };
 
  module GiSpiralThrust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpiralThrust";
  };
 
  module GiSplash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplash";
  };
 
  module GiSplashyStream = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplashyStream";
  };
 
  module GiSplitArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplitArrows";
  };
 
  module GiSplitBody = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplitBody";
  };
 
  module GiSplitCross = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplitCross";
  };
 
  module GiSplurt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSplurt";
  };
 
  module GiSpookyHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpookyHouse";
  };
 
  module GiSpoon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpoon";
  };
 
  module GiSportMedal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSportMedal";
  };
 
  module GiSpotedFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpotedFlower";
  };
 
  module GiSpottedArrowhead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpottedArrowhead";
  };
 
  module GiSpottedBug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpottedBug";
  };
 
  module GiSpottedMushroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpottedMushroom";
  };
 
  module GiSpottedWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpottedWound";
  };
 
  module GiSpoutnik = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpoutnik";
  };
 
  module GiSpray = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpray";
  };
 
  module GiSpring = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpring";
  };
 
  module GiSprint = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSprint";
  };
 
  module GiSproutDisc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSproutDisc";
  };
 
  module GiSprout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSprout";
  };
 
  module GiSpy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpy";
  };
 
  module GiSpyglass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSpyglass";
  };
 
  module GiSquareBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquareBottle";
  };
 
  module GiSquare = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquare";
  };
 
  module GiSquib = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquib";
  };
 
  module GiSquidHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquidHead";
  };
 
  module GiSquid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquid";
  };
 
  module GiSquirrel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSquirrel";
  };
 
  module GiSriLanka = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSriLanka";
  };
 
  module GiStabbedNote = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStabbedNote";
  };
 
  module GiStack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStack";
  };
 
  module GiStagHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStagHead";
  };
 
  module GiStahlhelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStahlhelm";
  };
 
  module GiStairsCake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStairsCake";
  };
 
  module GiStairsGoal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStairsGoal";
  };
 
  module GiStairs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStairs";
  };
 
  module GiStakeHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStakeHammer";
  };
 
  module GiStakesFence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStakesFence";
  };
 
  module GiStalactites = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStalactites";
  };
 
  module GiStalagtite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStalagtite";
  };
 
  module GiStamper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStamper";
  };
 
  module GiStandingPotion = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStandingPotion";
  };
 
  module GiStarAltar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarAltar";
  };
 
  module GiStarCycle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarCycle";
  };
 
  module GiStarFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarFlag";
  };
 
  module GiStarFormation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarFormation";
  };
 
  module GiStarGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarGate";
  };
 
  module GiStarProminences = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarProminences";
  };
 
  module GiStarPupil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarPupil";
  };
 
  module GiStarSattelites = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarSattelites";
  };
 
  module GiStarShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarShuriken";
  };
 
  module GiStarSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarSkull";
  };
 
  module GiStarStruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarStruck";
  };
 
  module GiStarSwirl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarSwirl";
  };
 
  module GiStarfighter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarfighter";
  };
 
  module GiStarsStack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStarsStack";
  };
 
  module GiStaryu = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStaryu";
  };
 
  module GiStaticGuard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStaticGuard";
  };
 
  module GiStaticWaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStaticWaves";
  };
 
  module GiStatic = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStatic";
  };
 
  module GiSteak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteak";
  };
 
  module GiStealthBomber = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStealthBomber";
  };
 
  module GiSteamBlast = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteamBlast";
  };
 
  module GiSteamLocomotive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteamLocomotive";
  };
 
  module GiSteam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteam";
  };
 
  module GiSteampunkGoggles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteampunkGoggles";
  };
 
  module GiSteelClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteelClaws";
  };
 
  module GiSteelDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteelDoor";
  };
 
  module GiSteeltoeBoots = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteeltoeBoots";
  };
 
  module GiSteelwingEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteelwingEmblem";
  };
 
  module GiSteeringWheel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteeringWheel";
  };
 
  module GiStegosaurusScales = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStegosaurusScales";
  };
 
  module GiStethoscope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStethoscope";
  };
 
  module GiSteyrAug = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSteyrAug";
  };
 
  module GiStickFrame = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStickFrame";
  };
 
  module GiStickGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStickGrenade";
  };
 
  module GiStickSplitting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStickSplitting";
  };
 
  module GiStickingPlaster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStickingPlaster";
  };
 
  module GiStickyBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStickyBoot";
  };
 
  module GiStigmata = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStigmata";
  };
 
  module GiStiletto = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStiletto";
  };
 
  module GiStitchedWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStitchedWound";
  };
 
  module GiStockpiles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStockpiles";
  };
 
  module GiStomach = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStomach";
  };
 
  module GiStompTornado = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStompTornado";
  };
 
  module GiStomp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStomp";
  };
 
  module GiStoneAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneAxe";
  };
 
  module GiStoneBlock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneBlock";
  };
 
  module GiStoneBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneBridge";
  };
 
  module GiStoneBust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneBust";
  };
 
  module GiStoneCrafting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneCrafting";
  };
 
  module GiStonePath = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStonePath";
  };
 
  module GiStonePile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStonePile";
  };
 
  module GiStoneSpear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneSpear";
  };
 
  module GiStoneSphere = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneSphere";
  };
 
  module GiStoneStack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneStack";
  };
 
  module GiStoneTablet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneTablet";
  };
 
  module GiStoneThrone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneThrone";
  };
 
  module GiStoneTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneTower";
  };
 
  module GiStoneWall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneWall";
  };
 
  module GiStoneWheel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStoneWheel";
  };
 
  module GiStonedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStonedSkull";
  };
 
  module GiStopSign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStopSign";
  };
 
  module GiStopwatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStopwatch";
  };
 
  module GiStorkDelivery = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStorkDelivery";
  };
 
  module GiStrafe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrafe";
  };
 
  module GiStraightPipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStraightPipe";
  };
 
  module GiStrawberry = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrawberry";
  };
 
  module GiStreetLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStreetLight";
  };
 
  module GiStrikingArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrikingArrows";
  };
 
  module GiStrikingBalls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrikingBalls";
  };
 
  module GiStrikingClamps = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrikingClamps";
  };
 
  module GiStrikingDiamonds = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrikingDiamonds";
  };
 
  module GiStrikingSplinter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrikingSplinter";
  };
 
  module GiStripedSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStripedSun";
  };
 
  module GiStripedSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStripedSword";
  };
 
  module GiStrongMan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrongMan";
  };
 
  module GiStrong = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrong";
  };
 
  module GiStrongbox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStrongbox";
  };
 
  module GiStumpRegrowth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStumpRegrowth";
  };
 
  module GiStunGrenade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiStunGrenade";
  };
 
  module GiSubmarineMissile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSubmarineMissile";
  };
 
  module GiSubmarine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSubmarine";
  };
 
  module GiSubway = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSubway";
  };
 
  module GiSuckeredTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuckeredTentacle";
  };
 
  module GiSugarCane = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSugarCane";
  };
 
  module GiSuicide = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuicide";
  };
 
  module GiSuitcase = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuitcase";
  };
 
  module GiSuits = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuits";
  };
 
  module GiSummits = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSummits";
  };
 
  module GiSunCloud = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunCloud";
  };
 
  module GiSunRadiations = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunRadiations";
  };
 
  module GiSunSpear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunSpear";
  };
 
  module GiSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSun";
  };
 
  module GiSunbeams = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunbeams";
  };
 
  module GiSundial = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSundial";
  };
 
  module GiSunflower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunflower";
  };
 
  module GiSunglasses = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunglasses";
  };
 
  module GiSunkenEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunkenEye";
  };
 
  module GiSunrise = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunrise";
  };
 
  module GiSunset = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSunset";
  };
 
  module GiSuperMushroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuperMushroom";
  };
 
  module GiSupersonicArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSupersonicArrow";
  };
 
  module GiSupersonicBullet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSupersonicBullet";
  };
 
  module GiSurfBoard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSurfBoard";
  };
 
  module GiSurprisedSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSurprisedSkull";
  };
 
  module GiSurprised = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSurprised";
  };
 
  module GiSurroundedEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSurroundedEye";
  };
 
  module GiSurroundedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSurroundedShield";
  };
 
  module GiSushis = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSushis";
  };
 
  module GiSuspensionBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuspensionBridge";
  };
 
  module GiSuspicious = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSuspicious";
  };
 
  module GiSverdIFjell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSverdIFjell";
  };
 
  module GiSwallow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwallow";
  };
 
  module GiSwallower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwallower";
  };
 
  module GiSwampBat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwampBat";
  };
 
  module GiSwamp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwamp";
  };
 
  module GiSwanBreeze = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwanBreeze";
  };
 
  module GiSwan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwan";
  };
 
  module GiSwapBag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwapBag";
  };
 
  module GiSwimfins = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwimfins";
  };
 
  module GiSwirlRing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwirlRing";
  };
 
  module GiSwirlString = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwirlString";
  };
 
  module GiSwirledShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwirledShell";
  };
 
  module GiSwissArmyKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwissArmyKnife";
  };
 
  module GiSwitchWeapon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwitchWeapon";
  };
 
  module GiSwitchblade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwitchblade";
  };
 
  module GiSwitzerland = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwitzerland";
  };
 
  module GiSwordAltar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordAltar";
  };
 
  module GiSwordArray = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordArray";
  };
 
  module GiSwordBrandish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordBrandish";
  };
 
  module GiSwordBreak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordBreak";
  };
 
  module GiSwordClash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordClash";
  };
 
  module GiSwordHilt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordHilt";
  };
 
  module GiSwordInStone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordInStone";
  };
 
  module GiSwordMold = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordMold";
  };
 
  module GiSwordSlice = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordSlice";
  };
 
  module GiSwordSmithing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordSmithing";
  };
 
  module GiSwordSpade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordSpade";
  };
 
  module GiSwordSpin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordSpin";
  };
 
  module GiSwordTie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordTie";
  };
 
  module GiSwordWound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordWound";
  };
 
  module GiSwordman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordman";
  };
 
  module GiSwordsEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordsEmblem";
  };
 
  module GiSwordsPower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordsPower";
  };
 
  module GiSwordwoman = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSwordwoman";
  };
 
  module GiSydneyOperaHouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSydneyOperaHouse";
  };
 
  module GiSyringe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiSyringe";
  };
 
  module GiTBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTBrick";
  };
 
  module GiTShirt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTShirt";
  };
 
  module GiTabiBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTabiBoot";
  };
 
  module GiTable = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTable";
  };
 
  module GiTablet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTablet";
  };
 
  module GiTabletopPlayers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTabletopPlayers";
  };
 
  module GiTacos = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTacos";
  };
 
  module GiTadpole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTadpole";
  };
 
  module GiTakeMyMoney = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTakeMyMoney";
  };
 
  module GiTalk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTalk";
  };
 
  module GiTallBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTallBridge";
  };
 
  module GiTambourine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTambourine";
  };
 
  module GiTangerine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTangerine";
  };
 
  module GiTankTread = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTankTread";
  };
 
  module GiTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTank";
  };
 
  module GiTanzania = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTanzania";
  };
 
  module GiTap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTap";
  };
 
  module GiTargetArrows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargetArrows";
  };
 
  module GiTargetDummy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargetDummy";
  };
 
  module GiTargetLaser = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargetLaser";
  };
 
  module GiTargetPrize = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargetPrize";
  };
 
  module GiTargetShot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargetShot";
  };
 
  module GiTargeted = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargeted";
  };
 
  module GiTargeting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTargeting";
  };
 
  module GiTatteredBanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTatteredBanner";
  };
 
  module GiTaurus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTaurus";
  };
 
  module GiTeacher = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeacher";
  };
 
  module GiTeamDowngrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeamDowngrade";
  };
 
  module GiTeamIdea = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeamIdea";
  };
 
  module GiTeamUpgrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeamUpgrade";
  };
 
  module GiTeapotLeaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeapotLeaves";
  };
 
  module GiTeapot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeapot";
  };
 
  module GiTearTracks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTearTracks";
  };
 
  module GiTearing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTearing";
  };
 
  module GiTec9 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTec9";
  };
 
  module GiTechnoHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTechnoHeart";
  };
 
  module GiTeePipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeePipe";
  };
 
  module GiTelefrag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTelefrag";
  };
 
  module GiTelepathy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTelepathy";
  };
 
  module GiTeleport = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeleport";
  };
 
  module GiTelescopicBaton = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTelescopicBaton";
  };
 
  module GiTellerMine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTellerMine";
  };
 
  module GiTemplarEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTemplarEye";
  };
 
  module GiTemplarHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTemplarHeart";
  };
 
  module GiTemplarShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTemplarShield";
  };
 
  module GiTempleDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTempleDoor";
  };
 
  module GiTemporaryShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTemporaryShield";
  };
 
  module GiTemptation = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTemptation";
  };
 
  module GiTennisBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTennisBall";
  };
 
  module GiTennisCourt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTennisCourt";
  };
 
  module GiTennisRacket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTennisRacket";
  };
 
  module GiTensionSnowflake = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTensionSnowflake";
  };
 
  module GiTentacleHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTentacleHeart";
  };
 
  module GiTentacleStrike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTentacleStrike";
  };
 
  module GiTentaclesBarrier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTentaclesBarrier";
  };
 
  module GiTentaclesSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTentaclesSkull";
  };
 
  module GiTentacurl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTentacurl";
  };
 
  module GiTerror = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTerror";
  };
 
  module GiTeslaCoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeslaCoil";
  };
 
  module GiTeslaTurret = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTeslaTurret";
  };
 
  module GiTesla = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTesla";
  };
 
  module GiTestTubes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTestTubes";
  };
 
  module GiTexas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTexas";
  };
 
  module GiTheaterCurtains = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTheaterCurtains";
  };
 
  module GiThermometerCold = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThermometerCold";
  };
 
  module GiThermometerHot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThermometerHot";
  };
 
  module GiThermometerScale = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThermometerScale";
  };
 
  module GiThink = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThink";
  };
 
  module GiThirdEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThirdEye";
  };
 
  module GiThompsonM1 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThompsonM1";
  };
 
  module GiThompsonM1928 = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThompsonM1928";
  };
 
  module GiThorFist = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThorFist";
  };
 
  module GiThorHammer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThorHammer";
  };
 
  module GiThornHelix = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThornHelix";
  };
 
  module GiThornedArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThornedArrow";
  };
 
  module GiThornyTentacle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThornyTentacle";
  };
 
  module GiThornyVine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThornyVine";
  };
 
  module GiThreeBurningBalls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThreeBurningBalls";
  };
 
  module GiThreeFriends = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThreeFriends";
  };
 
  module GiThreeKeys = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThreeKeys";
  };
 
  module GiThreeLeaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThreeLeaves";
  };
 
  module GiThreePointedShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThreePointedShuriken";
  };
 
  module GiThroneKing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThroneKing";
  };
 
  module GiThrowingBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrowingBall";
  };
 
  module GiThrownCharcoal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrownCharcoal";
  };
 
  module GiThrownDaggers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrownDaggers";
  };
 
  module GiThrownKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrownKnife";
  };
 
  module GiThrownSpear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrownSpear";
  };
 
  module GiThrustBend = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrustBend";
  };
 
  module GiThrust = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThrust";
  };
 
  module GiThumbDown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThumbDown";
  };
 
  module GiThumbUp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThumbUp";
  };
 
  module GiThunderBlade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThunderBlade";
  };
 
  module GiThunderSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThunderSkull";
  };
 
  module GiThunderStruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThunderStruck";
  };
 
  module GiThunderball = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThunderball";
  };
 
  module GiThwomp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiThwomp";
  };
 
  module GiTiara = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTiara";
  };
 
  module GiTicTacToe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTicTacToe";
  };
 
  module GiTick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTick";
  };
 
  module GiTicket = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTicket";
  };
 
  module GiTie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTie";
  };
 
  module GiTiedScroll = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTiedScroll";
  };
 
  module GiTigerHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTigerHead";
  };
 
  module GiTiger = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTiger";
  };
 
  module GiTightrope = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTightrope";
  };
 
  module GiTimeBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTimeBomb";
  };
 
  module GiTimeSynchronization = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTimeSynchronization";
  };
 
  module GiTimeTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTimeTrap";
  };
 
  module GiTinker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTinker";
  };
 
  module GiTipi = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTipi";
  };
 
  module GiTireTracks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTireTracks";
  };
 
  module GiToadTeeth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToadTeeth";
  };
 
  module GiToaster = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToaster";
  };
 
  module GiToggles = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToggles";
  };
 
  module GiToken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToken";
  };
 
  module GiTomahawk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTomahawk";
  };
 
  module GiTomato = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTomato";
  };
 
  module GiTombstone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTombstone";
  };
 
  module GiTongue = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTongue";
  };
 
  module GiToolbox = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToolbox";
  };
 
  module GiTooth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTooth";
  };
 
  module GiTopHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTopHat";
  };
 
  module GiTopPaw = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTopPaw";
  };
 
  module GiTopaz = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTopaz";
  };
 
  module GiTorc = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTorc";
  };
 
  module GiTorch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTorch";
  };
 
  module GiTornadoDiscs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTornadoDiscs";
  };
 
  module GiTornado = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTornado";
  };
 
  module GiTorpedo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTorpedo";
  };
 
  module GiTortoise = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTortoise";
  };
 
  module GiTotemHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTotemHead";
  };
 
  module GiTotemMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTotemMask";
  };
 
  module GiTotem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTotem";
  };
 
  module GiToucan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToucan";
  };
 
  module GiTowerBridge = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTowerBridge";
  };
 
  module GiTowerFall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTowerFall";
  };
 
  module GiTowerFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTowerFlag";
  };
 
  module GiToyMallet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiToyMallet";
  };
 
  module GiTrackedRobot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrackedRobot";
  };
 
  module GiTrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrade";
  };
 
  module GiTrafficCone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrafficCone";
  };
 
  module GiTrafficLightsGreen = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrafficLightsGreen";
  };
 
  module GiTrafficLightsOrange = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrafficLightsOrange";
  };
 
  module GiTrafficLightsReadyToGo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrafficLightsReadyToGo";
  };
 
  module GiTrafficLightsRed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrafficLightsRed";
  };
 
  module GiTrail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrail";
  };
 
  module GiTrample = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrample";
  };
 
  module GiTransform = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTransform";
  };
 
  module GiTransfuse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTransfuse";
  };
 
  module GiTransparentSlime = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTransparentSlime";
  };
 
  module GiTransparentTubes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTransparentTubes";
  };
 
  module GiTransportationRings = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTransportationRings";
  };
 
  module GiTrapMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrapMask";
  };
 
  module GiTrashCan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrashCan";
  };
 
  module GiTravelDress = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTravelDress";
  };
 
  module GiTread = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTread";
  };
 
  module GiTreasureMap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreasureMap";
  };
 
  module GiTrebuchet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrebuchet";
  };
 
  module GiTreeBeehive = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeBeehive";
  };
 
  module GiTreeBranch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeBranch";
  };
 
  module GiTreeDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeDoor";
  };
 
  module GiTreeFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeFace";
  };
 
  module GiTreeGrowth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeGrowth";
  };
 
  module GiTreeRoots = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeRoots";
  };
 
  module GiTreeSwing = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTreeSwing";
  };
 
  module GiTrefoilLily = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrefoilLily";
  };
 
  module GiTrefoilShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrefoilShuriken";
  };
 
  module GiTrenchAssault = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrenchAssault";
  };
 
  module GiTrenchBodyArmor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrenchBodyArmor";
  };
 
  module GiTrenchKnife = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrenchKnife";
  };
 
  module GiTrenchSpade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrenchSpade";
  };
 
  module GiTriangleTarget = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriangleTarget";
  };
 
  module GiTribalMask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTribalMask";
  };
 
  module GiTribalPendant = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTribalPendant";
  };
 
  module GiTriceratopsHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriceratopsHead";
  };
 
  module GiTridentShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTridentShield";
  };
 
  module GiTrident = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrident";
  };
 
  module GiTriforce = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriforce";
  };
 
  module GiTriggerHurt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriggerHurt";
  };
 
  module GiTrilobite = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrilobite";
  };
 
  module GiTrinacria = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrinacria";
  };
 
  module GiTriorb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriorb";
  };
 
  module GiTripleBeak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleBeak";
  };
 
  module GiTripleClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleClaws";
  };
 
  module GiTripleCorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleCorn";
  };
 
  module GiTripleGate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleGate";
  };
 
  module GiTripleLock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleLock";
  };
 
  module GiTripleNeedle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleNeedle";
  };
 
  module GiTriplePlier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriplePlier";
  };
 
  module GiTripleScratches = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleScratches";
  };
 
  module GiTripleShells = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleShells";
  };
 
  module GiTripleSkulls = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleSkulls";
  };
 
  module GiTripleYin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripleYin";
  };
 
  module GiTripwire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTripwire";
  };
 
  module GiTriquetra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTriquetra";
  };
 
  module GiTrireme = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrireme";
  };
 
  module GiTritonHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTritonHead";
  };
 
  module GiTroglodyte = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTroglodyte";
  };
 
  module GiTrojanHorse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrojanHorse";
  };
 
  module GiTroll = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTroll";
  };
 
  module GiTronArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTronArrow";
  };
 
  module GiTrophiesShelf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrophiesShelf";
  };
 
  module GiTrophyCup = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrophyCup";
  };
 
  module GiTrophy = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrophy";
  };
 
  module GiTropicalFish = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTropicalFish";
  };
 
  module GiTrousers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrousers";
  };
 
  module GiTrowel = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrowel";
  };
 
  module GiTruck = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTruck";
  };
 
  module GiTrumpetFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrumpetFlag";
  };
 
  module GiTrumpet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrumpet";
  };
 
  module GiTrunkMushroom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTrunkMushroom";
  };
 
  module GiTumbleweed = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTumbleweed";
  };
 
  module GiTumor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTumor";
  };
 
  module GiTumulus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTumulus";
  };
 
  module GiTunePitch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTunePitch";
  };
 
  module GiTurban = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurban";
  };
 
  module GiTurbine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurbine";
  };
 
  module GiTurd = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurd";
  };
 
  module GiTurnstile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurnstile";
  };
 
  module GiTurret = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurret";
  };
 
  module GiTurtleShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurtleShell";
  };
 
  module GiTurtle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTurtle";
  };
 
  module GiTusksFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTusksFlag";
  };
 
  module GiTvRemote = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTvRemote";
  };
 
  module GiTvTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTvTower";
  };
 
  module GiTv = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTv";
  };
 
  module GiTwinShell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwinShell";
  };
 
  module GiTwirlCenter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwirlCenter";
  };
 
  module GiTwirlyFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwirlyFlower";
  };
 
  module GiTwister = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwister";
  };
 
  module GiTwoCoins = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwoCoins";
  };
 
  module GiTwoFeathers = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwoFeathers";
  };
 
  module GiTwoHandedSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwoHandedSword";
  };
 
  module GiTwoShadows = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTwoShadows";
  };
 
  module GiTyre = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiTyre";
  };
 
  module GiUbisoftSun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUbisoftSun";
  };
 
  module GiUdder = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUdder";
  };
 
  module GiUfo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUfo";
  };
 
  module GiUltrasound = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUltrasound";
  };
 
  module GiUluru = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUluru";
  };
 
  module GiUmbrellaBayonet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUmbrellaBayonet";
  };
 
  module GiUmbrella = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUmbrella";
  };
 
  module GiUnbalanced = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnbalanced";
  };
 
  module GiUncertainty = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUncertainty";
  };
 
  module GiUnderhand = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnderhand";
  };
 
  module GiUnderwearShorts = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnderwearShorts";
  };
 
  module GiUnderwear = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnderwear";
  };
 
  module GiUnfriendlyFire = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnfriendlyFire";
  };
 
  module GiUnicorn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnicorn";
  };
 
  module GiUnicycle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnicycle";
  };
 
  module GiUnionJack = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnionJack";
  };
 
  module GiUnlitBomb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnlitBomb";
  };
 
  module GiUnlitCandelabra = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnlitCandelabra";
  };
 
  module GiUnlocking = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnlocking";
  };
 
  module GiUnplugged = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnplugged";
  };
 
  module GiUnstableOrb = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnstableOrb";
  };
 
  module GiUnstableProjectile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUnstableProjectile";
  };
 
  module GiUpCard = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUpCard";
  };
 
  module GiUpgrade = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUpgrade";
  };
 
  module GiUprising = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUprising";
  };
 
  module GiUrsaMajor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUrsaMajor";
  };
 
  module GiUruguay = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUruguay";
  };
 
  module GiUsaFlag = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUsaFlag";
  };
 
  module GiUsable = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUsable";
  };
 
  module GiUsbKey = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUsbKey";
  };
 
  module GiUshanka = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUshanka";
  };
 
  module GiUzi = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiUzi";
  };
 
  module GiVacuumCleaner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVacuumCleaner";
  };
 
  module GiValley = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiValley";
  };
 
  module GiValve = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiValve";
  };
 
  module GiVampireCape = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVampireCape";
  };
 
  module GiVampireDracula = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVampireDracula";
  };
 
  module GiVanDammeSplit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVanDammeSplit";
  };
 
  module GiVanillaFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVanillaFlower";
  };
 
  module GiVelocipede = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVelocipede";
  };
 
  module GiVelociraptorTracks = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVelociraptorTracks";
  };
 
  module GiVelociraptor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVelociraptor";
  };
 
  module GiVendingMachine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVendingMachine";
  };
 
  module GiVenezuela = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVenezuela";
  };
 
  module GiVenusOfWillendorf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVenusOfWillendorf";
  };
 
  module GiVerticalBanner = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVerticalBanner";
  };
 
  module GiVerticalFlip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVerticalFlip";
  };
 
  module GiVial = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVial";
  };
 
  module GiVibratingBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVibratingBall";
  };
 
  module GiVibratingShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVibratingShield";
  };
 
  module GiVibratingSmartphone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVibratingSmartphone";
  };
 
  module GiVideoCamera = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVideoCamera";
  };
 
  module GiVideoConference = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVideoConference";
  };
 
  module GiVikingChurch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVikingChurch";
  };
 
  module GiVikingHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVikingHead";
  };
 
  module GiVikingHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVikingHelmet";
  };
 
  module GiVikingLonghouse = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVikingLonghouse";
  };
 
  module GiVikingShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVikingShield";
  };
 
  module GiVileFluid = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVileFluid";
  };
 
  module GiVillage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVillage";
  };
 
  module GiVineFlower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVineFlower";
  };
 
  module GiVineLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVineLeaf";
  };
 
  module GiVineWhip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVineWhip";
  };
 
  module GiVintageRobot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVintageRobot";
  };
 
  module GiViola = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiViola";
  };
 
  module GiViolin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiViolin";
  };
 
  module GiVirgo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVirgo";
  };
 
  module GiVirtualMarker = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVirtualMarker";
  };
 
  module GiVirus = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVirus";
  };
 
  module GiVisoredHelm = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVisoredHelm";
  };
 
  module GiVitruvianMan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVitruvianMan";
  };
 
  module GiVolcano = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVolcano";
  };
 
  module GiVolleyballBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVolleyballBall";
  };
 
  module GiVomiting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVomiting";
  };
 
  module GiVoodooDoll = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVoodooDoll";
  };
 
  module GiVortex = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVortex";
  };
 
  module GiVote = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVote";
  };
 
  module GiVrHeadset = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVrHeadset";
  };
 
  module GiVulture = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVulture";
  };
 
  module GiVuvuzelas = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiVuvuzelas";
  };
 
  module GiWalk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalk";
  };
 
  module GiWalkieTalkie = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalkieTalkie";
  };
 
  module GiWalkingBoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalkingBoot";
  };
 
  module GiWalkingScout = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalkingScout";
  };
 
  module GiWalkingTurret = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalkingTurret";
  };
 
  module GiWallLight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWallLight";
  };
 
  module GiWallet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWallet";
  };
 
  module GiWalrusHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWalrusHead";
  };
 
  module GiWaltherPpk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaltherPpk";
  };
 
  module GiWantedReward = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWantedReward";
  };
 
  module GiWarAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarAxe";
  };
 
  module GiWarBonnet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarBonnet";
  };
 
  module GiWarPick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarPick";
  };
 
  module GiWarlockEye = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarlockEye";
  };
 
  module GiWarlockHood = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarlockHood";
  };
 
  module GiWarpPipe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWarpPipe";
  };
 
  module GiWaspSting = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaspSting";
  };
 
  module GiWatch = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWatch";
  };
 
  module GiWatchtower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWatchtower";
  };
 
  module GiWaterBolt = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterBolt";
  };
 
  module GiWaterBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterBottle";
  };
 
  module GiWaterDivinerStick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterDivinerStick";
  };
 
  module GiWaterDrop = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterDrop";
  };
 
  module GiWaterFlask = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterFlask";
  };
 
  module GiWaterFountain = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterFountain";
  };
 
  module GiWaterGallon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterGallon";
  };
 
  module GiWaterGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterGun";
  };
 
  module GiWaterPolo = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterPolo";
  };
 
  module GiWaterRecycling = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterRecycling";
  };
 
  module GiWaterSplash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterSplash";
  };
 
  module GiWaterTank = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterTank";
  };
 
  module GiWaterfall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaterfall";
  };
 
  module GiWateringCan = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWateringCan";
  };
 
  module GiWatermelon = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWatermelon";
  };
 
  module GiWaveCrest = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaveCrest";
  };
 
  module GiWaveStrike = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaveStrike";
  };
 
  module GiWaveSurfer = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaveSurfer";
  };
 
  module GiWaves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaves";
  };
 
  module GiWavyChains = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWavyChains";
  };
 
  module GiWavyItinerary = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWavyItinerary";
  };
 
  module GiWaxSeal = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaxSeal";
  };
 
  module GiWaxTablet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWaxTablet";
  };
 
  module GiWebSpit = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWebSpit";
  };
 
  module GiWeightCrush = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWeightCrush";
  };
 
  module GiWeightLiftingDown = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWeightLiftingDown";
  };
 
  module GiWeightLiftingUp = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWeightLiftingUp";
  };
 
  module GiWeight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWeight";
  };
 
  module GiWell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWell";
  };
 
  module GiWerewolf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWerewolf";
  };
 
  module GiWesternHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWesternHat";
  };
 
  module GiWhaleTail = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhaleTail";
  };
 
  module GiWheat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWheat";
  };
 
  module GiWheelbarrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWheelbarrow";
  };
 
  module GiWhip = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhip";
  };
 
  module GiWhiplash = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhiplash";
  };
 
  module GiWhirlpoolShuriken = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhirlpoolShuriken";
  };
 
  module GiWhirlwind = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhirlwind";
  };
 
  module GiWhisk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhisk";
  };
 
  module GiWhistle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhistle";
  };
 
  module GiWhiteBook = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhiteBook";
  };
 
  module GiWhiteTower = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWhiteTower";
  };
 
  module GiWideArrowDunk = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWideArrowDunk";
  };
 
  module GiWifiRouter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWifiRouter";
  };
 
  module GiWildfires = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWildfires";
  };
 
  module GiWilliamTellSkull = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWilliamTellSkull";
  };
 
  module GiWilliamTell = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWilliamTell";
  };
 
  module GiWinchesterRifle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWinchesterRifle";
  };
 
  module GiWindHole = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindHole";
  };
 
  module GiWindSlap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindSlap";
  };
 
  module GiWindTurbine = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindTurbine";
  };
 
  module GiWindmill = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindmill";
  };
 
  module GiWindowBars = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindowBars";
  };
 
  module GiWindow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindow";
  };
 
  module GiWindpump = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindpump";
  };
 
  module GiWindsock = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindsock";
  };
 
  module GiWindyStripes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWindyStripes";
  };
 
  module GiWineBottle = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWineBottle";
  };
 
  module GiWineGlass = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWineGlass";
  };
 
  module GiWingCloak = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingCloak";
  };
 
  module GiWingedArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedArrow";
  };
 
  module GiWingedEmblem = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedEmblem";
  };
 
  module GiWingedLeg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedLeg";
  };
 
  module GiWingedScepter = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedScepter";
  };
 
  module GiWingedShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedShield";
  };
 
  module GiWingedSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingedSword";
  };
 
  module GiWingfoot = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWingfoot";
  };
 
  module GiWinterGloves = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWinterGloves";
  };
 
  module GiWinterHat = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWinterHat";
  };
 
  module GiWireCoil = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWireCoil";
  };
 
  module GiWireframeGlobe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWireframeGlobe";
  };
 
  module GiWisdom = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWisdom";
  };
 
  module GiWitchFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWitchFace";
  };
 
  module GiWitchFlight = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWitchFlight";
  };
 
  module GiWizardFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWizardFace";
  };
 
  module GiWizardStaff = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWizardStaff";
  };
 
  module GiWolfHead = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWolfHead";
  };
 
  module GiWolfHowl = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWolfHowl";
  };
 
  module GiWolfTrap = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWolfTrap";
  };
 
  module GiWolverineClaws = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWolverineClaws";
  };
 
  module GiWomanElfFace = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWomanElfFace";
  };
 
  module GiWoodAxe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodAxe";
  };
 
  module GiWoodBeam = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodBeam";
  };
 
  module GiWoodCabin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodCabin";
  };
 
  module GiWoodCanoe = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodCanoe";
  };
 
  module GiWoodClub = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodClub";
  };
 
  module GiWoodPile = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodPile";
  };
 
  module GiWoodStick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodStick";
  };
 
  module GiWoodenChair = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenChair";
  };
 
  module GiWoodenClogs = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenClogs";
  };
 
  module GiWoodenCrate = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenCrate";
  };
 
  module GiWoodenDoor = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenDoor";
  };
 
  module GiWoodenFence = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenFence";
  };
 
  module GiWoodenHelmet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenHelmet";
  };
 
  module GiWoodenPegleg = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenPegleg";
  };
 
  module GiWoodenPier = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenPier";
  };
 
  module GiWoodenSign = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWoodenSign";
  };
 
  module GiWool = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWool";
  };
 
  module GiWorld = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWorld";
  };
 
  module GiWormMouth = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWormMouth";
  };
 
  module GiWorms = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWorms";
  };
 
  module GiWorriedEyes = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWorriedEyes";
  };
 
  module GiWrappedHeart = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWrappedHeart";
  };
 
  module GiWrappedSweet = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWrappedSweet";
  };
 
  module GiWrappingStar = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWrappingStar";
  };
 
  module GiWreckingBall = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWreckingBall";
  };
 
  module GiWrench = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWrench";
  };
 
  module GiWyvern = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiWyvern";
  };
 
  module GiXylophone = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiXylophone";
  };
 
  module GiYarn = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiYarn";
  };
 
  module GiYinYang = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiYinYang";
  };
 
  module GiZBrick = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZBrick";
  };
 
  module GiZatGun = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZatGun";
  };
 
  module GiZebraShield = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZebraShield";
  };
 
  module GiZeppelin = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZeppelin";
  };
 
  module GiZeusSword = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZeusSword";
  };
 
  module GiZigArrow = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZigArrow";
  };
 
  module GiZigzagCage = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZigzagCage";
  };
 
  module GiZigzagHieroglyph = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZigzagHieroglyph";
  };
 
  module GiZigzagLeaf = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZigzagLeaf";
  };
 
  module GiZigzagTune = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZigzagTune";
  };
 
  module GiZipper = {
    @module("react-icons/gi") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "GiZipper";
  };
