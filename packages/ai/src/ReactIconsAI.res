
  module AiFillAccountBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAccountBook";
  };
 
  module AiFillAlert = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAlert";
  };
 
  module AiFillAlipayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAlipayCircle";
  };
 
  module AiFillAlipaySquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAlipaySquare";
  };
 
  module AiFillAliwangwang = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAliwangwang";
  };
 
  module AiFillAmazonCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAmazonCircle";
  };
 
  module AiFillAmazonSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAmazonSquare";
  };
 
  module AiFillAndroid = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAndroid";
  };
 
  module AiFillApi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillApi";
  };
 
  module AiFillApple = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillApple";
  };
 
  module AiFillAppstore = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAppstore";
  };
 
  module AiFillAudio = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillAudio";
  };
 
  module AiFillBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBackward";
  };
 
  module AiFillBank = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBank";
  };
 
  module AiFillBehanceCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBehanceCircle";
  };
 
  module AiFillBehanceSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBehanceSquare";
  };
 
  module AiFillBell = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBell";
  };
 
  module AiFillBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBook";
  };
 
  module AiFillBoxPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBoxPlot";
  };
 
  module AiFillBug = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBug";
  };
 
  module AiFillBuild = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBuild";
  };
 
  module AiFillBulb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillBulb";
  };
 
  module AiFillCalculator = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCalculator";
  };
 
  module AiFillCalendar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCalendar";
  };
 
  module AiFillCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCamera";
  };
 
  module AiFillCar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCar";
  };
 
  module AiFillCaretDown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCaretDown";
  };
 
  module AiFillCaretLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCaretLeft";
  };
 
  module AiFillCaretRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCaretRight";
  };
 
  module AiFillCaretUp = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCaretUp";
  };
 
  module AiFillCarryOut = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCarryOut";
  };
 
  module AiFillCheckCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCheckCircle";
  };
 
  module AiFillCheckSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCheckSquare";
  };
 
  module AiFillChrome = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillChrome";
  };
 
  module AiFillCiCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCiCircle";
  };
 
  module AiFillClockCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillClockCircle";
  };
 
  module AiFillCloseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCloseCircle";
  };
 
  module AiFillCloseSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCloseSquare";
  };
 
  module AiFillCloud = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCloud";
  };
 
  module AiFillCodeSandboxCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCodeSandboxCircle";
  };
 
  module AiFillCodeSandboxSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCodeSandboxSquare";
  };
 
  module AiFillCode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCode";
  };
 
  module AiFillCodepenCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCodepenCircle";
  };
 
  module AiFillCodepenSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCodepenSquare";
  };
 
  module AiFillCompass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCompass";
  };
 
  module AiFillContacts = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillContacts";
  };
 
  module AiFillContainer = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillContainer";
  };
 
  module AiFillControl = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillControl";
  };
 
  module AiFillCopy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCopy";
  };
 
  module AiFillCopyrightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCopyrightCircle";
  };
 
  module AiFillCreditCard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCreditCard";
  };
 
  module AiFillCrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCrown";
  };
 
  module AiFillCustomerService = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillCustomerService";
  };
 
  module AiFillDashboard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDashboard";
  };
 
  module AiFillDatabase = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDatabase";
  };
 
  module AiFillDelete = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDelete";
  };
 
  module AiFillDiff = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDiff";
  };
 
  module AiFillDingtalkCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDingtalkCircle";
  };
 
  module AiFillDingtalkSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDingtalkSquare";
  };
 
  module AiFillDislike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDislike";
  };
 
  module AiFillDollarCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDollarCircle";
  };
 
  module AiFillDownCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDownCircle";
  };
 
  module AiFillDownSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDownSquare";
  };
 
  module AiFillDribbbleCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDribbbleCircle";
  };
 
  module AiFillDribbbleSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDribbbleSquare";
  };
 
  module AiFillDropboxCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDropboxCircle";
  };
 
  module AiFillDropboxSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillDropboxSquare";
  };
 
  module AiFillEdit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillEdit";
  };
 
  module AiFillEnvironment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillEnvironment";
  };
 
  module AiFillEuroCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillEuroCircle";
  };
 
  module AiFillExclamationCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillExclamationCircle";
  };
 
  module AiFillExperiment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillExperiment";
  };
 
  module AiFillEyeInvisible = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillEyeInvisible";
  };
 
  module AiFillEye = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillEye";
  };
 
  module AiFillFacebook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFacebook";
  };
 
  module AiFillFastBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFastBackward";
  };
 
  module AiFillFastForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFastForward";
  };
 
  module AiFillFileAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileAdd";
  };
 
  module AiFillFileExcel = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileExcel";
  };
 
  module AiFillFileExclamation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileExclamation";
  };
 
  module AiFillFileImage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileImage";
  };
 
  module AiFillFileMarkdown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileMarkdown";
  };
 
  module AiFillFilePdf = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFilePdf";
  };
 
  module AiFillFilePpt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFilePpt";
  };
 
  module AiFillFileText = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileText";
  };
 
  module AiFillFileUnknown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileUnknown";
  };
 
  module AiFillFileWord = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileWord";
  };
 
  module AiFillFileZip = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFileZip";
  };
 
  module AiFillFile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFile";
  };
 
  module AiFillFilter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFilter";
  };
 
  module AiFillFire = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFire";
  };
 
  module AiFillFlag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFlag";
  };
 
  module AiFillFolderAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFolderAdd";
  };
 
  module AiFillFolderOpen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFolderOpen";
  };
 
  module AiFillFolder = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFolder";
  };
 
  module AiFillFormatPainter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFormatPainter";
  };
 
  module AiFillForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillForward";
  };
 
  module AiFillFrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFrown";
  };
 
  module AiFillFund = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFund";
  };
 
  module AiFillFunnelPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillFunnelPlot";
  };
 
  module AiFillGift = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGift";
  };
 
  module AiFillGithub = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGithub";
  };
 
  module AiFillGitlab = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGitlab";
  };
 
  module AiFillGold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGold";
  };
 
  module AiFillGolden = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGolden";
  };
 
  module AiFillGoogleCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGoogleCircle";
  };
 
  module AiFillGooglePlusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGooglePlusCircle";
  };
 
  module AiFillGooglePlusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGooglePlusSquare";
  };
 
  module AiFillGoogleSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillGoogleSquare";
  };
 
  module AiFillHdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHdd";
  };
 
  module AiFillHeart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHeart";
  };
 
  module AiFillHighlight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHighlight";
  };
 
  module AiFillHome = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHome";
  };
 
  module AiFillHourglass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHourglass";
  };
 
  module AiFillHtml5 = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillHtml5";
  };
 
  module AiFillIdcard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillIdcard";
  };
 
  module AiFillIeCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillIeCircle";
  };
 
  module AiFillIeSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillIeSquare";
  };
 
  module AiFillInfoCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillInfoCircle";
  };
 
  module AiFillInstagram = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillInstagram";
  };
 
  module AiFillInsurance = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillInsurance";
  };
 
  module AiFillInteraction = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillInteraction";
  };
 
  module AiFillLayout = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLayout";
  };
 
  module AiFillLeftCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLeftCircle";
  };
 
  module AiFillLeftSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLeftSquare";
  };
 
  module AiFillLike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLike";
  };
 
  module AiFillLinkedin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLinkedin";
  };
 
  module AiFillLock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillLock";
  };
 
  module AiFillMacCommand = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMacCommand";
  };
 
  module AiFillMail = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMail";
  };
 
  module AiFillMedicineBox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMedicineBox";
  };
 
  module AiFillMediumCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMediumCircle";
  };
 
  module AiFillMediumSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMediumSquare";
  };
 
  module AiFillMeh = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMeh";
  };
 
  module AiFillMessage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMessage";
  };
 
  module AiFillMinusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMinusCircle";
  };
 
  module AiFillMinusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMinusSquare";
  };
 
  module AiFillMobile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMobile";
  };
 
  module AiFillMoneyCollect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillMoneyCollect";
  };
 
  module AiFillNotification = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillNotification";
  };
 
  module AiFillPauseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPauseCircle";
  };
 
  module AiFillPayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPayCircle";
  };
 
  module AiFillPhone = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPhone";
  };
 
  module AiFillPicture = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPicture";
  };
 
  module AiFillPieChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPieChart";
  };
 
  module AiFillPlayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPlayCircle";
  };
 
  module AiFillPlaySquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPlaySquare";
  };
 
  module AiFillPlusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPlusCircle";
  };
 
  module AiFillPlusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPlusSquare";
  };
 
  module AiFillPoundCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPoundCircle";
  };
 
  module AiFillPrinter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPrinter";
  };
 
  module AiFillProfile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillProfile";
  };
 
  module AiFillProject = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillProject";
  };
 
  module AiFillPropertySafety = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPropertySafety";
  };
 
  module AiFillPushpin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillPushpin";
  };
 
  module AiFillQqCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillQqCircle";
  };
 
  module AiFillQqSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillQqSquare";
  };
 
  module AiFillQuestionCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillQuestionCircle";
  };
 
  module AiFillRead = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRead";
  };
 
  module AiFillReconciliation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillReconciliation";
  };
 
  module AiFillRedEnvelope = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRedEnvelope";
  };
 
  module AiFillRedditCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRedditCircle";
  };
 
  module AiFillRedditSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRedditSquare";
  };
 
  module AiFillRest = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRest";
  };
 
  module AiFillRightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRightCircle";
  };
 
  module AiFillRightSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRightSquare";
  };
 
  module AiFillRobot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRobot";
  };
 
  module AiFillRocket = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillRocket";
  };
 
  module AiFillSafetyCertificate = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSafetyCertificate";
  };
 
  module AiFillSave = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSave";
  };
 
  module AiFillSchedule = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSchedule";
  };
 
  module AiFillSecurityScan = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSecurityScan";
  };
 
  module AiFillSetting = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSetting";
  };
 
  module AiFillShop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillShop";
  };
 
  module AiFillShopping = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillShopping";
  };
 
  module AiFillSignal = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSignal";
  };
 
  module AiFillSketchCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSketchCircle";
  };
 
  module AiFillSketchSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSketchSquare";
  };
 
  module AiFillSkin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSkin";
  };
 
  module AiFillSkype = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSkype";
  };
 
  module AiFillSlackCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSlackCircle";
  };
 
  module AiFillSlackSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSlackSquare";
  };
 
  module AiFillSliders = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSliders";
  };
 
  module AiFillSmile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSmile";
  };
 
  module AiFillSnippets = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSnippets";
  };
 
  module AiFillSound = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSound";
  };
 
  module AiFillStar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillStar";
  };
 
  module AiFillStepBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillStepBackward";
  };
 
  module AiFillStepForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillStepForward";
  };
 
  module AiFillStop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillStop";
  };
 
  module AiFillSwitcher = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillSwitcher";
  };
 
  module AiFillTablet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTablet";
  };
 
  module AiFillTag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTag";
  };
 
  module AiFillTags = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTags";
  };
 
  module AiFillTaobaoCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTaobaoCircle";
  };
 
  module AiFillTaobaoSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTaobaoSquare";
  };
 
  module AiFillThunderbolt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillThunderbolt";
  };
 
  module AiFillTool = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTool";
  };
 
  module AiFillTrademarkCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTrademarkCircle";
  };
 
  module AiFillTrophy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTrophy";
  };
 
  module AiFillTwitterCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTwitterCircle";
  };
 
  module AiFillTwitterSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillTwitterSquare";
  };
 
  module AiFillUnlock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillUnlock";
  };
 
  module AiFillUpCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillUpCircle";
  };
 
  module AiFillUpSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillUpSquare";
  };
 
  module AiFillUsb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillUsb";
  };
 
  module AiFillVideoCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillVideoCamera";
  };
 
  module AiFillWallet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWallet";
  };
 
  module AiFillWarning = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWarning";
  };
 
  module AiFillWechat = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWechat";
  };
 
  module AiFillWeiboCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWeiboCircle";
  };
 
  module AiFillWeiboSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWeiboSquare";
  };
 
  module AiFillWindows = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillWindows";
  };
 
  module AiFillYahoo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillYahoo";
  };
 
  module AiFillYoutube = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillYoutube";
  };
 
  module AiFillYuque = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillYuque";
  };
 
  module AiFillZhihuCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillZhihuCircle";
  };
 
  module AiFillZhihuSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiFillZhihuSquare";
  };
 
  module AiOutlineAccountBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAccountBook";
  };
 
  module AiOutlineAim = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAim";
  };
 
  module AiOutlineAlert = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlert";
  };
 
  module AiOutlineAlibaba = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlibaba";
  };
 
  module AiOutlineAlignCenter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlignCenter";
  };
 
  module AiOutlineAlignLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlignLeft";
  };
 
  module AiOutlineAlignRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlignRight";
  };
 
  module AiOutlineAlipayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlipayCircle";
  };
 
  module AiOutlineAlipay = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAlipay";
  };
 
  module AiOutlineAliwangwang = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAliwangwang";
  };
 
  module AiOutlineAliyun = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAliyun";
  };
 
  module AiOutlineAmazon = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAmazon";
  };
 
  module AiOutlineAndroid = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAndroid";
  };
 
  module AiOutlineAntCloud = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAntCloud";
  };
 
  module AiOutlineAntDesign = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAntDesign";
  };
 
  module AiOutlineApartment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineApartment";
  };
 
  module AiOutlineApi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineApi";
  };
 
  module AiOutlineApple = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineApple";
  };
 
  module AiOutlineAppstoreAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAppstoreAdd";
  };
 
  module AiOutlineAppstore = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAppstore";
  };
 
  module AiOutlineAreaChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAreaChart";
  };
 
  module AiOutlineArrowDown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineArrowDown";
  };
 
  module AiOutlineArrowLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineArrowLeft";
  };
 
  module AiOutlineArrowRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineArrowRight";
  };
 
  module AiOutlineArrowUp = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineArrowUp";
  };
 
  module AiOutlineArrowsAlt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineArrowsAlt";
  };
 
  module AiOutlineAudioMuted = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAudioMuted";
  };
 
  module AiOutlineAudio = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAudio";
  };
 
  module AiOutlineAudit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineAudit";
  };
 
  module AiOutlineBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBackward";
  };
 
  module AiOutlineBank = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBank";
  };
 
  module AiOutlineBarChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBarChart";
  };
 
  module AiOutlineBarcode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBarcode";
  };
 
  module AiOutlineBars = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBars";
  };
 
  module AiOutlineBehanceSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBehanceSquare";
  };
 
  module AiOutlineBehance = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBehance";
  };
 
  module AiOutlineBell = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBell";
  };
 
  module AiOutlineBgColors = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBgColors";
  };
 
  module AiOutlineBlock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBlock";
  };
 
  module AiOutlineBold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBold";
  };
 
  module AiOutlineBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBook";
  };
 
  module AiOutlineBorderBottom = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderBottom";
  };
 
  module AiOutlineBorderHorizontal = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderHorizontal";
  };
 
  module AiOutlineBorderInner = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderInner";
  };
 
  module AiOutlineBorderLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderLeft";
  };
 
  module AiOutlineBorderOuter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderOuter";
  };
 
  module AiOutlineBorderRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderRight";
  };
 
  module AiOutlineBorderTop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderTop";
  };
 
  module AiOutlineBorderVerticle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderVerticle";
  };
 
  module AiOutlineBorder = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorder";
  };
 
  module AiOutlineBorderlessTable = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBorderlessTable";
  };
 
  module AiOutlineBoxPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBoxPlot";
  };
 
  module AiOutlineBranches = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBranches";
  };
 
  module AiOutlineBug = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBug";
  };
 
  module AiOutlineBuild = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBuild";
  };
 
  module AiOutlineBulb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineBulb";
  };
 
  module AiOutlineCalculator = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCalculator";
  };
 
  module AiOutlineCalendar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCalendar";
  };
 
  module AiOutlineCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCamera";
  };
 
  module AiOutlineCar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCar";
  };
 
  module AiOutlineCaretDown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCaretDown";
  };
 
  module AiOutlineCaretLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCaretLeft";
  };
 
  module AiOutlineCaretRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCaretRight";
  };
 
  module AiOutlineCaretUp = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCaretUp";
  };
 
  module AiOutlineCarryOut = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCarryOut";
  };
 
  module AiOutlineCheckCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCheckCircle";
  };
 
  module AiOutlineCheckSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCheckSquare";
  };
 
  module AiOutlineCheck = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCheck";
  };
 
  module AiOutlineChrome = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineChrome";
  };
 
  module AiOutlineCiCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCiCircle";
  };
 
  module AiOutlineCi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCi";
  };
 
  module AiOutlineClear = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineClear";
  };
 
  module AiOutlineClockCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineClockCircle";
  };
 
  module AiOutlineCloseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloseCircle";
  };
 
  module AiOutlineCloseSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloseSquare";
  };
 
  module AiOutlineClose = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineClose";
  };
 
  module AiOutlineCloudDownload = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloudDownload";
  };
 
  module AiOutlineCloudServer = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloudServer";
  };
 
  module AiOutlineCloudSync = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloudSync";
  };
 
  module AiOutlineCloudUpload = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloudUpload";
  };
 
  module AiOutlineCloud = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCloud";
  };
 
  module AiOutlineCluster = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCluster";
  };
 
  module AiOutlineCodeSandbox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCodeSandbox";
  };
 
  module AiOutlineCode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCode";
  };
 
  module AiOutlineCodepenCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCodepenCircle";
  };
 
  module AiOutlineCodepen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCodepen";
  };
 
  module AiOutlineCoffee = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCoffee";
  };
 
  module AiOutlineColumnHeight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineColumnHeight";
  };
 
  module AiOutlineColumnWidth = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineColumnWidth";
  };
 
  module AiOutlineComment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineComment";
  };
 
  module AiOutlineCompass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCompass";
  };
 
  module AiOutlineCompress = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCompress";
  };
 
  module AiOutlineConsoleSql = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineConsoleSql";
  };
 
  module AiOutlineContacts = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineContacts";
  };
 
  module AiOutlineContainer = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineContainer";
  };
 
  module AiOutlineControl = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineControl";
  };
 
  module AiOutlineCopy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCopy";
  };
 
  module AiOutlineCopyrightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCopyrightCircle";
  };
 
  module AiOutlineCopyright = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCopyright";
  };
 
  module AiOutlineCreditCard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCreditCard";
  };
 
  module AiOutlineCrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCrown";
  };
 
  module AiOutlineCustomerService = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineCustomerService";
  };
 
  module AiOutlineDash = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDash";
  };
 
  module AiOutlineDashboard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDashboard";
  };
 
  module AiOutlineDatabase = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDatabase";
  };
 
  module AiOutlineDeleteColumn = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDeleteColumn";
  };
 
  module AiOutlineDeleteRow = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDeleteRow";
  };
 
  module AiOutlineDelete = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDelete";
  };
 
  module AiOutlineDeliveredProcedure = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDeliveredProcedure";
  };
 
  module AiOutlineDeploymentUnit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDeploymentUnit";
  };
 
  module AiOutlineDesktop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDesktop";
  };
 
  module AiOutlineDiff = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDiff";
  };
 
  module AiOutlineDingding = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDingding";
  };
 
  module AiOutlineDingtalk = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDingtalk";
  };
 
  module AiOutlineDisconnect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDisconnect";
  };
 
  module AiOutlineDislike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDislike";
  };
 
  module AiOutlineDollarCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDollarCircle";
  };
 
  module AiOutlineDollar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDollar";
  };
 
  module AiOutlineDotChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDotChart";
  };
 
  module AiOutlineDoubleLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDoubleLeft";
  };
 
  module AiOutlineDoubleRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDoubleRight";
  };
 
  module AiOutlineDownCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDownCircle";
  };
 
  module AiOutlineDownSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDownSquare";
  };
 
  module AiOutlineDown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDown";
  };
 
  module AiOutlineDownload = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDownload";
  };
 
  module AiOutlineDrag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDrag";
  };
 
  module AiOutlineDribbbleSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDribbbleSquare";
  };
 
  module AiOutlineDribbble = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDribbble";
  };
 
  module AiOutlineDropbox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineDropbox";
  };
 
  module AiOutlineEdit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEdit";
  };
 
  module AiOutlineEllipsis = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEllipsis";
  };
 
  module AiOutlineEnter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEnter";
  };
 
  module AiOutlineEnvironment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEnvironment";
  };
 
  module AiOutlineEuroCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEuroCircle";
  };
 
  module AiOutlineEuro = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEuro";
  };
 
  module AiOutlineException = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineException";
  };
 
  module AiOutlineExclamationCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExclamationCircle";
  };
 
  module AiOutlineExclamation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExclamation";
  };
 
  module AiOutlineExpandAlt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExpandAlt";
  };
 
  module AiOutlineExpand = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExpand";
  };
 
  module AiOutlineExperiment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExperiment";
  };
 
  module AiOutlineExport = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineExport";
  };
 
  module AiOutlineEyeInvisible = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEyeInvisible";
  };
 
  module AiOutlineEye = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineEye";
  };
 
  module AiOutlineFacebook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFacebook";
  };
 
  module AiOutlineFall = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFall";
  };
 
  module AiOutlineFastBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFastBackward";
  };
 
  module AiOutlineFastForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFastForward";
  };
 
  module AiOutlineFieldBinary = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFieldBinary";
  };
 
  module AiOutlineFieldNumber = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFieldNumber";
  };
 
  module AiOutlineFieldString = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFieldString";
  };
 
  module AiOutlineFieldTime = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFieldTime";
  };
 
  module AiOutlineFileAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileAdd";
  };
 
  module AiOutlineFileDone = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileDone";
  };
 
  module AiOutlineFileExcel = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileExcel";
  };
 
  module AiOutlineFileExclamation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileExclamation";
  };
 
  module AiOutlineFileGif = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileGif";
  };
 
  module AiOutlineFileImage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileImage";
  };
 
  module AiOutlineFileJpg = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileJpg";
  };
 
  module AiOutlineFileMarkdown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileMarkdown";
  };
 
  module AiOutlineFilePdf = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFilePdf";
  };
 
  module AiOutlineFilePpt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFilePpt";
  };
 
  module AiOutlineFileProtect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileProtect";
  };
 
  module AiOutlineFileSearch = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileSearch";
  };
 
  module AiOutlineFileSync = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileSync";
  };
 
  module AiOutlineFileText = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileText";
  };
 
  module AiOutlineFileUnknown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileUnknown";
  };
 
  module AiOutlineFileWord = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileWord";
  };
 
  module AiOutlineFileZip = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFileZip";
  };
 
  module AiOutlineFile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFile";
  };
 
  module AiOutlineFilter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFilter";
  };
 
  module AiOutlineFire = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFire";
  };
 
  module AiOutlineFlag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFlag";
  };
 
  module AiOutlineFolderAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFolderAdd";
  };
 
  module AiOutlineFolderOpen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFolderOpen";
  };
 
  module AiOutlineFolderView = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFolderView";
  };
 
  module AiOutlineFolder = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFolder";
  };
 
  module AiOutlineFontColors = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFontColors";
  };
 
  module AiOutlineFontSize = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFontSize";
  };
 
  module AiOutlineFork = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFork";
  };
 
  module AiOutlineForm = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineForm";
  };
 
  module AiOutlineFormatPainter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFormatPainter";
  };
 
  module AiOutlineForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineForward";
  };
 
  module AiOutlineFrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFrown";
  };
 
  module AiOutlineFullscreenExit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFullscreenExit";
  };
 
  module AiOutlineFullscreen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFullscreen";
  };
 
  module AiOutlineFunction = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFunction";
  };
 
  module AiOutlineFundProjectionScreen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFundProjectionScreen";
  };
 
  module AiOutlineFundView = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFundView";
  };
 
  module AiOutlineFund = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFund";
  };
 
  module AiOutlineFunnelPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineFunnelPlot";
  };
 
  module AiOutlineGateway = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGateway";
  };
 
  module AiOutlineGif = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGif";
  };
 
  module AiOutlineGift = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGift";
  };
 
  module AiOutlineGithub = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGithub";
  };
 
  module AiOutlineGitlab = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGitlab";
  };
 
  module AiOutlineGlobal = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGlobal";
  };
 
  module AiOutlineGold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGold";
  };
 
  module AiOutlineGooglePlus = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGooglePlus";
  };
 
  module AiOutlineGoogle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGoogle";
  };
 
  module AiOutlineGroup = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineGroup";
  };
 
  module AiOutlineHdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHdd";
  };
 
  module AiOutlineHeart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHeart";
  };
 
  module AiOutlineHeatMap = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHeatMap";
  };
 
  module AiOutlineHighlight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHighlight";
  };
 
  module AiOutlineHistory = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHistory";
  };
 
  module AiOutlineHome = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHome";
  };
 
  module AiOutlineHourglass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHourglass";
  };
 
  module AiOutlineHtml5 = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineHtml5";
  };
 
  module AiOutlineIdcard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineIdcard";
  };
 
  module AiOutlineIe = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineIe";
  };
 
  module AiOutlineImport = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineImport";
  };
 
  module AiOutlineInbox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInbox";
  };
 
  module AiOutlineInfoCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInfoCircle";
  };
 
  module AiOutlineInfo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInfo";
  };
 
  module AiOutlineInsertRowAbove = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInsertRowAbove";
  };
 
  module AiOutlineInsertRowBelow = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInsertRowBelow";
  };
 
  module AiOutlineInsertRowLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInsertRowLeft";
  };
 
  module AiOutlineInsertRowRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInsertRowRight";
  };
 
  module AiOutlineInstagram = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInstagram";
  };
 
  module AiOutlineInsurance = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInsurance";
  };
 
  module AiOutlineInteraction = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineInteraction";
  };
 
  module AiOutlineIssuesClose = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineIssuesClose";
  };
 
  module AiOutlineItalic = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineItalic";
  };
 
  module AiOutlineKey = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineKey";
  };
 
  module AiOutlineLaptop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLaptop";
  };
 
  module AiOutlineLayout = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLayout";
  };
 
  module AiOutlineLeftCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLeftCircle";
  };
 
  module AiOutlineLeftSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLeftSquare";
  };
 
  module AiOutlineLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLeft";
  };
 
  module AiOutlineLike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLike";
  };
 
  module AiOutlineLineChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLineChart";
  };
 
  module AiOutlineLineHeight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLineHeight";
  };
 
  module AiOutlineLine = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLine";
  };
 
  module AiOutlineLink = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLink";
  };
 
  module AiOutlineLinkedin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLinkedin";
  };
 
  module AiOutlineLoading3Quarters = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLoading3Quarters";
  };
 
  module AiOutlineLoading = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLoading";
  };
 
  module AiOutlineLock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLock";
  };
 
  module AiOutlineLogin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLogin";
  };
 
  module AiOutlineLogout = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineLogout";
  };
 
  module AiOutlineMacCommand = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMacCommand";
  };
 
  module AiOutlineMail = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMail";
  };
 
  module AiOutlineMan = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMan";
  };
 
  module AiOutlineMedicineBox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMedicineBox";
  };
 
  module AiOutlineMediumWorkmark = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMediumWorkmark";
  };
 
  module AiOutlineMedium = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMedium";
  };
 
  module AiOutlineMeh = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMeh";
  };
 
  module AiOutlineMenuFold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMenuFold";
  };
 
  module AiOutlineMenuUnfold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMenuUnfold";
  };
 
  module AiOutlineMenu = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMenu";
  };
 
  module AiOutlineMergeCells = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMergeCells";
  };
 
  module AiOutlineMessage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMessage";
  };
 
  module AiOutlineMinusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMinusCircle";
  };
 
  module AiOutlineMinusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMinusSquare";
  };
 
  module AiOutlineMinus = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMinus";
  };
 
  module AiOutlineMobile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMobile";
  };
 
  module AiOutlineMoneyCollect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMoneyCollect";
  };
 
  module AiOutlineMonitor = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMonitor";
  };
 
  module AiOutlineMore = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineMore";
  };
 
  module AiOutlineNodeCollapse = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineNodeCollapse";
  };
 
  module AiOutlineNodeExpand = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineNodeExpand";
  };
 
  module AiOutlineNodeIndex = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineNodeIndex";
  };
 
  module AiOutlineNotification = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineNotification";
  };
 
  module AiOutlineNumber = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineNumber";
  };
 
  module AiOutlineOneToOne = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineOneToOne";
  };
 
  module AiOutlineOrderedList = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineOrderedList";
  };
 
  module AiOutlinePaperClip = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePaperClip";
  };
 
  module AiOutlinePartition = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePartition";
  };
 
  module AiOutlinePauseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePauseCircle";
  };
 
  module AiOutlinePause = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePause";
  };
 
  module AiOutlinePayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePayCircle";
  };
 
  module AiOutlinePercentage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePercentage";
  };
 
  module AiOutlinePhone = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePhone";
  };
 
  module AiOutlinePicCenter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePicCenter";
  };
 
  module AiOutlinePicLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePicLeft";
  };
 
  module AiOutlinePicRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePicRight";
  };
 
  module AiOutlinePicture = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePicture";
  };
 
  module AiOutlinePieChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePieChart";
  };
 
  module AiOutlinePlayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePlayCircle";
  };
 
  module AiOutlinePlaySquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePlaySquare";
  };
 
  module AiOutlinePlusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePlusCircle";
  };
 
  module AiOutlinePlusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePlusSquare";
  };
 
  module AiOutlinePlus = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePlus";
  };
 
  module AiOutlinePoundCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePoundCircle";
  };
 
  module AiOutlinePound = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePound";
  };
 
  module AiOutlinePoweroff = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePoweroff";
  };
 
  module AiOutlinePrinter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePrinter";
  };
 
  module AiOutlineProfile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineProfile";
  };
 
  module AiOutlineProject = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineProject";
  };
 
  module AiOutlinePropertySafety = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePropertySafety";
  };
 
  module AiOutlinePullRequest = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePullRequest";
  };
 
  module AiOutlinePushpin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlinePushpin";
  };
 
  module AiOutlineQq = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineQq";
  };
 
  module AiOutlineQrcode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineQrcode";
  };
 
  module AiOutlineQuestionCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineQuestionCircle";
  };
 
  module AiOutlineQuestion = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineQuestion";
  };
 
  module AiOutlineRadarChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadarChart";
  };
 
  module AiOutlineRadiusBottomleft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadiusBottomleft";
  };
 
  module AiOutlineRadiusBottomright = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadiusBottomright";
  };
 
  module AiOutlineRadiusSetting = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadiusSetting";
  };
 
  module AiOutlineRadiusUpleft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadiusUpleft";
  };
 
  module AiOutlineRadiusUpright = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRadiusUpright";
  };
 
  module AiOutlineRead = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRead";
  };
 
  module AiOutlineReconciliation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineReconciliation";
  };
 
  module AiOutlineRedEnvelope = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRedEnvelope";
  };
 
  module AiOutlineReddit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineReddit";
  };
 
  module AiOutlineRedo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRedo";
  };
 
  module AiOutlineReload = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineReload";
  };
 
  module AiOutlineRest = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRest";
  };
 
  module AiOutlineRetweet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRetweet";
  };
 
  module AiOutlineRightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRightCircle";
  };
 
  module AiOutlineRightSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRightSquare";
  };
 
  module AiOutlineRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRight";
  };
 
  module AiOutlineRise = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRise";
  };
 
  module AiOutlineRobot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRobot";
  };
 
  module AiOutlineRocket = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRocket";
  };
 
  module AiOutlineRollback = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRollback";
  };
 
  module AiOutlineRotateLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRotateLeft";
  };
 
  module AiOutlineRotateRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineRotateRight";
  };
 
  module AiOutlineSafetyCertificate = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSafetyCertificate";
  };
 
  module AiOutlineSafety = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSafety";
  };
 
  module AiOutlineSave = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSave";
  };
 
  module AiOutlineScan = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineScan";
  };
 
  module AiOutlineSchedule = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSchedule";
  };
 
  module AiOutlineScissor = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineScissor";
  };
 
  module AiOutlineSearch = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSearch";
  };
 
  module AiOutlineSecurityScan = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSecurityScan";
  };
 
  module AiOutlineSelect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSelect";
  };
 
  module AiOutlineSend = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSend";
  };
 
  module AiOutlineSetting = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSetting";
  };
 
  module AiOutlineShake = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShake";
  };
 
  module AiOutlineShareAlt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShareAlt";
  };
 
  module AiOutlineShop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShop";
  };
 
  module AiOutlineShoppingCart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShoppingCart";
  };
 
  module AiOutlineShopping = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShopping";
  };
 
  module AiOutlineShrink = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineShrink";
  };
 
  module AiOutlineSisternode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSisternode";
  };
 
  module AiOutlineSketch = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSketch";
  };
 
  module AiOutlineSkin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSkin";
  };
 
  module AiOutlineSkype = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSkype";
  };
 
  module AiOutlineSlackSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSlackSquare";
  };
 
  module AiOutlineSlack = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSlack";
  };
 
  module AiOutlineSliders = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSliders";
  };
 
  module AiOutlineSmallDash = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSmallDash";
  };
 
  module AiOutlineSmile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSmile";
  };
 
  module AiOutlineSnippets = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSnippets";
  };
 
  module AiOutlineSolution = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSolution";
  };
 
  module AiOutlineSortAscending = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSortAscending";
  };
 
  module AiOutlineSortDescending = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSortDescending";
  };
 
  module AiOutlineSound = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSound";
  };
 
  module AiOutlineSplitCells = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSplitCells";
  };
 
  module AiOutlineStar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStar";
  };
 
  module AiOutlineStepBackward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStepBackward";
  };
 
  module AiOutlineStepForward = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStepForward";
  };
 
  module AiOutlineStock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStock";
  };
 
  module AiOutlineStop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStop";
  };
 
  module AiOutlineStrikethrough = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineStrikethrough";
  };
 
  module AiOutlineSubnode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSubnode";
  };
 
  module AiOutlineSwapLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSwapLeft";
  };
 
  module AiOutlineSwapRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSwapRight";
  };
 
  module AiOutlineSwap = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSwap";
  };
 
  module AiOutlineSwitcher = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSwitcher";
  };
 
  module AiOutlineSync = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineSync";
  };
 
  module AiOutlineTable = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTable";
  };
 
  module AiOutlineTablet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTablet";
  };
 
  module AiOutlineTag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTag";
  };
 
  module AiOutlineTags = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTags";
  };
 
  module AiOutlineTaobaoCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTaobaoCircle";
  };
 
  module AiOutlineTaobao = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTaobao";
  };
 
  module AiOutlineTeam = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTeam";
  };
 
  module AiOutlineThunderbolt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineThunderbolt";
  };
 
  module AiOutlineToTop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineToTop";
  };
 
  module AiOutlineTool = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTool";
  };
 
  module AiOutlineTrademarkCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTrademarkCircle";
  };
 
  module AiOutlineTrademark = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTrademark";
  };
 
  module AiOutlineTransaction = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTransaction";
  };
 
  module AiOutlineTranslation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTranslation";
  };
 
  module AiOutlineTrophy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTrophy";
  };
 
  module AiOutlineTwitter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineTwitter";
  };
 
  module AiOutlineUnderline = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUnderline";
  };
 
  module AiOutlineUndo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUndo";
  };
 
  module AiOutlineUngroup = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUngroup";
  };
 
  module AiOutlineUnlock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUnlock";
  };
 
  module AiOutlineUnorderedList = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUnorderedList";
  };
 
  module AiOutlineUpCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUpCircle";
  };
 
  module AiOutlineUpSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUpSquare";
  };
 
  module AiOutlineUp = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUp";
  };
 
  module AiOutlineUpload = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUpload";
  };
 
  module AiOutlineUsb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUsb";
  };
 
  module AiOutlineUserAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUserAdd";
  };
 
  module AiOutlineUserDelete = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUserDelete";
  };
 
  module AiOutlineUserSwitch = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUserSwitch";
  };
 
  module AiOutlineUser = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUser";
  };
 
  module AiOutlineUsergroupAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUsergroupAdd";
  };
 
  module AiOutlineUsergroupDelete = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineUsergroupDelete";
  };
 
  module AiOutlineVerified = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerified";
  };
 
  module AiOutlineVerticalAlignBottom = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerticalAlignBottom";
  };
 
  module AiOutlineVerticalAlignMiddle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerticalAlignMiddle";
  };
 
  module AiOutlineVerticalAlignTop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerticalAlignTop";
  };
 
  module AiOutlineVerticalLeft = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerticalLeft";
  };
 
  module AiOutlineVerticalRight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVerticalRight";
  };
 
  module AiOutlineVideoCameraAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVideoCameraAdd";
  };
 
  module AiOutlineVideoCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineVideoCamera";
  };
 
  module AiOutlineWallet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWallet";
  };
 
  module AiOutlineWarning = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWarning";
  };
 
  module AiOutlineWechat = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWechat";
  };
 
  module AiOutlineWeiboCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWeiboCircle";
  };
 
  module AiOutlineWeiboSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWeiboSquare";
  };
 
  module AiOutlineWeibo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWeibo";
  };
 
  module AiOutlineWhatsApp = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWhatsApp";
  };
 
  module AiOutlineWifi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWifi";
  };
 
  module AiOutlineWindows = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWindows";
  };
 
  module AiOutlineWoman = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineWoman";
  };
 
  module AiOutlineYahoo = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineYahoo";
  };
 
  module AiOutlineYoutube = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineYoutube";
  };
 
  module AiOutlineYuque = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineYuque";
  };
 
  module AiOutlineZhihu = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineZhihu";
  };
 
  module AiOutlineZoomIn = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineZoomIn";
  };
 
  module AiOutlineZoomOut = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiOutlineZoomOut";
  };
 
  module AiTwotoneAccountBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneAccountBook";
  };
 
  module AiTwotoneAlert = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneAlert";
  };
 
  module AiTwotoneApi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneApi";
  };
 
  module AiTwotoneAppstore = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneAppstore";
  };
 
  module AiTwotoneAudio = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneAudio";
  };
 
  module AiTwotoneBank = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBank";
  };
 
  module AiTwotoneBell = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBell";
  };
 
  module AiTwotoneBook = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBook";
  };
 
  module AiTwotoneBoxPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBoxPlot";
  };
 
  module AiTwotoneBug = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBug";
  };
 
  module AiTwotoneBuild = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBuild";
  };
 
  module AiTwotoneBulb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneBulb";
  };
 
  module AiTwotoneCalculator = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCalculator";
  };
 
  module AiTwotoneCalendar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCalendar";
  };
 
  module AiTwotoneCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCamera";
  };
 
  module AiTwotoneCar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCar";
  };
 
  module AiTwotoneCarryOut = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCarryOut";
  };
 
  module AiTwotoneCheckCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCheckCircle";
  };
 
  module AiTwotoneCheckSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCheckSquare";
  };
 
  module AiTwotoneCiCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCiCircle";
  };
 
  module AiTwotoneCi = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCi";
  };
 
  module AiTwotoneClockCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneClockCircle";
  };
 
  module AiTwotoneCloseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCloseCircle";
  };
 
  module AiTwotoneCloseSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCloseSquare";
  };
 
  module AiTwotoneCloud = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCloud";
  };
 
  module AiTwotoneCode = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCode";
  };
 
  module AiTwotoneCompass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCompass";
  };
 
  module AiTwotoneContacts = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneContacts";
  };
 
  module AiTwotoneContainer = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneContainer";
  };
 
  module AiTwotoneControl = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneControl";
  };
 
  module AiTwotoneCopy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCopy";
  };
 
  module AiTwotoneCopyrightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCopyrightCircle";
  };
 
  module AiTwotoneCopyright = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCopyright";
  };
 
  module AiTwotoneCreditCard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCreditCard";
  };
 
  module AiTwotoneCrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCrown";
  };
 
  module AiTwotoneCustomerService = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneCustomerService";
  };
 
  module AiTwotoneDashboard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDashboard";
  };
 
  module AiTwotoneDatabase = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDatabase";
  };
 
  module AiTwotoneDelete = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDelete";
  };
 
  module AiTwotoneDiff = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDiff";
  };
 
  module AiTwotoneDislike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDislike";
  };
 
  module AiTwotoneDollarCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDollarCircle";
  };
 
  module AiTwotoneDollar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDollar";
  };
 
  module AiTwotoneDownCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDownCircle";
  };
 
  module AiTwotoneDownSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneDownSquare";
  };
 
  module AiTwotoneEdit = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEdit";
  };
 
  module AiTwotoneEnvironment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEnvironment";
  };
 
  module AiTwotoneEuroCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEuroCircle";
  };
 
  module AiTwotoneEuro = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEuro";
  };
 
  module AiTwotoneExclamationCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneExclamationCircle";
  };
 
  module AiTwotoneExperiment = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneExperiment";
  };
 
  module AiTwotoneEyeInvisible = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEyeInvisible";
  };
 
  module AiTwotoneEye = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneEye";
  };
 
  module AiTwotoneFileAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileAdd";
  };
 
  module AiTwotoneFileExcel = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileExcel";
  };
 
  module AiTwotoneFileExclamation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileExclamation";
  };
 
  module AiTwotoneFileImage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileImage";
  };
 
  module AiTwotoneFileMarkdown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileMarkdown";
  };
 
  module AiTwotoneFilePdf = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFilePdf";
  };
 
  module AiTwotoneFilePpt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFilePpt";
  };
 
  module AiTwotoneFileText = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileText";
  };
 
  module AiTwotoneFileUnknown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileUnknown";
  };
 
  module AiTwotoneFileWord = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileWord";
  };
 
  module AiTwotoneFileZip = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFileZip";
  };
 
  module AiTwotoneFile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFile";
  };
 
  module AiTwotoneFilter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFilter";
  };
 
  module AiTwotoneFire = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFire";
  };
 
  module AiTwotoneFlag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFlag";
  };
 
  module AiTwotoneFolderAdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFolderAdd";
  };
 
  module AiTwotoneFolderOpen = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFolderOpen";
  };
 
  module AiTwotoneFolder = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFolder";
  };
 
  module AiTwotoneFrown = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFrown";
  };
 
  module AiTwotoneFund = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFund";
  };
 
  module AiTwotoneFunnelPlot = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneFunnelPlot";
  };
 
  module AiTwotoneGift = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneGift";
  };
 
  module AiTwotoneGold = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneGold";
  };
 
  module AiTwotoneHdd = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHdd";
  };
 
  module AiTwotoneHeart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHeart";
  };
 
  module AiTwotoneHighlight = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHighlight";
  };
 
  module AiTwotoneHome = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHome";
  };
 
  module AiTwotoneHourglass = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHourglass";
  };
 
  module AiTwotoneHtml5 = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneHtml5";
  };
 
  module AiTwotoneIdcard = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneIdcard";
  };
 
  module AiTwotoneInfoCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneInfoCircle";
  };
 
  module AiTwotoneInsurance = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneInsurance";
  };
 
  module AiTwotoneInteraction = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneInteraction";
  };
 
  module AiTwotoneLayout = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneLayout";
  };
 
  module AiTwotoneLeftCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneLeftCircle";
  };
 
  module AiTwotoneLeftSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneLeftSquare";
  };
 
  module AiTwotoneLike = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneLike";
  };
 
  module AiTwotoneLock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneLock";
  };
 
  module AiTwotoneMail = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMail";
  };
 
  module AiTwotoneMedicineBox = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMedicineBox";
  };
 
  module AiTwotoneMeh = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMeh";
  };
 
  module AiTwotoneMessage = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMessage";
  };
 
  module AiTwotoneMinusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMinusCircle";
  };
 
  module AiTwotoneMinusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMinusSquare";
  };
 
  module AiTwotoneMobile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMobile";
  };
 
  module AiTwotoneMoneyCollect = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneMoneyCollect";
  };
 
  module AiTwotoneNotification = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneNotification";
  };
 
  module AiTwotonePauseCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePauseCircle";
  };
 
  module AiTwotonePhone = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePhone";
  };
 
  module AiTwotonePicture = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePicture";
  };
 
  module AiTwotonePieChart = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePieChart";
  };
 
  module AiTwotonePlayCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePlayCircle";
  };
 
  module AiTwotonePlaySquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePlaySquare";
  };
 
  module AiTwotonePlusCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePlusCircle";
  };
 
  module AiTwotonePlusSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePlusSquare";
  };
 
  module AiTwotonePoundCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePoundCircle";
  };
 
  module AiTwotonePrinter = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePrinter";
  };
 
  module AiTwotoneProfile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneProfile";
  };
 
  module AiTwotoneProject = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneProject";
  };
 
  module AiTwotonePropertySafety = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePropertySafety";
  };
 
  module AiTwotonePushpin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotonePushpin";
  };
 
  module AiTwotoneQuestionCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneQuestionCircle";
  };
 
  module AiTwotoneReconciliation = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneReconciliation";
  };
 
  module AiTwotoneRedEnvelope = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneRedEnvelope";
  };
 
  module AiTwotoneRest = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneRest";
  };
 
  module AiTwotoneRightCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneRightCircle";
  };
 
  module AiTwotoneRightSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneRightSquare";
  };
 
  module AiTwotoneRocket = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneRocket";
  };
 
  module AiTwotoneSafetyCertificate = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSafetyCertificate";
  };
 
  module AiTwotoneSave = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSave";
  };
 
  module AiTwotoneSchedule = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSchedule";
  };
 
  module AiTwotoneSecurityScan = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSecurityScan";
  };
 
  module AiTwotoneSetting = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSetting";
  };
 
  module AiTwotoneShop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneShop";
  };
 
  module AiTwotoneShopping = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneShopping";
  };
 
  module AiTwotoneSkin = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSkin";
  };
 
  module AiTwotoneSliders = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSliders";
  };
 
  module AiTwotoneSmile = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSmile";
  };
 
  module AiTwotoneSnippets = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSnippets";
  };
 
  module AiTwotoneSound = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSound";
  };
 
  module AiTwotoneStar = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneStar";
  };
 
  module AiTwotoneStop = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneStop";
  };
 
  module AiTwotoneSwitcher = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneSwitcher";
  };
 
  module AiTwotoneTablet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTablet";
  };
 
  module AiTwotoneTag = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTag";
  };
 
  module AiTwotoneTags = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTags";
  };
 
  module AiTwotoneThunderbolt = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneThunderbolt";
  };
 
  module AiTwotoneTool = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTool";
  };
 
  module AiTwotoneTrademarkCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTrademarkCircle";
  };
 
  module AiTwotoneTrophy = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneTrophy";
  };
 
  module AiTwotoneUnlock = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneUnlock";
  };
 
  module AiTwotoneUpCircle = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneUpCircle";
  };
 
  module AiTwotoneUpSquare = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneUpSquare";
  };
 
  module AiTwotoneUsb = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneUsb";
  };
 
  module AiTwotoneVideoCamera = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneVideoCamera";
  };
 
  module AiTwotoneWallet = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneWallet";
  };
 
  module AiTwotoneWarning = {
    @module("react-icons/ai") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "AiTwotoneWarning";
  };
