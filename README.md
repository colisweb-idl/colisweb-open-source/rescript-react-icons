# ReScript React icons bindings

Bindings generated with a script. List of supported icons :

- "ai"
- "fc"
- "fi"
- "gi"
- "gr"
- "fa"
- "io"
- "md"
- "ri"
- "ti"

## Setup

There are a package for each font

```bash
yarn add @colisweb/rescript-react-icons-fa
```

Add it to `bsconfig.json`

```
"bs-dependencies":  ["@colisweb/rescript-react-icons-fa"],
```

## Usage

By default everything is exposed as `ReactIcons[NAME]` like `ReactIconsFA` :

```rescript
@react.component
let make = () => <ReactIconsFA.FaAccessibleIcon size=20 />
```

### Props

| Props     |  type  |
| --------- | :----: |
| size      |  int   |
| className | string |
| color     | string |
