const fs = require("fs");
const path = require("path");
const bsconfig = require("./templates/bsconfig.json");
const package = require("./templates/package.json");

const supportedIcons = [
  "ai",
  "fc",
  "fi",
  "gi",
  "gr",
  "fa",
  "io",
  "md",
  "ri",
  "ti",
];
const makeTemplate = ({ moduleName, libName }) => `
  module ${moduleName} = {
    @module("react-icons/${libName}") @react.component
    external make:
      (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
      "${moduleName}";
  };
`;

supportedIcons.forEach((libName) => {
  let content = fs.readFileSync(
    path.join(__dirname, `./node_modules/react-icons/${libName}/index.js`),
    "utf8"
  );

  let data = content.match(/(module\.exports).\w+/g);

  if (data) {
    const formattedData = data
      .map((s) => s.replace("module.exports.", "").replace(".displayName", ""))
      .map((moduleName) => makeTemplate({ moduleName, libName }));

    const folderPath = path.join(__dirname, `./packages/${libName}`);
    const srcFolderPath = path.join(folderPath, "./src");

    fs.mkdir(srcFolderPath, { recursive: true }, (err) => {
      if (err) {
        console.log(`📛 folder creation error ${libName}`);
      } else {
        fs.writeFile(
          path.join(folderPath, `./package.json`),
          JSON.stringify(
            {
              ...package,
              name: `${package.name}-${libName}`,
            },
            null,
            2
          ),
          (err) => {
            if (err) {
              console.log(`📛 An error occured for ${libName}`);
              console.log(err);
            }
          }
        );
        fs.writeFile(
          path.join(folderPath, `./bsconfig.json`),
          JSON.stringify(
            {
              ...bsconfig,
              name: `${bsconfig.name}-${libName}`,
            },
            null,
            2
          ),
          (err) => {
            if (err) {
              console.log(`📛 An error occured for ${libName}`);
              console.log(err);
            }
          }
        );
        fs.writeFile(
          path.join(srcFolderPath, `./ReactIcons${libName.toUpperCase()}.res`),
          formattedData.join(" "),
          (err) => {
            if (err) {
              console.log(`📛 An error occured for ${libName}`);
              console.log(err);
            } else {
              console.log(`\n✅ Done ${libName}`);
            }
          }
        );
      }
    });
  } else {
    console.log(`📛  regex failure for ${libName}`);
  }
});
